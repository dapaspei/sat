<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Globals {

    /**
     * Constructor para cargar las variables globales
     */
    public function __construct($config = array())
    {
        foreach ($config as $key => $value) {
            $data[$key] = $value;
        }

        $CI =& get_instance();
        $CI->load->vars($data);
    }

    /**
	 * Fetch a global file item
	 *
	 * @param	string	$item	Global item name
	 * @param	string	$index	Index name
	 * @return	string|null	The configuration item or NULL if the item doesn't exist
	 */
	// public function item($item, $index = '')
	// {
	// 	if ($index == '')
	// 	{
	// 		return isset($this->config[$item]) ? $this->config[$item] : NULL;
	// 	}

	// 	return isset($this->config[$index], $this->config[$index][$item]) ? $this->config[$index][$item] : NULL;
    // }
    public function item($item)
	{
        return isset($this->config[$item]) ? $this->config[$item] : NULL;
	}

}