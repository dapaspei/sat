<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubrics_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('teachers_model');
    }
    /**
     * Método para crear y almacenar un profesor en la BBDD
     */
    public function create($data) {
        // var_dump("<br>create(data)=".json_encode($data));
        // exit(0);
        
        // Ponemos los profesores a los que se les puede asignar la rúbrica
        $t_available_for = $data['t_available_for'];
        if(empty($t_available_for))
            $data['t_available_for'] = '';
        else    
            $data['t_available_for'] = json_encode($t_available_for);
        
        // Extraemos el último elemento donde están los niveles de docencia.
        //$levels = array_pop($data);
        $levels = $data['teaching_levels'];
        // Convertir los niveles a datos JSON
        $levels_json[] = json_encode($levels);
        $data['teaching_levels'] = $levels_json[0];
                
        if (!$this->db->insert('rubrics', $data)) {
            // Si hay error, devolvemos falso
            return false;
        } else {
            // Si la inserción ha sido exitosa
            // Devolvemos el id de la fila insertada.
            return $this->db->insert_id();

            // $rubric_id = array (
            //     'rubric_id' => $this->db->insert_id(),
            // ); 
            // return $rubric_id;
        }
    }

    /**
     * Función para actualizar los datos de una rúbrica
     */
    public function update($rubric_id, $data) {
        // Ponemos los profesores a los que se les puede asignar la rúbrica
        $t_available_for = $data['t_available_for'];
        if(empty($t_available_for))
            $data['t_available_for'] = '';
        else    
            $data['t_available_for'] = json_encode($t_available_for);

        // Extraemos el último elemento donde están los niveles de docencia.
        // $levels = array_pop($data);
        $levels = $data['teaching_levels'];

        // Convertir los niveles a datos JSON
        $levels_json[] = json_encode($levels);
        $data['teaching_levels'] = $levels_json[0];
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $rubric_id);
        $this->db->update('rubrics', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar los items/criterion de una Rúbrica
     */
    public function updateCriteria($rubric_id, $data) {
        $data_json = json_encode($data);
        // var_dump($data_json);
        // var_dump("<br>rubric_id = ".$rubric_id);
        // exit(0);
        $this->db->trans_start();
        $this->db->where('id', $rubric_id);
        $this->db->update('rubrics', array('data' => $data_json)); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para eliminar una rúbrica de la BBDD
     */
    public function delete($rubric_id) { 
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('rubrics', array('id' => $rubric_id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para obtener el listado de Rúbricas
     */
    public function getRubrics() {
        $sql = $this->db->order_by('name', 'ASC')->get('rubrics');
        return $sql->result();
    }

    /**
     * Función para obtener las rúbricas de un profesor, bien porqué el las haya creado,
     * o bien porqué le hayan asigado y permitido verla.
     */
    public function getMyRubrics($teacher_id) {
        // $sql = $this->db->get_where('rubrics',array('id' => $rubric_id)); 
        $sql = $this->db->query("SELECT * from rubrics WHERE t_owner = '$teacher_id' or t_available_for LIKE '%\"$teacher_id\"%'")->result_array();
        // var_dump("sql = ".json_encode($sql));
        // exit(0);
        return $sql;
    }

    /**
     * Función para obtener los datos de 1 única Rúbrica
     */
    public function getRubric ($rubric_id) {
        // $sql = $this->db->get_where('rubrics', array('id' => $rubric_id));        
        // return $sql->row_array();
        $sql = $this->db->get_where('rubrics',array('id' => $rubric_id)); 
        // Decodificamos los datos guardados en JSON (teaching_levels)
        $data = $sql->row_array();

        // Ponemos los profesores a los que se les puede asignar la rúbrica
        $t_available_for = $data['t_available_for'];
        if(empty($t_available_for))
            $data['t_available_for'] = '';
        else {
            //$data['t_available_for'] = json_decode($t_available_for, true);
            $teachers_decoded = json_decode($t_available_for, true);
            foreach($teachers_decoded as $teacher)
                $teachers_result[] = $teacher;
            $data['t_available_for'] = $teachers_result;
        }
        // var_dump("<br>teachers_result=".json_encode($teachers_result));
        // exit(0);
        // Cogemos los niveles de docencia.
        //$levels_coded = array_pop($data);
        $levels_coded = $data['teaching_levels'];
        // var_dump($levels_coded);
        // var_dump("<br>");
        $levels = json_decode($levels_coded, true);
        //var_dump($levels);
        //exit(0);
        if(!is_array($levels)) {
            //if (count($levels) === 1) {
            // Si sólo tiene un curso
            $data['teaching_levels'] = $levels;
        } else {
            // Si tiene varios cursos asignados.
            foreach($levels as $level)
                $levels_result[] = $level;
            $data['teaching_levels'] = $levels_result;
        }

        // Obtenemos los criterios/items de la rúbrica.
        // $rubric_data = array_pop($data);
        $rubric_data = $data['data'];
        $rubric_data_decoded = json_decode($rubric_data, true); // True para que convierta en array();
        //var_dump("rubric data=".$rubric_data."<br>");
        // var_dump($rubric_data_decoded[0]['criteria']);
        // var_dump($rubric_data_decoded[1]['criteria']);
        // var_dump($rubric_data_decoded[0]['descriptions']);
        // var_dump($rubric_data_decoded[2]['descriptions'][1]);
        // exit(0);
        $data['data'] = $rubric_data_decoded;
         // $data['data']
        
        // var_dump($data);
        // exit(0);
        
        return $data;
    }
    /**
     * Función para obtener los Criterios de la rúbrica
     */
    public function getRubricCriteria($rubric_id = ''){

        //var_dump("rubric_id=".$rubric_id);
        $sql = $this->db->get_where('rubrics',array('id' => $rubric_id))->row_array(); 
        //var_dump($sql."<br>");
        // Decodificamos los datos guardados en JSON (data)
        $data = $sql['data'];
        // var_dump($data);
        
        $rubric_data_decoded = json_decode($data, true); // True para que convierta en array();
        //var_dump($rubric_data_decoded);
        //var_dump("rubric data=".$rubric_data."<br>");
        // var_dump($rubric_data_decoded[0]['criteria']);
        // var_dump($rubric_data_decoded[1]['criteria']);
        // var_dump($rubric_data_decoded[0]['descriptions']);
        // var_dump($rubric_data_decoded[2]['descriptions'][1]);
        // exit(0);
        $data = array();
        // $data['data'] = $rubric_data_decoded;
        foreach($rubric_data_decoded as $item=>$value)
            $data[] = $value['criteria'];
       
        // var_dump($data);
        // exit(0);
        
        return $data;   
    }
    /** 
     * Función para devolver el número de COLUMNAS de una rúbrica
     */
    public function getRubricCols($rubric_id = ''){
        $this->db->select('cols');
        $sql = $this->db->get_where('rubrics',array('id' => $rubric_id))->row_array(); 
        return $sql['cols'];
    }
    /** 
     * Función para devolver el número de FILAS de una rúbrica
     */
    public function getRubricRows($rubric_id = ''){
        $this->db->select('rows');
        $sql = $this->db->get_where('rubrics',array('id' => $rubric_id))->row_array(); 
        return $sql['rows'];
    }
     /** 
     * Función para obtener el valor máximo de calificación de la rúbrica.
     */
    public function getRubricMaxValue($rubric_id = ''){
        $this->db->select('max_value');
        $sql = $this->db->get_where('rubrics',array('id' => $rubric_id))->row_array(); 
        return $sql['max_value'];
    }
    /**
     * Función para obtener el profesor al que pertenece la rúbrica
     */
    public function getTeacherOwner($rubric_id = '') {
        $this->db->select('t_owner');
        $sql = $this->db->get_where('rubrics',array('id' => $rubric_id))->row_array(); 
        return $sql['t_owner'];
    }
    
    /*
     * Fetch members data from the database
     * @param array filter data based on the passed parameters
     */
    function getRows($params = array()){

        // $params = array ( 
        //     'where' => array('role' => 'teacher'),
        // );

        $this->table = 'rubrics';
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("where", $params)){
            foreach($params['where'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'desc');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }

    /**
     * Función para paginar los datos
     */
    public function get_Paginate($limit,$offset) {
        if($this->session->role === 'admin'){
            $sql = $this->db->order_by('name', 'ASC')->get('rubrics', $limit, $offset);
            return $sql->result();
        } else {
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $sql = $this->db->query("SELECT * from rubrics WHERE t_owner = '$teacher_id' or t_available_for LIKE '%\"$teacher_id\"%'")->result();
            return $sql;
        }
    }


}