<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges_outcomes_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        // $this->load->model('teachers_model','teachers');
        $this->load->model('students_model','students');
        $this->load->model('challenges_model');
    }
    /**
     * Método para crear y almacenar una Evaluación en la BBDD
     */
    public function create() {
        $data = array(
            'self_assessment_mark' => 0.0,
            'self_assessment_done' => 0,
            'self_assessment_data' => '',
            'peer_assessment_mark' => 0.0,
            'peer_assessment_done' => 0,
            'peer_assessment_data' => '',
            'teacher_assessment_mark' => 0.0,
            'teacher_assessment_done' => 0,
            'teacher_assessment_data' => '',
        );
        if (!$this->db->insert('challenge_learning_outcomes', $data)) {
            // Si hay error, devolvemos falso
            return false;
        } else {
            // Devolvemos el id de la fila insertada.
            $outcome_id = $this->db->insert_id();
            return $outcome_id;
        }
    }
    /**
     * Función para GUARDAR las notas de un PROFESOR
     */
    public function saveTeacherOutcomes($outcome_id, $teacher_assessment_data) {
        $t_done = $this->getTeacherOutcomesDone($outcome_id);
        // var_dump("tdone=".$t_done);
        // exit(0);
        $t_done = (int)$t_done + 1;
        // var_dump("tdone + 1=".$t_done);
        // exit(0);
        $data = array (
            'teacher_assessment_done' => $t_done,
            'teacher_assessment_data' => json_encode($teacher_assessment_data),
        );
        // var_dump("data = ".json_encode($data));
        // exit(0);
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $outcome_id);
        $this->db->update('challenge_learning_outcomes', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para obtener las evaluaciones hechas a un alumno
     */
    public function getTeacherOutcomesData($outcome_id) {
        $this->db->select('teacher_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes',array('id' => $outcome_id))->row_array();
        return json_decode($sql['teacher_assessment_data'],true);
    }

    /**
     * Función para BORRAR las notas de un PROFESOR
     */
    public function deleteTeacherOutcomes($outcome_id, $teacher_assessment_data) {
        $t_done = $this->getTeacherOutcomesDone($outcome_id);
        $t_done = (int)$t_done - 1;
        $teacher_id = array_keys($teacher_assessment_data);
        // var_dump("teacher_id=".$teacher_id[0]);
        // exit(0);

        $data = array (
            'teacher_assessment_done' => $t_done,
            'teacher_assessment_data' => json_encode(array($teacher_id[0] => '')),
        );
        // var_dump("data = ".json_encode($data));
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $outcome_id);
        $this->db->update('challenge_learning_outcomes', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para GUARDAR las notas de un ESTUDIANTE
     */
    public function saveSelfStudentOutcomes($outcome_id, $self_assessment_data) {
//         $t_done = $this->getSelfStudentOutcomesDone($outcome_id);
        // var_dump("tdone=".$t_done);
        // exit(0);
        $t_done = 1;
        // var_dump("tdone + 1=".$t_done);
        // exit(0);
        $data = array (
            'self_assessment_done' => $t_done,
            'self_assessment_data' => json_encode($self_assessment_data),
        );
        /* var_dump("outcome_id = ".$outcome_id.", data = ".json_encode($data)."<br>");
        exit(0); */
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $outcome_id);
        $this->db->update('challenge_learning_outcomes', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para obtener las evaluaciones hechas a un ESTUDIANTE
     */
    public function getSelfStudentOutcomesData($outcome_id) {
        $this->db->select('self_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes',array('id' => $outcome_id))->row_array();
        return json_decode($sql['self_assessment_data'],true);
    }

    /**
     * Función para BORRAR las notas de un ESTUDIANTE
     */
    public function deleteSelfStudentOutcomes($outcome_id) {
        $t_done = 0;
        $data = array (
            'self_assessment_done' => $t_done,
            'self_assessment_data' => json_encode(array()),
        );
        // var_dump("data = ".json_encode($data));
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $outcome_id);
        $this->db->update('challenge_learning_outcomes', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para GUARDAR las notas de un ESTUDIANTE CO-EVALUADO
     */
    public function savePeerStudentOutcomes($outcome_id, $peer_assessment_data) {
        $t_done = $this->getPeerOutcomesDone($outcome_id);
        // var_dump("tdone=".$t_done);
        // exit(0);
        $t_done = $t_done + 1;
        // var_dump("tdone + 1=".$t_done);
        // exit(0);
        $data = array (
            'peer_assessment_done' => $t_done,
            'peer_assessment_data' => json_encode($peer_assessment_data),
        );
        /* var_dump("outcome_id = ".$outcome_id.", data = ".json_encode($data)."<br>");
        exit(0); */
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $outcome_id);
        $this->db->update('challenge_learning_outcomes', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para obtener las evaluaciones hechas a un ESTUDIANTE
     */
    public function getPeerOutcomesData($outcome_id) {
        $this->db->select('peer_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes',array('id' => $outcome_id))->row_array();
        return json_decode($sql['peer_assessment_data'],true);
    }

    /**
     * Función para BORRAR las notas de un ESTUDIANTE COEVALUADO
     */
    public function deletePeerStudentOutcomes($outcome_id, $peer_assessment_data) {
        $t_done = $this->getPeerOutcomesDone($outcome_id);
        $t_done = (int)$t_done - 1;
        $peer_id = array_keys($peer_assessment_data);
        // var_dump("peer_id=".$peer_id[0]);
        // exit(0);

        $data = array (
            'peer_assessment_done' => $t_done,
            'peer_assessment_data' => json_encode(array($peer_id[0] => '')),
        );
        // var_dump("data = ".json_encode($data));
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $outcome_id);
        $this->db->update('challenge_learning_outcomes', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }


    /**
     * Función para actualizar los datos de una EVALUACIÓN
     */
    public function update($id, $data) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('challenge_learning_outcomes', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    
    /**
     * Función para obtener el listado de Evaluaciones de un alumno
     * @param $user Si hay un idenficador sólo se muestran los Retos
     * asignados a ese Profesor. 
     */
    public function getStudentMarks($user = '') {
    }

    /**
     * Función para obtener el número de profesores que han hecho la evaluación
     */
    public function getTeacherOutcomesDone ($id) {
        $this->db->select('teacher_assessment_done');
        $sql = $this->db->get_where('challenge_learning_outcomes',array('id' => $id))->row_array();
        return $sql['teacher_assessment_done'];

    }

    public function getPeerOutcomesDone ($id) {
        $this->db->select('peer_assessment_done');
        $sql = $this->db->get_where('challenge_learning_outcomes',array('id' => $id))->row_array();
        return $sql['peer_assessment_done'];
    }

    /**
     * Función para obtener los datos de una única Evaluación
     */
    public function getOutcomes ($id) {
    }

    /**
     * Función para obtener los Profesores asociados a una EVALUACIÓN de un alumno
     */
    public function getOutcomesForTeachers ($teacher_id) {

    }
    /**
     * Función para obtener los Estudiantes asociados a una EVALUACIÓN
     */
    public function getOutcomeIdForStudent ($challenge_id, $current_sprint, $student_id) {
        // var_dump("<br>challenge_id=".$challenge_id.", std_id=".$student_id.", sprint=".$current_sprint);
        // exit(0);
        $this->db->select('learning_outcomes_id');
        $sql = $this->db->get_where('challenge_assessment', array(
            'student_id' => $student_id,
            'current_sprint' => $current_sprint,
            'challenge_id' => $challenge_id))->row_array();
        // var_dump("learning_outcomes_id=".json_encode($sql['learning_outcomes_id']));
        // exit(0);
        // Si no ha encontrado resultados guardados o son null
        if (!is_null($sql))
            return $sql['learning_outcomes_id'];
        else return null;
        // $sql = $this->db->get_where('challenges_students_enrolment', array('challenge_id' => $id));
        // $students_id = $sql->result_array();

        // $students = $this->students_model->getStudentsInChallenge($students_id);
        // return $students;
    }
    /**
     * Función para obtener los identificadores de los OutComes de una Evalaución.
     */
    public function getAssessmentOutcomesId ($assessment_id) {
        $this->db->select('learning_outcomes_id');
        $sql = $this->db->get_where('challenge_assessment', array('id' => $assessment_id));
        return $sql->result_array()['learning_outcomes_id'];
    }
    /**
     * Función para obtener los identificadores de los OutComes de una Evalaución.
     */
    public function getChallengeOutcomesId ($challenge_id) {
        $this->db->select('learning_outcomes_id');
        $sql = $this->db->get_where('challenge_assessment', array('challenge_id' => $challenge_id))->result_array();
        $outcomes_ids = array();
        foreach($sql as $item) {
            $outcomes_ids[] = $item['learning_outcomes_id'];
        }
        return $outcomes_ids;
        //return $sql->result_array();
    }

    /**
     * Función para obtener la Rúbrica asociada a un nivel educativo (1-ESO, 2-ESO,...)
     */
    // public function getChallengesInLevel ($level) {
    //     // Utilizamos JSON_CONTAINS para buscar dentro de un tipo JSON
    //     $sql = $this->db->query("SELECT * from challenges WHERE JSON_CONTAINS(teaching_levels, '".$level."')")->result_array();        
    //     // var_dump($sql);
    //     // exit(0);
    //     return $sql;   
    //  }

    /**
     * Función para eliminar una Evaluación de la BBDD
     */
    public function delete($id) { 
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('challenge_learning_outcomes', array('id' => $id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

}