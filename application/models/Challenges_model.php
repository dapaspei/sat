<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('teachers_model','teachers');
        $this->load->model('students_model','students');
    }
    /**
     * Método para crear y almacenar un Reto en la BBDD
     */
    public function create($data) {

        // Como pueden haber varios cursos que entren en el reto, transformamos los nivels en datos JSON.
        $levels = $data['teaching_levels'];
        $data['teaching_levels'] = json_encode($levels);

        // Establecemos que ningún sprint se pueda valorar (todavía)
        $sprints_openfor_assess = array();
        for($i=0; $i<$data['sprints']; $i++)
            $sprints_openfor_assess[] = 0;

        $data['sprints_openfor_assess'] = json_encode($sprints_openfor_assess);

        if (!$this->db->insert('challenges', $data)) {
            // Si hay error, devolvemos falso
            return false;
        } else {
            // Si la inserción ha sido exitosa
            // Devolvemos el id de la fila insertada.
            $challenge_id = array (
                'challenge_id' => $this->db->insert_id(),
            ); 
            return $challenge_id;
        }
    }

    /**
     * Función para actualizar los datos de un RETO
     */
    public function update($id, $data) {
        if(!empty($data['teaching_levels'])) {
            // Convertimos los niveles del reto en STRING
            $levels = $data['teaching_levels'];
            $data['teaching_levels'] = json_encode($levels);
        }
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('challenges', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar la RÚBRICA asociada a un RETO
     */
    public function updateRubric($challenge_id, $rubric_id) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $challenge_id);
        $this->db->update('challenges', array('rubric_id' => $rubric_id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar los SPRINTS asociados a un RETO
     */
    public function updateSprintsDate($challenge_id, $sprints_date) {
        // Convertir las fechas a datos JSON
        //var_dump("sprints_date =".$sprints_date);

        $sprints_date_json[] = json_encode($sprints_date);
        // // var_dump("sprints_date_json =".$sprints_date_json[0]);
        // print_r("challenge_id=".$challenge_id);
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $challenge_id);
        $this->db->update('challenges', array('sprints_date' => $sprints_date_json[0])); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar la estructura de sprints a valorar
     */
    public function updateSprintsOpenForAssess($challenge_id, $number_of_sprints) {
        $sprints_openfor_assess = array();

        for($i=0; $i<$number_of_sprints; $i++)
            $sprints_openfor_assess[] = 0;

        $sprints_openfor_assess_json[] = json_encode($sprints_openfor_assess);
        // var_dump("<br>sprints_openfor_assess_json =".$sprints_openfor_assess_json[0]);
        // var_dump("<br>challenge_id=".$challenge_id);
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $challenge_id);
        $this->db->update('challenges', array('sprints_openfor_assess' => $sprints_openfor_assess_json[0])); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar cuando se valoran los SPRINTS asociados a un RETO
     */
    public function updateSprintsAssessments($challenge_id, $current_sprint) {  
        // var_dump("<br>updateSprintsAssessments(current_sprint=)".$current_sprint);

        // Obtener el número de Sprints
        $num_sprints = $this->getNumberSprints($challenge_id);

        // Si no se ha seleccionado ningún Sprint, lo ponemos todo a 0
        $sprints_openfor_assess = array();
        if(is_null($current_sprint)) {
            for($i=0; $i<$num_sprints; $i++)
                $sprints_openfor_assess[] = 0;
        } else {
            for($i=0; $i<$num_sprints; $i++) {
                if ($current_sprint == $i)
                    $sprints_openfor_assess[] = 1;
                else 
                    $sprints_openfor_assess[] = 0;
            }
        }
        // var_dump("updateSprintsAssessments(sprints_openfor_assess)=".json_encode($sprints_openfor_assess));
        // exit(0);

        $sprints_openfor_assess_json[] = json_encode($sprints_openfor_assess);
        // var_dump("<br>sprints_openfor_assess_json =".$sprints_openfor_assess_json[0]);
        // var_dump("<br>challenge_id=".$challenge_id);
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $challenge_id);
        $this->db->update('challenges', array('sprints_openfor_assess' => $sprints_openfor_assess_json[0])); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para actualizar el equipo al que pertenenece un alumno en un RETO
     */
    public function updateTeams($challenge_id, $student_id, $s_team) {

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where(array('challenge_id' => $challenge_id, 'student_id' => $student_id));
        $this->db->update('challenges_students_enrolment', array('s_team' => $s_team));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;


        // // Iniciar transacción para poder hacer rollback
        // $this->db->trans_start();
        // $this->db->where('id', $challenge_id);
        // $this->db->update('challenges', array('rubric_id' => $rubric_id));
        // $this->db->trans_complete();
        // return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para establecer los RAs asociados a un RETO
     */
    public function saveTechnicalSkills($challenge_id, $technical_skills) {  
        // var_dump("<br>set_technical_skills(technical_skills=)".json_encode($technical_skills));
        // exit(0);

        $technical_skills_json = json_encode($technical_skills);


        // $technical_skills_tostring = implode(",",$technical_skills);

        
        
        // var_dump("<br>technical_skills_json =".json_encode($technical_skills_tostring));
        // var_dump("<br>challenge_id=".$challenge_id);
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $challenge_id);
        // $this->db->update('challenges', array('technical_skills' => $technical_skills_tostring)); 
        // $this->db->update('challenges', array('technical_skills' => $technical_skills)); 
        $this->db->update('challenges', array('technical_skills' => $technical_skills_json)); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para ACTUALIZAR con nuevos RAs asociados a un RETO
     */
    public function updateTechnicalSkills($challenge_id, $technical_skills_id) {  
        // var_dump("<br>update_technical_skillss(technical_skills=)".$technical_skills_id);

        $previous_technical_skills = $this->getTechnicalSkills($challenge_id);
        // var_dump("<br>previous technical skills = ".json_encode($previous_technical_skills));
        // exit(0);

        $technical_skills_tostore = '';
        // Si ya tenía algun RA asociado, añadimos el nuevo.
        if(!empty($previous_technical_skills)) {
            // Borramos las llaves de los extremos
            // $previous_technical_skills = str_replace(array("{","}"), "", $previous_technical_skills);
            // $previous_technical_skills = json_decode($previous_technical_skills, TRUE);
            $previous_technical_skills[$technical_skills_id] = '0';

            // $technical_skills_tostore = strval($previous_technical_skills) . ',' . strval($technical_skills_id);
            // $to_store = array($technical_skills_id => '0');
            // $technical_skills_tostore = json_encode($to_store);
            // $technical_skills_tostore = json_encode($previous_technical_skills);
            $technical_skills_tostore = json_encode($previous_technical_skills);

        } else {
            // $technical_skills_tostore[$technical_skills_id] = '0';
            $technical_skills_tostore = json_encode(array($technical_skills_id => '100'));

        }
        // var_dump("<br>previous technical skills = ".json_encode($previous_technical_skills));

        // var_dump("<br>technical_skills_tostore=".json_encode($technical_skills_tostore));
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $challenge_id);
        $this->db->update('challenges', array('technical_skills' => $technical_skills_tostore)); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para obtener el listado de RETOS
     * @param $user Si hay un idenficador sólo se muestran los Retos
     * asignados a ese Profesor. 
     */
    public function getChallenges($user = '', $current_schoool_year) {

        // var_dump("<br> La fecha para buscar retos es:". $current_schoool_year."<br>");
        // exit(0);

        // Si es un profesor, sólo mostramos los retos en los que está asignado
        $challenges = array();
        if (!empty($user)) {
            $this->db->select('challenge_id');
            $sql = $this->db->get_where('challenges_teachers_enrolment', array('teacher_id' => $user));

            $challenges = $sql->result_array();
            // var_dump("<br>challenges= ".json_encode($challenges));
            // exit(0);
            $challenges_result = array();
            // Si no está en ninún reto, no se devuelven.
            if(count($challenges)==0)
                return array();

            foreach($challenges as $item) {               
                $challenges_result[] = $item['challenge_id'];
            }
            // var_dump("<br>challenges_result= ".json_encode($challenges_result));
            // exit(0);
            $challenges = $this->db->order_by('name', 'ASC')
                            ->from('challenges')
                            ->where_in('id', $challenges_result)
                            ->where('year', $current_schoool_year)
                            // ->where_in(array('id' => $challenges_result, 'year' => $date))
                            ->get()->result();

            // var_dump(json_encode($challenges));
            // exit(0);
        } else {
            // $challenges = $this->db->order_by('name', 'ASC')->get('challenges')->result_array();
            $challenges = $this->db->order_by('name', 'ASC')
                                    ->from('challenges')
                                    // ->having('year', $date)
                                    ->where('year', $current_schoool_year)
                                    ->get()->result();

            // // $sql['teaching_levels'] = $teaching_levels;
            // var_dump("sql=".json_encode($sql));
            // exit(0);
           
        }

        // Convertimos los niveles educativos a forma de array.
        // foreach($challenges as $item){
        //     // $teaching_levels = json_decode($item['teaching_levels'], true);
        //     // $item['teaching_levels'] = $teaching_levels;
        //     $item['teaching_levels'] = json_decode($item['teaching_levels'], true);;
        //     $item['sprints_openfor_assess'] = json_decode($item['sprints_openfor_assess'], true);

        //     // $sprints_openfor_assess = json_decode($item['sprints_openfor_assess'], true);
        //     // $item['sprints_openfor_assess'] = $sprints_openfor_assess;
        //     // var_dump("teaching_levels=".json_encode($teaching_levels));
        //     // exit(0);
        // }

        // var_dump("<br><br><br>".json_encode($challenges));

        return $challenges;
    }

    /**
     * Función para obtener los datos de 1 único RETO
     */
    public function getChallenge ($id, $current_schoool_year = '') {

        $challenge = $this->db->get_where('challenges',array('id' => $id))->row_array();    
        // var_dump($challenge);
        // exit(0);

        // Si el reto no es del curso escolar a mostrar, no se devuelve.
        // var_dump("<br>challenge[year]=".$challenge['year'].", and current_schoool_year=".$current_schoool_year);
        if($current_schoool_year != '')
            if($challenge['year'] !== $current_schoool_year)
                return '';
        // Convertir las fechas de los sprints en arrays
        // $sprints_date_json = $challenge['sprints_date'];
        $sprints_date = json_decode($challenge['sprints_date'], true);
        //print_r("Sprints_date =".$sprints_date[0]);
        //exit(0);
        $result_dates = array();
        
        if(!empty($sprints_date)) {
            foreach($sprints_date as $date) {
                $result_dates[] = $date;
            }    
        } else {
            // No hay fechas de sprint
            for ($i=0; $i<$challenge['sprints']; $i++)
                $result_dates[] = date('Y-m-d');
        }
        
        $challenge['sprints_date'] = $result_dates;
        //print_r($sql);
        //var_dump("sprint date 1= ".$sql['sprints_date'][0]);
        //exit(0);
        // var_dump("getChallenge=".json_encode($challenge));
        // exit(0);
        // Convertimos los niveles educativos a forma de array.
        $challenge['teaching_levels'] = json_decode($challenge['teaching_levels'], true);;
            // var_dump("teaching_levels=".json_encode($teaching_levels));
            // exit(0);

        return $challenge;
    }
    /**
     * Función para obtener el número de sprints
     */
    public function getChallengeSprints($id) {
        $this->db->select('sprints');
        $sql = $this->db->get_where('challenges', array('id' => $id))->row_array();
        return $sql['sprints'];
    }
    /**
     * Función para obtener la fecha de inicio del Reto
     */ 
    public function getChallengeStartDate($id) {
        $this->db->select('start_date');
        $sql = $this->db->get_where('challenges', array('id' => $id));
        return $sql->row_array();
    }
    /**
     * Función para obtener la fecha de fin del RETO
     */
    public function getChallengeFinishDate($id) {
        $this->db->select('finish_date');
        $sql = $this->db->get_where('challenges', array('id' => $id));
        return $sql->row_array();
    }

    /**
     * Función para obtener los Profesores asociados a un Reto
     */
    public function getChallengeTeachers ($challenge_id) {
        $this->db->select('teacher_id');
        $teachers_id = $this->db->get_where('challenges_teachers_enrolment', array('challenge_id' => $challenge_id))->result_array();
        //var_dump($sql->result());
        //var_dump($teachers_id);
        $teachers = $this->teachers_model->getTeachersInChallenge($teachers_id);
        // var_dump($teachers);
        // exit(0);
        return $teachers;
    }
    /**
     * Función para obtener los compañeros de equipo de un reto
     */
    public function getChallengeTeamMatesId ($challenge_id, $student_id) {
        $team = $this->students_model->getStudentTeam($challenge_id,$student_id);
        // print_r($team);
        // exit(0);
        $this->db->select('student_id');
        $sql = $this->db->get_where('challenges_students_enrolment', array(
            'challenge_id' => $challenge_id,
            's_team' => $team
            ))->result_array();
        // var_dump($sql);
        // exit(0);
        $all_team_mates = array();
        foreach($sql as $key => $value) {
            $all_team_mates[] = $value['student_id'];
        }
        // print_r($team_mates);
        $team_mates = array_diff($all_team_mates, array($student_id));
        // print_r($tmp);
        // exit(0);
        return $team_mates;
    }

    /**
     * Función para obtener todos los RETOS en los que participa un profesor.
     */
    public function getAllChallengesOfTeacher($teacher_id) {
        // $teacher_id = 3; // Profesor que no está en ningún reto.
        $this->db->select('challenge_id');
        $challenges_ids = $this->db->get_where('challenges_teachers_enrolment', array('teacher_id' => $teacher_id))->result_array();
        //var_dump($sql->result());
        //var_dump($challenges_ids);
        // exit(0);
        if(!empty($challenges_ids)) {
            foreach($challenges_ids as $id) {
                $result[] = $id['challenge_id'];
            }
        } else 
            $result = $challenges_ids;
        // foreach(
        return array_values($result);
    }
    /**
     * Función para obtener el id del profesor creador del RETO
     */
    public function getTeacherOwner ($challenge_id) {
        $this->db->select('t_owner');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        return $sql['t_owner'];
    }
    /**
     * Función para obtener los Estudianes asociados a un Reto
     */
    public function getChallengeStudents ($challenge_id, $current_team = -1) {
        
        if ($current_team == -1) {
            $this->db->select('student_id');
            $students_id = $this->db->get_where('challenges_students_enrolment', array('challenge_id' => $challenge_id))->result_array();
            $students = $this->students_model->getStudentsInChallenge($students_id);
            // print_r("Los estudiantes son:".json_encode($students)."<br>");
            // exit(0);
            return $students;
        } else {
            // var_dump("<br>challenge_id=".$challenge_id.", current_team=".$current_team);
            $this->db->select('student_id');
            $sql = $this->db->get_where('challenges_students_enrolment', array(
                'challenge_id' => $challenge_id,
                's_team' => $current_team))->result_array();
            $students_id = $sql;
            $students = $this->students_model->getStudentsInChallenge($students_id);
            // Hay que buscar los estudiantes de un equipo

            // $students = $this->students_model->getStudentsInChallenge($students_id, $current_team);
            // var_dump("Los estudiantes son:".json_encode($students)."<br>");
            // exit(0);
            return $students;
        }

    }
    public function getStudentsInTeam ($challenge_id) {
        $this->db->select('student_id, s_team');
        $students_in_teams = $this->db->get_where('challenges_students_enrolment', array('challenge_id' => $challenge_id))->result_array();
        // var_dump("students_id=".json_encode($students_in_team));
        // exit(0);
        return $students_in_teams;

    }
    /**
     * Función para obtener los componentes de un equipo
     */
    

    /**
     * Función para obtener el reto en el que está un estudiante
     */
    public function getChallengesOfStudent ($student_id) {
        // var_dump("student_id=".$student_id);
        $this->db->select('challenge_id');
        $sql = $this->db->get_where('challenges_students_enrolment', array('student_id' => $student_id));
        
        $students_id = $sql->result_array();
        // var_dump("students_id=".json_encode($students_id));
        // exit(0);
        return $students_id;
    }

    /**
     * Función para obtener la Rúbrica asociada a un Reto
     */
    public function getChallengeRubric ($challenge_id) {
        $this->db->select('rubric_id');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        return $sql['rubric_id'];
    }
    /**
     * Función para obtener el número de equipos que tiene un RETO
     */
    public function getChallengeTeams ($challenge_id) {
        $this->db->select('teams');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        return $sql['teams'];
    }

    /**
     * Función para obtener la Rúbrica asociada a un nivel educativo (1-ESO, 2-ESO,...)
     */
    public function getChallengesInLevel ($level) {
        // Utilizamos JSON_CONTAINS para buscar dentro de un tipo JSON
        $sql = $this->db->query("SELECT * from challenges WHERE JSON_CONTAINS(teaching_levels, '".$level."')")->result_array();        
        // var_dump($sql);
        // exit(0);
        return $sql;
     }

     /**
      * Función para devolver el estado de los Sprints, en su valoración.
      */
    public function getSprintsStatus($challenge_id) {
        $this->db->select('sprints_openfor_assess');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        // Devolvemos los estados en forma de array.
        //return explode(",", $sql['sprints_openfor_assess']);
        // return $sql['sprints_openfor_assess'];
        return json_decode($sql['sprints_openfor_assess'],true);

    }
    /**
     * Función para obtener el número de sprints que tiene el reto
     */
    public function getNumberSprints($challenge_id) {
        $this->db->select('sprints');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        return $sql['sprints'];
    }
    /**
     * Función para obtener los identificadores de los RAs que tiene el reto
     */
    public function getTechnicalSkillsIds($challenge_id) {
        $this->db->select('technical_skills');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        // Cambiamos el JSON para devolver un array con [id_technical_skill => porcentaje]
        if (!empty($sql)) {
            // Devolvemos solo los identificadores de las RAs.
            $ts = array_keys(json_decode($sql['technical_skills'], TRUE));
            return $ts;
        } else return $sql;
        //return $sql['technical_skills'];
    }

    /**
     * Función para obtener los porcentajes de los RAs que tiene el reto.
     */
    public function getTechnicalSkillsPercentages($challenge_id) {
        $this->db->select('technical_skills');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        // Cambiamos el JSON para devolver un array con [id_technical_skill => porcentaje]
        if (!empty($sql)) {
            // Devolvemos solo los porcentajes de las RAs.
            $ts = array_values(json_decode($sql['technical_skills'], TRUE));
            return $ts;
        } else return $sql;
        //return $sql['technical_skills'];
    }

    /**
     * Función para obtener los identificadores y los porcentajes juntos de los RAs que tiene el reto.
     */
    public function getTechnicalSkills($challenge_id) {
        $this->db->select('technical_skills');
        $sql = $this->db->get_where('challenges', array('id' => $challenge_id))->row_array();
        // Cambiamos el JSON para devolver un array con [id_technical_skill => porcentaje]
        if (!empty($sql)) {
            // Devolvemos solo los porcentajes de las RAs.
            $ts = json_decode($sql['technical_skills'], TRUE);
            return $ts;
        } else return $sql;
        //return $sql['technical_skills'];
    }


    /**
     * Función para eliminar un Reto de la BBDD
     */
    public function delete($id) { 
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('challenges', array('id' => $id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para borrar los Estudiantes y Profesores asignados a un RETO
     */
    public function disenrolChallenge($id) {
        $this->db->trans_start();
        $this->db->delete('challenges_students_enrolment', array('challenge_id' => $id));
        $this->db->delete('challenges_teachers_enrolment', array('challenge_id' => $id));
        $this->db->trans_complete();

        return !$this->db->trans_status() ? false : true;
    }

    
    /*
     * Fetch members data from the database
     * @param array filter data based on the passed parameters
     */
    function getRows($params = array()){

        // $params = array ( 
        //     'where' => array('role' => 'teacher'),
        // );

        $this->table = 'challenges';
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("where", $params)){
            foreach($params['where'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'desc');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }
   
    /**
     * Función para paginar los datos
     */
    public function get_Paginate($teacher_id, $limit, $offset, $current_schoool_year) {
        if (!empty($teacher_id)) {

            $this->db->select('challenge_id');
            $sql = $this->db->get_where('challenges_teachers_enrolment', array('teacher_id' => $teacher_id));
            $challenges = $sql->result_array();
            $challenges_result = array();
            // Si no está en ninún reto, no se devuelven.
            if(count($challenges)==0)
                return array();

            foreach($challenges as $item) {
                // $challenges_result[] = array(
                //     'id' => $item['challenge_id'],
                // );
                $challenges_result[] = $item['challenge_id'];
                // $sql = $this->db->order_by('name', 'ASC')->get_where('challenges', array('id' => $item['challenge_id']));
                // $challenges_result[] = $sql->result_array();
            }
            // var_dump($challenges[0]['challenge_id']);
            // var_dump("<br/>");
            // var_dump($challenges_result[0]);
            // var_dump(array_values($challenges_result));
            // exit(0);
            // return $challenges_result;
            

            // var_dump("<br> la fecha es: ".$date);
            // exit(0);

            $sql = $this->db->order_by('name', 'ASC')
                            ->from('challenges')
                            ->where('year', $current_schoool_year)
                            ->where_in('id', array_values($challenges_result))
                            // ->where('year', $current_schoool_year)

                            // ->where_in(array('id' => $challenges_result, 'year' => $current_schoool_year))
                            ->get()->result_array();

            // var_dump("<br>SQL=".json_encode($sql));
            // exit(0);
            return $sql;
            
        } else {
            // $sql = $this->db->order_by('name', 'ASC')->get('challenges', $limit, $offset)->result_array();
            $sql = $this->db->order_by('name', 'ASC')
                                    ->from('challenges')
                                    // ->having('year', $date)
                                    ->where('year', $current_schoool_year)
                                    ->get()->result_array();
            // Convert JSON data into array
            foreach($sql as $item) {
                // $sprints_tobe_assess_coded = $item['sprints_openfor_assess'];
                // $item['sprints_openfor_assess'] = json_decode($sprints_tobe_assess_coded, TRUE);
                $item['sprints_openfor_assess'] = json_decode($item['sprints_openfor_assess'], true);

            }
            return $sql;

        }
        
    }


}