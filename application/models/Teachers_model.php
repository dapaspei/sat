<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachers_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
    }
    /**
     * Método para crear y almacenar un profesor en la BBDD
     */
    public function create($data) {
        // Extraemos el último elemento donde están los niveles de docencia.
        $levels = array_pop($data);
        // Convertir los niveles a datos JSON
        $levels_json[] = json_encode($levels);
        $data['teaching_levels'] = $levels_json[0];
        return !$this->db->insert('teachers',$data) ? false: true;
    }
    /**
     * Función para actualizar los datos de un profesor
     */
    public function update($id, $data) {
        // Extraemos el último elemento donde están los niveles de docencia.
        $levels = array_pop($data);
        // var_dump("levels = ".$levels);
        // exit(0);
        // Convertir los niveles a datos JSON
        $levels_json[] = json_encode($levels);
        $data['teaching_levels'] = $levels_json[0];
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('user_id', $id);
        $this->db->update('teachers', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

     /** 
     * Función para almacenar un profesor en la base de datos
     */
    // public function save($data) {
    //     // Iniciar transacción para poder hacer rollback
    //     $this->db->trans_start();
    //     $this->db->insert('teachers',$data);
    //     $this->db->trans_complete();
    //     return !$this->db->trans_status() ? false : true;
    // }

    public function getTeachers ($level = '') {
        // var_dump("getTeachers(level)=".json_encode($level));
        if (empty($level)) {
            $sql = $this->db->order_by('lastname', 'ASC')->get('teachers')->result_array();
        }
        else {
            // $sql = $this->db->query("SELECT * from teachers WHERE teaching_levels LIKE '%\"$level\"%'")->result_array();
            $levels = implode(", ",$level);

            for($i=0; $i<count($level); $i++) {
                $like_statement[] = 'teaching_levels LIKE \'%'.$level[$i].'%\' or ';
            }
            // Quito los últimos caracteres 'and '
            $final_statement = substr_replace(implode($like_statement) ,"",-3);
            // var_dump("<br>like_statement=".json_encode($like_statement));
            // var_dump("<br>implode(like_statement)=".json_encode(implode($like_statement)));
            // var_dump("<br>final_statement=".$final_statement);
            // exit(0);
            $sql = $this->db->query("SELECT * from teachers WHERE ( ".$final_statement." )")->result_array();
        }
        $data = array();
        foreach($sql as $item) {
            //$levels_coded = array_pop($item);
            $levels_coded = $item['teaching_levels'];
            $levels = json_decode($levels_coded, true);
            // $levels = json_decode($item['teaching_levels'], true);
            // $item->teaching_levels = $levels[0];
            // Dejamos una array
            $item['teaching_levels'] = $levels;

            // $item['teaching_levels'] = json_decode($item['teaching_levels'], true);
            $data[] = $item;
        }
        // var_dump("<br>getTeachers(data)=".json_encode($data));
        // exit(0);
        return $data;
				
        // $sql = $this->db->query("SELECT * from teachers WHERE teaching_levels LIKE '%\"$level\"%'")->result();
        // //var_dump($sql);
        // //exit(0);
        // return $sql;

    }

    /**
     * Función para obtener los datos de 1 único usuario
     */
    public function getTeacher ($id) {
        $sql = $this->db->get_where('teachers',array('user_id' => $id))->row_array(); 
        // Decodificamos los datos guardados en JSON (levels)
        // $levels_coded = array_pop($data);
        $levels_coded = $sql['teaching_levels'];
        // var_dump($levels_coded);
        // var_dump("<br>");
        // $levels = json_decode($levels_coded);
        $levels = json_decode($sql['teaching_levels'], true);
        //var_dump($levels);
        //exit(0);

        if(!is_array($levels)) {
        //if (count($levels) === 1) {
            // Si sólo tiene un curso
            $data['teaching_levels'] = $levels;
        } else {
            // Si tiene varios cursos asignados.
            foreach($levels as $level)
                $levels_result[] = $level;
            $data['teaching_levels'] = $levels_result;
        }
        //var_dump($data);
        // exit(0);
        
        return $data;
    }

    /**
     * Función para obtener el Nombre + Apellidos de un único profesor
     */
    public function getTeacherFullName ($id) {
        $this->db->select('firstname, lastname');
        $sql = $this->db->get_where('teachers',array('id' => $id))->row_array();     
        return $sql['firstname'].' '.$sql['lastname'];
    }
    /**
     * Función para obtener el 'id' de un único profesor
     */
    public function getTeacherId ($id) {
        $this->db->select('id');
        $sql = $this->db->get_where('teachers',array('user_id' => $id))->row_array();     
        return $sql['id'];
    }

    /**
     * Función para obtener los niveles de docencia de un profesor
     */
    public function getTeachingLevels($id) {
        $this->db->select('teaching_levels');
        $sql = $this->db->get_where('teachers',array('user_id' => $id))->row_array(); 
        // Decodificamos los datos guardados en JSON (levels)
        $levels = json_decode($sql['teaching_levels']);
        //var_dump($levels);
        //exit(0);

        if(!is_array($levels)) {
            // Si sólo tiene un curso
            //$data['teaching_levels'] = $levels;
            $data = $levels;
        } else {
            // Si tiene varios cursos asignados.
            foreach($levels as $level)
                $levels_result[] = $level;
            //$data['teaching_levels'] = $levels_result;
            $data = $levels_result;
        }
        //var_dump($data);
        // exit(0);
        
        return $data;
    }
    /**
     * Función para obtener el listado de usuarios en un Reto
     */
    public function getTeachersInChallenge($teachers_ids = '') {

        if(empty($teachers_ids)) 
            return array();

        //$data = array('id' => '6', 'id' => '4');
        $sql_result = array();

        foreach ($teachers_ids as $item) {
            //$result[] = $item['teacher_id'];
            // var_dump($item['teacher_id']);
            // var_dump('<br>');
            // Obtenemos los datos del profesor/a
            $teacher = $this->db->get_where('teachers', array('id' => $item['teacher_id']))->result_array(); 
            $teacher = $teacher[0];

            // Ponemos en formato de Array los cursos a los que imparte
            $levels = json_decode($teacher['teaching_levels'], true);
            $teacher['teaching_levels'] = $levels;
            $sql_result[] = $teacher;

            // $sql_result[] = $teacher[0];

            
        }
        // var_dump("<br>getTeachersInChallenge(sql)=".json_encode($sql_result));
        // exit(0);

        return $sql_result;
    }


    /**
     * Función para eliminar un profesor de la BBDD
     */
    public function delete($id) { 
        // Borrar de los retos asociados
        $teacher_id = $this->getTeacherId($id);
        if(!$this->disenrolChallenge($teacher_id)) return false;
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('teachers', array('user_id' => $id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    
    /**
     * Función para CREAR los Retos de un Profesor
     */
    public function enrolChallenge($teacher_id, $challenge_id) {

        $data = array (
            'teacher_id' => $teacher_id,
            'challenge_id' => $challenge_id,
        );
        return !$this->db->insert('challenges_teachers_enrolment',$data) ? false: true;
    }
    /**
     * Función para BORRAR un profesor de un RETO
     */
    public function disenrolChallenge($teacher_id) {
        $this->db->trans_start();
        $this->db->delete('challenges_teachers_enrolment', array('teacher_id' => $teacher_id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    
    /**
     * Función para actualizar los RETOS de un profesor
     */
    public function updateChallenges($id, $data) {
        // Iniciar transacción para poder hacer rollback
        //var_dump("challenges id = ".$id."<br>");
        //var_dump("data = ".$data."<br>");
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('teachers', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para ver si un profesor está asigando a un RETO.
     */
    public function isTeacherEnrolledInChallenge($id) {
        $sql = $this->db->get_where('challenges_teachers_enrolment', array('teacher_id' => $id)); 
        return $sql->result();
    }
}