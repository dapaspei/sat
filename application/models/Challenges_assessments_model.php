<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges_assessments_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        // $this->load->model('teachers_model','teachers');
        // $this->load->model('students_model','students');
        $this->load->model('challenges_model');
        $this->load->model('challenges_outcomes_model');
    }

    /**
     * Método para inicializar las Evaluaciones en la BBDD
     */
    public function start($challenge_id, $challenge, $students, $teachers, $current_sprint) {
        // var_dump("start(teachers)=".json_encode($teachers));
        // exit(0);
        foreach($teachers as $t) {
            $teachers_ids_in[] = $t['id'];
            // var_dump("<br>t[0]['id']=".$t['id']);
            // "teachers=["4","6"]"
        }
        // var_dump("<br>t_ids_in=".json_encode($teachers_ids_in));
        // exit(0);

        // Profesores que pueden valorar el reto.
        $teachers_assessing = json_encode($teachers_ids_in);
        // var_dump($teachers_assessing);
        // exit(0);

        $start_date = date('Y-m-d', strtotime($challenge['start_date'].' -1 days'));
        $end_date = date('Y-m-d', strtotime($challenge['start_date'].' +1 days'));
        // OK

        // var_dump("start_date=".$start_date.", end_date=".$end_date);
        // exit(0);

        //for($i=0; $i<count($students); $i++) {
        foreach($students as $std) {
//             var_dump("student_id=".$std[0]->id);
            // Creamos una instancia de OUTCOMES en la BBDD para cada estudiante
            $outcome_id = $this->challenges_outcomes_model->create();
            $data = array (
                'challenge_id' => $challenge_id,
//                 'student_id' => $std[0]->id,
                'student_id' => $std['id'],
                'teachers_id' => $teachers_assessing,
                'current_sprint' => $current_sprint,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'course_date' => $challenge['year'],
                'learning_outcomes_id' => $outcome_id,
                'final_mark' => 0.0,
            );

//             var_dump("<br>data=".json_encode($data));
//             exit(0);
            if (!$this->db->insert('challenge_assessment', $data)) {
            // Si hay error, devolvemos falso
                return false;
            } else {
                // Si la inserción ha sido exitosa
                // Devolvemos el id de la fila insertada.
                $assessment_id[] = $this->db->insert_id();
            }
        }
        // var_dump("Assessments insertados".json_encode($assessment_id));
        // exit(0);
        return $assessment_id;

    }
    /**
     * Método para almacenar la Evaluación hecha por un profe, para un Alumno
     */
    public function saveTeacherAssessment($assessment_id, $student_id, $outcome_id, $teacher_assessment_data, $teachers_comments){
        // var_dump("teacher_assessment_data=".json_encode($teacher_assessment_data));
        // exit(0);

        $stored_teachers_data = $this->challenges_outcomes_model->getTeacherOutcomesData($outcome_id);
        // Comprobar que no esté vacío
        if(empty($stored_teachers_data))
            $stored_teachers_data = array();
        // var_dump("stored_teacher_assessment_data=".json_encode($stored_teachers_data));
        // exit(0);
        $data = $teacher_assessment_data + $stored_teachers_data;
        // var_dump("la suma es: ".json_encode($data));
        // exit(0);
        // Guardamos los datos del profesor en "challenge_learning_outcomes"
        if(!$this->challenges_outcomes_model->saveTeacherOutcomes($outcome_id, $data))
            return false;

        $stored_teachers_commments = $this->getTeachersComments($assessment_id);
        // var_dump("stored_teacher_comments".$stored_teachers_commments);
        // exit(0);

        // Actualizamos los comentarios del profesor
        if(!empty($stored_teachers_commments)) {
            $data = array ('teachers_comments' => json_encode($teachers_comments + json_decode($stored_teachers_commments, true)));
        } else {
            $data = array ('teachers_comments' => json_encode($teachers_comments));
        }

        //Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $assessment_id);
        $this->db->update('challenge_assessment', $data);
        $this->db->trans_complete();

        $status = $this->db->trans_status();
        if (!$status) {
            // Borramos los datos que acabamos de meter
            $this->challenges_outcomes_model->deleteTeacherOutcomes($outcome_id, $teacher_assessment_data);
            return false;
        }
        return true;

    }

    /**
     * Método para almacenar la Evaluación hecha por un profe, para un Alumno
     */
    public function saveSelfStudentAssessment($assessment_id, $student_id, $outcome_id, $self_assessment_data, $self_comments){
        // Guardamos los datos del estudiante en "challenge_learning_outcomes"
        if(!$this->challenges_outcomes_model->saveSelfStudentOutcomes($outcome_id, $self_assessment_data))
            return false;
        // Guardamos los comentarios del estudiante.
        $this->db->trans_start();
        $this->db->where('id', $assessment_id);
        $this->db->update('challenge_assessment', array('self_comments' => json_encode($self_comments)));
        $this->db->trans_complete();

        $status = $this->db->trans_status();
        if (!$status) {
            // Borramos los datos que acabamos de meter
            $this->challenges_outcomes_model->deleteStudentOutcomes($outcome_id, $self_assessment_data);
            return false;
        }
        return true;
    }

    /**
     * Método para almacenar la Evaluación hecha por un COMPAÑERO (coevaluation)
     * para los compañeros de un equipo.
     */
    public function savePeerStudentAssessment($outcome_id, $peer_assessment_data, $peer_comments){
//         var_dump("<br>outcome_id=".$outcome_id."<br>peer_comments=".json_encode($peer_comments));
//         exit(0);
        $stored_peer_data = $this->challenges_outcomes_model->getPeerOutcomesData($outcome_id);
        // Comprobar que no esté vacío
        if(empty($stored_peer_data))
            $stored_peer_data = array();
//         var_dump("stored_peer_assessment_data=".json_encode($stored_peer_data));
//         exit(0);
        $all_data = $peer_assessment_data + $stored_peer_data;
//         var_dump("peer + stored data=".json_encode($all_data));
        if(!$this->challenges_outcomes_model->savePeerStudentOutcomes($outcome_id, $all_data))
//         if(!$this->challenges_outcomes_model->savePeerStudentOutcomes($outcome_id, $peer_assessment_data))
            return false;

        // Ahora insertamos los comentarios
        $stored_peer_commments = $this->getPeerComments($outcome_id);
//         var_dump("<br>stored_peer_comments".$stored_peer_commments);
//         exit(0);

        // Actualizamos los comentarios del profesor
        if(!empty($stored_peer_commments)) {
            $data = array ('peer_comments' => json_encode($peer_comments + json_decode($stored_peer_commments, true)));
        } else {
            $data = array ('peer_comments' => json_encode($peer_comments));
        }
//         var_dump("<br>peer_comments a punto de guardar".json_encode($data));
//         exit(0);
        $this->db->trans_start();
        $this->db->where('learning_outcomes_id', $outcome_id);
        $this->db->update('challenge_assessment', $data);
        $this->db->trans_complete();

        $status = $this->db->trans_status();
        if (!$status) {
            // Borramos los datos que acabamos de meter
            $this->challenges_outcomes_model->deletePeerStudentOutcomes($outcome_id, $peer_assessment_data);
            return false;
        }
        return true;
    }


    /**
     * Función para actualizar los datos de una EVALUACIÓN
     */
    public function update($id, $data) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('challenge_assessment', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para obtener el listado de Evaluaciones de un alumno
     * @param $user Si hay un idenficador sólo se muestran los Retos
     * asignados a ese Profesor. 
     */
    public function getStudentAssessments($user = '') {
        // Si es un profesor, sólo mostramos los retos en los que está asignado
        if (!empty($user)) {
            $this->db->select('challenge_id');
            $sql = $this->db->get_where('challenges_students_enrolment', array('teacher_id' => $user));
            $challenges = $sql->result_array();
            $challenges_result = array();
            // Si no está en ninún reto, no se devuelven.
            if(count($challenges)==0)
                return array();

            foreach($challenges as $item) {
                // $challenges_result[] = array(
                //     'id' => $item['challenge_id'],
                // );
                $challenges_result[] = $item['challenge_id'];
                // $sql = $this->db->order_by('name', 'ASC')->get_where('challenges', array('id' => $item['challenge_id']));
                // $challenges_result[] = $sql->result_array();
            }
            // var_dump($challenges[0]['challenge_id']);
            // var_dump("<br/>");
            // var_dump($challenges_result[0]);
            // var_dump($challenges_result);
            // exit(0);
            // return $challenges_result;
            
            //$sql = $this->db->order_by('name', 'ASC')->get_where('challenges', array('id', $challenges_result[0]));
            $sql = $this->db->order_by('name', 'ASC')
                            ->from('challenges')
                            ->where_in('id', $challenges_result)
                            ->get();

            // var_dump($sql->result());
            // exit(0);
            return $sql->result();
            
        } else {
            $sql = $this->db->order_by('name', 'ASC')->get('challenges');
            return $sql->result();
        }

    }

    /**
     * Función para obtener los datos de una única Evaluación
     */
    public function getAssessments ($challenge_id, $current_sprint, $student_id = '') {
        // var_dump("<br>getAssessments:<br>challenge_id=".$challenge_id."<br>current_sprint=".$current_sprint);
        if(!empty($student_id))
            $sql = $this->db->get_where('challenge_assessment', array(
                'challenge_id' => $challenge_id,
                'current_sprint'=>$current_sprint,
                'student_id' => $student_id,
            ))->row_array(); // Sólo ha de devolver un resultado.
        else
            $sql = $this->db->get_where('challenge_assessment',array('challenge_id' => $challenge_id, 'current_sprint'=>$current_sprint))->result_array();
        // var_dump($sql);
        // exit(0);
        return $sql;
    }

    /**
     * Función para obtener los IDs de los ASSESSMENTs asociados a un Challenge_id
     */
    public function getAssessmentsId ($challenge_id) {
        $this->db->select('id');
        $sql = $this->db->get_where('challenge_assessment',array('challenge_id' => $challenge_id))->result_array();
        //var_dump($sql);
        foreach($sql as $item) {
            $data[] = $item['id']; // Cogemos los identificadores y creamos un array sólo con ellos.
        }
        // var_dump($data);
        // exit(0);
        return $data;
    }

    /**
     * Función para obtener los Profesores asociados a una EVALUACIÓN
     */
    public function getAssessmentTeachers ($id) {
        $this->db->select('teacher_id');
        $sql = $this->db->get_where('challenges_teachers_enrolment', array('challenge_id' => $id));
        $teachers_id = $sql->result_array();
        //var_dump($sql->result());
        //var_dump($teachers_id);
        $teachers = $this->teachers_model->getTeachersInChallenge($teachers_id);
        // var_dump($teachers);
        // exit(0);
        return $teachers;
    }
    /**
     * Función para obtener los IDENTIFICADORES de Estudiantes asociados a una EVALUACIÓN/RETO
     */
    public function getAssessmentStudentsIds ($challenge_id, $s_team = -1) {
        // var_dump("<br>s_team in getAssessmentsStudentsIds=".$s_team);
        
        if ($s_team == -1) {
            $this->db->select('student_id');
            $students = $this->db->get_where('challenges_students_enrolment', array('challenge_id' => $challenge_id))->result_array();
            // var_dump("<br>students id in enrolment=".json_encode($students));
            // exit(0);
            // Nos quedamos con los identificadores de los estudiantes del reto.
            foreach($students as $item)
                $students_id[] = $item['student_id'];

            // var_dump("<br>students_id=".json_encode($students_id));
            return $students_id;
        } else {
            $this->db->select('student_id');
            $students = $this->db->get_where('challenges_students_enrolment', array(
                'challenge_id' => $challenge_id,
                's_team' => $s_team))->result_array();
            // var_dump("<br>students id in enrolment=".json_encode($students));
            // exit(0);
            // Nos quedamos con los identificadores de los estudiantes del reto.
            foreach($students as $item)
                $students_id[] = $item['student_id'];

            // var_dump("<br>students_id=".json_encode($students_id));
                    // Comprobamos qué estudiantes están en el equipo
                    // $std_in_team = $this->students_model->getStudentIdsInTeam($s_team);
                    // var_dump("<br>std_in_team=".json_encode($std_in_team)."<br>students=".json_encode($students_id));
                    // $intersect = array_intersect($std_in_team, $students_id);
                    // var_dump("<br>intersect =".json_encode($intersect));
                    // return $intersect;
            // exit(0);
            return $students_id;

        }

    }
    /**
     * Función para obtener los Estudiantes asociados a una EVALUACIÓN
     */
    public function getAssessmentStudents ($id) {
        $this->db->select('student_id');
        $sql = $this->db->get_where('challenges_students_enrolment', array('challenge_id' => $id));
        $students_id = $sql->result_array();

        $students = $this->students_model->getStudentsInChallenge($students_id);
        return $students;
    }
    /**
     * Función para obtener el identificador del Reto asociada a una Evaluacion
     */
    public function getAssessmentChallengeId ($id) {
        $this->db->select('challenge_id');
        $sql = $this->db->get_where('challenge_assessment', array('id' => $id))->row_array();
        return $sql['challenge_id'];
    }
    /**
     * Función para obtener los comentarios de los profesores
     */
    public function getTeachersComments($asessment_id) {
        $this->db->select('teachers_comments');
        $sql = $this->db->get_where('challenge_assessment', array('id' => $asessment_id))->row_array();
        return $sql['teachers_comments'];
    }
    /**
     * Función para obtener los comentarios de los compañeros de un equipo
     */
    public function getPeerComments($outcome_id) {
        $this->db->select('peer_comments');
        $sql = $this->db->get_where('challenge_assessment', array('learning_outcomes_id' => $outcome_id))->row_array();
        return $sql['peer_comments'];
    }
    /**
     * Función para saber si un identificador de RETO está en la BBDD
     */
    public function isChallengeInAssessment($challenge_id, $current_sprint = -1) {
        // var_dump("isChallengeInAssessment:<br> current_sprint = ".$current_sprint);
        // exit(0);
        // // Buscamos los sprints que se pueden evaluar.
        // $sprints_openfor_assess = $this->challenges_model->getSprintsStatus($challenge_id);
        // var_dump("sprints_openfor_assess=".json_encode($sprints_openfor_assess[0]));
        // exit(0);
        $hasAssessments = false;
        // for($i=0; $i<count($sprints_openfor_assess); $i++){
        //     if ($sprints_openfor_assess != 0) {
        //         $this->db->select('challenge_id');
        //         $sql = $this->db->get_where('challenge_assessment', array(
        //             'challenge_id' => $challenge_id,
        //             'current_sprint' => $i,
        //         ));
        //         //var_dump($sql->result());
        //         ///exit(0);
        //         if (!empty($sql->result()))
        //             $hasAssessments = true;
        //             // return true;
        //         else 
        //             return false;
        //     }

        // }
        $this->db->select('challenge_id');
        if($current_sprint == -1) {
            $sql = $this->db->get_where('challenge_assessment', array(
                'challenge_id' => $challenge_id,
            ));
        } else {
            $sql = $this->db->get_where('challenge_assessment', array(
                'challenge_id' => $challenge_id,
                'current_sprint' => $current_sprint,
            ));
        }
        
        //var_dump($sql->result());
        ///exit(0);
        if (!empty($sql->result()))
            $hasAssessments = true;
            // return true;
        // else 
        //     return false;

        return $hasAssessments;
        
    }

    /** 
     * Funnción para devolver los datos de la evaluación de un profesor en un reto a unos alumnos.
     */
    public function getTeachersSprintAssessData($challenge_id, $current_sprint, $teacher_id, $students, $s_team) {
        $result = array();
        // Para cada estudiante, buscamos sus calificaciones.
        for($i=0; $i<count($students); $i++){
            // Obtenemos los identificadores de las posibles evaluaciones
            $this->db->select('learning_outcomes_id');
//             var_dump("array=".json_encode($array));
            $sql = $this->db->get_where('challenge_assessment', array(
                'challenge_id' => $challenge_id,
                'current_sprint' => $current_sprint,
                'student_id' => $students[$i]['id'],
            ))->row_array();
            $outcomes_id = $sql['learning_outcomes_id'];
//             var_dump("outcomes_id=".$outcomes_id);
            // buscamos las valoraciones del profesor.
            $this->db->select('teacher_assessment_data');
            $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcomes_id))->row_array();
            $outcomes_data = json_decode($sql['teacher_assessment_data'],true);
            // var_dump("<br>teacher_id=".$teacher_id.", outcomes_data=".json_encode($outcomes_data));
            // var_dump("<br>outcomes_data(keys)=".json_encode(array_keys($outcomes_data)));
//             var_dump("<br>out_=".json_encode($outcomes_data[$teacher_id]));
            // exit(0);
            if(!empty($outcomes_data) && array_key_exists($teacher_id, $outcomes_data)) {
                $result[] = array(
                    $students[$i]['id'] => $outcomes_data[$teacher_id]
                );
            }
            
        }
//         var_dump("result=".json_encode($result));
//         exit(0);
        return $result;
    }

    /**
     * Función para devolver los comentarios de un profesor en un Sprint determinado.
     */
    public function getTeachersSprintCommentsData($challenge_id, $current_sprint, $teacher_id, $students, $s_team) {
        $result = array();
        // Obtenemos los identificadores de las posibles evaluaciones
        for($i=0; $i<count($students); $i++) {
            // Buscamos los comentarios de profesores.
            $this->db->select('teachers_comments');
            $sql = $this->db->get_where('challenge_assessment', array(
                'challenge_id' => $challenge_id,
                'current_sprint' => $current_sprint,
                'student_id' => $students[$i]['id'],
            ))->row_array();
            $comments_data = json_decode($sql['teachers_comments'], true);
            // var_dump("comements_data=".json_encode($comments_data));
            if(!empty($comments_data) && array_key_exists($teacher_id, $comments_data)) {
                $result[] = array(
                    $students[$i]['id'] => $comments_data[$teacher_id]
                );
            }
            
        }
//         var_dump("result=".json_encode($result));
//         exit(0);
        return $result;
    }

    public function getTeachersAssessmentData($outcome_id) {
        $this->db->select('teacher_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
        $tmp = $sql['teacher_assessment_data'];

        return json_decode($tmp, true);
    }


    /*********************************************************/
    /********************* ESTUDIANTES ***********************/
    /*********************************************************/

    public function getStudentAssessmentData($outcome_id) {
        $this->db->select('self_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
        // return $sql->row_array();
        // print_r($sql);
        // print_r("<br>");
        $tmp = $sql['self_assessment_data'];
        // print_r($tmp);

        return json_decode($tmp, true);
    }

    public function getPeerAssessmentData($outcome_id) {
        $this->db->select('peer_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
        // print_r($sql);
        // print_r("<br>");
        $tmp = $sql['peer_assessment_data'];
        // print_r($tmp);

        return json_decode($tmp, true);
    }

    /**
     * Funnción para devolver los datos de la evaluación de un ESTUDIANTE en un reto.
     */
    public function getStudentsSprintAssessData($challenge_id, $current_sprint, $student_id, $s_team) {
        // var_dump("<br>challenge_id=".$challenge_id.",current_sprint=".$current_sprint.",student_id=".$student_id."s_team=".$s_team);
        // Obtenemos los identificadores de la evaluación del estudiante.
        $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, $current_sprint, $student_id);
        // var_dump("<br>outcome_id=".json_encode($outcome_id));
        $this->db->select('self_assessment_data');
        $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
        // var_dump("self_assessment_data=".json_encode($sql['self_assessment_data']));
        $result = array ('self_assessment_data' => json_decode($sql['self_assessment_data'], true));
        // var_dump("<br>result_self_ass_data".json_encode($result));
        // Buscamos las evaluaciones que los compañeros le hayan podido hacer a este alumno
        $students_ids = $this->getAssessmentStudentsIds($challenge_id, $s_team);
        // var_dump("<br>los ids de los students=".json_encode($students_ids));
        // Eliminamos el estudiante que hace la consulta, el estudiante de la autoevalución.
        $students_ids = array_diff($students_ids, array($student_id));
        $students_ids = array_values($students_ids);
//         var_dump("<br>students_ids=".json_encode($students_ids));
        $peer_assessment_data = array();
        for($i=0; $i<count($students_ids); $i++) {
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, $current_sprint, $students_ids[$i]);
            // var_dump("outcome_id=".$outcome_id);
            $this->db->select('peer_assessment_data');
            $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
//             $sql = $sql['peer_assessment_data'];
//             var_dump("sql_peer=".json_decode($sql['peer_assessment_data']));
            $tmp= json_decode($sql['peer_assessment_data'], true);
//             $peer_assessment_data[] = $tmp[$student_id];
            $peer_assessment_data[] = json_decode($sql['peer_assessment_data'], true);
//             $result['peer_assessment_data'] = array($peer_assessment_data);
        }
        $result['peer_assessment_data'] = $peer_assessment_data;
        // var_dump("<br>result_peer_assessment_data = ".json_encode($result));
        // exit(0);
        return $result;
    }

    /**
     * Función para devolver los comentarios de un profesor en un Sprint determinado.
     */
    public function getStudentsSprintCommentsData($challenge_id, $current_sprint, $student_id, $s_team) {
        // Obtenemos los identificadores de la evaluación del estudiante.
        $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, $current_sprint, $student_id);
//         var_dump("outcome_id=".$outcome_id);
        $this->db->select('self_comments, peer_comments');
        $sql = $this->db->get_where('challenge_assessment', array('learning_outcomes_id' => $outcome_id))->row_array();
//         var_dump("self_comments=".json_encode($sql));
        $result = array (
            'self_comments' => json_decode($sql['self_comments'], true),
//             'peer_comments' => json_decode($sql['peer_comments'], true),
//             'teacher_comments' => json_decode($sql['teacher_comments'], true),
        );
        // Buscamos los comentarios de que este alumno hace a otros compañeros
        $students_ids = $this->getAssessmentStudentsIds($challenge_id, $s_team);
        // Eliminamos el estudiante que hace la consulta, el estudiante de la autoevalución.
        $students_ids = array_diff($students_ids, array($student_id));
        $students_ids = array_values($students_ids);
        // var_dump("<br>students_ids=".json_encode($students_ids));
        $peer_comments = array();
        for($i=0; $i<count($students_ids); $i++) {
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, $current_sprint, $students_ids[$i]);
            // var_dump("outcome_id=".$outcome_id);
            $this->db->select('peer_comments');
            $sql = $this->db->get_where('challenge_assessment', array('learning_outcomes_id' => $outcome_id))->row_array();
//             $sql = $sql['peer_comments'];
//             var_dump("sql_peer=".json_decode($sql['peer_comments']));
            $tmp= json_decode($sql['peer_comments'], true);
//             $peer_comments[] = $tmp[$student_id];
            $peer_comments[] = json_decode($sql['peer_comments'], true);
//             $result['peer_comments'] = array($peer_comments);
        }
        $result['peer_comments'] = $peer_comments;
        // var_dump("<br>result_peer_comments = ".json_encode($result));
        // exit(0);
        return $result;
    }

    /** ***********************************************************************************
     *  *********************** GRADES - CALIFICACIONES  **********************************
     ************************************************************************************ */

    /**
     * Función para obtener y calcular las notas/calificaciones de un estudiante en todos los Sprints
    */
    public function getAllStudentGrades($challenge_id, $student_id = '', $sprints='') {
        // var_dump($challenge_id,$sprints,$student_id);
        // print_r("<br>");
        // Si no hay sprint, buscamos en todos los sprints
        if(empty($sprints)) {
            // var_dump("No hay sprint definido.");
            return array();
        }
        // Obtenemos el identificador del equipo del estudiante.
        // $s_team = $this->students_model->getStudentTeamForGrading($student_id);
        // $s_team = $this->students_model->getStudentTeam($challenge_id, $student_id);
        // var_dump("<br>s_team=".$s_team.", student_id=".$student_id);
        // exit(0);

        $self_data = array();
        $peer_assessment_data = array();
        $teacher_assessment_data = array();

        for($i=1; $i<=intval($sprints); $i++) {
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, $i /* # sprint */, $student_id);
            // print_r($outcome_id);
            // exit(0);
            // Si no hay resultados porqué no se han evaluado todavía los sprints, saltamos al siguiente
            if (is_null($outcome_id))
                continue;

            $this->db->select('self_assessment_data, peer_assessment_data, teacher_assessment_data');
            $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
            // var_dump("<br>Sql->all_data=".json_encode($sql));
            // print_r($sql);
            // exit(0);


            $hay_valores = false;
            // Comprobar si no hay resultados
            foreach ($sql as $key => $value) {
                if (!empty($value)) {
                   $hay_valores = true;
                }
            }


            // if(empty($sql)){
            if(!$hay_valores) {
                // print_r("<br>no da resultados!!");
            } else {
                // $valores_self_data = json_decode($sql['self_assessment_data'], TRUE);
                // var_dump($valores_self_data);
                // $array_values_de_self_data = array_values($valores_self_data);
                // print_r("<br>");
                // var_dump($array_values_de_self_data);
                // exit(0);
                // if(!empty($sql['self_assessment_data']) && ($sql['self_assessment_data']!== 'null')) {
                //     $self_data[] = array_values(json_decode($sql['self_assessment_data'], true))[0];
                // }
                // else 
                //     $self_data[] = array();
                if(empty($sql['self_assessment_data']) || is_null($sql['self_assessment_data']) || is_null(json_decode($sql['self_assessment_data'], true)) )
                    $self_data[] = array();
                else 
                    $self_data[] = array_values(json_decode($sql['self_assessment_data'], true))[0];


                // Comprobamos que tenga evaluaciones de los compañeros
                // if(!empty($sql['peer_assessment_data']) && ($sql['peer_assessment_data']!== 'null'))
                //     $peer_assessment_data[] = array_values(json_decode($sql['peer_assessment_data'], true));
                // else 
                //     $peer_assessment_data[] = array();
                if(empty($sql['peer_assessment_data']) || is_null($sql['peer_assessment_data']) || is_null(json_decode($sql['peer_assessment_data'], true)) )
                    $peer_assessment_data[] = array();
                else 
                    $peer_assessment_data[] = array_values(json_decode($sql['peer_assessment_data'], true));



                // Comprobamos que tenga evaluaciones de los PROFESORES
                // print_r($sql);
                // print_r("<br>");
                if(!is_null($sql['teacher_assessment_data'])) {
                    // print_r($outcome_id."=");
                    // print_r(json_decode($sql['teacher_assessment_data']));
                    // print_r("<br>");
                    // if(!empty($sql['teacher_assessment_data']) && ($sql['teacher_assessment_data'] !== 'null'))
                    if(empty($sql['teacher_assessment_data']) || is_null($sql['teacher_assessment_data']) || is_null(json_decode($sql['teacher_assessment_data'], true)) )
                    // if(!empty($sql['teacher_assessment_data']) && (!is_null($sql['teacher_assessment_data'])))
                        $teacher_assessment_data[] = array();
                    else 
                        $teacher_assessment_data[] = array_values(json_decode($sql['teacher_assessment_data'], true));
                    // else 
                    //     $teacher_assessment_data[] = array();

                }
                else 
                    $teacher_assessment_data[] = array();
            }
            // exit(0);
        }
        // var_dump("<br>getStudentGrades(self_data)=<br>".json_encode($self_data).",count(self_data)=".count($self_data)."<br>");
        // var_dump("<br>getStudentGrades(peer_data)=<br>".json_encode($peer_assessment_data).",count(peer_data)=".count($peer_assessment_data)."<br>");
        // var_dump("<br>getStudentGrades(teacher_data)=<br>".json_encode($teacher_assessment_data).",count(teacher_data)=".count($teacher_assessment_data)."<br>");
        // exit(0);
                // $all_data['sprints_data'] = array(
                //     'self_data' => $self_data,
                //     'peer_data' => $peer_assessment_data,
                //     'teacher_data' => $teacher_assessment_data,
                // );

        // Tenemos los resultados del PROPIO ALUMNO -> $self_data
        if(!empty($self_data)){
            // $result_self = $self_data[0];
            // Tendremos los resultados de todos los sprints
            $result_self = $self_data;
        } else {
            $result_self = array();
        }
        // var_dump("<br>result_self=".json_encode($result_self));
        // exit(0);

        // Buscamos cuantos valores tiene la rúbrica, es decir, cuantas columnas
        $rubric_cols = $this->rubrics_model->getRubricRows($this->challenges_model->getChallengeRubric($challenge_id));

        $result_peer = array();
        if(!empty($peer_assessment_data)) {
            // Vamos a unir los datos de los COMPAÑEROS
            // $peer_data = $peer_assessment_data[0];
            $peer_data = $peer_assessment_data;
            // var_dump("<br>peer_data=".json_encode($peer_data)."<br>count(P_D)=".count($peer_data));
            // exit(0);
            for($i=0; $i<count($peer_data); $i++) {
                $peer_data_sprint = array_values($peer_data[$i]);
                // var_dump("<br>peer_data_sprint=".json_encode($peer_data_sprint)."<br>count(tmp)=".count($peer_data_sprint));
                // Si no está calificado el sprint ponemos un array vacío.
                if(count($peer_data_sprint)==0) {
                    $result_peer[] = array();
                    continue;
                }
                // $peer_data_final[] = array_values($tmp);
                
                // $peer_data_final[] = $peer_data[$i];
                // var_dump("<br>peer_data_final=".json_encode($peer_data_final));
                // var_dump("<br>peer_data_final[0]=".json_encode($peer_data_final[0]));
                // var_dump("<br>peer_data_final[0][0]=".json_encode(array_values($peer_data_final[0])));
                $peer_data_values = array();
                foreach($peer_data_sprint as $item)
                    $peer_data_values[] = array_values($item)[0];

                // var_dump("<br>peer_values=".json_encode($peer_data_values));

                $result_peer_sprint = array();
                for($j=0; $j<$rubric_cols; $j++) {
                    $row_data = 0;
                    for($k=0; $k<count($peer_data_values); $k++) {
                        // $row_data += floatval(array_values($peer_data_final[$k])[$j]);
                        $row_data += floatval($peer_data_values[$k][$j]);
                    }
                    $result_peer_sprint[$j] = number_format(floatval($row_data)/count($peer_data_values), 2, '.', ',');
                }
                $result_peer[] = $result_peer_sprint;
            }
        }
       
        // Calculamos las notas de los profesores.
        $result_teacher = array();
        // var_dump("<br>rubric_cols=".$rubric_cols."<br");
        if(!empty($teacher_assessment_data)) {

            // Cada item tendrá las notas de todos los profesores de un sprint.
            $result_teacher = array();

            foreach($teacher_assessment_data as $item) {
                // var_dump("<br>teacher_data->item=".json_encode($item));
                // var_dump("<br>sizeof=".sizeof($item));

                if(!empty($item)) {
                    // print_r("<br>item=<br>");
                    // print_r($item);
                    // $result_teacher[] = $item[0];
                    // Creamos un array vacio para ir llenándolo con las notas de los profesores.
                    $sumArray = array_fill(0, $rubric_cols, "0");
                    // var_dump("sumArray=".json_encode($sumArray));
                    // exit(0);

                    for($i=0; $i<sizeof($item); $i++) {
                        // $tmp_result_teacher[] = $item[$i];
                        for($j=0; $j<$rubric_cols; $j++) {
                            $sumArray[$j] += $item[$i][$j];
                        }
                        // var_dump("<br>sumArray[item]=".json_encode($sumArray));
                    }
                    // Hacemos la media aritmética de los valores en función de la cantidad de profesores que ha valorado
                    for($j=0; $j<$rubric_cols; $j++) {
                        $sumArray[$j] = $sumArray[$j]/sizeof($item);
                    }
                    $result_teacher[] = $sumArray;
                    // var_dump("<br>result_teacher=".json_encode($result_teacher));
                    // var_dump("<br>sumArray=".json_encode($sumArray));

                } else {
                    $result_teacher[] = array();
                }
            }
        } else  {
            // Si está vacío 
        }
        // Tenemos los resultados de los PROFESORES -> $result_teacher
        // var_dump("<br>result_teacher=".json_encode($result_teacher));
        // print_r($result_teacher);
        // exit(0);

        // Si no tiene ninguna nota, es que no se ha realizado todavía ninguna valoración.
        if (empty($result_self) && empty($result_peer) && empty($result_teacher)) {
            // var_dump("No tiene NINGUNA CALIFICACIÓN");
            return array();
        }
        
        // Si tiene alguna valoración, calculamos la calificación.
        // Vamos a calcular la calificación final.
        $final_grades_persprint = array();
        for($sprint=0; $sprint<count($result_self); $sprint++) {
            $final_grades = array(0,0,0,0);
            // var_dump("<br>self=".json_encode($result_self[$sprint]).", peer=".json_encode($result_peer[$sprint]).", teacher=".json_encode($result_teacher[$sprint])."<br>");
            for($i=0; $i<$rubric_cols; $i++) {
                $s_value = empty($result_self[$sprint][$i]) ? floatval('0') : number_format(floatval($result_self[$sprint][$i])*0.2, 2, '.', ',');
                $p_value = empty($result_peer[$sprint][$i]) ? floatval('0') : number_format(floatval($result_peer[$sprint][$i])*0.2, 2, '.', ','); 
                if (!empty($result_teacher))
                    $t_value = empty($result_teacher[$sprint][$i]) ? floatval('0') : number_format(floatval($result_teacher[$sprint][$i])*0.6, 2, '.', ','); 
                else
                    $t_value = floatval('0');
                // var_dump("<br>s_value=".$result_self[$sprint][$i].", p_val=".$result_peer[$sprint][$i].", t_val=".$result_teacher[$sprint][$i]);

                // Hacemos que el valor numérico se guarde como cadena
                //$final_grades[$i] = strval($s_value + $p_value + $t_value);
                // $final_grades[$i] = $s_value + $p_value + $t_value;
                $final_grades[$i] = number_format($s_value + $p_value + $t_value, 2, '.', ',');
                
                // var_dump("<br>final_grades[".$i."]=".$final_grades[$i]);
            }    
            $sprint_number = 'sprint'.(string)($sprint+1);
            $all_data[$sprint_number] = $final_grades;
            // var_dump("<br>final_grades=".json_encode($final_grades));
            // exit(0);
            // $final_grades_persprint[] = $final_grades;
        }
        $final_grades_persprint = array();
        for($j=0; $j<$rubric_cols;$j++) {
            $value = 0;
            for($i=0; $i<count($result_self); $i++){
                $sprint_number = 'sprint'.(string)($i+1);
                $value += $all_data[$sprint_number][$j];
            }
            // var_dump("<br>value=".$value."<br>result_self=".json_encode(count($result_self)));
            if(count($result_self) == 0) {
                $value = number_format($value, 2, '.', ',');
            } else {
                $value = number_format($value/count($result_self), 2, '.', ',');
            }
            // var_dump("<br>value=".$value);
            $final_grades_persprint[] = $value;
        }
        // $final_grades_persprint = array();
        // for($j=0; $j<$rubric_cols;$j++) {
        //     $value = 0;
        //     for($i=0; $i<count($result_self); $i++){
        //         // Solo lo contamos si el profesor realizó la evaluación
        //         if ($teacher_did_assessment[$i]) {
        //               $sprint_number = 'sprint'.(string)($i+1);
        //             $value += $all_data[$sprint_number][$j];
        //         }
        //     }
        //     // var_dump("<br>value=".$value."<br>result_self=".json_encode(count($result_self)));
        //     // if(count($result_self) == 0) {
        //     if($count_teacher_did_assessment === 0) {
        //         $value = number_format($value, 2, '.', ',');
        //     } else {
        //         // $value = number_format($value/count($result_self), 2, '.', ',');
        //         $value = number_format($value/$count_teacher_did_assessment, 2, '.', ',');
        //     }
        //     // var_dump("<br>value=".$value);
        //     $final_grades_persprint[] = $value;
        // }
        // return $final_grades;
        $all_data['final_grades'] = $final_grades_persprint;
        // var_dump("<br>all_data=".json_encode($all_data)."<br>");
        // exit(0);

        return $all_data;
        
    }

    /**
     * Función para obtener y calcular las notas/calificaciones de un estudiante en
     * aquellos Sprints que algún profesor haya evaludo.
    */
    public function getStudentPartialGrades($challenge_id, $student_id = '', $sprints='', $teacher_did_assessment) {
        // var_dump($challenge_id,$sprints,$student_id);
        // print_r("<br>");
        // Si no hay sprint, buscamos en todos los sprints
        if(empty($sprints)) {
            // var_dump("No hay sprint definido.");
            return array();
        }
        // Obtenemos el identificador del equipo del estudiante.
        // $s_team = $this->students_model->getStudentTeamForGrading($student_id);
        // $s_team = $this->students_model->getStudentTeam($challenge_id, $student_id);
        // var_dump("<br>s_team=".$s_team.", student_id=".$student_id);
        // exit(0);

        $self_data = array();
        $peer_assessment_data = array();
        $teacher_assessment_data = array();
        $count_teacher_did_assessment = 0;

        for($i=0; $i<intval($sprints); $i++) {

            // Si el profesor no ha valorado ese sprint, lo saltamos
            if($teacher_did_assessment[$i] === false) {
                // var_dump("<br>Ningún profesor ha valorado el sprint ".($i));
                $self_data[] = array();
                $peer_assessment_data[] = array();
                $teacher_assessment_data[] = array();
                continue;
            }
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($i+1) /* # sprint */, $student_id);
            // print_r($outcome_id."<br>");
            // exit(0);
            // Si no hay resultados porqué no se han evaluado todavía los sprints, saltamos al siguiente
            if (is_null($outcome_id))
                continue;

            $this->db->select('self_assessment_data, peer_assessment_data, teacher_assessment_data');
            $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcome_id))->row_array();
            // var_dump("<br>Sql->all_data=".json_encode($sql));
            // print_r($sql);
            // exit(0);


            $hay_valores = false;
            // Comprobar si no hay resultados
            foreach ($sql as $key => $value) {
                if (!empty($value)) {
                   $hay_valores = true;
                }
            }


            // if(empty($sql)){
            if(!$hay_valores) {
                // print_r("<br>no da resultados!!");
            } else {
               
                // var_dump($array_values_de_self_data);
                // exit(0);
                if(!empty($sql['self_assessment_data']) && ($sql['self_assessment_data']!== 'null')) {
                    // var_dump("<br>".$sql['self_assessment_data']);
                    // $var_self_data = $sql['self_assessment_data'];
                    // if ($var_self_data !== 'null')

                    // if($sql['self_assessment_data'] != NULL)
                    // $self_data[] = array_values(json_decode($sql['self_assessment_data'], true))[0];
                    $self_data[] = array_values(json_decode($sql['self_assessment_data'], true))[0];
                }
                else 
                    $self_data[] = array();

                // Comprobamos que tenga evaluaciones de los compañeros
                if(!empty($sql['peer_assessment_data']) && ($sql['peer_assessment_data']!== 'null'))
                    $peer_assessment_data[] = array_values(json_decode($sql['peer_assessment_data'], true));
                else 
                    $peer_assessment_data[] = array();

                // Comprobamos que tenga evaluaciones de los PROFESORES
                // print_r($sql);
                if(!empty($sql['teacher_assessment_data']) && ($sql['teacher_assessment_data']!== 'null'))
                    $teacher_assessment_data[] = array_values(json_decode($sql['teacher_assessment_data'], true));
                else 
                    $teacher_assessment_data[] = array();
            }
            $count_teacher_did_assessment++;
            // exit(0);
        }
        // var_dump("<br>getStudentGrades(self_data)=<br>".json_encode($self_data).",count(self_data)=".count($self_data)."<br>");
        // var_dump("<br>getStudentGrades(peer_data)=<br>".json_encode($peer_assessment_data).",count(peer_data)=".count($peer_assessment_data)."<br>");
        // var_dump("<br>getStudentGrades(teacher_data)=<br>".json_encode($teacher_assessment_data).",count(teacher_data)=".count($teacher_assessment_data)."<br>");
        // exit(0);
        // print_r("<br>num teacher_did_assessment:".$count_teacher_did_assessment."<br>");
        // Tenemos los resultados del PROPIO ALUMNO -> $self_data
        if(!empty($self_data)){
            // $result_self = $self_data[0];
            // Tendremos los resultados de todos los sprints
            $result_self = $self_data;
        } else {
            $result_self = array();
        }
        // var_dump("<br>result_self=".json_encode($result_self));
        // exit(0);

        // Buscamos cuantos valores tiene la rúbrica, es decir, cuantas columnas
        $rubric_cols = $this->rubrics_model->getRubricRows($this->challenges_model->getChallengeRubric($challenge_id));

        $result_peer = array();
        if(!empty($peer_assessment_data)) {
            // Vamos a unir los datos de los COMPAÑEROS
            // $peer_data = $peer_assessment_data[0];
            $peer_data = $peer_assessment_data;
            // var_dump("<br>peer_data=".json_encode($peer_data)."<br>count(P_D)=".count($peer_data));
            // exit(0);
            for($i=0; $i<count($peer_data); $i++) {
                $peer_data_sprint = array_values($peer_data[$i]);
                // var_dump("<br>peer_data_sprint=".json_encode($peer_data_sprint)."<br>count(tmp)=".count($peer_data_sprint));
                // Si no está calificado el sprint ponemos un array vacío.
                if(count($peer_data_sprint)==0) {
                    $result_peer[] = array();
                    break;
                }
                // $peer_data_final[] = array_values($tmp);
                
                // $peer_data_final[] = $peer_data[$i];
                // var_dump("<br>peer_data_final=".json_encode($peer_data_final));
                // var_dump("<br>peer_data_final[0]=".json_encode($peer_data_final[0]));
                // var_dump("<br>peer_data_final[0][0]=".json_encode(array_values($peer_data_final[0])));
                $peer_data_values = array();
                foreach($peer_data_sprint as $item)
                    $peer_data_values[] = array_values($item)[0];

                // var_dump("<br>peer_values=".json_encode($peer_data_values));

                $result_peer_sprint = array();
                for($j=0; $j<$rubric_cols; $j++) {
                    $row_data = 0;
                    for($k=0; $k<count($peer_data_values); $k++) {
                        // $row_data += floatval(array_values($peer_data_final[$k])[$j]);
                        $row_data += floatval($peer_data_values[$k][$j]);
                    }
                    $result_peer_sprint[$j] = number_format(floatval($row_data)/count($peer_data_values), 2, '.', ',');
                }
                $result_peer[] = $result_peer_sprint;
            }
        }
        // Tenemos los resultados de los COMPAÑEROS -> $result_peer
        
        // Calculamos las notas de los profesores.
        $result_teacher = array();
        if(!empty($teacher_assessment_data)) {

            // Cada item tendrá las notas de todos los profesores de un sprint.
            $result_teacher = array();

            foreach($teacher_assessment_data as $item) {
                // var_dump("<br>teacher_data->item=".json_encode($item));
                // var_dump("<br>sizeof=".sizeof($item));

                if(!empty($item)) {
                    // $result_teacher[] = $item[0];
                    // Creamos un array vacio para ir llenándolo con las notas de los profesores.
                    $sumArray = array_fill(0, $rubric_cols, "0");
                    // var_dump("sumArray=".json_encode($sumArray));
                    // exit(0);

                    for($i=0; $i<sizeof($item); $i++) {
                        // $tmp_result_teacher[] = $item[$i];
                        for($j=0; $j<$rubric_cols; $j++) {
                            $sumArray[$j] += $item[$i][$j];
                        }
                        // var_dump("<br>sumArray[item]=".json_encode($sumArray));
                    }
                    // Hacemos la media aritmética de los valores en función de la cantidad de profesores que ha valorado
                    for($j=0; $j<$rubric_cols; $j++) {
                        $sumArray[$j] = $sumArray[$j]/sizeof($item);
                    }
                    $result_teacher[] = $sumArray;
                    // var_dump("<br>result_teacher=".json_encode($result_teacher));
                    // var_dump("<br>sumArray=".json_encode($sumArray));

                } else {
                    $result_teacher[] = array();
                }
            }
        } else  {
            // Si está vacío 
        }
        // Tenemos los resultados de los PROFESORES -> $result_teacher
        // var_dump("<br>result_teacher=".json_encode($result_teacher));
        // print_r($result_teacher);
        // exit(0);

        // Si no tiene ninguna nota, es que no se ha realizado todavía ninguna valoración.
        if (empty($result_self) && empty($result_peer) && empty($result_teacher)) {
            // var_dump("No tiene NINGUNA CALIFICACIÓN");
            return array();
        }
        
        // var_dump("Tiene alguna calificación");

        // Si tiene alguna valoración, calculamos la calificación.
        // exit(0);
        // Vamos a calcular la calificación final.
        $final_grades_persprint = array();
        for($sprint=0; $sprint<count($result_self); $sprint++) {
            $final_grades = array(0,0,0,0);
            // var_dump("<br>self=".json_encode($result_self[$sprint]).", peer=".json_encode($result_peer[$sprint]).", teacher=".json_encode($result_teacher[$sprint])."<br>");
            for($i=0; $i<$rubric_cols; $i++) {
                $s_value = empty($result_self[$sprint][$i]) ? floatval('0') : number_format(floatval($result_self[$sprint][$i])*0.2, 2, '.', ',');
                $p_value = empty($result_peer[$sprint][$i]) ? floatval('0') : number_format(floatval($result_peer[$sprint][$i])*0.2, 2, '.', ','); 
                if (!empty($result_teacher))
                    $t_value = empty($result_teacher[$sprint][$i]) ? floatval('0') : number_format(floatval($result_teacher[$sprint][$i])*0.6, 2, '.', ','); 
                else
                    $t_value = floatval('0');
                // var_dump("<br>s_value=".$result_self[$sprint][$i].", p_val=".$result_peer[$sprint][$i].", t_val=".$result_teacher[$sprint][$i]);

                // Hacemos que el valor numérico se guarde como cadena
                //$final_grades[$i] = strval($s_value + $p_value + $t_value);
                // $final_grades[$i] = $s_value + $p_value + $t_value;
                $final_grades[$i] = number_format($s_value + $p_value + $t_value, 2, '.', ',');
                
                // var_dump("<br>final_grades[".$i."]=".$final_grades[$i]);
            }    
            $sprint_number = 'sprint'.(string)($sprint+1);
            $all_data[$sprint_number] = $final_grades;
            // var_dump("<br>final_grades=".json_encode($final_grades));
            // exit(0);
            // $final_grades_persprint[] = $final_grades;
        }
        $final_grades_persprint = array();
        for($j=0; $j<$rubric_cols;$j++) {
            $value = 0;
            for($i=0; $i<count($result_self); $i++){
                // Solo lo contamos si el profesor realizó la evaluación
                if ($teacher_did_assessment[$i]) {
                      $sprint_number = 'sprint'.(string)($i+1);
                    $value += $all_data[$sprint_number][$j];
                }
            }
            // var_dump("<br>value=".$value."<br>result_self=".json_encode(count($result_self)));
            // if(count($result_self) == 0) {
            if($count_teacher_did_assessment === 0) {
                $value = number_format($value, 2, '.', ',');
            } else {
                // $value = number_format($value/count($result_self), 2, '.', ',');
                $value = number_format($value/$count_teacher_did_assessment, 2, '.', ',');
            }
            // var_dump("<br>value=".$value);
            $final_grades_persprint[] = $value;
        }
        // return $final_grades;
        $all_data['final_grades'] = $final_grades_persprint;
        // var_dump("<br>all_data=".json_encode($all_data)."<br><br>");
        // exit(0);

        return $all_data;
        
    }

    /**
     * Función para obtener los comentarios de un estudiante
     */
    public function getStudentComments ($challenge_id, $student_id, $sprints) {
        // Si no hay sprint, buscamos en todos los sprints
        if(empty($sprints)) {
            // var_dump("No hay sprint definido.");
            return array();
        }

        $self_comments = '';
        $peer_comments = '';
        $teacher_comments = '';

        // Buscamos los comentarios en todos los sprints
        for($i=1; $i<=intval($sprints); $i++) {
            $this->db->select('teachers_comments, self_comments, peer_comments');
            $sql = $this->db->get_where('challenge_assessment', array(
                'challenge_id' => $challenge_id,
                'student_id' => $student_id,
                'current_sprint' => $i,
            ))->row_array();
            // var_dump("<br>Sql->all_data=".json_encode($sql));
            if(!empty($sql)){
                // Ponemos el nombre del Sprint al principio de cada comentario
                $sprint = '<mark class="bg-teal-200">Sprint '.(string)($i).":</mark> ";

                // Comprobamos que tenga comentarios propios
                // var_dump("previo=".json_encode(json_decode($sql['self_comments'], true)));
                // $pre_self_comments = implode(array_values(json_decode($sql['self_comments'], true)));
                $pre_self_comments = json_decode($sql['self_comments'], true);
                // var_dump("preselfcomments=".json_encode($pre_self_comments));
                $mi_comentario = is_null($pre_self_comments) ? '' : array_values($pre_self_comments)[0];
                // var_dump("micomentario=".json_encode($mi_comentario));
                // if(empty($mi_comentario) || ($mi_comentario == "")){
                //     var_dump("<br>El comentario: ".$mi_comentario." está VACÍO<br>");
                // }
                // else var_dump("<br>El comentario: ".json_encode($mi_comentario)." NO está vacío<br>");

                // if ($mi_comentario != "") {
                if(!is_null($pre_self_comments) && !empty($mi_comentario) && ($mi_comentario != '')) {
                    // if(!is_null($pre_self_comments) && !empty($pre_self_comments)) {
                    // $self_comments .= implode(array_values($pre_self_comments));
                    // $self_comments .= $sprint.implode(array_values($pre_self_comments))."\n";
                    // $self_comments .= $sprint.implode($mi_comentario)."\n";
                    $self_comments .= $sprint.$mi_comentario."\n";

                }
                // var_dump("self_comments=".$self_comments);

                // Comprobamos que tenga comentarios de los compañeros
                if(!empty($sql['peer_comments'])) {
                    $peer_c = json_decode($sql['peer_comments'], true);
                    // var_dump("count(peer_comments)=".count($peer_c));
                    foreach($peer_c as $item) {
                        // Guardamos todos los comentarios, no importa quién lo ha hecho.
                        // $peer_comments[] = array_values($item)[0];
                        // $comentario = array_values($item)[0];
                        $comentario = is_null($item) ? '' : array_values($item)[0];

                        // var_dump($comentario[0]);
                        // if(!is_null($comentario[0]) && !empty($comentario[0])) {
                        if(!empty($comentario) && ($comentario != '')) {
                            // $peer_comments .= $sprint.implode($comentario);
                            $peer_comments .= $sprint.$comentario."\n";
                            // $peer_comments .= "\n";
                        }
                    }
                    // $peer_comments = implode($peer_c);
                    // var_dump("peer_comments=".json_encode($peer_comments));
                }
                // else 
                //     $peer_comments[] = array();

                // Comprobamos que tenga comentarios de los PROFESORES
                if(!empty($sql['teachers_comments'])) {
                    // $teacher_comments = $sprint;
                    $t_c = json_decode($sql['teachers_comments'], true);
                    // var_dump("t_c=".json_encode($t_c));
                    foreach($t_c as $item) {
                        // var_dump("item=".json_encode($item));
                        // $teacher_comments[] = $item;

                        // $comentario = array_values($item)[0];
                        $comentario = is_null($item) ? '' : $item;

                        // var_dump($comentario[0]);
                        // if(!is_null($comentario[0]) && !empty($comentario[0])) {
                        if(!empty($comentario) && ($comentario != '')) {
                            // $teacher_comments .= $sprint.implode($comentario);
                            $teacher_comments .= $sprint.$comentario."\n";
                            // $teacher_comments .= "\n";
                        }

                        // $teacher_comments .= $sprint.$item;
                        // $teacher_comments .= "\n";
                    }
                    // $teacher_comments[] = array_values(json_decode($sql['teachers_comments'], true))[0];
                }
                // else 
                //     $teacher_comments[] = array();

               
            }
            // exit(0);
        }
        
        $my_comments = array(
            'self_comments' => $self_comments,
            'peer_comments' => $peer_comments,
            'teacher_comments' => $teacher_comments,
        );
        // var_dump("<br>my_comments=".json_encode($my_comments));
        // exit(0);
        return $my_comments;

    }

   

    /**
     * Función para eliminar las Evaluaciones asociadas a un reto
     */
    public function deleteChallenge($challenge_id) { 
        // Obtener los OUTCOMES asociados al reto
        $outcomes_ids = $this->challenges_outcomes_model->getChallengeOutcomesId($challenge_id);
        // var_dump("outcomes_ids= ".json_encode($outcomes_ids));
        // exit(0);

        // Borramos las entradas en la tabla 'challenge_assessment'
        $this->db->trans_start();
        $this->db->delete('challenge_assessment', array('challenge_id' => $challenge_id));
        // $this->db->trans_complete();
        // Borramos las notas de 'challenge_learning_outcomes' asociadas al reto
        // $this->db->trans_start();
        for($i=0; $i<count($outcomes_ids); $i++) {
            $this->db->delete('challenge_learning_outcomes', array('id' => $outcomes_ids[$i]));
        }          
        $this->db->trans_complete();

        return !$this->db->trans_status() ? false : true;
    }
    
    
    /*
     * Fetch members data from the database
     * @param array filter data based on the passed parameters
     */
    function getRows($params = array()){

        // $params = array ( 
        //     'where' => array('role' => 'teacher'),
        // );

        $this->table = 'challenges';
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("where", $params)){
            foreach($params['where'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'desc');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }
    
    /**
     * Función para paginar los datos
     */
    public function get_Paginate($teacher_id,$limit,$offset) {
        if (!empty($teacher_id)) {

            $this->db->select('challenge_id');
            $sql = $this->db->get_where('challenges_teachers_enrolment', array('teacher_id' => $teacher_id));
            $challenges = $sql->result_array();
            $challenges_result = array();
            // Si no está en ninún reto, no se devuelven.
            if(count($challenges)==0)
                return array();

            foreach($challenges as $item)
                $challenges_result[] = $item['challenge_id'];
 
            $sql = $this->db->order_by('name', 'ASC')
                            ->from('challenges')
                            ->where_in('id', $challenges_result)
                            ->get();

            // var_dump($sql->result());
            // exit(0);
            return $sql->result();

        } else {
            $sql = $this->db->order_by('name', 'ASC')->get('challenges', $limit, $offset);
            return $sql->result();
        }
        
    }


}