<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technical_skills_assessments_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        // $this->load->model('teachers_model','teachers');
        // $this->load->model('students_model','students');
        // $this->load->model('challenges_model');
        // $this->load->model('challenges_outcomes_model');
    }

    /**
     * Método para inicializar las Evaluaciones de los RAs en la BBDD
     */
    // public function start($challenge_id, $challenge, $students, $teachers, $technical_skills) {
    public function start($challenge_id, $students, $teachers, $technical_skills) {
        // var_dump("start(teachers)=".json_encode($teachers));
        // exit(0);
        foreach($teachers as $t) {
            $teachers_ids_in[] = $t['id'];
            // var_dump("<br>t[0]['id']=".$t['id']);
            // "teachers=["4","6"]"
        }
        // var_dump("<br>t_ids_in=".json_encode($teachers_ids_in));
        // exit(0);

        // Profesores que pueden valorar .
        $teachers_assessing = json_encode($teachers_ids_in);
        // print_r($teachers_assessing);
        // exit(0);

        // Para cada RA creamos tantos registros como alumnos hay asignados al RETO
        foreach ($technical_skills as $tskill) {
            // var_dump("start()<br>");
            // print_r($tskill);
            // exit(0);
            // Creamos un nuevo registro para cada alumno.
            foreach($students as $std) {
    //             var_dump("student_id=".$std[0]->id);
                
                $data = array (
                    // 'technical_skills_id' => $tskill['id'],
                    'technical_skills_id' => $tskill,
                    'challenge_id' => $challenge_id,
                    'student_id' => $std['id'],
                    'teachers_id' => $teachers_assessing,
                );
    
                // var_dump("<br>data=".json_encode($data));
    //             exit(0);
                if (!$this->db->insert('technical_skills_assessment', $data)) {
                // Si hay error, devolvemos falso
                    return false;
                } else {
                    // Si la inserción ha sido exitosa
                    // Devolvemos el id de la fila insertada.
                    $assessment_id[] = $this->db->insert_id();
                }
            }
        }

        
        // print_r("<br>Assessments CREADOS".json_encode($assessment_id));
        // exit(0);
        return $assessment_id;

    }

    /**
     * Método para almacenar la Evaluación hecha por un profe, para un Alumno
     */
    public function saveTeacherMarkAssessment($technical_skill_id, $challenge_id, $student_id, $teacher_id, $teacher_assessment_data, $teachers_comments){
        // var_dump("<br>TS_id=".$technical_skill_id."     ---      CH_id= ".$challenge_id."       ---      Student_id= ".$student_id);
        // exit(0);
        $data = array();

        $this->db->select('final_mark,teachers_comments');
        $previous_data = $this->db->get_where('technical_skills_assessment', array(
            'technical_skills_id' => $technical_skill_id,
            'challenge_id' => $challenge_id,
            'student_id' => $student_id
        ))->row_array();

        // var_dump("<br>previous_data=".json_encode($previous_data));
        // exit(0);

        // if(is_null($previous_data['final_mark'])){
        //     print_r("final mark ES NULL");
        // } else {
        //     print_r("final mark NOOOOO ES NULL");

        // }
        // exit(0);


        if($previous_data !== null && !is_null($previous_data['final_mark'])) {
            // Si no está vacía la Calificación hay que comprobar si es la del profesor o la de otro profesor.
            if (!empty($previous_data['final_mark'])) {
                $prev_data_mark = json_decode($previous_data['final_mark'], TRUE);
                // var_dump("<br>prev_data_mark=".json_encode($prev_data_mark)."<br><br>");
                if(in_array($teacher_id, array_keys($prev_data_mark))) {
                    // print_r("<br>Si que hay nota de este profesor");
                    // Si está la nota de este profesor, la sustituimos por la nueva
                    if(!empty($teacher_assessment_data)){
                        // $prev_data_mark = array_replace($prev_data_mark, $teacher_assessment_data);
                        $data['final_mark'] = json_encode(array_replace($prev_data_mark, $teacher_assessment_data));
                    }
                    else {
                        $data['final_mark'] = json_encode($prev_data_mark);
                    }
                } else {
                    // print_r("<br>NO hay nota de este profesor");
                    // $data['final_mark'] = json_encode($prev_data_mark) + json_encode($teacher_assessment_data);
                    // var_dump("<br>data del profe: ".json_encode($teacher_assessment_data));
                    // var_dump("<br>datos previos: ".json_encode($prev_data_mark));
                    // $data['final_mark'] = json_encode($teacher_assessment_data + json_decode($prev_data_mark, TRUE));
                    $data['final_mark'] = json_encode($teacher_assessment_data + $prev_data_mark);

                }
            }

        } else {
            // Si no hay ninguna nota puesta todavía
            // print_r("final_mark -> ".json_encode($teacher_assessment_data));
            // exit(0);
            // $data['final_mark'] = json_encode($teacher_assessment_data);
            if(!empty($teacher_assessment_data))
                $data['final_mark'] = json_encode($teacher_assessment_data);
            else 
                $data['final_mark'] = json_encode(array($teacher_id => ''));

        }
        // exit(0);


        // Actualizamos los comentarios del profesor
        if($previous_data !== null && !is_null($previous_data['teachers_comments'])){
            // Si no están vacíos los comentarios si son del Profesor o de otro profesor.
            if (!empty($previous_data['teachers_comments'])) {
                $prev_data_comments = json_decode($previous_data['teachers_comments'], TRUE);
                // print_r("<br>prev_data_mark=".json_encode($prev_data_mark)."<br><br>");
                if(in_array($teacher_id, array_keys($prev_data_comments))) {
                    // print_r("<br>Si que hay nota de este profesor");
                    // Si está la nota de este profesor, la sustituimos por la nueva
                    // if(!empty($teachers_comments))
                    //     $data['teachers_comments'] = json_encode($teachers_comments);
                    // else 
                    //     $data['teachers_comments'] = json_encode($prev_data_comments);
                    if(!empty($teachers_comments)){
                        // $prev_data_comments = array_replace($prev_data_comments, $teachers_comments);
                        // $cesta = array_replace($base, $reemplazos, $reemplazos2);
                        $data['teachers_comments'] = json_encode(array_replace($prev_data_comments, $teachers_comments));
                    }
                    else {
                        $data['teachers_comments'] = json_encode($prev_data_comments);
                    }
                } else {
                    // print_r("<br>NO hay nota de este profesor");
                    $data['teachers_comments'] = json_encode($teachers_comments + $prev_data_comments);
                    // $data['teachers_comments'] = json_encode($teachers_comments) + json_decode($prev_data_comments, TRUE);
                    // $data = array ('teachers_comments' => json_encode($teachers_comments + json_decode($stored_teachers_commments, true)));

                }
            }

        } else {
            // Si es null
            if(!empty($teachers_comments))
                $data['teachers_comments'] = json_encode($teachers_comments);
            else 
                $data['teachers_comments'] = json_encode(array($teacher_id => ''));
        }
        // if(is_null($previous_data['teachers_comments']) && !empty($previous_data['teachers_comments'])) {
        //     $data['teachers_comments'] = json_encode($teachers_comments + json_decode($previous_data['teachers_comments'], true));
        // } else {
        //     // Si no hay ningún comentario puesto todavía.
        //     $data['teachers_comments'] = json_encode($teachers_comments);
        // }

        // var_dump("<br><br>DATA antes de guardar= <br>".json_encode($data));
        // exit(0);
        if($previous_data !== null) {
            //Iniciar transacción para poder hacer rollback
            $this->db->trans_start();
            $this->db->where( array(
                'technical_skills_id' => $technical_skill_id,
                'challenge_id' => $challenge_id,
                'student_id' => $student_id));
            $this->db->update('technical_skills_assessment', $data);
            $this->db->trans_complete();

            $status = $this->db->trans_status();
            if (!$status) {
                // Borramos los datos que acabamos de meter
                // $this->challenges_outcomes_model->deleteTeacherOutcomes($outcome_id, $teacher_assessment_data);
                return false;
            }
            return true;
        } else {
            $data_to_insert = array (
                // 'technical_skills_id' => $tskill['id'],
                'technical_skills_id' => $technical_skill_id,
                'challenge_id' => $challenge_id,
                'student_id' => $student_id,
                'final_mark' => $data['final_mark'],
                'teachers_comments' => $data['teachers_comments'],
            );

            // var_dump("<br>data=".json_encode($data));
//             exit(0);
            if (!$this->db->insert('technical_skills_assessment', $data_to_insert)) {
            // Si hay error, devolvemos falso
                return false;
            } else {
                // Si la inserción ha sido exitosa
                // Devolvemos el id de la fila insertada.
                // $assessment_id[] = $this->db->insert_id();
                return true;
            }
        }

    }

    /**
     * Función para actualizar los datos de una EVALUACIÓN
     */
    public function update($id, $data) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('challenge_assessment', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para obtener los datos de un RA
     */
    public function getTechnicalSkills ($challenge_id, $student_id) {
        // var_dump("<br>getTechnicalSkills:<br>challenge_id=".$challenge_id."<br>student_id=".$student_id);
        if(!empty($student_id))
            $sql = $this->db->get_where('technical_skills_assessment', array(
                'challenge_id' => $challenge_id,
                'student_id' => $student_id,
            ))->result_array(); 
        // else
        //     $sql = $this->db->get_where('technical_skills_assessment', array('challenge_id' => $challenge_id))->result_array();
        // print_r("<br>getTechnicalSkills=".json_encode($sql));
        // exit(0);
        return $sql;
    }

    /**
     * Función para obtener todos los identificadores de los RAs asociados a un RETO
     */
    public function getTechnicalSkillsIdsOfChallenge($challenge_id) {
        $this->db->select('technical_skills_id');
        $sql = $this->db->get_where('technical_skills_assessment', array(
            'challenge_id' => $challenge_id,
        ))->result_array(); 

        if(!empty($sql)) {
            // Only keep unique values, by using array_unique with SORT_REGULAR as flag.
            // We're using array_values here, to only retrieve the values and not the keys.
            // This way json_encode will give us a nicely formatted JSON string later on.
            $sql = array_values( array_unique( $sql, SORT_REGULAR ) );
            // No queremos los ids de los RAs duplicados.
            // print_r("<br>sql (array_unique)=".json_encode($sql));

            foreach($sql as $item) {
                // print_r("item=".json_encode($item));
                // Nos quedamos sólo con los valores de los identificadores.
                $sql2[] = array_values($item)[0];
            }

            $sql = $sql2;
        }
        
        // print_r("<br>getTechnicalSkillsNamesOfChallenge=".json_encode($sql));
        // exit(0);
        return $sql;

    }

    /**
     * Función para obtener las Notas Numéricas de un RA.
     */

    public function getFinalMark($technical_skills_id, $challenge_id, $student_id) {
        // print_r("<br>ts_id=".$technical_skills_id.", ch_id=".$challenge_id.", st_id=".$student_id);
        $this->db->select('final_mark');
        $sql = $this->db->get_where('technical_skills_assessment', array(
            'technical_skills_id' => $technical_skills_id,
            'challenge_id' => $challenge_id,
            'student_id' => $student_id
        ))->row_array();
        // print_r("<br>sql(final_mark)=".$sql['final_mark']);
        // exit(0);
        if ($sql !== null)
        // if(array_key_exists('final_mark',$sql))
            return $sql['final_mark'];
        else 
            return '';
    }
    
   
    /**
     * Función para obtener los comentarios de los profesores a un RA.
     */

    public function getTeachersComments($technical_skills_id, $challenge_id, $student_id) {
        // print_r("<br>ts_id=".$technical_skills_id.", ch_id=".$challenge_id.", st_id=".$student_id);
        $this->db->select('teachers_comments');
        $sql = $this->db->get_where('technical_skills_assessment', array(
            'technical_skills_id' => $technical_skills_id,
            'challenge_id' => $challenge_id,
            'student_id' => $student_id
        ))->row_array();
        // print_r("<br>sql(teachers_comments)=".$sql['teachers_comments']);
        // return $sql['teachers_comments'];
        if ($sql !== null)
        // if(array_key_exists('final_mark',$sql))
            return $sql['teachers_comments'];
        else 
            return '';
    }
    

    /**
     * Función para saber si un identificador de RETO está en la BBDD
     * @challenge_id Identificador del reto
     * @technical_skills Los RAs a evaluar
     */
    public function isTechnicalSkillInAssessment($challenge_id, $technical_skills) {
        // var_dump("isTechnicalSkillInAssessment(TS)=".json_encode($technical_skills));
        // exit(0);
        $hasAssessments = false;
        
        // $this->db->select('challenge_id');
        // $sql = $this->db->get_where('technical_skills_assessment', array(
        //     'challenge_id' => $challenge_id,
        // ));

        $sql = $this->db->select('*')->from('technical_skills_assessment')
                    ->group_start()
                            ->where_in('technical_skills_id', $technical_skills)
                            ->where('challenge_id', $challenge_id)
                    ->group_end()                
                    ->get();
        
        // var_dump($sql->result());
        // exit(0);
        if (!empty($sql->result()))
            $hasAssessments = true;
       
        return $hasAssessments;
        
    }

    /** 
     * Funnción para devolver los datos de la evaluación en un RA de un profesor en un reto a un alumno.
     */
    // public function getTeachersSprintAssessData($challenge_id, $current_sprint, $teacher_id, $students, $s_team) {
    public function getTeacherAssessData($technical_skill_id, $challenge_id, $student_id, $has_rubric) {
        
        // Si No es una rúbrica, obtenemos la nota numérica
        if(!$has_rubric) {
            $this->db->select('final_mark');
        } else 
            $this->db->select('rubric_mark');

        $sql = $this->db->get_where('technical_skills_assessment', array(
            'technical_skills_id' => $technical_skill_id,
            'challenge_id' => $challenge_id,
            'student_id' => $student_id,
        ))->row_array();

        // print_r("sql(marks) = <br>".json_encode($sql));
        // exit(0);
        return $sql;

//         // Obtenemos los identificadores de las posibles evaluaciones
//         $this->db->select('learning_outcomes_id');
// //             var_dump("array=".json_encode($array));
//         $sql = $this->db->get_where('challenge_assessment', array(
//             'challenge_id' => $challenge_id,
//             'current_sprint' => $current_sprint,
//             'student_id' => $students[$i]['id'],
//         ))->row_array();
//         $outcomes_id = $sql['learning_outcomes_id'];
// //             var_dump("outcomes_id=".$outcomes_id);
//         // buscamos las valoraciones del profesor.
//         $this->db->select('teacher_assessment_data');
//         $sql = $this->db->get_where('challenge_learning_outcomes', array('id' => $outcomes_id))->row_array();
//         $outcomes_data = json_decode($sql['teacher_assessment_data'],true);
//         // var_dump("<br>teacher_id=".$teacher_id.", outcomes_data=".json_encode($outcomes_data));
//         // var_dump("<br>outcomes_data(keys)=".json_encode(array_keys($outcomes_data)));
// //             var_dump("<br>out_=".json_encode($outcomes_data[$teacher_id]));
//         // exit(0);
//         if(!empty($outcomes_data) && array_key_exists($teacher_id, $outcomes_data)) {
//             $result[] = array(
//                 $students[$i]['id'] => $outcomes_data[$teacher_id]
//             );
//         }
            
        
// //         var_dump("result=".json_encode($result));
// //         exit(0);
//         return $result;
    }

    /**
     * Función para devolver los comentarios de un profesor en un Sprint determinado.
     */
    public function getTeacherCommentsData($technical_skill_id, $challenge_id, $student_id) {

        $this->db->select('teachers_comments');
        $sql = $this->db->get_where('technical_skills_assessment', array(
            'technical_skills_id' => $technical_skill_id,
            'challenge_id' => $challenge_id,
            'student_id' => $student_id,
        ))->row_array();

        // print_r("sql(comments) = <br>".json_encode($sql));
        // exit(0);
        return $sql;
//         $result = array();
//         // Obtenemos los identificadores de las posibles evaluaciones
//         for($i=0; $i<count($students); $i++) {
//             // Buscamos los comentarios de profesores.
//             $this->db->select('teachers_comments');
//             $sql = $this->db->get_where('challenge_assessment', array(
//                 'challenge_id' => $challenge_id,
//                 'current_sprint' => $current_sprint,
//                 'student_id' => $students[$i]['id'],
//             ))->row_array();
//             $comments_data = json_decode($sql['teachers_comments'], true);
//             // var_dump("comements_data=".json_encode($comments_data));
//             if(!empty($comments_data) && array_key_exists($teacher_id, $comments_data)) {
//                 $result[] = array(
//                     $students[$i]['id'] => $comments_data[$teacher_id]
//                 );
//             }
            
//         }
// //         var_dump("result=".json_encode($result));
// //         exit(0);
//         return $result;
    }

    /** 
     * Función para eliminar las evaluaciones de los RAs asociadas a un RETO
     */
    public function deleteTechnicalSkillsInChallenge($technical_skill_id, $challenge_id) {
        // var_dump("<br>ts=".$technical_skill_id.", ch_id=".$challenge_id);
        // exit(0);
        // Borramos las entradas en la tabla 'technical_skills_assessments' asociadas al RA y al RETO
        $this->db->trans_start();
        $this->db->where('challenge_id', $challenge_id)
                    ->where('technical_skills_id', $technical_skill_id);
        $this->db->delete('technical_skills_assessment');    
        $this->db->trans_complete();

        return !$this->db->trans_status() ? false : true;
    }
    
    /** 
     * Función para eliminar las evaluaciones de un RA
     */
    public function delete($technical_skill_id) {
        // Borramos las entradas en la tabla 'technical_skills_assessments' asociadas al RA
        $this->db->trans_start();
        $this->db->delete('technical_skills_assessment', array('technical_skills_id' => $technical_skill_id));    
        $this->db->trans_complete();

        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para borrar todas las evaluaciones asociadas a un reto.
     */
    public function deleteChallenge ($challenge_id) {
        // var_dump("<br>CH to delete = ".json_encode($challenge_id));

        // 1. Hay que borrar de la columa "challenges_in" el identificador del reto
        $sql = $this->db->select('*')
                            ->from('technical_skills')
                            ->like('challenges_in', $challenge_id)
                            ->get();
        // var_dump("<br>");
        // var_dump($sql->result_array());
        // var_dump("<br>");
        // exit(0);

        $technical_skills = $sql->result_array();

        if (!empty($technical_skills)) {

            // var_dump("<br>");   
            // var_dump($technical_skills);
            // var_dump("<br>");
            foreach($technical_skills as $item) {
                // var_dump($item);
                // exit(0);
                $challenge_id_todelete = $challenge_id.',';
                $tmp_challenges_in = $item['challenges_in'];
                // var_dump("<br> tmp ch_in=".$tmp_challenges_in);
                $new_challenges_in = str_replace($challenge_id_todelete, "", $tmp_challenges_in);
                // var_dump("<br>CH_ID_ToDelete = ".$challenge_id_todelete."<br>NEW TS = ".json_encode($new_challenges_in));

                
                if(!$this->technical_skills_model->update($item['id'], array('challenges_in' => $new_challenges_in))){
                    // return false;
                }

            }
            
        }

        // $technical_skills = $this->getTechnicalSkillsIdsOfChallenge($challenge_id);
        // // var_dump("<br>TS to delete = ".json_encode($technical_skills));
        // // exit(0);
        // if(!empty($technical_skills)) {

        // 2. Borrar todas las asignaciones del reto.
        $this->db->trans_start();
        $this->db->delete('technical_skills_assessment', array('challenge_id' => $challenge_id));    
        $this->db->trans_complete();

        // }
        return true;

        

    }


}