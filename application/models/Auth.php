<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Model {

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Función de login
     */
    public function login($username, $pass){
        // $data = $this->db->get_where('users',array('username' => $user, 'password' => $pass), 1);
       
        // if (!$data->result()) {
        //     return false;
        // } else {
        //     // Devolvemos un solo objeto:
        //     return $data->row();
        // }
        //$this->db->select('password');
        $query = $this->db->get_where('users',array('username' => $username), 1);

        // El usuairo existe en la BBDD.
        if($query->num_rows() == 1) {
            //$query = $this->db->get_where('users',array('username' => $username, 'password' => $pass), 1);
            
            $row = $query->row();
            //var_dump($row->password);

            // Compruebo la contraseña
            if(password_verify($pass, $row->password)) {
                // Es correcto
                $data = array (
                    'tupla' => $row 
                );
                return $data;
                //return $row;
                //return $query;
            }
            // Contraseña incorrecta
            else {
                return array (
                    'error_msg' => 'La contraseña es incorrecta.'
                );
     
            }
        }
        return array ('error_msg' => 'El usuario \''.$username.'\' no existe.');

    }

}
