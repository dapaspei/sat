<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students_model extends CI_Model {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método para crear y almacenar un estudiante en la BBDD
     * @param firstname 
     * @param lastname
     */
    public function create($data) {

        // Extraemos el último elemento donde están los niveles de docencia.
        $levels = array_pop($data);
            
        // var_dump($data);
        // exit(0);
        // return !$this->db->insert('students',$data) ? false: true;
        // $sql = $this->db->insert('students',$data);
        // if(!$student) // No se ha podido crear el estudiante.
        //     return false; 

        //     $technical_skills_id = array (
        //         'technical_skills_id' => $this->db->insert_id(),
        //     );
        $student = array();
        if (!$this->db->insert('students', $data)) {
            // Si hay error, devolvemos falso
            return false;
        } else {
            // Si la inserción ha sido exitosa
            $student_id = $this->db->insert_id();
            // Devolvemos el id de la fila insertada.
            $student = array (
                'student_id' => $student_id,
                'school_year' => '2020-21',
                'level' => json_encode($levels), // Convertir los niveles a datos JSON
            ); 
            // return $technical_skills_id;
            // print_r("<br>Student_id=".json_encode($student));
            // exit(0);

            // Insertamos el registro del curso enrolado.
            if(!$this->db->insert('student_level_enrolled', $student)) {
                // ERROR
                return false;
            } else {
                // Actualizamos el registro del estudiante con el identificador de 
                // 
                return true;
            }

            // return $student;
        }

    }
    /**
     * Función para actualizar los datos de un Estudiante
     */
    public function update($user_id, $data) {
        // var_dump("<br>Los datos son:<br>".json_encode($data));
        // exit(0);
        // ********************************************************************
        // Extraemos el último elemento donde están los niveles de docencia.
        $levels = array_pop($data);
        // Extraemos también el año del curso. 
        $school_year = array_pop($data);
        
        
        // // Convertir los niveles a datos JSON
        // $levels_json[] = json_encode($levels);
        // //$data['teaching_levels'] = '['.$levels_json[0].']';
        // $levels = $levels_json[0];
        // ********************************************************************
        // YA NO HACE FALTA, LOS NIVELES DE DOCENCIA NO SE PONEN EN EL USUARIO

        // Obtenemos el identificador de estudiante, no de usuario.
        $student_id = $this->getStudentId($user_id);
        // Creamos un nuevo registro para enrolar a este alumno al curso.
        $student = array (
            'student_id' => $student_id,
            'school_year' => $school_year,
            'level' => json_encode($levels),
        ); 

        // print_r("Student tiene: <br>".json_encode($student));
        // exit(0);
        $this->db->trans_start();
        // Comprobar que no exista ya
        // if($this->db->limit(1)->get_where('student_level_enrolled', compact('student_id'))->num_rows() === 0 && 
        //     $this->db->limit(1)->get_where('student_level_enrolled', compact('schoo_year'))->num_rows() === 0)

        if($this->db->limit(1)->get_where('student_level_enrolled', array('student_id' => $student_id, 'school_year' => $school_year))->num_rows() === 0 )
        {
            // Insertamos el estudiante aunque ya esté.
            $this->db->insert('student_level_enrolled', $student);
        }
        // if(!$this->db->insert('student_level_enrolled', $student)) {
        //     // ERROR
        //     print_r("error");
        //     exit(0);
        //     return false;
        // }     
        $this->db->trans_complete();

        // Eliminamos el campo 'school_year' de los datos a introducir.
        // print_r($data);
        // print_r("<br><br>");
        // array_pop($data);
        // print_r($data);
        // exit(0);
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('user_id', $user_id);
        $this->db->update('students', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para actualizar los Retos de un Estudiante
     */
    public function updateChallenge($id, $data) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('students', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    
    /**
     * Función para obtener el listado de usuarios en función del nivel
     */
    public function getStudents ($level = array(), $school_year = '') {
        // var_dump("students(level)=".json_encode(implode(", ",$level)));
        // var_dump("<br>level=".json_encode($level));
        // var_dump("<br>count(level)=".count($level));
        // exit(0);
        $levels = implode(", ",$level);

        for($i=0; $i<count($level); $i++) {
            // $like_statement[] = '`teaching_levels` LIKE \'%'.$level[$i].'%\' or ';
            $like_statement[] = '`level` LIKE \'%'.$level[$i].'%\' or ';
        }
        // Quito los últimos caracteres 'and '
        $final_statement = substr_replace(implode($like_statement) ,"",-3);
        // var_dump("<br>like_statement=".json_encode($like_statement));
        // var_dump("<br>final_statement=".$final_statement);
        // exit(0);
        
        // $sql = $this->db->query("SELECT * from `students` WHERE ( ".$final_statement." )")->result_array();

        // SELECT students.id,firstname,lastname,user_id,level as teaching_levels from `students`
        // JOIN `student_level_enrolled` ON students.id = student_level_enrolled.student_id AND student_level_enrolled.school_year = "2020-21"
        // order by students.id desc
        $sql = $this->db->query("SELECT students.id,firstname,lastname,user_id,level as teaching_levels from `students`
                                JOIN `student_level_enrolled` ON students.id = student_level_enrolled.student_id 
                                                            AND student_level_enrolled.school_year = '".$school_year."'
                                                            WHERE ( ".$final_statement." )")
                        ->result_array();
        // var_dump("<br>getStudents(sql)=".json_encode($sql));
        // exit(0);
        $data = array();
        foreach($sql as $item) {
            //$levels_coded = array_pop($item);
            $levels_coded = $item['teaching_levels'];
            $levels = json_decode($levels_coded, true);

            // Dejamos una cadena con el curso en el que está el alumno
            $item['teaching_levels'] = $levels[0];

            // $item['teaching_levels'] = json_decode($item['teaching_levels'], true);
            $data[] = $item;
        }
        // var_dump("<br>data=<br>".json_encode($data));
        // exit(0);
        return $data;

    }

    /**
     * Función para obtener los datos de 1 único usuario
     */
    public function getStudent ($id) {
        // $sql = $this->db->order_by('lastname', 'ASC')->get_where('students',array('user_id' => $id));
        // return $sql->row_array();
        $sql = $this->db->get_where('students',array('user_id' => $id)); 
        // Decodificamos los datos guardados en JSON (levels)
        $data = $sql->row_array();
        $levels_coded = array_pop($data);
        // var_dump($levels_coded);
        // var_dump("<br>");
        $levels = json_decode($levels_coded, true);
        // $data['teaching_levels'] = $levels[0];
        // // foreach($levels as $level)
        // //     $levels_result[] = $level;
        //$tmp = array ('teaching_levels' => $levels);
        //$data['teaching_levels'] = array($levels);
        $data['teaching_levels'] = $levels;
//         var_dump($data);
//         exit(0);
        return $data;
    }

    /**
     * Función para obtener el identificador primario (id) de un Estudiante
     */
    public function getStudentId ($user_id) {
        $this->db->select('id');
        $sql = $this->db->get_where('students',array('user_id' => $user_id))->row_array();
        return $sql['id'];
    }
    /**
     * Función para obtener el identificador de usuario (user_id) de un estudiante
     */
    public function getUserIdFromStudent ($student_id)
    {
        $this->db->select('user_id');
        $sql = $this->db->get_where('students',array('id' => $student_id))->row_array();
        return $sql['user_id'];
    }

     /**
     * Función para obtener el EQUIPO en el que está un Estudiante
     */
    public function getStudentTeam ($challenge_id, $user_id) {
        $this->db->select('s_team');
        $sql = $this->db->get_where('challenges_students_enrolment',array('student_id' => $user_id, 'challenge_id' => $challenge_id ))->row_array();
        return $sql['s_team'];
    }
    public function getStudentTeamForGrading ($id) {
        $this->db->select('s_team');
        $sql = $this->db->get_where('students',array('id' => $id))->row_array();
        return $sql['s_team'];
    }

    /**
     * Función para obtener los ids de los estudiantes en un equipo
     */
    // public function getStudentIdsInTeam ($s_team) {
    //     $this->db->select('id');
    //     $sql = $this->db->get_where('students',array('s_team' => $s_team))->result_array();
    //     $result = array();
    //     foreach ($sql as $item) {
    //         $result[] = $item['id'];
    //     }

    //     return $result;
    // }
    /**
     * Función para obtener los estudiantes que están en un Reto
     */
    public function getStudentsInChallenge ($students_id = array(), $current_team = -1) {
        if(empty($students_id)) 
            return array();
        
        $students_result = array();

        foreach ($students_id as $item) {
            // print_r("<br>item=".json_encode($item)."<br>");

            if($current_team == -1){
                $data = $this->db->order_by('lastname', 'ASC')
                        ->get_where('students', array('id' => $item['student_id']))->row_array();
            } else {
                $data = $this->db->order_by('lastname', 'ASC')
                        ->get_where('students', array(
                            'id' => $item['student_id'],
                            's_team' => $current_team,
                        ))->row_array();
                
            }
            // var_dump("<br>getStudentsInChallenge(data)=".json_encode($data));
            // exit(0);
            if(!empty($data)) {
                // $sql = $this->db->query("SELECT students.id,firstname,lastname,user_id,level as teaching_levels from `students`
                //                 JOIN `student_level_enrolled` ON students.id = student_level_enrolled.student_id 
                //                                             AND student_level_enrolled.school_year = '".$school_year."'
                //                                             WHERE ( ".$final_statement." )")
                //         ->result_array();

                // Obtenemos el nivel de docencia que cursa actualmente.
                $this->db->select('level');
                $sql = $this->db->get_where('student_level_enrolled', array(
                    'student_id' => $data['id'],
                    'school_year' => $this->session->school_year,   
                ))->row_array();
                // print_r("<br>SQL para ver level=".json_encode($sql)."<br>");

                // Eliminamos los caracteres [, ], " del nivel
                $find = array("[","]","\"");
                $replace = "";
                
                $data['teaching_levels'] = str_replace($find,$replace,$sql['level']);
                // $data['teaching_levels'] = json_decode($sql['level'], true);

                // Guardamos la información del estudiante en el array resultado.
                $students_result[] = $data;

            }

        }
       
        // print_r("<br>Students_result=".json_encode($students_result)."<br><br>");
        return $students_result;
        
    }
    /**
     * Función para obtener los estudiantes A VALORAR en un Reto
     */
    public function getStudentsInChallengeToAssess ($data = array()) {
        if(isset($data)){
            $sql_result = array();

            foreach ($data as $item) {
                $sql = $this->db->order_by('lastname', 'ASC')->get_where('students', array('id' => $item['student_id']));
                $sql_result[] = $sql->row_array();
            }
            /**
             * array(4) { 
             *  [0]=> array(9) 
             *          { ["id"]=> string(1) "7" ["firstname"]=> string(13) "Cinco o cinco" ["lastname"]=> string(15) "Cinco Apellidos" ["s_group"]=> string(0) "" ["s_team"]=> string(0) "" ["s_role"]=> string(0) "" ["user_id"]=> string(2) "32" ["challenge_id"]=> string(1) "0" ["teaching_levels"]=> string(9) "["3-ESO"]" } [1]=> array(9) { ["id"]=> string(1) "9" ["firstname"]=> string(5) "Kevin" ["lastname"]=> string(7) "Borrell" ["s_group"]=> string(0) "" ["s_team"]=> string(0) "" ["s_role"]=> string(0) "" ["user_id"]=> string(2) "35" ["challenge_id"]=> string(1) "0" ["teaching_levels"]=> string(9) "["3-ESO"]" } [2]=> array(9) { ["id"]=> string(2) "58" ["firstname"]=> string(5) "AIMAR" ["lastname"]=> string(12) "RUBIO PEIDRO" ["s_group"]=> string(1) "C" ["s_team"]=> string(2) "C1" ["s_role"]=> string(8) "ÀRBITRE" ["user_id"]=> string(3) "203" ["challenge_id"]=> string(1) "0" ["teaching_levels"]=> string(9) "["3-ESO"]" } [3]=> array(9) { ["id"]=> string(2) "59" ["firstname"]=> string(6) "AITANA" ["lastname"]=> string(14) "FERRERO MILLAN" ["s_group"]=> string(1) "C" ["s_team"]=> string(2) "C1" ["s_role"]=> string(9) "BALLARINS" ["user_id"]=> string(3) "204" ["challenge_id"]=> string(1) "0" ["teaching_levels"]=> string(9) "["3-ESO"]" } }
             */
            // var_dump($sql_result);
            // exit(0);
            return $sql_result;
        } else {
            // $sql = $this->db->order_by('lastname', 'ASC')->get('students');
            // return $sql->result();
            return array();
        }
    }

    /**
     * Función para eliminar un estudiante de la BBDD
     */
    public function delete($id) { 
        // Borrar de los retos asociados
        $student_id = $this->getStudentId($id);
        if(!$this->disenrolChallenge($student_id)) return false;
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('students', array('user_id' => $id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para CREAR los Retos de un Estudiante
     */
    public function enrolChallenge($student_id, $challenge_id) {

        $data = array (
            'student_id' => $student_id,
            'challenge_id' => $challenge_id,
        );
        return !$this->db->insert('challenges_students_enrolment',$data) ? false: true;
    }

    /**
     * Función para eliminar un Estudiante de un Reto.
     */
    public function disenrolChallenge($student_id) {
        $this->db->trans_start();
        $this->db->delete('challenges_students_enrolment', array('student_id' => $student_id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /** 
     * Función para eliminar de un equipo a un alumno
     */
    // public function removeFromTeam($student_id) {
    //     $this->db->trans_start();
    //     $this->db->where('id', $student_id);
    //     $this->db->update('students', array('s_team' => '-1')); 
    //     $this->db->trans_complete();
    //     return !$this->db->trans_status() ? false : true;
    // }

    /**
     * Función para improtar todos los usuarios
     */
    public function insert($data, $condition) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('email', $condition['email']);
        $this->db->update('students', $data); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    public function insertUser($data) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->insert('students', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

}