<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('teachers_model','teachers');
        $this->load->model('students_model','students');

        $this->config->load('globals.php');
    }
    /**
     * Método para insertar los datos de un usuario en la BBDD
     */
    public function create($data) {
        // Comprobamos que no existan duplicados
        $sql = $this->db->get_where('users',array('username' => $data['username']));
        if ($sql->num_rows() != 0) {
            //Ya existe el username en la base de datos
            return $user_id = array (
                'error_msg' => 'El nombre de usuario ya existe.'
            );
        } 
        $sql = $this->db->get_where('users',array('email' => $data['email']));
        if ($sql->num_rows() != 0) {
            //Ya existe el email en la base de datos
            return $user_id = array (
                'error_msg' => 'El email ya existe.'
            );
        } 


        if (!$this->db->insert('users', $data)) {
            // Si hay error, devolvemos falso
            return false;
        } 
        // Si la inserción ha sido exitosa
        // Devolvemos el id de la fila insertada.
        //return $this->db->insert_id();
        $user_id = array ('id' => $this->db->insert_id()); 
        return $user_id;
    }
    /**
     * Función para inertar usuarios SOLO IMPORTADOS de fichero
     */
    public function insertUserImported($data) {
        // Creamos la estructura de datos a introducir
        $common_data = array(
            'username' => $data['username'],
            'password' => $data['password'],
            'email' => $data['email'],
            'role' => $data['role'],
        );
        
        if (!$this->db->insert('users', $common_data)) {
            // Si hay error, devolvemos falso
            // print_r("ya están en la bbdd");
            // exit(0);
            return false;
        }
        // Si la inserción ha sido exitosa recuperamos el 'id' de la fila insertada.
        $user_id = array ('id' => $this->db->insert_id()); 
        
        if($data['role'] === 'student') {
            
            // Extraemos el  elemento donde están los niveles de docencia.
            $levels = $data['teaching_levels'][0];
            // Convertir los niveles a datos JSON
            // $levels_json[] = json_encode($levels);
            // $levels_json = json_encode($levels);
            //str_replace('\"', '', $levels_json);

            $all_courses = $this->config->item('g_levels');
            // var_dump("<br>levels names=".json_encode($all_courses));
            // var_dump("<br>teaching_levels = ".$data['teaching_levels'][0]);
            $posicion = array_search($levels,$all_courses, TRUE);
            // var_dump("posición (insertUserImported)= ".$posicion);
            // exit(0);
            $course_name= $this->config->item('g_levels_name')[$posicion];

            $student_data = array(
                'user_id' => $user_id['id'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                's_group' => $data['s_group'],
                'course_name' => $course_name,
                's_team' => $data['s_team'],
                's_role' => $data['s_role'],
                'teaching_levels' => $data['teaching_levels'],
                // 'teaching_levels' => $levels,
            );
            // $student_data['teaching_levels'] = $levels_json[0];
            // var_dump($levels_json);
            // var_dump("student data = ".json_encode($student_data));
            // exit(0);
            if(!$this->students_model->create($student_data)) return false;

        } else {
            $teacher_data = array(
                'user_id' => $user_id['id'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                't_role' => $data['t_role'],
                'department' => $data['department'],
                'challenge_coordinator' => $data['challenge_coordinator'],
                'project_coordinator' => $data['project_coordinator'],
                'teaching_levels' => $data['teaching_levels'],
            );
            //$levels = $data['teaching_levels'];
            // Convertir los niveles a datos JSON
            // $levels_json[] = json_encode($levels);
            // $teacher_data['teaching_levels'] = $levels_json[0];
            if(!$this->teachers_model->create($teacher_data))
                return false;
        }
        return true;
    }
     /** 
     * Función para almacenar un usuario en la base de datos
     */
    public function save($data) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->insert('users',$data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para cambiar el estado de un usuario a 'active'
     */
    public function saveStatus($id) {
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        //$this->db->update('users', $data); 
        $this->db->update('users', array('status' => 'active')); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para obtener el listado de usuarios
     */
    public function getUsers() {
        // Si están en una única tabla.
        $sql = $this->db->order_by('username', 'ASC')->get('users');
        return $sql->result();
    }

    /**
     * Función para obtener los datos de 1 único usuario
     */
    public function getUser ($id) {
        $sql = $this->db->get_where('users',array('id' => $id));        
        return $sql->row_array();
    }
    /**
     * Función para obtener el 'id' a partir de un 'username'
     */
    public function getUserId($username = '', $email = '') {
        // var_dump("username=".$username);
        $this->db->select('id');
        // $this->db->where('users',array('username' => $username));
        // $this->db->or_where('users',array('email' => $email));
        $this->db->where('username', $username); 
        $this->db->or_where('email', $email);
        $sql = $this->db->get('users');        
        return $sql->row_array()['id'];
    }
    
    /**
    * Función para obtener el rol del usuario
    */
    public function getRole ($id) {
    	$this->db->select('role');
    	$sql = $this->db->get_where('users',array('id' => $id));
    	return $sql->row_array();
    }

    /**
     * Función para eliminar un estudiante de la BBDD
     */
    public function delete($id) { 
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('users', array('id' => $id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar los datos de un Usuario
     */
    public function updateStudent($id, $data) {
    
        // var_dump($data);
        // exit(0);
        // Extraemos el  elemento donde están los niveles de docencia.
        $levels = $data['teaching_levels'][0];
        
        $all_courses = $this->config->item('g_levels');
        
        $posicion = array_search($levels,$all_courses, TRUE);
        // var_dump("posición (insertUserImported)= ".$posicion);
        // exit(0);
        $course_name= $this->config->item('g_levels_name')[$posicion];

        $user_data = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            's_group' => isset($data['s_group']) ? $data['s_group'] : '',
            's_team' => isset($data['s_team']) ? $data['s_team'] : '',
            's_role' => isset($data['s_role']) ? $data['s_role'] : '',
            'course_name' => $course_name,
            'school_year' => $data['school_year'],
            // 'teaching_levels' => $levels,
            'teaching_levels' => $data['teaching_levels'],
        );

        // var_dump("<br>teaching_levels=".json_encode($data['teaching_levels'])."<br><br>");
        // var_dump($user_data);
        // exit(0);
        if (isset($data['password'])){
            $user = array (
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
            );
        } else {
            $user = array (
                'username' => $data['username'],
                'email' => $data['email'],
            );
        }
    
        // var_dump($user_data);
        // exit(0);
        
        if(!$this->students->update($id, $user_data))
            return false;

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('users', $user); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    /**
     * Función para actualizar los datos de un Usuario
     */
    public function updateTeacher($id, $data) {
    
        // var_dump($data);
        // exit(0);

        $user_data = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            't_role' => isset($data['t_role']) ? $data['t_role'] : '',
            'department' => isset($data['department']) ? $data['department'] : '',
            'challenge_coordinator' => $data['challenge_coordinator'],
            'project_coordinator' => $data['project_coordinator'],
            'teaching_levels' => $data['teaching_levels'],
        );

        //var_dump($user_data);
        if (isset($data['password'])){
            $user = array (
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => $data['password'],
            );
        } else {
            $user = array (
                'username' => $data['username'],
                'email' => $data['email'],
            );
        }
    
        //var_dump($user_data);
        //exit(0);
        if(!$this->teachers->update($id, $user_data))
            return false;
        
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        //$this->db->update('users', $data); 
        $this->db->update('users', $user); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para actualizar la contraseña de un usuario
     */
    public function updatePassword($id, $password) {
        $this->db->select('password_changed');
        $p_changed = intval($this->db->get_where('users', array('id' => $id))->row_array());
        $p_changed += 1;
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('users', array('password' => password_hash($password, PASSWORD_DEFAULT), 'password_changed' => $p_changed)); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
     /**
     * Función para resetear la contraseña de un alumno
     */
    public function resetPassword($id) {
        $this->db->select('username');
        $sql = $this->db->get_where('users', array('id' => $id))->row_array();
        // var_dump("<br> El SQL = ".json_encode($sql));
        // $p_changed = intval($sql['password_changed']);
        $username = $sql['username'];
        // $p_changed = intval($this->db->get_where('users', array('id', $id))->row_array()['password_changed']);
        // var_dump("<br>Las veces que ha cambiado la contraseñ son: ". $p_changed);

        // Reseteamos el contador para que le pida cambiar la contraseña
        $p_changed = 0;
        $status = 'first_time';
        
        // var_dump("<br>El ID del usuario es: ". $id);
        // var_dump("<br>El nombre de usuario es: ". $username);
        // exit(0);

        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('users', array('password' => password_hash($username, PASSWORD_DEFAULT), 
                                        'password_changed' => $p_changed,
                                        'status' => $status)); 
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /*
     * Fetch members data from the database
     * @param array filter data based on the passed parameters
     */
    function getRows($params = array()){
        $this->table = 'users';
        $this->db->select('*');
        $this->db->from($this->table);
        
        if(array_key_exists("where", $params)){
            foreach($params['where'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'desc');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }

    /**
     * Función para paginar los datos
     */
    public function get_Paginate($limit,$offset) {
        $sql = $this->db->order_by('username', 'ASC')->get('users',$limit,$offset);
        return $sql->result();
    }


}