<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technical_skills_model extends CI_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('teachers_model','teachers');
        $this->load->model('students_model','students');
        $this->load->model('challenges_model','challenges');
        $this->load->model('technical_skills_assessments_model');

    }
    /**
     * Método para crear y almacenar un RA (Resultado de Aprendizaje) en la BBDD
     */
    public function create($data) {

        // var_dump("<br>Los challenges a los que puede estas asigando el RA son: ".json_encode($data['challenges_in']));

        $challenges = array_values($data['challenges_in']);
        // var_dump("<br>challenges=".json_encode($challenges));

        if(!empty($challenges)) {
            $challenges_result = '';
            foreach($challenges as $item) {
                // var_dump("<br>item=".$item);
                $challenges_result .= $item . ',';
                // añadimos una coma al final.

            
            }
            // var_dump("<br>challenges_result=".$challenges_result);
            $data['challenges_in'] = $challenges_result;

            
        }
        // var_dump("<br>challenges_in = ".json_encode($data['challenges_in']));
        // exit(0);

        if (!$this->db->insert('technical_skills', $data)) {
            // Si hay error, devolvemos falso
            return false;
        } else {
            // Si la inserción ha sido exitosa
            // Devolvemos el id de la fila insertada.
            $technical_skills_id = array (
                'technical_skills_id' => $this->db->insert_id(),
            ); 
            // return $technical_skills_id;
        }

        // Si está asignado el RA a un RETO, creamos la estructura de ASSESSMENTS en la bbdd
        if(!empty($challenges)) {
            foreach($challenges as $challenge_id) {
            //     // var_dump("<br>item=".$item);
                
                // Recuperamos los estudiantes que tiene asignados. 
                $students_in_challenge = $this->challenges_model->getChallengeStudents($item);
                // var_dump("<br>students in challenge=".json_encode($students_in_challenge));

                // Recuperamos los profesores que tienen docencia en el RETO
                $teachers_in_challenge = $this->challenges_model->getChallengeTeachers($item);
                // var_dump("<br>teachers in challenge=".json_encode($teachers_in_challenge));

                // Creamos los registros de ASSESSMENT para cada estudiante. 
                if(!$this->technical_skills_assessments_model->start($challenge_id, $students_in_challenge, $teachers_in_challenge, $technical_skills_id)) {
                    var_dump("ERRORR al crear los assessments en la BBDD!!");
                    return false;
            
                }
 
            }
        }

        // exit(0);
        // Finalmente devolvemos el identificador del RA creado.
        return $technical_skills_id;

    }

    /**
     * Función para actualizar los datos de un RA (Resultado de Aprendizaje)
     */
    public function update($id, $data) {
       
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('technical_skills', $data);
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }

    /**
     * Función para actualizar los RAs con el Reto asociado
     */
    public function updateTechnicalSkillsInChallenge($technical_skills, $challenge_id) {
        // var_dump("<br>technical_skills = ".json_encode($technical_skills));
        // var_dump("<br>challenge_id=".$challenge_id);

        $challanges_tostore = '';
        $current_school_year = $this->session->school_year;


        $prev_technical_skills = array();
        // $this->db->select('challenges_in');
        // $prev_challenges = $this->db->get_where('technical_skills', array('id' => $ts_id))->row_array()['challenges_in'];
        $query = "SELECT id
                    FROM technical_skills WHERE school_year='$current_school_year' 
                    AND challenges_in LIKE '%$challenge_id%'";
        $sql = $this->db->query($query)->result_array();
        if(!empty($sql))
            foreach($sql as $item) {
                $prev_challenges[] = $item['id'];
            }
        else $prev_challenges = array();
        // var_dump("<br>lo que tiene previamente es:".json_encode($prev_challenges));

        // Comparamos lo nuevo (del formulario) con lo anterior (de la BBDD).

        $diferencia1=array_diff($technical_skills,$prev_challenges);
        // var_dump("<br>diferencia 1: ".json_encode($diferencia1));
        if(!empty($diferencia1)) {
            // Si hay algún elemento, 
            // AÑADIR ELEMENTOS NUEVOS a la BBDD
            foreach($diferencia1 as $item) {
                // var_dump("<br>item(d1)=".$item);
                $tostore = '';
                $this->db->select('challenges_in');
                $sql_data = $this->db->get_where('technical_skills', array('id' => $item))->row_array()['challenges_in'];
                // var_dump(" -- sql_data=".$sql_data);
                // Añadimos el identificador del reto
                $tostore = $sql_data.$challenge_id.',';
                // var_dump("<br>(d1)tostore reemplazado=".$tostore);

                // Actualizamos el campo de la BBDD
                $this->db->trans_start();
                $this->db->where('id', $item);
                $this->db->update('technical_skills', array('challenges_in' => $tostore));
                $this->db->trans_complete();

                // CREAR los Assessments para los nuevos RAs
                // Si está asignado el RA a un RETO, creamos la estructura de ASSESSMENTS en la bbdd
            
                        
                // Recuperamos los estudiantes que tiene asignados. 
                $students_in_challenge = $this->challenges_model->getChallengeStudents($challenge_id);
                // var_dump("<br>students in challenge=".json_encode($students_in_challenge));

                // Recuperamos los profesores que tienen docencia en el RETO
                $teachers_in_challenge = $this->challenges_model->getChallengeTeachers($challenge_id);
                // var_dump("<br>teachers in challenge=".json_encode($teachers_in_challenge));

                // exit(0);
                // Creamos los registros de ASSESSMENT para cada estudiante. 
                if(!$this->technical_skills_assessments_model->start($challenge_id, $students_in_challenge, $teachers_in_challenge, array($item))) {
                    var_dump("ERRORR al crear los assessments en la BBDD!!");
                    return false;
            
                }

            }
        }

        // Comparamos lo anterior con lo nuevo
        $diferencia2=array_values(array_diff($prev_challenges, $technical_skills));
        // print_r("<br>diferencia 2:".json_encode($diferencia2));
        if(!empty($diferencia2)) {
            // Si hay algún elemento
            // BORRAR ELEMENTOS de la BBDD
            foreach($diferencia2 as $item) {
                // var_dump("<br>item(d2)=".$item);
                $tostore = '';
                $this->db->select('challenges_in');
                $sql_data = $this->db->get_where('technical_skills', array('id' => $item))->row_array()['challenges_in'];
                // var_dump(" -- sql_data=".$sql_data);
                // Eliminamos las ocurrencias de este reto
                $tostore = str_replace($challenge_id.',','',$sql_data);
                // var_dump("<br>tostore reemplazado=".$tostore);

                // Actualizamos el campo de la BBDD
                $this->db->trans_start();
                $this->db->where('id', $item);
                $this->db->update('technical_skills', array('challenges_in' => $tostore));
                $this->db->trans_complete();

                // BORRAR los Assessments para los nuevos RAs
                if(!$this->technical_skills_assessments_model->deleteTechnicalSkillsInChallenge($item, $challenge_id)) {
                    var_dump("error al borrar de la base de datos!!");
                    return false;
                }

                
            }
           
            
        }
        

        // exit(0);        
        return !$this->db->trans_status() ? false : true;

    }
    
    
    /**
     * Función para obtener el listado de RAs
     * @param $user Si hay un idenficador sólo se muestran los RAs
     * asignados a ese Profesor. 
     */
    public function getAllTechnicalSkills($teacher_id = '', $current_school_year= '', $limit='', $offset='') {

        // var_dump("<br> getTechnicalSkills (t_owner) = ".$t_owner);
        // var_dump("<br> getTechnicalSkills (school_year) = ".$current_schoool_year);

        // var_dump("<br> GetTechnicalSkills (limit)=".$limit."<br> (offset)=".$offset);
        // exit(0);

        // Profesor Daniel Pastor Peidro
        // $teacher_id = '30'; 

        // if( ! (empty($user) && empty($current_school_year)) ){
        if( ! empty($teacher_id)  ){

            // Buscar todos los retos en los que está involucrado el profesor.
            $challenges_in = $this->challenges_model->getAllChallengesOfTeacher($teacher_id);

            // print_r("<br>challenges=".json_encode($challenges_in));
            // exit(0);

            // Con ellos, obtener los Technical_Skills_ids que tenga el reto.
            $ts_ids_of_challenge = array();
            if(!empty($challenges_in)) {
                    // $ts_ids_of_challenge = $this->db->select('technical_skills')
                    //                                 ->from('challenges')
                    //                                 ->where_in(array('id' => $challenges_in))
                    //                                 ->get()
                    //             
                                    // ->result_array();
                foreach($challenges_in as $chg) {
                    // print_r("<br>challenge=".$chg);
                    $sql  = $this->db->select('technical_skills')
                                        ->from('challenges')
                                        ->where('id', $chg)
                                        ->get()
                                        ->row_array()['technical_skills'];
                    if(!empty($sql)) {
                        // Añadimos los ids uno a uno y por separado.
                        // print_r("<br>sql = ".$sql);
                        $sql = explode(",", $sql);
                        // print_r("<br>sql2 = ".json_encode($sql));
                        foreach($sql as $item) {
                            // Si no está, la introducimos
                            $ts_ids_of_challenge[] = $item;

                        }

                    }
                }
            }
            // print_r("<br>ts_ids_of_challenge= ".json_encode($ts_ids_of_challenge));
            // exit(0);
            



            if (! (empty($limit) && empty($offset)) ){
                $technical_skills = $this->db->select('*')
                                            ->order_by('name', 'ASC')
                                            ->from('technical_skills')
                                            ->group_start()
                                                // ->where('school_year', $current_schoool_year)
                                                ->where('t_owner', $teacher_id)
                                            ->group_end()
                                            ->limit($limit,$offset)
                                            ->get()
                                            ->result_array();
            } else {
                $technical_skills = $this->db->select('*')
                                            ->order_by('name', 'ASC')
                                            ->from('technical_skills')
                                            ->group_start()
                                                // ->where('school_year', $current_schoool_year)
                                                ->where('t_owner', $teacher_id)
                                            ->group_end()
                                            ->get()
                                            ->result_array();
            }

            // Añadimos las TS obtenidas de los retos.
            if (!empty($ts_ids_of_challenge)) {
                // print_r("<br>Añadimos las TS de los retos asociados");
                foreach($ts_ids_of_challenge as $item) {
                    // print_r("<br>item = ".$item);
                    $query = $this->db->select('*')
                                                    ->order_by('name', 'ASC')
                                                    ->from('technical_skills')
                                                    // ->where('id', $item)
                                                    ->group_start()
                                                        // ->where('school_year', $current_schoool_year)
                                                        ->where('id', $item)
                                                    ->group_end()
                                                    ->get()
                                                    ->result_array();
                    // print_r("<br>query=".json_encode($query));
                    if(!empty($query))
                        $technical_skills[] = $query[0];
                }

            }

        } else {
            $technical_skills = $this->db->select('*')
                                            ->order_by('name', 'ASC')
                                            ->from('technical_skills')
                                            // ->where('year', $current_school_year)
                                            ->limit($limit)
                                            ->offset($offset)
                                            ->get()
                                            ->result_array();
        }
        // print_r("<br>El resultado de los RAs son:".json_encode($technical_skills));
        // exit(0);

        // Eliminar RAs duplicados.
        if(!empty($technical_skills)) {
            // $technical_skills = array_intersect_key($technical_skills, array_unique(array_map('serialize', $technical_skills), SORT_REGULAR));
            $technical_skills = array_values(array_intersect_key($technical_skills, array_unique(array_map('serialize', $technical_skills), SORT_REGULAR)));
        }
        // if(is_null($technical_skills))
        //     return array();

        // print_r("<br>El resultado de los RAs SIN DUPLICADOS ES:".json_encode(array_values($technical_skills))."<br><br>");


        return $technical_skills;

        
    }

    /**
     * Función para obtener los datos de 1 único RA
     */
    public function getOneTechnicalSkill ($technical_skills_id, $current_school_year = '') {

        $technical_skill = $this->db->get_where('technical_skills',array('id' => $technical_skills_id))->row_array();    
        // var_dump($technical_skill);
        // exit(0);

        // Si el RA no es del curso escolar a mostrar, no se devuelve.
        // var_dump("<br>challenge[year]=".$challenge['year'].", and current_schoool_year=".$current_schoool_year);
        // if($current_schoool_year != '')
        //     if($technical_skill['school_year'] !== $current_schoool_year)
        //         return '';
        
        // Convertimos los niveles educativos a forma de array.
        // $technical_skill['teaching_levels'] = json_decode($technical_skill['teaching_levels'], true);;
            // var_dump("teaching_levels=".json_encode($teaching_levels));
            // exit(0);

        return $technical_skill;
    }

    /**
     * Función para obtener el nombre de una RA de la BBDD
     */
    public function getTechnicalSkillsName($technical_skills_id) {
        $this->db->select('name');
        $sql = $this->db->get_where('technical_skills', array('id' => $technical_skills_id))->row_array();
        return $sql['name'];
    }

    
    /**
     * Función para obtener los Profesores asociados a un RA
     */
    public function getTechnicalSkillTeachers ($technical_skill_id) {
        // $this->db->select('teacher_id');
        // $teachers_id = $this->db->get_where('challenges_teachers_enrolment', array('challenge_id' => $challenge_id))->result_array();
        // //var_dump($sql->result());
        // //var_dump($teachers_id);
        // $teachers = $this->teachers_model->getTeachersInChallenge($teachers_id);
        // // var_dump($teachers);
        // // exit(0);
        // return $teachers;
    }
    /**
     * Función para obtener el id del profesor creador del RA
     */
    public function getTechnicalSkillsTeacherOwner ($technical_skills_id) {
        $this->db->select('t_owner');
        $sql = $this->db->get_where('technical_skills', array('id' => $technical_skills_id))->row_array();
        return $sql['t_owner'];
    }
    /**
     * Función para obtener los Estudianes asociados a un RA
     */
    public function getTechnicalSkillStudents ($challenge_id, $current_team = -1) {
        
        // if ($current_team == -1) {
        //     $this->db->select('student_id');
        //     $students_id = $this->db->get_where('challenges_students_enrolment', array('challenge_id' => $challenge_id))->result_array();
        //     $students = $this->students_model->getStudentsInChallenge($students_id);
        //     // print_r("Los estudiantes son:".json_encode($students)."<br>");
        //     // exit(0);
        //     return $students;
        // } else {
        //     // var_dump("<br>challenge_id=".$challenge_id.", current_team=".$current_team);
        //     $this->db->select('student_id');
        //     $sql = $this->db->get_where('challenges_students_enrolment', array(
        //         'challenge_id' => $challenge_id,
        //         's_team' => $current_team))->result_array();
        //     $students_id = $sql;
        //     $students = $this->students_model->getStudentsInChallenge($students_id);
        //     // Hay que buscar los estudiantes de un equipo

        //     // $students = $this->students_model->getStudentsInChallenge($students_id, $current_team);
        //     // var_dump("Los estudiantes son:".json_encode($students)."<br>");
        //     // exit(0);
        //     return $students;
        // }

    }

    /**
     * Función para obtener el RA en el que está un estudiante
     */
    public function getTechnicalSkillsOfStudent ($student_id) {
        // var_dump("student_id=".$student_id);
        // $this->db->select('challenge_id');
        // $sql = $this->db->get_where('challenges_students_enrolment', array('student_id' => $student_id));
        
        // $students_id = $sql->result_array();
        // // var_dump("students_id=".json_encode($students_id));
        // // exit(0);
        // return $students_id;
    }

    /**
     * Función para obtener la Rúbrica asociada a un RA
     */
    public function getTechnicalSkillsRubric ($technical_skills_id) {
        $this->db->select('rubric_id');
        $sql = $this->db->get_where('technical_skills', array('id' => $technical_skills_id))->row_array();
        return $sql['rubric_id'];
    }
    
    /**
     * Función para obtener todos los Retos en los que está este RA
     */
    public function getAllChallengesOfTechnicalSkills ($technical_skill_id) {

        // $sql = $this->db->query("SELECT * from technical_skills WHERE JSON_CONTAINS(teaching_levels, '".$level."')")->result_array();        
        // var_dump($sql);
        // exit(0);
        // var_dump("<br>TS = ".$technical_skill_id);
        // $this->db->select('id');
        // $sql = $this->db->where_in('technical_skills', array($technical_skill_id))->get('challenges')->result_array();
        $this->db->select('challenges_in');
        $sql = $this->db->get_where('technical_skills', array('id' => $technical_skill_id))->row_array();
        // var_dump("<br>getAllChallengesOfTechnicalSkills = ".json_encode($sql));
        // exit(0);

        return $sql['challenges_in'];
     }

    /**
     * Función para comprobar si la RA tiene rúbrica o nota numérica
     * Devuleve un 0 si no tiene rúbrica y el id de la rúbrica en caso contrario.
     */
    public function hasRubric($technical_skills_id) {
        $this->db->select('has_rubric, rubric_id');
        $sql = $this->db->get_where('technical_skills', array('id' => $technical_skills_id))->row_array();

        // print_r("<br>sql -> ".json_encode($sql));
        if($sql['has_rubric'] != 0)
            return $sql['rubric_id'];
        
        return $sql['has_rubric'];
    }


    /**
     * Función para eliminar un RA de la BBDD
     */
    public function delete($technical_skills_id) { 
        // Iniciar transacción para poder hacer rollback
        $this->db->trans_start();
        $this->db->delete('technical_skills', array('id' => $technical_skills_id));
        $this->db->trans_complete();
        return !$this->db->trans_status() ? false : true;
    }
    
    /*
     * Fetch data from the database
     * @param array filter data based on the passed parameters
     */
    function getRows($params = array()) {

        $this->db->select('*');
        $this->db->from('technical_skills');
        
        if(array_key_exists("where", $params)){
            foreach($params['where'] as $key => $val){
                $this->db->where($key, $val);
            }
        }
        
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $this->db->count_all_results();
        }else{
            if(array_key_exists("id", $params)){
                $this->db->where('id', $params['id']);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('id', 'desc');
                if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit'],$params['start']);
                }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                    $this->db->limit($params['limit']);
                }
                
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        // Return fetched data
        return $result;
    }
   
    /**
     * Función para paginar los datos
     */
    public function get_Paginate($teacher_id, $limit, $offset, $current_school_year) {

        // From Users
        //         $sql = $this->db->order_by('username', 'ASC')->get('users',$limit,$offset);


        return $this->getTechnicalSkills($teacher_id, $current_school_year, $limit, $offset);

        // if (!empty($teacher_id)) {

        //     $this->db->select('challenge_id');
        //     $sql = $this->db->get_where('challenges_teachers_enrolment', array('teacher_id' => $teacher_id));
        //     $challenges = $sql->result_array();
        //     $challenges_result = array();
        //     // Si no está en ninún RA, no se devuelven.
        //     if(count($challenges)==0)
        //         return array();

        //     foreach($challenges as $item) {
        //         // $challenges_result[] = array(
        //         //     'id' => $item['challenge_id'],
        //         // );
        //         $challenges_result[] = $item['challenge_id'];
        //         // $sql = $this->db->order_by('name', 'ASC')->get_where('challenges', array('id' => $item['challenge_id']));
        //         // $challenges_result[] = $sql->result_array();
        //     }
        //     // var_dump($challenges[0]['challenge_id']);
        //     // var_dump("<br/>");
        //     // var_dump($challenges_result[0]);
        //     // var_dump($challenges_result);
        //     // exit(0);
        //     // return $challenges_result;
            

        //     // var_dump("<br> la fecha es: ".$date);
        //     // exit(0);

        //     $sql = $this->db->order_by('name', 'ASC')
        //                     ->from('technical_skills')
        //                     ->where_in('id', $challenges_result)
        //                     // ->where('year', $current_schoool_year)
        //                     ->get()->result_array();

        //     // var_dump(json_encode($sql));
        //     // exit(0);
        //     return $sql;
            
        // } else {
        //     $sql = $this->db->order_by('name', 'ASC')
        //                             ->from('technical_skills')
        //                             // ->where('year', $current_schoool_year)
        //                             ->get()->result_array();
        //     // Convert JSON data into array
        //     // foreach($sql as $item) {
        //     //     $item['sprints_openfor_assess'] = json_decode($item['sprints_openfor_assess'], true);
        //     // }
        //     return $sql;

        // }
        
    }

}