<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_users extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 10,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'username' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                                'unique' => TRUE,
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                                'unique' => TRUE,
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'role' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                ));
		// Primary Key
                $this->dbforge->add_key('id', TRUE);
                $attributes = array('ENGINE' => 'InnoDB');

                $this->dbforge->create_table('users', TRUE);
        }

        public function down()
        {
                $this->dbforge->drop_table('users', TRUE);
        }
}