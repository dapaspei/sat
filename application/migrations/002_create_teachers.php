<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_teachers extends CI_Migration {
	
	    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        //$this->load->helper('db');
    }

	
        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 10,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'firstname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'lastname' => array (
                                'type' => 'VARCHAR',
                                'constraint' => '250',
                        ),
                        'department' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'role' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => 10,
                                'unsigned' => TRUE,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (user_id) REFERENCES users(id)'); 
                $this->dbforge->create_table('teachers');

        }

        public function down()
        {
                $this->dbforge->drop_table('teachers');
        }
}