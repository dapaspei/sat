<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Variables globales accesibles desde Controladores, Modelos y Vistas
 */

// Nombre de la aplicación
$config['g_appname'] = 'SAT';
$config['g_appname_desc'] = 'Skills Assessment Tool';
$config['g_applang'] = 'cat';

// Niveles de docencia
$config['g_levels'] = array(
    '1ESO-A','1ESO-B','1ESO-C','1ESO-D',            //  1
    '2ESO-A','2ESO-B','2ESO-C','2ESO-D',            //  2
    '3ESO-A', '3ESO-B', '3ESO-C',                   //  3
    '4ESO-A', '4ESO-B', '4ESO-C',                   //  4
    '1BAC-A', '1BAC-B', '1BAC-TEC', '1BAC-D',       //  5
    '2BAC-TEC',                                     //  6
    '1CFM-A', '1CFM-D', '1CFM-E',                   //  7
    '1CFM-M', '1CFM-T', '1CFM-U',                   //  8
    '1CFM-V', '1CFM-Z',                             //  9 
    '1CFS-A', '1CFS-B', '1CFS-D',                   // 10
    '1CFS-F', '1CFS-O', '1CFS-P',                   // 11
    '1CFS-U',                                       // 12
    '2CFM-A', '2CFM-D', '2CFM-E',                   // 13
    '2CFM-M', '2CFM-T', '2CFM-U',                   // 14
    '2CFM-V', '2CFM-Z', '2CFS-A',                   // 15
    '2CFS-B', '2CFS-D', '2CFS-F',                   // 16
    '2CFS-O', '2CFS-P',                             // 17
    '5ESO-A', '5ESO-B',
    );
$config['g_levels_name'] = array( 
    'SECUNDÀRIA 1rA','SECUNDÀRIA 1rB','SECUNDÀRIA 1rC','SECUNDÀRIA 1rD',                            //  1
    'SECUNDÀRIA 2nA','SECUNDÀRIA 2nB','SECUNDÀRIA 2nC','SECUNDÀRIA 2nD',                            //  2
    'SECUNDÀRIA 3rA', 'SECUNDÀRIA 3rB','SECUNDÀRIA 3rC',                                            //  3
    'SECUNDÀRIA 4tA','SECUNDÀRIA 4tB', 'SECUNDÀRIA 4tC',                                            //  4
    '1r BATX CIENTÍFIC - A', '1r BATX CIENTÍFIC - B', '1r BATX TECNOLÒGIC', '1r BATX CIENTÍFIC - D',//  5 
    '2n BATX TECNOLÒGIC',                                                                           //  6
    '1r ACTIVITATS COMERCIALS MITJÀ', '1r ELECTR-DISTÀNCIA MITJÀ', '1r ELECTRICITAT MITJÀ',         //  7
    '1r MECANITZAT MITJÀ', '1r FAB. I ENNOBLIMENT MITJÀ', '1r FUSTERIA I MOBLE MITJÀ',              //  8
    '1r ELECTROM.V.AUTOM. MITJÀ', '1r CONDUCCIÓ VEH. TRANSP.',                                      //  9
    '1r AUTOMOCIÓ SUPER', '1r AUT.ROBOTICA IND.', '1r EERR SEMIPRESENCIAL',                         // 10
    '1r PROG.PROD.FAB.MECÀNICA', '1r COM. INTERNACIONAL', '1r MARQUETING I PUBLICITAT',             // 11
    '1r DISSENY I MOBLAMENT',                                                                       // 12
    '2n ACT. COMERCIALS', '2n ELECTR-DISTÀNCIA MITJÀ', '2n ELECTRICITAT MITJÀ',                     // 13
    '2n MECANITZAT MITJÀ', '2n FAB. I ENNOBLIMENT', '2n FUSTERIA I MOBLE MITJÀ',                    // 14
    '2n ELECTROM.V.AUTOM. MITJÀ', '2n CONDUCCIÓ VEHICLES TRANSPORT', '2n AUTOMOCIÓ SUPERIOR',       // 15
    '2n AUT. ROBOTICA IND.', '2n EERR SEMIPRESENCIAL', '2n PROG. PRO. FAB. MEC.',                   // 16
    '2n COM. INTERNACIONAL', '2n MARQUETING Y PUBLICIDAD',                                          // 17
    'SECUNDÀRIA 5éA', 'SECUNDÀRIA 5éB',
    );

// Datos de los RETOS
// $config['g_school_year'] = array('2019-20', '2020-21', '2021-22', '2022-23');
$config['g_school_year'] = array('2019-20', '2020-21', '2021-22');
$config['g_sprints'] = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10');
$config['g_teams_num'] = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10');
$config['g_sprints_min'] = '1';
$config['g_sprints_max'] = '10';
// Porcentaje para las cualificaciones: 60% PROFES, 20% AUTOevaluacion, 20% COevaluación
$config['g_percentage'] = array(0.6, 0.2, 0.2);

// Datos de las rúbricas
$config['g_min_cols'] = '3';
$config['g_max_cols'] = '6';
$config['g_min_rows'] = '1';
$config['g_max_rows'] = '10';
$config['g_min_value_1'] = '0';
$config['g_min_value_2'] = '1';
$config['g_max_value_1'] = '2';
$config['g_max_value_2'] = '10';
// Color de las columnas de una rúbrica
// $config['g_bg_descriptions'] = array(
//     '3' => array('bg-red-300', 'bg-yellow-300','bg-green-200'),
//     '4' => array('bg-red-400', 'bg-orange-300', 'bg-yellow-200','bg-green-300'),
//     '5' => array('bg-red-300', 'bg-orange-300', 'bg-yellow-200','bg-teal-200', 'bg-green-200'),
//     '6' => array('bg-red-300', 'bg-red-200', 'bg-orange-200', 'bg-yellow-200','bg-teal-200', 'bg-green-200'),
// );
$config['g_bg_descriptions'] = array(
    '3' => array('bg-gray-100', 'bg-gray-100', 'bg-gray-100'),
    '4' => array('bg-gray-100', 'bg-gray-100', 'bg-gray-100', 'bg-gray-100'),
    '5' => array('bg-gray-100', 'bg-gray-100', 'bg-gray-100', 'bg-gray-100', 'bg-gray-100'),
    '6' => array('bg-gray-100', 'bg-gray-100', 'bg-gray-100', 'bg-gray-100', 'bg-gray-100', 'bg-gray-100'),
);
$config['g_bg_criteria'] = 'bg-blue-200'; 
$config['g_bg_score'] = array (
    0 => 'bg-transparent',
    1 => 'bg-red-300',
    2 => 'bg-red-300',
    3 => 'bg-orange-300',
    4 => 'bg-orange-300',
    5 => 'bg-blue-300',
    6 => 'bg-blue-300',
    7 => 'bg-blue-300',
    8 => 'bg-green-300',
    9 => 'bg-green-300',
    10 => 'bg-purple-300',
    11 => 'bg-purple-300',
);

// Palabras a usar para cambio de idioma
$config['g_access'] = array(
    'es' => 'Acceder', 
    'cat' =>'Accedir',
    'en' => 'Access',
);
$config['g_import'] = array(
    'es' => 'Importar', 
    'cat' =>'Importar',
    'en' => 'Import',
);
$config['g_exit'] = array(
    'es' => 'Salir', 
    'cat' =>'Eixir',
    'en' => 'Exit',
);
$config['g_challenges_name'] = array(
    'es' => 'Retos', 
    'cat' =>'Reptes',
    'en' => 'Challenges',
);
$config['g_technical_skills_name'] = array(
    'es' => 'Tareas Evaluables', 
    'cat' =>'Tasques Avaluables',
    'en' => 'Evaluable Tasks',
);
$config['g_soft_skills_name'] = array(
    'es' => 'Habilidades Transversales', 
    'cat' =>'Habilitats Transversals',
    'en' => 'Soft Skills',
);
$config['g_final_qualification_name'] = array(
    'es' => 'Calificación Final', 
    'cat' =>'Qualificació Final',
    'en' => 'Final Mark',
);
$config['g_projects_name'] = array(
    'es' => 'Proyectos Globales',
    'cat' => 'Projectes Globals',
    'en' => 'Global Projects',
);
$config['g_rubrics_name'] = array(
    'es' => 'Rúbricas',
    'cat' => 'Rúbriques',
    'en' => 'Rubrics',
);
$config['g_numeric_grade_name'] = array(
    'es' => 'Numérica 0-10',
    'cat' => 'Numèrica 0-10',
    'en' => 'Numeric 0-10',
);
$config['g_users_name'] = array(
    'es' => 'Usuarios',
    'cat' => 'Usuaris',
    'en' => 'Users',
);
$config['g_students_name'] = array(
    'es' => 'Estudiantes',
    'cat' => 'Estudiants',
    'en' => 'Students',
);
$config['g_all'] = array(
    'es' => 'Todos',
    'cat' => 'Tots',
    'en' => 'All',
);
$config['g_teachers_name'] = array(
    'es' => 'Profesorado',
    'cat' => 'Professorat',
    'en' => 'Teachers',
);
$config['g_coordinator_name'] = array(
    'es' => 'Profesorado Coordinador',
    'cat' => 'Professorat Coordinador',
    'en' => 'Coordinator Teacher',
);
$config['g_profile_name'] = array(
    'es' => 'Mi perfil',
    'cat' => 'El meu perfil',
    'en' => 'My profile',
);
$config['g_grades'] = array(
    'es' => 'Calificaciones',
    'cat' => 'Qualificacions',
    'en' => 'Grades',
);
$config['g_NO_grades'] = array(
    'es' => 'Sin calificar',
    'cat' => 'Sense qualificar',
    'en' => 'No marked',
);
$config['g_pdf_report'] = array(
    'es' => 'Generar Informes PDF', 
    'cat' => 'Generar Informes PDF', 
    'en' => 'Make PDF\'s reports', 
);
$config['g_create'] = array(
    'es' => 'Crear',
    'cat' => 'Crear',
    'en' => 'Create',
);
$config['g_submit'] = array(
    'es' => 'Enviar',
    'cat' => 'Enviar',
    'en' => 'Submit',
);
$config['g_define'] = array(
    'es' => 'Definir',
    'cat' => 'Definir',
    'en' => 'Define',
);
$config['g_assign'] = array(
    'es' => 'Asignar',
    'cat' => 'Assignar',
    'en' => 'Assign',
);
$config['g_next'] = array(
    'es' => 'Siguiente',
    'cat' => 'Següent',
    'en' => 'Next',
);
$config['g_back'] = array(
    'es' => 'Anterior',
    'cat' => 'Anterior',
    'en' => 'Previous',
);
$config['g_return'] = array(
    'es' => 'Volver',
    'cat' => 'Tornar',
    'en' => 'Go Back',
);
$config['g_edit'] = array(
    'es' => 'Editar',
    'cat' => 'Editar',
    'en' => 'Edit',
);
$config['g_update'] = array(
    'es' => 'Actualizar',
    'cat' => 'Actualitzar',
    'en' => 'Update',
);
$config['g_resetPassword'] = array(
    'es' => 'Cambiar contraseña alumnado',
    'cat' => 'Canviar contrasenya alumnat',
    'en' => 'Change student\'s password',
);
$config['g_assignTechnicalSkills'] = array(
    'es' => 'Asignar Tareas',
    'cat' => 'Assignar Tasques',
    'en' => 'Assign Tasks',
);
$config['g_updateSprints'] = array(
    'es' => 'Actualizar Fechas de Sprints',
    'cat' => 'Actualitzar Dates d\'Sprints',
    'en' => 'Update Sprint Dates',
);
$config['g_defineGroups'] = array(
    'es' => 'Definir Equipos',
    'cat' => 'Definir Equips',
    'en' => 'Establish Teams',
);
$config['g_showGroups'] = array(
    'es' => 'Ver Equipos',
    'cat' => 'Vore Equips',
    'en' => 'Show Teams',
);
$config['g_finish'] = array(
    'es' => 'Finalizar',
    'cat' => 'Finalitzar',
    'en' => 'End',
);
$config['g_delete'] = array(
    'es' => '¡Borra definitivamente!',
    'cat' => 'Esborrar definitivament!',
    'en' => 'Delete permanently!',
);
$config['g_cancel'] = array(
    'es' => 'Cancelar',
    'cat' => 'Cancel·lar',
    'en' => 'Cancel',
);
$config['g_show'] = array(
    'es' => 'Ver',
    'cat' => 'Vore',
    'en' => 'Show',
);
$config['g_assess'] = array(
    'es' => 'Valoración',
    'cat' => 'Valoració',
    'en' => 'Assess',
);
$config['g_soft_assess'] = array(
    'es' => 'Valoración de habilidades transversales',
    'cat' => 'Valoració d\'habilitats transversals',
    'en' => 'Soft Skills assessments',
);
$config['g_technical_assess'] = array(
    'es' => 'Valoración de tareas técnicas',
    'cat' => 'Valoració de tasques tècniques',
    'en' => 'Hard Skills assessments',
);

$config['g_technical_skills_percentage'] = array(
    'es' => 'Porcentaje',
    'cat' => 'Percentatge',
    'en' => 'Percentage',
);
$config['g_modify'] = array(
    'es' => 'Modificar',
    'cat' => 'Modificar',
    'en' => 'Modify',
);
$config['g_search'] = array(
    'es' => 'Buscar',
    'cat' => 'Buscar',
    'en' => 'Search',
);
$config['g_chPassword'] = array(
    'es' => 'Cambiar la contraseña',
    'cat' => 'Canviar la contrasenya',
    'en' => 'Change password',
);
$config['g_team'] = array(
    'es' => 'Equipo',
    'cat' => 'Equip',
    'en' => 'Team',
);
$config['g_year_name'] = array(
    'es' => 'Curso escolar',
    'cat' => 'Curs escolar',
    'en' => 'School Year',
);
$config['g_ts_select_error'] = array(
    'es' => 'Escoge al menos una tarea',
    'cat' => 'Tria al menys una tasca',
    'en' => 'Select one task at least',
);


// Rubricas
$config['g_name'] = array(
    'es' => 'Nombre',
    'cat' => 'Nom',
    'en' => 'Name',
);
$config['g_description'] = array(
    'es' => 'Descripción (opcional)',
    'cat' => 'Descripció (opcional)',
    'en' => 'Description (not mandatory)',
);
$config['g_cols'] = array(
    'es' => 'Columnas',
    'cat' => 'Columnes',
    'en' => 'Columns',
);
$config['g_rows'] = array(
    'es' => 'Filas',
    'cat' => 'Files',
    'en' => 'Rows',
);
$config['g_qualifications_name'] = array(
    'es' => 'Calificación',
    'cat' => 'Qualificació',
    'en' => 'Grade',
);
$config['g_qualifications'] = array(
    'es' => 'Calificación Máxima',
    'cat' => 'Qualificació Màxima',
    'en' => 'Max Grade',
);
$config['g_course'] = array(
    'es' => '¿Para qué curso? (Elige, al menos, uno)',
    'cat' => 'Per a quin curs? (Tria, al menys un)',
    'en' => 'Which course for? (Choose at least one)',
);
$config['g_for_teachers'] = array(
    'es' => '¿Qué profesores podrán verla?',
    'cat' => 'Quins professors podran vore-la?',
    'en' => 'What teachers will see this rubric?',
);


// RETOS
$config['g_ch_fechainicio'] = array(
    'es' => 'Inicio',
    'cat' => 'Inici',
    'en' => 'Start',
);
$config['g_ch_fechafin'] = array(
    'es' => 'Fin',
    'cat' => 'Acabament',
    'en' => 'End',
);
$config['g_ch_teachers'] = array(
    'es' => 'Profesorado implicado',
    'cat' => 'Professorat implicat',
    'en' => 'Involved Teachers',
);
$config['g_ch_students'] = array(
    'es' => 'Alumnado implicado',
    'cat' => 'Alumnatat implicat',
    'en' => 'Involved Students',
);
$config['g_ch_levels'] = array(
    'es' => 'Cursos',
    'cat' => 'Cursos',
    'en' => 'Levels',
);
$config['g_ch_subject'] = array(
    'es' => 'Asignatura/Módulos',
    'cat' => 'Assignatura/Mòdul',
    'en' => 'Subject',
);
$config['g_ch_self_assessment'] = array(
    'es' => 'AUTO Evaluación del ',
    'cat' => 'AUTO Avaluació de l\'',
    'en' => 'SELF Assessment of ',
);
$config['g_ch_peer_assessment'] = array(
    'es' => 'CO Evaluación del ',
    'cat' => 'CO Avaluació de l\'',
    'en' => 'PEER Assessment of ',
);
$config['g_ch_comments'] = array(
    'es' => 'Comentarios',
    'cat' => 'Comentaris',
    'en' => 'Comments',
);
$config['g_ch_self_comments'] = array(
    'es' => 'Comentarios de',
    'cat' => 'Comentaris de',
    'en' => 'Comments of',
);
$config['g_ch_my_self_comments'] = array(
    'es' => 'Mis comentarios:',
    'cat' => 'Els meus comentaris:',
    'en' => 'My comments:',
);
$config['g_ch_teacher_comments'] = array(
    'es' => 'Profesorado:',
    'cat' => 'Professorat:',
    'en' => 'Teachers:',
);
$config['g_ch_team_comments'] = array(
    'es' => 'Compañer@s de equipo:',
    'cat' => 'Companys/es d\'equip:',
    'en' => 'Teammates:',
);
$config['g_ch_peer_comments'] = array(
    'es' => 'Comenarios para',
    'cat' => 'Comentaris per a',
    'en' => 'Comments for',
);
$config['g_ch_assessment'] = array(
    'es' => 'Valoración del',
    'cat' => 'Valoració de l\'',
    'en' => 'Assessment for',
);
$config['g_ch_sprints_openclose'] = array(
    'es' => 'Abrir - Cerrar Sprints',
    'cat' => 'Obrir - Tancar Sprints',
    'en' => 'Open - Close Sprints',
);
$config['g_ch_sprints_openclose_advise'] = array(
    'es' => 'Marca el/los Sprints a valorar',
    'cat' => 'Marca el/els Sprints a valorar',
    'en' => 'Check the Sprints to Assess',
);
$config['g_ch_assign_ts_text'] = array(
    'es' => 'Escoge las Tareas a asignar',
    'cat' => 'Tria les Tasques a assignar',
    'en' => 'Check the Tasks to Assign',
);
$config['g_ch_operation_ok'] = array(
    'es' => 'Operación realizada correctamente',
    'cat' => 'Operació realitzata correctament',
    'en' => 'Operation done',
);


