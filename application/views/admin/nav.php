        <nav class="sticky top-0 w-full flex items-center justify-between flex-wrap bg-teal-500 p-3 -mt-2">
            <div class="flex items-center flex-no-shrink text-white mr-6">
                <a class="font-bold text-2xl tracking-tight" href="<?=base_url('/')?>" title="<?= $g_appname_desc ?>"><?= $g_appname ?></a>
            </div>
 
            <label for="menu-toggle" class="pointer-cursor md:hidden block mt-2">
                <svg class="fill-current text-teal-100" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title>Menú</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                </svg>
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle" />

            <div id="menu" class="w-full flex-grow sm:flex items-center sm:w-auto hidden">
                <div class="justify-start flex-grow sm:flex">
                    <!-- Users: create, delete, ...  -->
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="firstlevel font-bold whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="#">
                                <?=$g_users_name[$g_applang]?>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users')?>">
                                    <?=$g_show[$g_applang]?> <?=$g_users_name[$g_applang]?>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/createStudent')?>">
                                    <?=$g_create[$g_applang]?> <?=$g_students_name[$g_applang]?>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/createTeacher')?>">
                                    <?=$g_create[$g_applang]?> <?=$g_teachers_name[$g_applang]?>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/createTeacherCoord')?>">
                                    <?=$g_create[$g_applang]?> <?=$g_coordinator_name[$g_applang]?>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/import')?>">
                                    <?=$g_import[$g_applang]?> <?=$g_users_name[$g_applang]?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Rubrics: create, asses  -->
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="#">
                                <?=$g_rubrics_name[$g_applang]?>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/rubrics')?>">
                                    <?=$g_show[$g_applang]?> <?=$g_rubrics_name[$g_applang]?>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/rubrics/create')?>">
                                    <?=$g_create[$g_applang]?> <?=$g_rubrics_name[$g_applang]?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Challenges: create, asses  -->
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="#">
                                <span class="firstlevel"><?=$g_challenges_name[$g_applang]?></span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/challenges')?>">
                                        <span class=""><?=$g_show[$g_applang]?> <?=$g_challenges_name[$g_applang]?></span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/challenges/create')?>">
                                        <span class=""><?=$g_create[$g_applang]?> <?=$g_challenges_name[$g_applang]?></span>
                                    </a>
                                </li>
                                <!-- <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/challenges_grades')?>">
                                        <span class=""><?=$g_grades[$g_applang]?> <?=$g_challenges_name[$g_applang]?></span>
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                    <!-- Technical Skills: create, delete,...  -->
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="#">
                                <span class="firstlevel"><?=$g_technical_skills_name[$g_applang]?></span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/technical_skills')?>">
                                        <span class=""><?=$g_show[$g_applang]?> <?=$g_technical_skills_name[$g_applang]?></span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/technical_skills/create')?>">
                                        <span class=""><?=$g_create[$g_applang]?> <?=$g_technical_skills_name[$g_applang]?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Global Projects: create, asses  -->
                    <!-- <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="<?=base_url('teachers/projects')?>">
                                <span class="firstlevel"><?=$g_projects_name[$g_applang]?></span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/projects')?>">
                                        <span class=""><?=$g_view[$g_applang]?> <?=$g_projects_name[$g_applang]?></span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/projects/create')?>">
                                        <span class=""><?=$g_create[$g_applang]?> <?=$g_projects_name[$g_applang]?></span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/projects/assess')?>">
                                        <span class=""><?=$g_assess[$g_applang]?> <?=$g_projects_name[$g_applang]?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul> -->

                    <!-- Profile: change password  -->
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="#">
                                <span class="firstlevel"><?=$g_profile_name[$g_applang]?></span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <!--<li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/dashboard')?>">
                                        <span class=""><?=$g_profile_name[$g_applang]?></span>
                                    </a>
                                </li>//-->
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/dashboard/editPassword')?>">
                                        <span class=""><?=$g_chPassword[$g_applang]?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
                <!-- Right side of the nav bar  -->
                <div class="justify-end flex-grow sm:flex">
                    <!-- Dropdown: Year of the Challenges -->
                    <div x-data="{ show: false }"  @click.away="show = false" class="mb-2 mr-2">
                        <button @click="show = ! show" type="button" class="flex bg-blue-600 text-gray-200 rounded-lg px-3 py-2 focus:outline-none focus:border-white text-sm">
                        <span class="pr-0 text-lg"><?= $g_year_name[$g_applang].' '.$school_year ?></span> 
                            <svg class="fill-current text-gray-100" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M7 10l5 5 5-5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        </button>
                        <div x-show="show" class="absolute bg-gray-100 z-10 shadow-md rounded" style="min-width:8rem">
                            <?php foreach($g_school_year as $item): ?>
                                <a class="block px-3 py-2" href="<?= base_url('changeSchoolYear/').$item ?>"><?= $item ?></a>
                            <?php endforeach; ?>

                        </div>
                        <!-- Guardar la URL actual para volver en caso de cambiar de año -->
                        <?php $this->session->set_userdata('previous_url', current_url()); ?>
                    </div>

                    <!-- Búsqueda - Search -->
<!--                    <form class="mr-2 md:mr-4 " href="#">//-->
                    <!-- <form class="mr-2 md:mr-4 relative" href="#">
                        <input class="bg-grey-lightest border-2 focus:outline-none focus:border-blue p-2 rounded-lg shadow-inner h-8 w-40" placeholder="Buscar" type="search" name="search" >
                        <button type="submit" class="absolute right-0 top-0 mt-2 mr-4">
                            <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                                 viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve"
                                 width="512px" height="512px">
                                <path
                                      d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                            </svg>
                        </button>
                    </form> -->
                    <a href="<?=base_url('login/logout')?>" class="inline-block text-lg px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 mb-2 md:mt-0 md:mb-0">
                        <?=$g_exit[$g_applang]?>
                    </a>
                </div>


            </div>
        </nav>