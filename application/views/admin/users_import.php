<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <div class="justify-center">
        <div class="max-w-md mx-auto m-4">
            <label class="block text-center text-2xl py-3 px-3 mb-2 text-gray-200 font-bold rounded bg-blue-400 hover:bg-blue-600"> 
                Importar usuarios
            </label>
        </div>

        <div class="max-w-md mx-auto mt-12 m-2">
            <form action="<?=base_url('admin/users/importUsers')?>" method="POST" class="flex flex-wrap bg-gray-100 -my-3 mx-2 p-2 text-center" enctype="multipart/form-data">
            <div class="flex-col justify-around md:mt-4">
                    <input type="file" name="file" id="file" class="appearance-none mb-8"/>
                    <input type="submit" name="importSubmit" value="Importar" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-400 hover:bg-blue-600"/>
            </div>
            </form>
        </div>
    </div>

</div>