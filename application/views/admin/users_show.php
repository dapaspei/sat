<div class="w-full mx-auto shadow-md bg-grey-200">
    <div class="bg-white mx-auto shadow-md rounded my-2 pb-0 mb-2">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
            <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-2 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <div class="flex flex-wrap w-full">
        <div class="flex flex-wrap p-1"> 
            <?php foreach($data as $item): ?>
                <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col p-1">
                    <div class="bg-white border-t rounded-lg shadow-lg overflow-hidden flex-1 flex flex-wrap">
                        <div class="px-1 md:px-0 w-full flex md:mr-2 md:flex-1">
                            <img class="h-32 w-32 md:h-24 md:w-24 rounded-full border-4 border-grey-900 ml-2 mr-2 md:ml-1 md:mr-1" src="<?= base_url('assets/img/user.svg'); ?>" 
                                alt="Foto del usuario">
                            <div class="flex flex-col items-center ml-4 md:ml-1">
                                <a href="<?=base_url('admin/users/edit/'.$item->id);?>" class="inline-block text-2xl md:text-lg py-1 px-3 mb-2 mt-2 text-white font-bold rounded bg-blue-400 hover:bg-blue-600">
                                    Editar</a>
                                <a href="<?=base_url('admin/users/delete/'.$item->id);?>" class="inline-block text-2xl md:text-lg text-white font-bold px-3 py-1 rounded bg-red-400 hover:bg-red-600">
                                    Borrar</a>
                            </div>
                        </div>
                        <div class="px-1 w-full md:w-1/2 flex md:flex-col border-t">
                            <div class="flex-1 text-sm text-center py-2 md:py-1 px-2 md:ml-2"><?= $item->username ?></div>
                            <div class="flex-1 text-sm text-center py-2 md:py-0 px-2"><?= $item->role ?></div>
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><?= $item->email ?></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?= $this->pagination->create_links(); ?>

</div>