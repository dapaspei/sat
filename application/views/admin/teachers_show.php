<!-- <h1 class="text-center">Listado de Profesores</h1> -->
<div class="table-responsive">
    <!-- Muestra los mensajes de estado -->
    <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="alert alert-success text-center" role="alert">
        <?= $msg ?>
        </div>
    <?php endif; ?>
    <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="alert alert-error text-center" role="alert">
        <?= $msg ?>
        </div>
    <?php endif; ?>
    <table class="table table-responsive-sm">
    <thead>
        <tr>
        <th scope="col">Usario</th>
        <th scope="col">Nombre</th>
        <th scope="col">Apellidos</th>
        <th scope="col">E-Mail</th>
        <th scope="col">Role</th>
        <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($data as $item): ?>
            <tr>
            <th scope="row"><?= $item->username ?></th>
            <td><?= $item->firstname ?></td>
            <td><?= $item->lastname ?></td>
            <td><?= $item->email ?></td>
            <td><?= $item->role ?></td>
            <td><a class="btn btn-warning" href="<?=base_url('teachers/edit/'.$item->id);?>" role="button">Editar</a> 
                <a class="btn btn-danger" href="<?=base_url('teachers/delete/'.$item->id);?>" role="button">Borrar</a>
            </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <?= $this->pagination->create_links(); ?>

</div>