<div class="w-full mx-auto h-screen shadow-md bg-teal-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('admin/users/updateStudent')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
        <input type="hidden" value="<?= set_value('id', isset($user['id']) ? $user['id'] : '');?>" name="id">

        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Nombre
                </label>
                <input type="text" name="firstname" value="<?= set_value('firstname', isset($user['firstname']) ? $user['firstname'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                <?php echo form_error('firstname'); ?>
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Apellidos
                </label>
                <input type="text" name="lastname" value="<?= set_value('lastname', isset($user['lastname']) ? $user['lastname'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">                
                    <?php echo form_error('lastname'); ?>
            </div>
        </div>
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Nombre de usuario
                </label>
                <input type="text" name="username" value="<?= set_value('username', isset($user['username']) ? $user['username'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                <?php echo form_error('username'); ?>
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    E-mail
                </label>
                <input type="text" name="email" value="<?= set_value('email', isset($user['email']) ? $user['email'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">                
                    <?php echo form_error('email'); ?>
            </div>
        </div>
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Contraseña
                </label>
                <input type="password" name="password" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="********">
                <?php echo form_error('password'); ?>
            </div>
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Confirmar Contraseña
                </label>
                <input type="password" name="password_confirm" value="<?php echo set_value('password_confirm'); ?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="********">
                    <div><?php echo form_error('password_confirm'); ?></div>
            </div>
        </div>
        <?php 
        // var_dump("<br>");
        // var_dump("cuantos elementos hay:".count($user['teaching_levels']));
        // var_dump($user['teaching_levels']);

        // if (($user['teaching_levels']) === '3-ESO')
        //     var_dump("Es igual!!!!!");
        // else 
        //     var_dump("Son Diferentes!!!!!");
        ?>
        <div class="w-full justify-center -mx-6 md:mx-0 mb-2">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Selecciona un curso
            </label>
            <select size="10" name="teaching_levels" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                <?php
                    for($i=0; $i<count($g_levels); $i++) {
                        if($user['teaching_levels'][0] === $g_levels[$i]) echo '<option value="'.$g_levels[$i].'" selected>'.$g_levels[$i].'</option>\n';
                        else echo '<option value="'.$g_levels[$i].'" >'.$g_levels[$i].'</option>\n';
                    }
                ?>
            </select>
        </div>
        <div class="flex justify-around">
            <input type="submit" name="submit" value="Actualizar" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-400 hover:bg-blue-600" >
            <input type="submit" name="submit" value="Cancelar" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded text-xs bg-red-400 hover:bg-red-600">
        </div>
    </form>