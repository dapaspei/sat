<div class="w-full mx-auto h-screen shadow-md bg-teal-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('admin/users/update')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
        <input type="hidden" value="<?= set_value('id', isset($user['id']) ? $user['id'] : '');?>" name="id">
        <input type="hidden" value="<?= set_value('role', isset($user['role']) ? $user['role'] : '');?>" name="role">

        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Nombre
                </label>
                <input type="text" name="firstname" value="<?= set_value('firstname', isset($user['firstname']) ? $user['firstname'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                <?php echo form_error('firstname'); ?>
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Apellidos
                </label>
                <input type="text" name="lastname" value="<?= set_value('lastname', isset($user['lastname']) ? $user['lastname'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">                
                    <?php echo form_error('lastname'); ?>
            </div>
        </div>
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Nombre de usuario
                </label>
                <input type="text" name="username" value="<?= set_value('username', isset($user['username']) ? $user['username'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                <?php echo form_error('username'); ?>
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    E-mail
                </label>
                <input type="text" name="email" value="<?= set_value('email', isset($user['email']) ? $user['email'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">                
                    <?php echo form_error('email'); ?>
            </div>
        </div>
        <div class="flex flex-wrap -mx-3 mb-6">
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Contraseña
                </label>
                <input type="password" name="password" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-grey-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="********">
                <?php echo form_error('password'); ?>
            </div>
            <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Coonfirmar Contraseña
                </label>
                <input type="password" name="password_confirm" value="<?php echo set_value('password_confirm'); ?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="********">
                    <div><?php echo form_error('password_confirm'); ?></div>
            </div>
            <!-- No permitimos el cambio de rol, de momento -->
            <!-- <div class="w-full md:w-1/2 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Rol
                </label>
                <div class="relative w-full">
                    <select name="role" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option value="">Elige un rol</option>
                            <option <?= isset($user['role']) == 'admin' ? 'selected' : ''; ?> value="admin">Administrador</option>
                            <option <?= isset($user['role']) == 'teacher' ? 'selected' : ''; ?> value="teacher">Profesorado</option>
                            <option <?= isset($user['role']) == 'student' ? 'selected' : ''; ?> value="student">Estudiante</option>
                        </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
            </div> -->
        </div>
        <?php 
        // var_dump("<br>");
        // var_dump("cuantos elementos hay:".count($user['teaching_levels']));
        // var_dump($user['teaching_levels'][0]);
        // var_dump($user['teaching_levels'][1]);

        // if (($user['teaching_levels']) === '3-ESO')
        //     var_dump("Es igual!!!!!");
        // else 
        // var_dump("Son Diferentes!!!!!");
        //var_dump("elementos de la lista: ". count($level_tmp));
        ?>
        <!-- <div class="flex flex-1 -mx-3 mb-6">
            <div class="w-full md:max-w-lg justify-center md:mx-0 mb-2 md:mb-6"> -->
            <div class="w-full justify-center -mx-6 md:mx-0 mb-2">

                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Selecciona (al menos) un curso
                </label>
                <?php  ?>
                <select multiple size="10" name="levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    <?php
                        $level_tmp = ['1-ESO', '2-ESO', '3-ESO', '4-ESO', '1-BAT', '2-BAT', '1-CFGM', '2-CFGM', '1-CFGS', '2-CFGS'];
                        for($i=0; $i<count($level_tmp); $i++) {
                            $selected = false; 
                            for($j=0; $j<count($user['teaching_levels']); $j++) {
                                if($user['teaching_levels'][$j] === $level_tmp[$i]) {
                                    $selected = true;
                                    break;
                                }
                            }
                            if ($selected) echo '<option value="'.$level_tmp[$i].'" selected>'.$level_tmp[$i].'</option>\n';
                            else echo '<option value="'.$level_tmp[$i].'" >'.$level_tmp[$i].'</option>\n';
                        }
                    ?>
                </select>
                <div><?php echo form_error('levels'); ?></div>
            </div>
        <!-- </div> -->
        <div class="flex justify-around">
            <input type="submit" name="submit" value="Actualizar" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-400 hover:bg-blue-600" >
            <input type="submit" name="submit" value="Cancelar" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded text-xs bg-red-400 hover:bg-red-600">
        </div>
        
    </form>

</!-->