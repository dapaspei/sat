        <nav class="flex items-center justify-between flex-wrap bg-teal-500 p-3">
            <div class="flex items-center flex-no-shrink text-white mr-6">
                <a class="font-bold text-2xl tracking-tight" href="<?=base_url('/')?>" title="<?= $g_appname_desc ?>"><?= $g_appname ?></a>
            </div>
 
            <label for="menu-toggle" class="pointer-cursor md:hidden block mt-2">
                <svg class="fill-current text-teal-100" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title>Menú</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                </svg>
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle" />

            <div id="menu" class="w-full flex-grow sm:flex items-center sm:w-auto hidden">
                <div class="justify-start flex-grow sm:flex">
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="<?=base_url('rubrics')?>">
                                Rúbricas
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('rubrics/create')?>">
                                        Crear una Rúbrica
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="<?=base_url('teachers/challenges')?>">
                                <span class="firstlevel">Retos</span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/challenges/create')?>">
                                        <span class="">Crear un Reto</span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/challenges/assess')?>">
                                        <span class="">Valorar un Reto</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="<?=base_url('teachers/projects')?>">
                                <span class="firstlevel">Proyectos Globales</span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/projects/create')?>">
                                        <span class="">Crear un P. Glboal</span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('teachers/projects/assess')?>">
                                        <span class="">Valorar un P. Gobal</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="<?=base_url('admin/users')?>">
                                <span class="firstlevel">Usuarios</span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/createStudent')?>">
                                        <span class="">Crear Estudiante</span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/createTeacher')?>">
                                        <span class="">Crear Profesor/a</span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/createTeacherCoord')?>">
                                        <span class="">Crear Prof. Coordinador/a</span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('admin/users/import')?>">
                                        <span class="">Importar Usuarios</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul> -->
                    
                </div>
                <div class="justify-end flex-grow sm:flex">
                    <form class="mr-2 md:mr-4">
                        <input class="bg-grey-lightest border-2 focus:border-blue p-2 rounded-lg shadow-inner h-8 w-40" placeholder="Buscar" type="text">
                        <button type="submit" class="hidden">Submit</button>
                    </form>
                    <a href="<?=base_url('login/logout')?>" class="inline-block text-lg px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 mb-2 md:mt-0 md:mb-0">
                        Salir
                    </a>
                </div>
            </div>
        </nav>