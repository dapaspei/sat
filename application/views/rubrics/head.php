<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Para hacer que se vea bien en los móviles -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title><?= $g_appname ?> - <?= $g_rubricas_name[$g_applang] ?></title>
        <!-- Framework Tailwind CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/cotesavaluapp.css">
        <style>
            /* NavBAR Responsive de Tailwind */
            #menu-toggle:checked + #menu {
                display: block;
            }
            /* Marcar los radiobuttons con color bg-teal-600 */
            input:checked + label {
                background-color: #319795;
            }
            /* Responsive Navbar for TailwindCSS */
            .group:hover .group-hover\:block {
                display: block !important;
            }
        </style>

    </head>
    <body>