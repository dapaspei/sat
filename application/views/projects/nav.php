        <nav class="flex items-center justify-between flex-wrap bg-teal-500 p-3">
            <div class="flex items-center flex-no-shrink text-white mr-6">
                <a class="font-bold text-3xl tracking-tight" href="<?=base_url('/')?>">CAT</a>
            </div>

            <label for="menu-toggle" class="pointer-cursor md:hidden block mt-2">
                <svg class="fill-current text-teal-100" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title>Menú</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                </svg>
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle" />

            <div id="menu" class="w-full flex-grow sm:flex items-center sm:w-auto hidden">
            	<div class="text-2xl md:text-lg flex-grow md:w-1/3 mr-0">
                    <a href="<?=base_url('challenges/create')?>" class="no-underline border-b-2 border-transparent hover:border-white font-bold block mt-4 sm:inline-block sm:mt-0 text-teal-100 hover:text-white mr-4">
            		Crear
            		</a>
                    <!-- <a href="<?=base_url('challenges/import')?>" class="no-underline border-b-2 border-transparent hover:border-white font-bold block mt-4 sm:inline-block sm:mt-0 text-teal-100 hover:text-white mr-4">
            		Importar
            		</a>                     -->
                    <a href="<?=base_url('challenges/assess')?>" class="no-underline border-b-2 border-transparent hover:border-white font-bold block mt-4 sm:inline-block sm:mt-0 text-teal-100 hover:text-white mr-4">
            		Valorar
            		</a> 
            	</div>                    
                <!-- Search field -->
                <div class="text-base flex-grow w-full mt-4 md:mt-0 md:w-1/3 md:-ml-16 md:mr-0">
                    <!-- <form class="mb-0 w-full md:mb-0 md:w-1/4"> -->
                    <form>
                        <!-- <label class="hidden" for="search-form">Search</label> -->
                        <input class="bg-grey-lightest border-2 focus:border-orange p-2 rounded-lg shadow-inner h-8 w-40" placeholder="Buscar" type="text">
                        <button type="submit" class="hidden">Submit</button>
                    </form>
                </div>
                <!-- END Search field -->
            	<div class="text-lg md:text-base flex-grow md:w-1/3 md:ml-0">
                    <a href="<?=base_url('admin/users')?>" class="no-underline border-b-2 border-transparent hover:border-white font-bold block mt-4 sm:inline-block sm:mt-0 text-teal-100 hover:text-white mr-4">
            		Usuarios
            		</a>
                    <a href="<?=base_url('rubrics')?>" class="no-underline border-b-2 border-transparent hover:border-white font-bold block mt-4 sm:inline-block sm:mt-0 text-teal-100 hover:text-white mr-4">
            		Rúbricas
            		</a>
                    <a href="<?=base_url('projects')?>" class="no-underline border-b-2 border-transparent hover:border-white font-bold block mt-4 sm:inline-block sm:mt-0 text-teal-100 hover:text-white mr-4">
            		Proy. Globales
            		</a>
                </div>
            	<!-- Boton salida -->
            	<div>
                    <a href="<?=base_url('login/logout')?>" class="inline-block text-lg px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-2 mb-2 lg:mt-0">
                        Salir
                    </a>
                </div>

            </div>
        </nav>
       