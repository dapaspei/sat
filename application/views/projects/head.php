<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Para hacer que se vea bien en los móviles -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>CAT - Retos</title>
        <!-- Framework Tailwind CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/cotesavaluapp.css">
        <style>
            /* NavBAR Responsive de Tailwind */
            #menu-toggle:checked + #menu {
                display: block;
            }
            input:checked + label {
                /* background-color: red; */
                background-color: #319795;
            }
        </style>

    </head>
    <body>