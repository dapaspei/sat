<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Para hacer que se vea bien en los móviles -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/svg+xml" href="<?=base_url()?>/favicon.svg">
        <link rel="alternate icon" href="<?=base_url()?>/favicon.ico">
        <link rel="mask-icon" href="<?=base_url()?>/favicon.svg" color="#ff8a01">

        <title><?= $g_appname ?> - Login</title>
        <!-- Framework Tailwind CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/cotesavaluapp.css">
    </head>
    <body>
    	<div class="flex justify-center h-screen w-full bg-blue-100">
        <div class="mx-auto mt-10 w-full max-w-sm">
            <h1 class="text-center bg-white rounded text-xl text-gray-800 py-3 font-bold"><?= $g_appname ?> - <?= $g_appname_desc ?></h1>
            <?php echo form_open('login/validate','class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"'); ?>
                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                    Usuari
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight focus:outline-none focus:shadow-outline" name="username" id="username" type="text" placeholder="nom d'usuari" value="<?php echo set_value('username'); ?>">
                <div><?php echo form_error('username'); ?></div>
                </div>
                <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                    Contrasenya
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-1 leading-tight focus:outline-none focus:shadow-outline" name="password" id="password" type="password" placeholder="********" value="<?php echo set_value('password'); ?>">
                <div><?php echo form_error('password'); ?></div>
                <?php if(isset($errors)): ?>
                    <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert"><?=$errors?></div>
                <?php endif ?>
                </div>
                <div class="flex justify-center">
                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Entrar
                </button>
                </div>
            </form>
        </div>
        </div>
    </body>
</html>
 