<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Para hacer que se vea bien en los móviles -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Registro de usuarios</title>
        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
        <!-- Framework Tailwind CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/cotesavaluapp.css">
    </head>
    <body>
        <nav class="flex items-center justify-between flex-wrap bg-teal-500 p-4">
            <div class="flex items-center flex-shrink-0 text-white mr-6">
                <a class="font-semibold text-xl tracking-tight" href="<?=base_url('dashboard')?>">CotApp</a>
            </div>
            <div class="block sm:hidden">
                <button class="navbar-burger flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
                    <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
                </button>
            </div>
            <div id="main-nav" class="w-full flex-grow sm:flex items-center sm:w-auto hidden">
                <!--
                <div class="text-sm sm:flex-grow">
                    <a href="<?=base_url('login')?>" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0">
                        Iniciar sesión
                    </a>
                </div>
                -->
                <div class="text-sm sm:flex-grow"> 
	                <h2 class="block text-gray-700 text-sm font-bold mb-2 text-center">Registro de usuarios</h2>
                </div>
                <div>
                    <a href="<?=base_url('login/logout')?>" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0">
                        Cerrar sesión
                    </a>
                
                </div>
            </div>
        </nav>


        <div class="mx-auto mt-4 w-full max-w-xl">
        <!-- Mostrar errores sin sesión -->
            <?php if(isset($error_msg)): ?>
                <div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">
                    <?= $error_msg ?>
                </div>
            <?php endif; ?>
            <!-- Mostrar confirmación del registro -->
            <?php if(isset($success_msg)): ?>
                <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">
                    <?= $success_msg ?>
                </div>
            <?php endif; ?>
            <!--<h2 class="block text-gray-700 text-sm font-bold mb-2 text-center">Registro de usuarios</h2>-->
            <div class="mx-auto w-full bg-white shadow-md rounded px-4 pt-2 pb-2 mb-3 mt-3">
            <?php echo form_open('register/create'); ?>
                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="firstame">
                    Nombre
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="firstname" id="firstname" type="text" placeholder="Nombre" value="<?php echo set_value('firstname'); ?>">
                <div><?php echo form_error('firstname'); ?></div>
                </div>

                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="lastname">
                    Apellidos
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="lastname" id="lastname" type="text" placeholder="Apellidos" value="<?php echo set_value('lastname'); ?>">
                <div><?php echo form_error('lastname'); ?></div>
                </div>

                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                    Nombre de usuario
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="username" id="username" type="text" placeholder="Usuario" value="<?php echo set_value('username'); ?>">
                <div><?php echo form_error('username'); ?></div>
                </div>

                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                    Correo electrónico
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="email" id="email" type="text" placeholder="fulanito@iescotesbaixes.org" value="<?php echo set_value('email'); ?>">
                <div><?php echo form_error('email'); ?></div>
                </div>

                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                    Contraseña
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" name="password" id="password" type="password" placeholder="********" value="<?php echo set_value('password'); ?>">
                <div><?php echo form_error('password'); ?></div>
                <?php if(isset($errors)): ?>
                    <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert"><?=$errors?></div>
                <?php endif ?>
                </div>

                <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="password_confirm">
                    Confirma la contraseña
                </label>
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" name="password_confirm" id="password_confirm" type="password" placeholder="Repite la contraseña" value="<?php echo set_value('password_confirm'); ?>">
                <div><?php echo form_error('password_confirm'); ?></div>
                <?php if(isset($errors)): ?>
                    <div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert"><?=$errors?></div>
                <?php endif ?>
                </div>

				<div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                    Rol del Usuario
                </label>
                <select name="role" id="role" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        <option value="">Selecciona el rol</option>
                        <option <?= set_value('role') == 'admin' ? 'selected' : ''; ?> value="admin">Administrador</option>
                        <option <?= set_value('role') == 'teacher' ? 'selected' : ''; ?> value="teacher">Profesorado</option>
                        <option <?= set_value('role') == 'student' ? 'selected' : ''; ?> value="student">Estudiante</option>
                    </select>
                </div>


                <div class="flex justify-center">
                <!-- Para dos botones -->
                <!-- <div class="flex items-center justify-between"> -->
                <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Registrar
                </button>
                <!-- <a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="#">
                    Forgot Password?
                </a> -->
                </div>
            </form>
            </div>
        </div>
    </body>

  
    <!-- -->
    
        <script>
        
        // Navbar Toggle
        document.addEventListener('DOMContentLoaded', function () {

        // Get all "navbar-burger" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

                // Add a click event on each of them
                $navbarBurgers.forEach(function ($el) {
                $el.addEventListener('click', function () {

                    // Get the "main-nav" element
                    var $target = document.getElementById('main-nav');

                    // Toggle the class on "main-nav"
                    $target.classList.toggle('hidden');

                });
                });
            }

        });
        		
        //Javascript to toggle the menu
		// document.getElementById('navbar-burguer').onclick = function(){
		// 	document.getElementById("main-nav").classList.toggle("hidden");
		// }
        </script>
</html>
