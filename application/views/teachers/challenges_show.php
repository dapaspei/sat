<div class="w-full mx-auto shadow-md bg-grey-200">
    <div class="bg-white mx-auto shadow-md rounded my-2 pb-0 mb-2">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-2 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <div class="flex flex-wrap w-full">
        <div class="flex flex-wrap p-1"> 
            <?php foreach($data as $item): ?>
                <?php 
                    $sprints_openfor_assess = $item['sprints_openfor_assess'];
                    // Buscamos si algún sprint está activo para valorar.
                    $isopen = strpos($sprints_openfor_assess, '1');
                ?>
                <div class="w-full md:w-1/3 flex flex-col p-1">
                    <div class="bg-white border-t rounded-lg shadow-lg overflow-hidden flex-1 flex flex-wrap">
                        <!-- Título + Boton de Sprints -->
                        <div class="px-1 md:px-0 w-full flex md:mr-2">
                            <a href="<?=base_url('teachers/challenges/edit/'.$item['id']);?>" class="w-full inline-block align-middle border-2 rounded-lg bg-blue-100 font-semibold text-blue-600 text-lg text-center py-1 md:py-1 px-0 md:ml-0">
                                <label>
                                    <?= $item['name'] ?>
                                </label>
                            </a>                            
                            <div class="flex items-center mt-1 mb-1 ml-1">
                                <a href="<?=base_url('teachers/challenges/switchSprints/'.$item['id']);?>" class="flex text-lg md:text-lg py-4 px-2 mr-0 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 58 58">
                                    <path d="M45 26H13C5.85 26 0 20.15 0 13S5.85 0 13 0h32c7.15 0 13 5.85 13 13s-5.85 13-13 13zM13 58h32c7.15 0 13-5.85 13-13s-5.85-13-13-13H13C5.85 32 0 37.85 0 45s5.85 13 13 13z" fill="#e7eced"/><circle cx="45" cy="13" r="9" fill="#88c057" stroke="#659c35" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10"/><circle cx="13" cy="45" r="9" fill="#ed7161" stroke="#d75a4a" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10"/>
                                </svg>
                                <span class="inline-block text-base ml-1">Sprints</span>
                                </a>
                                
                            </div>
                            
                        </div>
                        <div class="w-full grid grid-cols-2 gap-2 place-items-auto">
                            <!-- Botón de Technical, Soft + Scores  -->
                            <div class="px-1 w-full flex items-center md:flex-row border-t ml-1">
                                <!-- TECHNICAL SKILLS -->
                                <div class="flex w-full mt-1 mb-1 ml-0 mr-2">
                                        <a href="<?= base_url('teachers/technical_skills_assessments/create/'.$item['id'])  ?>" class="inline-block text-base text-center md:text-lg text-white font-bold px-2 py-2 rounded bg-yellow-700 hover:bg-yellow-500">
                                            <?=$g_technical_assess[$g_applang]?></a>
                                </div>
                                <!-- SOFT SKILLS -->
                                <div class="flex w-full mt-1 mb-1 ml-0 mr-0">
                                        <a href="<?= $isopen ? base_url('teachers/challenges_assessments/create/'.$item['id']) : '#' ;?>" class="<?= $isopen ? '' : 'disabled opacity-25 cursor-not-allowed' ?> inline-block text-base text-center md:text-lg text-white font-bold px-2 py-2 rounded bg-teal-500 hover:bg-teal-700">
                                            <?=$g_soft_assess[$g_applang]?></a>
                                </div>
                            </div>
                            <!-- Botón de generar informes en PDF -->
                            <div class="px-1 w-full flex items-center md:flex-row border-t ml-1">
                                <!-- SCORES -->
                                <div class="flex w-full mt-1 mb-1">
                                    <a href="<?=base_url('teachers/challenges_grades/show/'.$item['id']);?>" class="inline-block mx-auto md:h-full text-sm text-center md:text-base py-2 md:py-2 px-3 text-white font-bold rounded bg-purple-500 hover:bg-purple-700"><?=$g_grades[$g_applang]?></a>
                                </div>
                                <!-- Generate PDFs -->
                                <div class="flex w-full mt-1 mb-1">
                                    <a href="<?=base_url('teachers/challenges_grades/show/'.$item['id'].'/1');?>" class="inline-block mx-auto md:h-full text-sm text-center md:text-base py-2 md:py-2 px-3 text-white font-bold rounded bg-orange-500 hover:bg-orange-700"><?=$g_pdf_report[$g_applang]?></a>
                                </div>
                            </div>
                        </div>
                        <div class="px-1 w-full flex md:flex-row border-t">
                            <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_description[$g_applang]?>:</strong> <?= $item['description'] ?></div>
                            
                        </div>
                        <div class="px-1 w-full flex md:flex-col border-t">
                            <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_subject[$g_applang]?>:</strong> <?= $item['subject'] ?></div>
                            <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_levels[$g_applang]?>:</strong> 
                            <?php
                                $levels = json_decode($item['teaching_levels'], true);
                                // Si no hay ningún curso asignado no se muestra.
                                if(!empty($levels)) {
                                    for($i=0; $i<count($levels); $i++) {
                                        if(($i+1) === count($levels)) echo $levels[$i];
                                        else echo $levels[$i].',';
                                    }    
                                }
                                
                            ?>
                            </div>
                        </div>
                        <div class="px-1 w-full md:w-1/2 flex md:flex-grow border-t">
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_fechainicio[$g_applang]?>:</strong><br> <?= date("d-m-Y", strtotime($item['start_date'])); ?></div>
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_fechafin[$g_applang]?>:</strong><br> <?= date("d-m-Y", strtotime($item['finish_date'])); ?></div>
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong><?=$g_year_name[$g_applang]?>:</strong><br> <?= $item['year'] ?></div>
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong>Sprints:</strong><br> <?= $item['sprints'] ?></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?= $this->pagination->create_links(); ?>
</div>
