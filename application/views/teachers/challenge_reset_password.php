<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/challenges/resetPasswords')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
		<input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id']: '';?>" name="challenge_id">
        <input type="hidden" value="<?= count($students); ?>" name="num_students">

        <div class="flex-col md:flex-row mt-4 ">

            <?php 
                for ($i=0; $i<count($students); $i++) {
            ?>
                <div class="w-full md:w-1/2 justify-center p-3">
                    <label class="flex justify-start items-start">
                        <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">
                        <input type="checkbox" name="students[]" value="<?=$students[$i]['id']?>" class="opacity-0 absolute" onclick="return ValidateSelection();">
                            <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
                        </div>
                        <div class="font-semibold text-gray-700 select-none"> <?= $students[$i]['firstname'].' '.$students[$i]['lastname'] ?> </div>
                    </label>
                </div>
            <?php 
                }
            ?>
   
        </div>
        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_update[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded bg-red-500 hover:bg-red-600">
        </div>
    </form>

    <style>
        input:checked + svg {
            display: block;
        }
    </style>
    <script type="text/javascript">  
        function ValidateSelection()  
        {  
            var checkboxes = document.getElementsByName("student");  
            var numberOfCheckedItems = 0;  
            for(var i = 0; i < checkboxes.length; i++)  
            {  
                if(checkboxes[i].checked)  
                    numberOfCheckedItems++;  
            }  
            // if(numberOfCheckedItems > 1)  
            // {  
            //     alert("Tria només un Sprint!");  
            //     return false;  
            // }  
        }  
    </script>

</div>
