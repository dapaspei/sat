<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/rubrics/update')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
		<input type="hidden" value="<?= isset($rubric['id']) ? $rubric['id'] : '';?>" name="id">
		<input type="hidden" value="<?= isset($rubric['rows']) ? $rubric['rows'] : '';?>" name="rows">
		<input type="hidden" value="<?= isset($rubric['cols']) ? $rubric['cols'] : '';?>" name="cols">

        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-2/3 px-3 mb-4 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?= $g_name[$g_applang] ?>
                </label>
                <input type="text" name="name" value="<?=isset($rubric['name']) ? $rubric['name'] : '';?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                    <?php echo form_error('name', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>
            </div>
            <div class="w-1/3 px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_qualifications[$g_applang]?>
                </label>
                <select <?= $isowner ? '' : 'disabled'?> name="max_value" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    <?php 
                        for($i=$g_max_value_1; $i<=$g_max_value_2; $i++) {
                            if ($i === intval($rubric['max_value']))
                                echo '<option selected value="'.$i.'">'.$i.'</option>';
                            else 
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?= $g_description[$g_applang] ?>
                </label>
                <textarea rows="3" name="description" value="<?=isset($rubric['description']) ? $rubric['description'] : '';?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= set_value('description', isset($rubric['description']) ? $rubric['description'] : '');?></textarea>
                    <?php echo form_error('description', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>
            </div>
        </div>

        <div class="w-full flex-wrap -mx-3 mt-2 mb-2">
            <div class="flex flex-col md:flex-row w-full justify-around px-3 mb-6 md:mb-0 md:mt-4">
                <div class="w-full md:w-1/2 mx-1">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_course[$g_applang]?>
                    </label>
                    <select <?= $isowner ? '' : 'disabled'?> multiple size="10" name="teaching_levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 ml-3 md:-ml-3 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($g_levels); $i++) {
                                $selected = false;
                                for($j=0; $j<count($rubric['teaching_levels']); $j++) {
                                    if($g_levels[$i] === $rubric['teaching_levels'][$j]) {
                                        $selected = true;
                                        break;
                                    }
                                }
                                if ($selected) echo '<option selected value="'.$g_levels[$i].'" >'.$g_levels[$i].' - '.$g_levels_name[$i].'</option>\n';
                                else  echo '<option value="'.$g_levels[$i].'" >'.$g_levels[$i].' - '.$g_levels_name[$i].'</option>\n';
                            }
                        ?>
                    </select>
                </div>
                <div class="w-full md:w-1/2 mx-1 mt-4 md:mt-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_for_teachers[$g_applang]?>
                    </label>
                    <select <?= $isowner ? '' : 'disabled'?> multiple size="10" name="teachers[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($teachers); $i++) {
                                $selected = false;
                                for($j=0; $j<count($rubric['t_available_for']); $j++) {
                                    if($teachers[$i]['id'] == $rubric['t_available_for'][$j]) {
                                        $selected = true;
                                        break;
                                    }
                                }
                                if ($selected) echo '<option value="'.$teachers[$i]['id'].'" selected>'.$teachers[$i]['firstname'].' '.$teachers[$i]['lastname'].'</option>\n';
                                else echo '<option value="'.$teachers[$i]['id'].'" >'.$teachers[$i]['firstname'].' '.$teachers[$i]['lastname'].'</option>\n';
                            }
                        ?>
                    </select> 
                </div>
            </div>
        </div>
        <!-- --  -->
        <div class="flex flex-wrap justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_update[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 mb-2 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_modify[$g_applang]?> Items" class="inline-block text-2xl md:text-lg py-1 px-3 mb-2 text-white font-bold rounded bg-teal-500 hover:bg-teal-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg px-3 py-1 mb-2 text-white font-bold rounded bg-red-500 hover:bg-red-700">
        </div>
        
    </form>
    <?php if ($isowner) { ?>
        <div class="flex justify-around mt-4 mb-4">
            <a href="<?=base_url('teachers/rubrics/delete/'.$rubric['id']);?>" class="inline-block text-lg md:text-lg text-white font-bold px-3 py-1 rounded bg-red-500 hover:bg-red-700">
                <?=$g_delete[$g_applang]?></a>
        </div>
    <?php } ?>
</div>