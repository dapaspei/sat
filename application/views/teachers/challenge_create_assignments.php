<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/challenges/storeAssignments')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
		<input type="hidden" value="<?= isset($challenge_id) ? $challenge_id : '';?>" name="challenge_id">
		<input type="hidden" value="<?= isset($name) ? $name : '';?>" name="name">
		<!-- <input type="hidden" value="<?= isset($teaching_levels) ? $teaching_levels : '';?>" name="teaching_levels[]"> -->

        <div class="flex flex-wrap -mx-3 mt-2 mb-2"> 
            <div class="flex flex-col md:flex-row w-full justify-around px-3 mb-6 md:mb-0 md:mt-4">
                <div class="w-full md:w-1/2 mx-1">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_students_name[$g_applang] ?>
                    </label>
                    <select multiple size="10" name="students[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php foreach($students as $item): ?>
                            <option value="<?= $item['id'] ?>"><?= $item['firstname'].' '.$item['lastname']. ' ('.$item['teaching_levels'].')' ?></option>
                        <?php endforeach; ?>

                    </select>
                </div>
                <div class="w-full md:w-1/2 mx-1">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_teachers_name[$g_applang] ?>
                    </label>
                    <select multiple size="10" name="teachers[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php foreach($teachers as $item): ?>
                            <option value="<?= $item['id'] ?>"><?= $item['firstname'].' '.$item['lastname'] ?></option>

                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="flex w-full justify-around px-3 mb-6 md:mb-0 md:mt-4">
<!--            <div class="w-full mx-1 md:mt-4 mb-0">//-->
                <div class="w-full md:w-1/2 mx-1">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_rubrics_name[$g_applang] ?>
                    </label>
                    <select name="rubric_id" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php foreach($rubrics as $item): ?>
                        <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="w-full md:w-1/2 mx-1">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Nº de Equipos de alumnos
                    </label>
                    <select name="num_teams" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($g_teams_num); $i++)
                                echo '<option value="'.$g_teams_num[$i].'" >'.$g_teams_num[$i].'</option>\n';
                		?>
                    </select>
                </div>
            </div>
        </div>
        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_next[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded text-xs bg-red-500 hover:bg-red-600">
        </div>
    </form>
</div>
