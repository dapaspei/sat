<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
<div x-data="{ isOpen: false }">    
    <form action="<?=base_url('teachers/challenges/update')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
		<input type="hidden" value="<?= set_value('id', isset($challenge['id']) ? $challenge['id'] : '');?>" name="id">

        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-full px-3 mb-4 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_name[$g_applang]?>
                </label>
                <input type="text" name="name" value="<?= set_value('name', isset($challenge['name']) ? $challenge['name'] : '');?>"  
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white">
                    <?php echo form_error('name'); ?>
            </div>
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold md:mt-2 mb-2">
                    <?=$g_description[$g_applang]?>
                </label>
                <textarea rows="3" name="description" value="<?= set_value('description', isset($challenge['description']) ? $challenge['description'] : '');?>" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= set_value('description', isset($challenge['description']) ? $challenge['description'] : '');?></textarea>
                    <?php echo form_error('description'); ?>
            </div>
        </div>
        <div class="flex flex-grow flex-wrap -mx-3 mt-2 mb-2">
            <div class="flex w-full justify-around px-3 mb-6 md:mb-0">
                <div class="mr-2 w-1/3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_ch_fechainicio[$g_applang] ?>
                    </label>
                    <!-- <input id="start_date" type="date" min="2019-09-10" max="2020-06-20" name="start_date" value="<?= set_value('start_date', isset($challenge['start_date']) ? $challenge['start_date'] : '');?>" -->
                    <input id="start_date" type="date" name="start_date" value="<?= set_value('start_date', isset($challenge['start_date']) ? $challenge['start_date'] : '');?>"
                        class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </div>
                <div class="mr-2 w-1/3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_ch_fechafin[$g_applang] ?>
                    </label>
                    <!-- <input id="finish_date" type="date" min="2019-09-10" max="2020-06-20" name="finish_date" value="<?= set_value('finish_date', isset($challenge['finish_date']) ? $challenge['finish_date'] : '');?>" -->
                    <input id="finish_date" type="date" name="finish_date" value="<?= set_value('finish_date', isset($challenge['finish_date']) ? $challenge['finish_date'] : '');?>"
                    class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </div>
                <div class="w-1/3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Sprints
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> name="sprints" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                            for($i=$g_sprints_min; $i<=$g_sprints_max; $i++) {
                                if ($i === intval($challenge['sprints']))
                                    echo '<option selected value="'.$i.'">'.$i.'</option>';
                                else 
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>


            <div class="flex w-full justify-around px-3 mb-6 md:mb-0 md:mt-4">
                <div class="mr-2 w-1/2 md:w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_year_name[$g_applang]?>
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> name="year" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                        for($i=0; $i<count($g_school_year); $i++) {
                            if ($g_school_year[$i] === $challenge['year'])
                                echo '<option selected value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                            else 
                                echo '<option value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="mr-2 w-1/2 md:w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_ch_subject[$g_applang]?>
                    </label>
                    <input type="text" name="subject" value="<?= set_value('subject', isset($challenge['subject']) ? $challenge['subject'] : '');?>"   
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">

                </div>
            </div>
            <div class="flex w-full justify-around px-3 mb-2 md:mt-4">
                
                <div class="w-full">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_ch_levels[$g_applang]?>
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> multiple size="5" name="teaching_levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 ml-3 md:-ml-3 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    <?php
                        for($i=0; $i<count($g_levels); $i++) {
                            $selected = false;
                            for($j=0; $j<count($challenge['teaching_levels']); $j++) {
                                if($g_levels[$i] === $challenge['teaching_levels'][$j]) {
                                    $selected = true;
                                    break;
                                }
                            }
                            if ($selected) echo '<option selected value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                            else  echo '<option value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="flex w-full flex-col md:flex-row justify-around px-3 mb-6 md:mb-0 md:mt-1">
                <div class="w-full md:w-1/2 mx-1 md:mx-4">
                    <?php 
                        // var_dump("students=".json_encode($students[0]['id'])."<br>students_selected=".json_encode($students_selected[0]['id']));
                        // var_dump("students=".json_encode($students[0])."<br>students_selected=".json_encode($students_selected[0]));
                        // var_dump("count(students)=".count($students));
                        // var_dump("count(std_selected)=".count($students_selected)); 
                        // var_dump("es igua?=". ($students[$i] == $students_selected[$j]) ? 'cierto' : 'falso');
                    ?>
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_ch_students[$g_applang]?>
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> multiple size="15" name="students[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php

                            for($i=0; $i<count($students); $i++) {
                                $selected = false;
                                for($j=0; $j<count($students_selected); $j++) {
                                    if($students[$i]['id'] == $students_selected[$j]['id']) {
                                        $selected = true;
                                        break;
                                    }
                                }
                                if ($selected) echo '<option selected value="'.$students[$i]['id'].'" selected>'.$students[$i]['firstname'].' '.$students[$i]['lastname'].' ('.$students[$i]['teaching_levels'].')'.'</option>\n';
                                else echo '<option value="'.$students[$i]['id'].'" >'.$students[$i]['firstname'].' '.$students[$i]['lastname'].' ('.$students[$i]['teaching_levels'].')'.'</option>\n';
                            }
                            ?>
                    </select>
                </div>
                <div class="w-full md:w-1/2 mt-4 md:mt-0 mx-1 md:mx-4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_ch_teachers[$g_applang]?>
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> multiple size="15" name="teachers[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($teachers); $i++) {
                                $selected = false;
                                for($j=0; $j<count($teachers_selected); $j++) {
                                    if($teachers[$i] == $teachers_selected[$j]) {
                                        $selected = true;
                                        break;
                                    }
                                }
                                if ($selected) echo '<option value="'.$teachers[$i]['id'].'" selected>'.$teachers[$i]['firstname'].' '.$teachers[$i]['lastname'].'</option>\n';
                                else echo '<option value="'.$teachers[$i]['id'].'" >'.$teachers[$i]['firstname'].' '.$teachers[$i]['lastname'].'</option>\n';
                            }
                        ?>
                    </select>
                </div>

            </div>
            <div class="flex w-full flex-col md:flex-row mx-4 md:mx-6 md:mt-4 mb-0">
                <div class="w-full md:w-1/3 mx-1 md:mx-4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Nº de Equipos
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> name="num_teams" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($g_teams_num); $i++)
                                if ($num_teams == $g_teams_num[$i]) 
                                    echo '<option value="'.$g_teams_num[$i].'" selected>'.$g_teams_num[$i].'</option>\n';
                                else
                                    echo '<option value="'.$g_teams_num[$i].'" >'.$g_teams_num[$i].'</option>\n';
                		?>
                    </select>
                </div>
                <div class="w-full md:w-2/3 mx-1 md:mx-4">

                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Rúbrica de Evaluación
                    </label>
                    <select <?= $assessment_in_process ? 'disabled' : ''?> name="rubric_id" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php foreach($rubrics as $item): ?>
                        <!-- <option value="<?= $item->id ?>"><?= $item->name ?></option> -->
                        <option <?= $item->id === $rubric_selected ? 'selected' : ''; ?> value="<?= $item->id ?>"><?= $item->name ?></option>

                        <?php endforeach; ?>
                    </select>
                    
                </div>
            </div>
        </div>

        <div class="flex flex-col md:flex-row justify-center md:justify-around mt-4">
                <!-- <input type="submit" name="submit" value="<?=$g_update[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700"> -->
                <input type="submit" name="submit" value="<?=$g_resetPassword[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_assignTechnicalSkills[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?= $assessment_in_process ? $g_showGroups[$g_applang] : $g_defineGroups[$g_applang] ?>" 
                            class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block  mb-2 text-xl md:text-lg text-white font-bold px-3 py-1 rounded bg-red-500 hover:bg-red-700">
 
            <!-- <?php if(!$assessment_in_process) { ?>
                <input type="submit" name="submit" value="<?=$g_update[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_updateSprints[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_defineGroups[$g_applang]?>" 
                            class="<?= $assessment_in_process ? 'opacity-25 cursor-not-allowed' : '' ?> inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700"
                            <?= $assessment_in_process ? 'disabled' : '' ?>>
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block  mb-2 text-xl md:text-lg text-white font-bold px-3 py-1 rounded bg-red-500 hover:bg-red-700">
            <?php } else { ?>
                <input type="submit" name="submit" value="<?=$g_update[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_updateSprints[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_showGroups[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block  mb-2 text-xl md:text-lg text-white font-bold px-3 py-1 rounded bg-red-500 hover:bg-red-700">
            <?php } ?> -->
        </div>
    </form>
    <?php if($isowner) { ?>
        <div class="flex justify-around mt-4 mb-4">
            <!-- <a href="<?=base_url('teachers/challenges/delete/'.$challenge['id']);?>" class="inline-block items-center text-3xl md:text-2xl text-white font-bold px-3 py-1 rounded bg-red-500 hover:bg-red-700">
                        <?=$g_delete[$g_applang]?></a> -->
            <button 
                class="inline-block items-baseline h-full bg-red-500 hover:bg-red-700 text-center text-3xl md:text-2xl text-white font-bold p-4 rounded"
                @click="isOpen = true
                        $nextTick(() => $refs.modalCloseButton.focus())
                        writeModal(<?=$challenge['id']?>)
                        "
            >
                <?=$g_delete[$g_applang]?>
            </button>
        </div>

    <?php }?>

    <!-- MODAL para borrar  -->
    <div id="codigoModal"></div>

    <script>

        function writeModal(challenge_id) {
            // Cogemos la URL Base
            var base = window.location.origin;
            // Montamos la URL para borrar el RA seleccionado
            base_url = base+'/teachers/challenges/delete/'+challenge_id;
            // alert(base_url);
            codeBlock = '<div style="background-color: rgba(0, 0, 0, .5)" class="mx-auto absolute top-0 left-0 w-full h-screen flex items-center shadow-lg overflow-y-auto" x-show="isOpen">'
            codeBlock += '  <div class="container mx-auto rounded p-4 mt-2 overflow-y-auto">'
            codeBlock += '      <div class="bg-white rounded px-8 py-8">'
            codeBlock += '          <h1 class="font-bold text-2xl mb-3 text-center">ATENCIÓ!!!</h1>'
            codeBlock += '               <div class="modal-body">'
            codeBlock += '                    <p class="text-center">ESTÀS A PUNT DE ESBORRAR UN REPTE SENCER. </p>'
            codeBlock += '                    <p class="text-center">HAS PENSAT EN LES CONSEQUÈNCIES??</p>'
            codeBlock += '                </div>'
            codeBlock += '                <div class="flex mt-4 justify-center">'
            codeBlock += '                    <a href="'+this.base_url+'"'
            codeBlock += '                        class=" bg-red-500 hover:bg-red-700 text-white px-4 py-3 mt-4 text-sm rounded"'
            codeBlock += '                        @click="isOpen = false"'
            codeBlock += '                       x-ref="modalCloseButton"'
            codeBlock += '                    >'
            codeBlock += '                        <?=$g_delete[$g_applang]?>'
            codeBlock += '                    </a>'
            codeBlock += '                    <button class=" bg-blue-600 hover:bg-blue-800 text-white px-4 py-3 mt-4 ml-4 text-sm rounded"'
            codeBlock += '                        @click="isOpen = false"'
            codeBlock += '                        x-ref="modalCloseButton">'
            codeBlock += '                        <?=$g_cancel[$g_applang]?>'
            codeBlock += '                    </button>'
            codeBlock += '                </div>'
            codeBlock += '            </div>'
            codeBlock += '        </div>'
            codeBlock += '    </div>'

            // Inserting the code block to wrapper element
            document.getElementById("codigoModal").innerHTML = codeBlock
        }
    </script>
</div>
</div>

