
<div class="w-full mx-auto shadow-md bg-grey-200">
    <!-- Display status message -->
    <div class="bg-white mx-auto shadow-md rounded my-2 pb-0 mb-2">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-2 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <div class="flex flex-wrap w-full">
        <div class="w-full flex-wrap md:flex-1 md:w-1/2 bg-gray-200 rounded shadow-lg m-4">
            <div class="w-full flex justify-center px-6 py-4">
                <div class="font-bold text-center text-2xl mb-2">Retos</div>
            </div>
            <div class="w-full flex justify-center px-6 py-4">
                <?php
                    if ($challenges === 'si')
                        $link1 = base_url('teachers/challenges');
                    else 
                        $link1 = '';
                ?>
                <a href="<?= $link1 ?>" class="<?= empty($link1) ? 'opacity-25 cursor-not-allowed' : '' ?> inline-block text-lg md:text-lg py-1 px-3 mr-2 text-white font-bold rounded bg-teal-500 hover:bg-blue-700">
                    Acceder</a>
            </div>
        </div>
        <div class="w-full flex-wrap md:flex-1 md:w-1/2  bg-gray-200 rounded shadow-lg m-4">
            <div class="w-full flex justify-center px-6 py-4">
                <div class="font-bold text-center text-2xl mb-2">Proyectos</div>
            </div>
            <div class="w-full flex justify-center px-6 py-4">
            <?php
                    if ($projects === 'si') 
                        $link2 = base_url('teachers/projects');
                    else 
                        $link2 = '';
                ?>
                  <a href="<?= $link2 ?>" class="<?= empty($link2) ? 'opacity-25 cursor-not-allowed' : '' ?> inline-block text-lg md:text-lg py-1 px-3 mr-2 text-white font-bold rounded bg-teal-500 hover:bg-blue-700">
                    Acceder</a>
            </div>
        </div>
    </div>
</div>
