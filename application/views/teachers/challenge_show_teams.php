<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/challenges/edit/'.$challenge['id'].'')?>" method="POST" class="mx-auto md:-ml-2 text-center">
		<input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id']: '';?>" name="challenge_id">
		<input type="hidden" value="<?= isset($num_teams) ? $num_teams: '';?>" name="num_teams">
        <input type="hidden" value="<?= count($students); ?>" name="num_students">

        <div class="w-full flex flex-col px-3 mb-6 md:mb-0 md:mt-4">
                <?php 
                    for ($equip=0; $equip<$num_teams; $equip++) {
                        // var_dump("<br>students=".json_encode($students[0]['id']));
                        // var_dump("<br><br><br>students_selected=".json_encode($students_selected[0][0]['id']));
                        // var_dump("<br>firstname = ".json_encode($students[$equip]['firstname']));
                ?>
                    <div class="w-full p-2 mx-auto">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            <?=$g_team[$g_applang].' '.($equip+1)?>
                        </label>
                        <select multiple size="5" name="std_teams<?=$equip?>[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <!-- <?php
                            for($i=0; $i<count($students); $i++) {
                                $selected = false;
                                for($j=0; $j<count($students_selected[$equip]); $j++) {
                                    if($students[$i] == $students_selected[$equip][$j]) {
                                        $selected = true;
                                        break;
                                    }
                                }
                                if ($selected) echo '<option value="'.$students[$i]['id'].'" selected>'.$students[$i]['firstname'].' '.$students[$i]['lastname'].' ('.$students[$i]['teaching_levels'].'-'.$students[$i]['s_group'].')'.'</option>\n';
                                //else echo '<option value="'.$students[$i]['id'].'" >'.$students[$i]['firstname'].' '.$students[$i]['lastname'].' ('.$students[$i]['teaching_levels'].'-'.$students[$i]['s_group'].')'.'</option>\n';
                            }
                        ?> -->
                        <?php
                            for($i=0; $i<count($students); $i++) {
                                // if ($students[$i]['s_team'] == $equip)
                                if (intval($students_in_teams[$i]['s_team']) === $equip)
                                    echo '<option value="'.$students[$i]['id'].'" selected>'.$students[$i]['firstname'].' '.$students[$i]['lastname'].' ('.$students[$i]['teaching_levels'].')'.'</option>\n';
                                //else echo '<option value="'.$students[$i]['id'].'" >'.$students[$i]['firstname'].' '.$students[$i]['lastname'].' ('.$students[$i]['teaching_levels'].')'.'</option>\n';
                            }
                        ?>
                    </select>
                    </div>
                <?php
                    }
                ?>
            </div>
        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_return[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-500 hover:bg-blue-700">
        </div>
    </form>
</div>
