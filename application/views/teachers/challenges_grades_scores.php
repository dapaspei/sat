<div class="w-full mx-auto shadow-md">
    <div class="w-full p-1">
        <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
            <div class="flex justify-center">
                <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                <div>
                    <p class="font-bold"><?= $g_grades[$g_applang]?></p>
                </div>
            </div>

            <!-- Información sobre el código de color de las calificaciones -->
            <div class="w-full p-1 border-1"> 
                <div class="w-full border-2 py-0 items-center justify-start">
                    <div class="shadow w-full bg-gray-200 py-0 items-center">
                        <div class="<?= $g_bg_score[1]?> text-xs md:text-sm rounded-lg leading-none py-0 px-2 text-left text-teal" 
                            style="width: 30%">0 - 2.99
                        </div>
                    </div>
                    <div class="shadow w-full bg-gray-200 py-0 items-center">
                        <div class="<?= $g_bg_score[4]?> text-xs md:text-sm rounded-lg leading-none py-0 px-2 text-left text-teal" 
                            style="width: 50%">3 - 4.99
                        </div>
                    </div>
                    <div class="shadow w-full bg-gray-200 py-0 items-center">
                        <div class="<?= $g_bg_score[6]?> text-xs md:text-sm rounded-lg leading-none py-0 px-2 text-left text-teal" 
                            style="width: 80%">5 - 7.99
                        </div>
                    </div>
                    <div class="shadow w-full bg-gray-200 py-0 items-center">
                        <div class="<?= $g_bg_score[9]?> text-xs md:text-sm rounded-lg leading-none py-0 px-2 text-left text-teal" 
                            style="width: 99%">8 - 9.99
                        </div>
                    </div>
                    <div class="shadow w-full bg-gray-200 py-0 items-center">
                        <div class="<?= $g_bg_score[10.5]?> text-xs md:text-sm rounded-lg leading-none py-0 px-2 text-left text-teal" 
                            style="width: 100%">10
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Tabs Content Start here -->
        <div 
            x-data="{
                openTab: 0,
                activeClasses: 'border-l border-t border-r rounded-t text-blue-700 bg-gray-300',
                inactiveClasses: 'text-blue-500 hover:text-blue-800 bg-gray-100'
            }" 
            class="p-0"
        >
            <!-- Scroll automático -->
            <ul class="flex border-b relative overflow-x-auto"> 
                <!-- <label x-text="openTab"></label> -->
                <?php 
                    $students_ids = array_keys($all_grades);
                    // print_r("TOP -> count(students_ids) = ".count($students_ids));
                    for($student=0; $student<count($students_ids); $student++) { 
                            $current_student_id = $students_ids[$student];
                            $current_grades = array_values($all_grades[$current_student_id]); ?>
                            <!-- <li @click="openTab = <?=$student?>" :class="{ '-mb-px': openTab === <?=$student?> }" class="-mb-px mr-1 "> -->
                            <li @click="openTab = <?=$student?>" :class="{ '-mb-px': openTab === <?=$student?> }" class="-mb-px mr-1 flex-none">
                                <a :class="openTab === <?=$student?> ? activeClasses : inactiveClasses" class="text-xs inline-block py-4 px-3 font-semibold" href="#">
                                <label class="w-full text-center uppercase tracking-wide text-gray-700 font-bold mb-2">
                                    <?= $students[$student]['firstname']." ".$students[$student]['lastname'][0]."." ?>
                                </label>
                                </a>
                            </li>
                        
                <?php 
                    } ?>
                    <li @click="openTab = <?=count($students_ids)?>" :class="{ '-mb-px': openTab === <?=count($students_ids)?> }" class="-mb-px mr-1 flex-none">
                        <a :class="openTab === <?=count($students_ids)?> ? activeClasses : inactiveClasses" class="text-xs inline-block py-4 px-3 font-semibold" href="#">
                        <label class="w-full text-center uppercase tracking-wide text-gray-700 font-bold mb-2">
                            <?= $g_all[$g_applang]; ?>
                        </label>
                        </a>
                    </li>

            </ul>
            
            <div class="w-full pt-4">

                <?php 
                    // $students_ids = array_keys($all_grades);
                    for($student=0; $student<count($students_ids); $student++) { 
                        $current_student_id = $students_ids[$student];
                        // print_r("current_student_id=".$current_student_id);
                        $current_grades = array_values($all_grades[$current_student_id]);
                ?>
                    <div x-show="openTab === <?=$student?>">
                        
                        
                        <div class="w-full mt-2">
                            <!-- Tabla con las calificaciones finales -->
                            <div class="border-2 border-gray-600  rounded-lg shadow-3xl">
                                <div class="flex mx-auto">
                                    <label class="w-1/4 md:w-1/3 block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold border-r-2 border-gray-700">
                                        <strong><?=$students[$student]['firstname']." ".$students[$student]['lastname']?></strong>
                                    </label>
                                    <div class="w-3/4 md:w-2/3 flex flex-row md:flex-1">
                                        <div class="flex flex-col border-r-2 md:w-full">
                                            <label class="w-full text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                                <strong><?= $g_soft_skills_name[$g_applang] ?></strong>
                                            </label>
                                            <label class="w-full text-center uppercase tracking-wide text-gray-700 text-base md:text-lg font-bold mb-2">
                                                <strong><?= !empty($all_sprints_grades[$current_student_id]) ? $all_marks[$current_student_id] : '&nbsp;' ?></strong>
                                            </label>
                                        </div>
                                        <div class="flex flex-col border-r-2 md:w-full">
                                            <label class="w-full text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                                <strong><?= $g_technical_skills_name[$g_applang] ?></strong>
                                            </label>
                                            <label class="w-full  text-center uppercase tracking-wide text-gray-700 text-base md:text-lg font-bold mb-2">
                                            <?php if ($num_technical_skills > 0) { ?>
                                                <strong><?= $all_average_technical_marks[$student] ?></strong>
                                            <?php } ?>
                                            </label>
                                        </div>
                                        <div class="flex flex-col border-2 md:w-full border-yellow-300 bg-yellow-200">
                                            <label class="w-full bg-white text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                                <strong><?= $g_final_qualification_name[$g_applang] ?></strong>
                                            </label>
                                            <label class="w-full h-full text-center uppercase tracking-wide text-gray-700 text-lg md:text-lg font-bold mb-2">
                                            <?php if ($num_technical_skills > 0) { ?>
                                                <!-- SoftSkills 40% - TechnicalSkills 60% -->
                                                <strong><?= number_format(floatval($all_marks[$current_student_id])*0.4 + floatval($all_average_technical_marks[$student])*0.6, 2) ?></strong>
                                            <?php } ?>
                                            </label>
                                        </div>
                                   
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- SOFT SKILLS - HABILIDADES TRANSVERSALES -->
                            <label class="w-full block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold mb-2">
                                <?= $g_soft_skills_name[$g_applang]. " - <strong>". (!empty($all_sprints_grades[$current_student_id]) ? $all_marks[$current_student_id] : '&nbsp;') ."</strong>" ?>
                            </label>
                            <div class="md:flex">
                                <!-- SPRINS TOTAL -->
                                <div class="w-full md:w-3/5 p-1 border-1"> 
                                    <div class="w-full border-0 items-center justify-center">
                                        <?php 
                                            if (!empty($all_sprints_grades[$current_student_id])) {
                                                for($k=0; $k<$cols; $k++) { ?>
                                                <div class="shadow w-full bg-gray-200 border-1 py-1 items-center">
                                                    <div class="<?= $g_bg_score[floor($current_grades[$k])]?> text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left text-teal" 
                                                        style="width: <?= ($current_grades[$k]==0) ? 100 : (10*$current_grades[$k]) ?>%"><?= ($current_grades[$k]==0) ? '' : number_format($current_grades[$k],2).' - ' ?><?= $criteria[$k] ?>
                                                    </div>
                                                </div>
                                        <?php   } 
                                            } ?>
                                    </div>
                                </div>
                                <!-- SPRINTS PARTIAL -->
                                <div class="w-full md:w-2/5">
                                    <?php 
                                        if (!empty($all_sprints_grades[$current_student_id])) {
                                            for($j=0; $j<$cols; $j++) { ?>
                                            <div class="shadow w-full bg-gray-400 border-1 py-0 items-center">
                                                <div class="text-xs rounded-lg leading-none py-0 px-0 text-center text-teal"><?= $criteria[$j].' - '.(1/$cols*100)."%" ?>
                                                </div>
                                            </div>
                                            <div class="w-full border-1 items-center justify-center mb-2">
                                                <?php for($i=0; $i<count($all_sprints_grades[$current_student_id]);$i++){ ?>
                                                    <div class="shadow w-full bg-gray-200 border-1 py-0 items-center">
                                                        <div class="<?= $g_bg_score[floor($all_sprints_grades[$current_student_id][$i][$j])]?> text-xs rounded-lg leading-none py-0 px-0 text-left text-teal pl-2" 
                                                            style="width: <?= ($all_sprints_grades[$current_student_id][$i][$j]==0) ? 100 : (10*$all_sprints_grades[$current_student_id][$i][$j]) ?>%">S<?=($i+1)?> - <?= ($all_sprints_grades[$current_student_id][$i][$j]==0) ? '' : number_format($all_sprints_grades[$current_student_id][$i][$j],2)?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                    <?php   }
                                        } ?>
                                </div>
                            </div>
                        </div>
                        <!-- SOFT SKILLS COMMENTS  -->
                        <div class="md:flex w-full">
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <div class="mb-2">
                                    <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_my_self_comments[$g_applang]?></div>
                                        <span class="text-grey-darker text-xs md:text-sm"><?= !empty($all_sprints_grades[$current_student_id]) ? nl2br($all_comments[$current_student_id]['self_comments']) : '&nbsp;' ?></span>
                                </div>
                            </div>
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <div class="mb-2">
                                    <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_team_comments[$g_applang]?></div>
                                        <span class="text-grey-darker text-xs md:text-sm"><?= !empty($all_sprints_grades[$current_student_id]) ? nl2br($all_comments[$current_student_id]['peer_comments']) : '&nbsp;' ?></span>
                                </div>
                            </div>
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <div class="mb-2">
                                    <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_teacher_comments[$g_applang]?></div>
                                        <span class="text-grey-darker text-xs md:text-sm"><?= !empty($all_sprints_grades[$current_student_id]) ? nl2br($all_comments[$current_student_id]['teacher_comments']) : '&nbsp;' ?></span>
                                </div>
                            </div>
                        </div>
                        <!-- Mostrar quién ha valorado el Sprint -->
                        
                        <div class="md:flex w-full">
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <label class="w-full block text-center uppercase tracking-wide text-gray-800 text-sm md:text-lg font-bold mb-2"><?= 'SPRINTS VALORATS' ?> </label>
                                <div class="mb-2">
                                    <?php
                                        // var_dump($who_has_assess);
                                        // var_dump("<br>Cuantos han valorado:<br>".count($who_has_assess));
                                        // exit(0);
                                        $self_data = array_values($who_has_assess[0][$student]);
                                        // print_r($self_data);
                                        // print_r("primer estudiante:<br>");
                                        // print_r($self_data[$student]);
                                        $self_data_name = array_keys($self_data[0]);
                                        // $self_data_name = array_keys($self_data[$student]);
                                        // var_dump("<br>Nombre: ". $self_data_name[0]);
                                        // $self_data_values = array_values($self_data[$student]);
                                        $self_data_values = array_values($self_data[0]);
                                        // var_dump("<br>VAlores: ". json_encode($self_data_values));
                                        // exit(0);                          
                                            ?>
                                            <div class="flex-none md:flex p-1 border-2 border-dotted border-blue-400">
                                                <label class="flex md:w-1/6 font-semibold text-grey-darker text-xs md:text-sm mr-2"><?= $self_data_name[0].':' ?> </label>
                                                <div class="flex md:w-1/3 p-1 md:p-0">
                                                    <label class="ml-0 md:ml-1 mr-2 text-xs md:text-sm">Sprints:</label>
                                                    <?php 
                                                        foreach($self_data_values[0] as $key => $valor) { 
                                                    ?>
                                                        <div class="flex items-center mr-2 md:mr-4 mb-0">
                                                            <input type="checkbox" class="opacity-0 absolute h-8 w-8 mr-0" disabled <?= $valor === true ? 'checked' : ''; ?> >
                                                            <div class="bg-white border-2 rounded-md border-blue-400 w-4 h-4 flex flex-shrink-0 justify-center items-center mr-0 md:mr-2 focus-within:border-blue-500">
                                                                <svg class="fill-current <?= $valor === true ? 'block' : 'hidden'; ?> w-3 h-3 text-blue-600 pointer-events-none" version="1.1" viewBox="0 0 17 12" xmlns="http://www.w3.org/2000/svg">
                                                                    <g fill="none" fill-rule="evenodd">
                                                                        <g transform="translate(-9 -11)" fill="#1F73F1" fill-rule="nonzero">
                                                                            <path d="m25.576 11.414c0.56558 0.55188 0.56558 1.4439 0 1.9961l-9.404 9.176c-0.28213 0.27529-0.65247 0.41385-1.0228 0.41385-0.37034 0-0.74068-0.13855-1.0228-0.41385l-4.7019-4.588c-0.56584-0.55188-0.56584-1.4442 0-1.9961 0.56558-0.55214 1.4798-0.55214 2.0456 0l3.679 3.5899 8.3812-8.1779c0.56558-0.55214 1.4798-0.55214 2.0456 0z" />
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <label class="text-grey-darker text-xs md:text-sm"> <?=($key+1)?> </label>
                                                        </div>
                                                    <?php 
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            
                                        <?php ?>
                                        <div class="border-2 border-blue-800 mt-1 mb-1 md:mb-2">
                                            <?php 
                                            $teachers_data = array_values($who_has_assess[1][$student]);
                                            // print_r("<br>teacher data:<br>");
                                            // print_r($teachers_data[0]);
                                            // exit(0);
                                            $teachers_data_name= array_keys($teachers_data[0]);
                                            $teachers_data_values = array_values($teachers_data[0]);
                                            // var_dump("<br>Teacher names:".json_encode($teachers_data_name)."<br> Teachers values:".json_encode($teachers_data_values));
                                            // exit(0);

                                            // ---
                                            $num_teacher = 0;
                                            foreach($teachers_data_name as $key => $teacher_name) {
                                                // echo $teacher_name."<br>";
                                                ?>
                                                <div class="flex-none md:flex p-1">
                                                    <label class="flex md:w-1/6 font-semibold text-grey-darker text-xs md:text-sm mr-2"><?= $teacher_name.':' ?> </label>
                                                    <div class="flex md:w-1/3 p-1 md:p-0">
                                                        <label class="ml-0 md:ml-1 mr-2 text-xs md:text-sm">Sprints:</label>
                                            <?php 
                                                    foreach($teachers_data_values[$num_teacher] as $key => $teacher_value) {
                                                    ?>
                                                        <!-- <label class="text-grey-darker text-xs md:text-sm">Sprint <?=($key+1)?> </label> -->
                                                        <!-- <input class="" type="checkbox" id="cbox2" value="second_checkbox" disabled <?= $teacher_value === true ? 'checked' : ''; ?> > -->
                                                        <div class="flex items-center mr-2 md:mr-4 mb-0">
                                                            <input type="checkbox" class="opacity-0 absolute h-8 w-8 mr-0" disabled <?= $teacher_value === true ? 'checked' : ''; ?> >
                                                            <div class="bg-white border-2 rounded-md border-blue-400 w-4 h-4 flex flex-shrink-0 justify-center items-center mr-0 md:mr-2 focus-within:border-blue-500">
                                                                <svg class="fill-current <?= $teacher_value === true ? 'block' : 'hidden'; ?> w-3 h-3 text-blue-600 pointer-events-none" version="1.1" viewBox="0 0 17 12" xmlns="http://www.w3.org/2000/svg">
                                                                    <g fill="none" fill-rule="evenodd">
                                                                        <g transform="translate(-9 -11)" fill="#1F73F1" fill-rule="nonzero">
                                                                            <path d="m25.576 11.414c0.56558 0.55188 0.56558 1.4439 0 1.9961l-9.404 9.176c-0.28213 0.27529-0.65247 0.41385-1.0228 0.41385-0.37034 0-0.74068-0.13855-1.0228-0.41385l-4.7019-4.588c-0.56584-0.55188-0.56584-1.4442 0-1.9961 0.56558-0.55214 1.4798-0.55214 2.0456 0l3.679 3.5899 8.3812-8.1779c0.56558-0.55214 1.4798-0.55214 2.0456 0z" />
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </div>
                                                            <label class="text-grey-darker text-xs md:text-sm"> <?=($key+1)?> </label>
                                                        </div>

                                                    <?php 
                                                    }
                                                    $num_teacher++;
                                                
                                                ?>
                                                    </div>
                                                </div>
                                            <?php
                                            } ?>
                                        </div>
                                        <?php

                                        if (!empty($who_has_assess[2][$student])) { ?>
                                        <div class="p-1 border-2 border-dotted border-blue-400">
                                            <?php
                                                $peer_data = array_values($who_has_assess[2][$student]);
                                                $peer_data_name = array_keys($peer_data[0]);
                                                $peer_data_values = array_values($peer_data[0])  ; 
                                                // var_dump("<br>Peer names:".json_encode($peer_data_name)."<br> Peer values:".json_encode($peer_data_values));
                                                // exit(0);
                                                $num_peer = 0;
                                                foreach($peer_data_name as $key => $peer_name) {
                                                    ?>
                                                    <div class="flex-none md:flex mb-1 md:mb-2">
                                                        <label class="flex md:w-1/6 font-semibold text-grey-darker text-xs md:text-sm mr-2"><?= $peer_name.':' ?> </label>
                                                        <div class="flex md:w-1/3 p-1 md:p-0">
                                                            <label class="ml-0 md:ml-1 mr-2 text-xs md:text-sm">Sprints:</label>
                                                    
                                                <?php 
                                                        foreach($peer_data_values[$num_peer] as $key => $peer_value) {
                                                        ?>
                                                            <!-- <label class="text-grey-darker text-xs md:text-sm">Sprint <?=($key+1)?> </label> -->
                                                            <!-- <input class="" type="checkbox" id="cbox2" value="second_checkbox" disabled <?= $peer_value === true ? 'checked' : ''; ?> > -->
                                                            <div class="flex items-center mr-2 md:mr-4 mb-0">
                                                                <input type="checkbox" class="opacity-0 absolute h-8 w-8 mr-0" disabled <?= $peer_value === true ? 'checked' : ''; ?> >
                                                                <div class="bg-white border-2 rounded-md border-blue-400 w-4 h-4 flex flex-shrink-0 justify-center items-center mr-0 md:mr-2 focus-within:border-blue-500">
                                                                    <svg class="fill-current <?= $peer_value === true ? 'block' : 'hidden'; ?> w-3 h-3 text-blue-600 pointer-events-none" version="1.1" viewBox="0 0 17 12" xmlns="http://www.w3.org/2000/svg">
                                                                        <g fill="none" fill-rule="evenodd">
                                                                            <g transform="translate(-9 -11)" fill="#1F73F1" fill-rule="nonzero">
                                                                                <path d="m25.576 11.414c0.56558 0.55188 0.56558 1.4439 0 1.9961l-9.404 9.176c-0.28213 0.27529-0.65247 0.41385-1.0228 0.41385-0.37034 0-0.74068-0.13855-1.0228-0.41385l-4.7019-4.588c-0.56584-0.55188-0.56584-1.4442 0-1.9961 0.56558-0.55214 1.4798-0.55214 2.0456 0l3.679 3.5899 8.3812-8.1779c0.56558-0.55214 1.4798-0.55214 2.0456 0z" />
                                                                            </g>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <label class="text-grey-darker text-xs md:text-sm"> <?=($key+1)?> </label>
                                                            </div>

                                                        <?php 
                                                        }
                                                        $num_peer++;
                                                    ?>
                                                        </div>
                                                    </div>

                                            <?php
                                                }
                                                ?>
                                        </div>
                                        <?php
                                        } 
                                    ?>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- Fin - Mostrar quién ha valorado el Sprint -->


                        <!-- Si NO HAY definido ningún RA en el RETO, no se muestran los HARD SKILLS.  -->
                        <?php if ($num_technical_skills > 0) { ?>
                            <hr class="border-t border-4 border-gray-400 shadow">
                            <!-- Technical Skills Marks -->
                            <label class="w-full block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold mb-2 mt-4">
                                <?= $g_technical_skills_name[$g_applang] ?> - <strong><?= $all_average_technical_marks[$student] ?></strong>
                            </label>
                            <div class="w-full mt-2">
                                <!-- RAs NAMES -->
                                <div class="flex-col w-full p-0 border-1"> 
                                    <?php for($k=0; $k<$num_technical_skills; $k++) { ?>
                                        <div class="flex w-full border-0 items-center justify-center">
                                            <div class="bg-white text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left">
                                                    <?= $technical_skills_percentages[$k] ?>%
                                                </div>
                                            <div class="flex shadow w-full border-1 py-1 items-center">
                                                <div class="bg-orange-200 text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left text-gray-800 w-full">
                                                    <?= $all_technical_names[$current_student_id][$k] ?>
                                                </div>
                                            </div>
                                            <div class="flex shadow w-1/7 bg-gray-200 border-1 p-1 items-center">
                                                <div class="bg-white text-sm md:text-lg min-w-full rounded-lg leading-none py-2 px-2 text-left flex-1 w-full
                                                    <?= $all_technical_marks[$current_student_id][$k] == '0' ? 'text-gray-400 disabled' : 'text-gray-800' ?>">
                                                    <?= $all_technical_marks[$current_student_id][$k] == '0' ? $g_NO_grades[$g_applang] : $all_technical_marks[$current_student_id][$k] ?>
                                                    <!-- <?= $all_technical_marks[$current_student_id][$k] == '0' ? '0.00' : $all_technical_marks[$current_student_id][$k] ?> -->
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- HARD SKILLS COMMENTS -->
                            <div class="w-full">
                                <div class="w-full flex-1 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                    <div class="mb-2">
                                        <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_comments[$g_applang]?>:</div>
                                            <span class="text-grey-darker text-base md:text-lg">
                                                <?= nl2br($all_technical_comments[$current_student_id]) ?>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?> <!-- END $num_technical_skills > 0 -->


                    </div>
                <?php } ?>

                <!-- <hr class="border-t border-4 border-gray-400 shadow"> -->
                <!-- All Students -->
                <!-- <?= var_dump("<br>DOWNcount(students_ids)="."4"); ?> -->
                <div id="toPdf" x-show="openTab === <?=count($students_ids)?>">
                <?php 
                        // $students_ids = array_keys($all_grades);
                        for($student=0; $student<count($students_ids); $student++) { 
                                $current_student_id = $students_ids[$student];
                            $current_grades = array_values($all_grades[$current_student_id]);
                    ?>
                        <div class="w-full mt-2">
                            <!-- Tabla con las calificaciones finales -->
                            <div class="border-2 border-gray-600  rounded-lg shadow-3xl">
                                <div class="flex mx-auto">
                                    <label class="w-1/4 md:w-1/3 block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold border-r-2 border-gray-700">
                                        <strong><?=$students[$student]['firstname']." ".$students[$student]['lastname']?></strong>
                                    </label>
                                    <div class="w-3/4 md:w-2/3 flex flex-row md:flex-1">
                                        <div class="flex flex-col border-r-2 md:w-full">
                                            <label class="w-full text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                                <strong><?= $g_soft_skills_name[$g_applang] ?></strong>
                                            </label>
                                            <label class="w-full text-center uppercase tracking-wide text-gray-700 text-base md:text-lg font-bold mb-2">
                                                <strong><?= !empty($all_sprints_grades[$current_student_id]) ? $all_marks[$current_student_id] : '&nbsp;' ?></strong>
                                            </label>
                                        </div>
                                        <div class="flex flex-col border-r-2 md:w-full">
                                            <label class="w-full text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                                <strong><?= $g_technical_skills_name[$g_applang] ?></strong>
                                            </label>
                                            <label class="w-full  text-center uppercase tracking-wide text-gray-700 text-base md:text-lg font-bold mb-2">
                                            <?php if ($num_technical_skills > 0) { ?>
                                                <strong><?= $all_average_technical_marks[$student] ?></strong>
                                            <?php } ?>
                                            </label>
                                        </div>
                                        <div class="flex flex-col border-2 md:w-full border-yellow-300 bg-yellow-200">
                                            <label class="w-full bg-white text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                                <strong><?= $g_final_qualification_name[$g_applang] ?></strong>
                                            </label>
                                            <label class="w-full h-full text-center uppercase tracking-wide text-gray-700 text-lg md:text-lg font-bold mb-2">
                                            <?php if (($num_technical_skills > 0) && (!empty($all_grades))) { ?>
                                                <!-- SoftSkills 40% - TechnicalSkills 60% -->
                                                <strong><?= number_format(floatval($all_marks[$current_student_id])*0.4 + floatval($all_average_technical_marks[$student])*0.6, 2) ?></strong>
                                            <?php } ?>
                                            </label>
                                        </div>
                                   
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="md:flex">
                                <!-- SPRINS TOTAL -->
                                <div class="w-full md:w-3/5 p-1 border-1"> 
                                    <div class="w-full border-0 items-center justify-center">
                                        <?php
                                            if (!empty($all_sprints_grades[$current_student_id])) {

                                                for($k=0; $k<$cols; $k++) { ?>
                                                <div class="shadow w-full bg-gray-200 border-1 py-1 items-center">
                                                    <div class="<?= $g_bg_score[floor($current_grades[$k])]?> text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left text-teal" 
                                                        style="width: <?= ($current_grades[$k]==0) ? 100 : (10*$current_grades[$k]) ?>%"><?= ($current_grades[$k]==0) ? '' : number_format($current_grades[$k],2).' - ' ?><?= $criteria[$k] ?>
                                                    </div>
                                                </div>
                                        <?php   }
                                            } ?>
                                    </div>
                                </div>
                                <!-- SPRINTS PARTIAL -->
                                <div class="w-full md:w-2/5">
                                    <?php
                                        if (!empty($all_sprints_grades[$current_student_id])) {
                                            for($j=0; $j<$cols; $j++) { ?>
                                            <div class="shadow w-full bg-gray-400 border-1 py-0 items-center">
                                                <div class="text-xs rounded-lg leading-none py-0 px-0 text-center text-teal"><?= $criteria[$j].' - '.(1/$cols*100)."%" ?>
                                                </div>
                                            </div>
                                            <div class="w-full border-1 items-center justify-center mb-2">
                                                <?php for($i=0; $i<count($all_sprints_grades[$current_student_id]);$i++){ ?>
                                                    <div class="shadow w-full bg-gray-200 border-1 py-0 items-center">
                                                        <div class="<?= $g_bg_score[floor($all_sprints_grades[$current_student_id][$i][$j])]?> text-xs rounded-lg leading-none py-0 px-0 pl-2 text-left text-teal" 
                                                            style="width: <?= ($all_sprints_grades[$current_student_id][$i][$j]==0) ? 100 : (10*$all_sprints_grades[$current_student_id][$i][$j]) ?>%">S<?=($i+1)?> - <?= ($all_sprints_grades[$current_student_id][$i][$j]==0) ? '' : number_format($all_sprints_grades[$current_student_id][$i][$j],2)?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                    <?php   }
                                        } ?>
                                </div>
                            </div>

                            <div class="md:flex w-full">
                                <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                    <div class="mb-2">
                                        <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_my_self_comments[$g_applang]?></div>
                                            <span class="text-grey-darker text-xs md:text-sm"><?= !empty($all_sprints_grades[$current_student_id]) ? nl2br($all_comments[$current_student_id]['self_comments']) : '&nbsp;' ?></span>
                                    </div>
                                </div>
                                <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                    <div class="mb-2">
                                        <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_team_comments[$g_applang]?></div>
                                            <span class="text-grey-darker text-xs md:text-sm"><?= !empty($all_sprints_grades[$current_student_id]) ? nl2br($all_comments[$current_student_id]['peer_comments']) : '&nbsp;' ?></span>
                                    </div>
                                </div>
                                <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                    <div class="mb-2">
                                        <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_teacher_comments[$g_applang]?></div>
                                            <span class="text-grey-darker text-xs md:text-sm"><?= !empty($all_sprints_grades[$current_student_id]) ? nl2br($all_comments[$current_student_id]['teacher_comments']) : '&nsbp;' ?></span>
                                    </div>
                                </div>
                            </div>

                            <!-- Mostrar quién ha valorado el Sprint -->
             
                            <!-- Fin - Mostrar quién ha valorado el Sprint -->

                            <!-- Si NO HAY definido ningún RA en el RETO, no se muestran los HARD SKILLS.  -->
                            <?php if ($num_technical_skills > 0) { ?>
                                <!-- Technical Skills Marks -->
                                <label class="w-full block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold mb-2 mt-4">
                                    <?= $g_technical_skills_name[$g_applang] ?> - <strong><?= $all_average_technical_marks[$student] ?></strong>
                                </label>
                                <div class="w-full mt-2">
                                    <!-- RAs NAMES -->
                                    <div class="flex-col w-full p-0 border-1"> 
                                        <?php for($k=0; $k<$num_technical_skills; $k++) { ?>
                                            <div class="flex w-full border-0 items-center justify-center">
                                                <div class="flex shadow w-full border-1 py-1 items-center">
                                                    <div class="bg-white text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left">
                                                        <?= $technical_skills_percentages[$k] ?>%
                                                    </div>
                                                    <div class="bg-orange-200 text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left text-gray-800 w-full">
                                                        <?= $all_technical_names[$current_student_id][$k] ?>
                                                    </div>
                                                </div>
                                                <div class="flex shadow w-1/7 bg-gray-200 border-1 p-1 items-center">
                                                    <div class="bg-white text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left  w-full
                                                    <?= $all_technical_marks[$current_student_id][$k] == '0' ? 'text-gray-400 disabled' : 'text-gray-800' ?>">
                                                        <?= $all_technical_marks[$current_student_id][$k] == '0' ? '0.00' : $all_technical_marks[$current_student_id][$k] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- HARD SKILLS COMMENTS -->
                                <div class="w-full">
                                    <div class="w-full flex-1 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                        <div class="mb-2">
                                            <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_comments[$g_applang]?>:</div>
                                                <span class="text-grey-darker text-base md:text-lg"><?= nl2br($all_technical_comments[$current_student_id]) ?></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <hr class="before border-t border-4 border-gray-400 shadow">
                            <?php } ?> <!-- END $num_technical_skills > 0 -->

                        </div> <!-- Fin de. w-full mt-2 -->
                        <!-- </div> -->
                    <?php } ?>
                </div> <!-- fin de: openTab === 4 -->
            </div> <!-- Fin de: w-full pt-4 -->

        </div> <!-- Fin X-data -->

        <!-- End Tabs -->
        
    <div>

</div>
