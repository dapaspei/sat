
<div class="w-full mx-auto shadow-md bg-grey-200">
    <div class="bg-white mx-auto shadow-md rounded my-2 pb-0 mb-2">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-2 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <div class="flex flex-wrap w-full" x-data="{ isOpen: false }">
        <div class="flex flex-wrap p-1"> 
            <?php foreach($data as $item): ?>
                <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col p-1">
                    <div class="bg-white border-t rounded-lg shadow-lg overflow-hidden flex-1 flex flex-wrap">
                        <!-- Título + Boton de Borrar -->
                        <div class="px-1 md:px-0 w-full flex md:mr-0">
                            <a href="<?=base_url('teachers/rubrics/edit/'.$item->id);?>" class="w-full inline-block align-middle border-2 rounded-lg bg-blue-100 font-semibold text-blue-600 text-lg text-center py-1 md:py-1 px-0 md:ml-0">
                                <label>
                                    <?= $item->name ?>
                                </label>
                            </a>
                            <?php if ($teacher_id === $item->t_owner) { ?>
                                <!-- Si és el creador de la rúbrica le permitimos borrarla.  -->
                                <div class="flex justify-around ml-4">
                                    <button 
                                        class="inline-block items-baseline h-full text-center font-bold bg-red-500 hover:bg-red-700 text-white px-4 py-1 text-base rounded"
                                        @click="isOpen = true
                                                $nextTick(() => $refs.modalCloseButton.focus())
                                                "
                                    >
                                        <?=$g_delete[$g_applang]?>
                                    </button>
                                </div>
                            <?php } ?>

                            <!-- Modal para borrar una Rúbrica -->
                            <div
                                style="background-color: rgba(0, 0, 0, .5)" 
                                class="mx-auto absolute top-0 left-0 w-full h-full flex items-center shadow-lg overflow-y-auto"   
                                x-show="isOpen"       
                            >
                                <div class="container mx-auto rounded p-4 mt-2 overflow-y-auto">
                                    <div class="bg-white rounded px-8 py-8">
                                        <h1 class="font-bold text-2xl mb-3 text-center">ATENCIÓ!!!</h1>
                                        <div class="modal-body">
                                            <p class="text-center">Estàs a punt de borrar una Rúbrica. </p>
                                            <p class="text-center">ESTÀS SEGUR??</p>      
                                        </div>
                                        <div class="flex mt-4 justify-center">
                                            <a href="<?=base_url('teachers/rubrics/delete/'.$item->id);?>"
                                                class=" bg-red-500 hover:bg-red-700 text-white px-4 py-3 mt-4 text-sm rounded"
                                                @click="isOpen = false"
                                                x-ref="modalCloseButton"
                                            >
                                                <?=$g_delete[$g_applang]?>
                                            </a>
                                            <button class=" bg-blue-600 hover:bg-blue-800 text-white px-4 py-3 mt-4 ml-4 text-sm rounded"
                                                @click="isOpen = false"
                                                x-ref="modalCloseButton">
                                                <?=$g_cancel[$g_applang]?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="px-1 w-full flex md:flex-col border-t">
                            <div class="flex-1 md:flex-shrink text-xs md:text-base text-left py-2 md:py-0 px-2 md:px-0"><strong>Desc:</strong> <?= $item->description ?></div>
                        </div>
                        <div class="px-1 w-full flex md:flex-col border-t">
                            <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong>Nivel(s):</strong> 
                                <?php 
                                    $levels = json_decode($item->teaching_levels, true);
                                    for($i=0; $i<count($levels); $i++) {
                                        if(($i+1) === count($levels)) echo $levels[$i];
                                        else echo $levels[$i].',';
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="px-1 w-full md:w-1/2 flex md:flex-grow border-t">
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong>Cols:</strong> <?= $item->cols ?></div>
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong>Rows:</strong> <?= $item->rows ?></div>
                            <!-- <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong>Min:</strong> <?= $item->min_value ?></div> -->
                            <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong>Nota:</strong> <?= $item->max_value ?></div>
                        </div>
                        <!-- <div class="px-1 w-full flex md:flex-col border-t md:mb-3">
                            <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><?= $item->data ?></div>
                        </div> -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?= $this->pagination->create_links(); ?>
</div>
