<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <!-- <?= header('Content-Type: text/html; charset=utf-8'); ?> -->
   
    <form action="<?=base_url('teachers/technical_skills_assessments/storeMark')?>" method="POST" 
          class="flex flex-col -my-3 mx-3 p-1 text-center" accept-charset="UTF-8">

        <input type="hidden" value="<?= isset($technical_skills_id) ? $technical_skills_id : '';?>" name="technical_skills_id">
        <input type="hidden" value="<?= isset($challenge_id) ? $challenge_id : '';?>" name="challenge_id">
		<input type="hidden" value="<?= isset($num_students) ? $num_students : '';?>" name="num_students">
        <input type="hidden" value="<?= isset($current_team) ? $current_team : '';?>" name="current_team">
        <?php 
            // print_r("challenge_id=".$challenge_id);
            // print_r("std_ids=".json_encode($students_ids));
        ?>

        <div class="w-full flex flex-wrap justify-center mt-4 mr-4">
                <?php 
                    for($std=0; $std<count($students); $std++) {
                    ?>
                    <input type="hidden" value="<?= $students[$std]['id'];?>" name="students_ids[]">
                    <div class="flex-col w-full justify-center md:mr-4">
                       
                        <div class="flex justify-center text-center my-4 mx-auto">
                            <label class="flex tracking-wide text-gray-700 text-sm md:text-base font-bold mb-2 mr-4">
                                <strong class="uppercase"><?= $students[$std]['firstname'].' '.$students[$std]['lastname'] ?></strong>                        
                            </label>
                            <input id="mark_value-<?=$std?>" name="mark_value-<?=$std?>" type="number" placeholder="0.00" step="0.01" min="0" max="10" 
                                value="<?= empty($assessments) ? '' :  $assessments[$std] ?>"
                                class=" bg-gray-300 border-2 border-gray-500 rounded shadow-lg w-16 md:w-20 text-center">
                        </div>

                        <div class="w-full px-3">
                            <label class="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                <?=$g_ch_peer_comments[$g_applang]?>: <?= $students[$std]['firstname'] ?>
                            </label>
                            <textarea class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                rows="2" name="comments[]" value=""><?= empty($comments) ? '' : $comments[$std] ?></textarea>
                        </div>

                    </div>
                 <?php 
                    }
                ?>
        </div>

        <div class="flex flex-col md:flex-row md:justify-around mt-4">
            <input type="submit" name="submit" value="<?=$g_assess[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block  mb-2 text-xl md:text-lg text-white font-bold px-3 py-1 rounded bg-red-500 hover:bg-red-700">
        </div>
    </form>
</div>
