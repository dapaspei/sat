<div class="w-full mx-auto shadow-md bg-grey-200">
    <div class="bg-white mx-auto shadow-md rounded my-2 pb-0 mb-2">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-2 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <div class="flex flex-wrap w-full" x-data="{ isOpen: false }">
        <div class="flex flex-wrap p-1"> 
            <?php foreach($data as $item): ?>
                <div class="w-full md:w-1/3 flex flex-col p-1">
                    <div class="bg-white border-t rounded-lg shadow-lg overflow-hidden flex-1 flex flex-wrap">
                        <!-- Título + Boton de Borrar -->
                        <div class="px-1 md:px-0 w-full flex md:mr-0">
                            <a href="<?=base_url('teachers/technical_skills/edit/'.$item['id']);?>" class="w-full inline-block align-middle border-2 rounded-lg bg-blue-100 font-semibold text-blue-600 text-lg text-center py-1 md:py-1 px-0 md:ml-0">
                                <label>
                                    <?= $item['name'] ?>
                                </label>
                            </a>
                            <div class="flex justify-around ml-4">
                                <button 
                                    class="inline-block items-baseline h-full text-center font-bold bg-red-500 hover:bg-red-700 text-white px-4 py-1 text-base rounded
                                        <?= $item['t_owner'] != $teacher_id ? 'opacity-25 pointer-events-none' : '' ?>"
                                    @click="isOpen = true
                                            $nextTick(() => $refs.modalCloseButton.focus())
                                            writeModal(<?=$item['id']?>)
                                            "
                                >
                                    <?=$g_delete[$g_applang]?>
                                </button>
                                
                            </div>
                            
                        </div>
                        <!-- Descripción -->
                        <div class="px-1 w-full flex md:flex-row border-t">
                            <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_description[$g_applang]?>:</strong> <?= $item['description'] ?></div>
                            
                        </div>
                        <!-- Curso escolar en el que se aplica los RAs -->
                        <div class="px-1 w-full flex md:flex-row border-t">
                                <div class="flex-1 text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_challenges_name[$g_applang]?>:</strong> 
                            </div>
                            <div class="flex md:flex items-center text-xs text-center py-2 md:py-0 px-2 md:px-0">
                                <div class="flex-col md:flex px-1 border-trasparent bordre-r-2 border rounded-lg mr-1">
                                    <div class="">
                                        <strong><?=$g_year_name[$g_applang]?> </strong>
                                    </div>
                                    <div class="">
                                        <?= $item['school_year'] ?>
                                    </div>
                                </div>
                                <div class="md:ml-0 px-1 border-trasparent bordre-r-2 border rounded-lg">
                                    <div class="">
                                        <strong><?=$g_qualifications_name[$g_applang]?></strong>
                                    </div>
                                    <div class="">
                                        <?php 
                                            if ($item['has_rubric'] === '1') {
                                                echo $g_rubrics_name[$g_applang];
                                            } else {
                                                echo $g_numeric_grade_name[$g_applang];
                                            }

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <!-- MODAL para borrar  -->
        <div id="codigoModal"></div>

        <script>

            function writeModal(TareaEvaluable) {
                // Cogemos la URL Base
                var base = window.location.href;
                // Montamos la URL para borrar el RA seleccionado
                base_url = base+'/delete/'+TareaEvaluable;
                // alert(base_url);
                codeBlock = '<div style="background-color: rgba(0, 0, 0, .5)" class="mx-auto absolute top-0 left-0 w-full h-full flex items-center shadow-lg overflow-y-auto" x-show="isOpen">'
                codeBlock += '  <div class="container mx-auto rounded p-4 mt-2 overflow-y-auto">'
                codeBlock += '      <div class="bg-white rounded px-8 py-8">'
                codeBlock += '          <h1 class="font-bold text-2xl mb-3 text-center">ATENCIÓ!!!</h1>'
                codeBlock += '               <div class="modal-body">'
                codeBlock += '                    <p class="text-center">Estàs a punt de borrar un Resultat d\'Aprenentage. </p>'
                codeBlock += '                    <p class="text-center">ESTÀS SEGUR??</p>'
                codeBlock += '                </div>'
                codeBlock += '                <div class="flex mt-4 justify-center">'
                codeBlock += '                    <a href="'+this.base_url+'"'
                codeBlock += '                        class=" bg-red-500 hover:bg-red-700 text-white px-4 py-3 mt-4 text-sm rounded"'
                codeBlock += '                        @click="isOpen = false"'
                codeBlock += '                       x-ref="modalCloseButton"'
                codeBlock += '                    >'
                codeBlock += '                        <?=$g_delete[$g_applang]?>'
                codeBlock += '                    </a>'
                codeBlock += '                    <button class=" bg-blue-600 hover:bg-blue-800 text-white px-4 py-3 mt-4 ml-4 text-sm rounded"'
                codeBlock += '                        @click="isOpen = false"'
                codeBlock += '                        x-ref="modalCloseButton">'
                codeBlock += '                        <?=$g_cancel[$g_applang]?>'
                codeBlock += '                    </button>'
                codeBlock += '                </div>'
                codeBlock += '            </div>'
                codeBlock += '        </div>'
                codeBlock += '    </div>'

                // Inserting the code block to wrapper element
                document.getElementById("codigoModal").innerHTML = codeBlock
            }
        </script>

    </div>
</div>
