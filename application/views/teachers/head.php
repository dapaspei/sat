<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Para hacer que se vea bien en los móviles -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/svg+xml" href="<?=base_url()?>/favicon.svg">
        <link rel="alternate icon" href="<?=base_url()?>/favicon.ico">
        <link rel="mask-icon" href="<?=base_url()?>/favicon.svg" color="#ff8a01">

        <!-- JS libreria para imprimir PDF -->
        <script type="text/javascript" src="<?=base_url()?>assets/js/html2pdf.bundle.js" ></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/jszip.js" ></script>
        <script type="text/javascript" src="<?=base_url()?>assets/js/FileSaver.js" ></script>

        <title><?= $g_appname ?> - <?= $g_teachers_name[$g_applang] ?></title>
        <!-- Framework Tailwind CSS -->
        <link rel="stylesheet" href="<?=base_url()?>assets/css/cotesavaluapp.css">
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js" defer></script>
        <style>
            /* NavBAR Responsive de Tailwind */
            #menu-toggle:checked + #menu {
                display: block;
            }
            /* Marcar los radiobuttons con color bg-teal-600 */
            input:checked + label {
                background-color: #319795;
            }
            /* Responsive Navbar for TailwindCSS */
            .group:hover .group-hover\:block {
                display: block !important;
            }


            /* CSS styling for before/after/avoid. */
            .before {
                page-break-before: always;
            }
            .after {
                page-break-after: always;
            }
            .avoid {
                page-break-inside: avoid;
            }

            
        </style>        
    </head>
    <body>