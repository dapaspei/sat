<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <!-- <script type="module" src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
    <script nomodule src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine-ie11.min.js" defer></script> -->

    <style>
          [x-cloak] {
              display: none;
          }
    </style>
    <label class="block text-center uppercase tracking-wide text-gray-700 text-base font-bold mt-4 mb-2">
        <?= $g_ch_assign_ts_text[$g_applang]?>
    </label>
    <?php
        // var_dump("<br> selected_TS = ".json_encode($selected_technical_skills));
        // echo "<br>";
        // for($j=0; $j<count($technical_skills); $j++) {
        //     if (in_array($technical_skills[$j]['id'], $selected_technical_skills))
        //         echo 'Sí está dentro ->'.$technical_skills[$j]['id'].'--'.($j+1).' - '.$technical_skills[$j]['name'].'<br>';
        //     else 
        //         echo 'NO está dentro ->'.$technical_skills[$j]['id'].'--'.($j+1).' - '.$technical_skills[$j]['name'].'<br>';
        // }
    ?>
    <select x-cloak id="select" class="w-full flex flex-col items-center px-3 mb-6">
        <?php
            $ts_selected = [];
            // Si no hay seleccionado ningún RA todavía
            if(empty($selected_technical_skills)) {
                for($i=0; $i<count($technical_skills); $i++)
                    echo '<option value="'.$technical_skills[$i]['id'].'">'.($i+1).' - '.$technical_skills[$i]['name'].'</option>\n';
            } else {
                // Si ya hay algún RA seleccionado, lo marcamos.
                for($i=0; $i<count($technical_skills); $i++) {
                    // echo '<option value="'.$technical_skills[$i]['id'].'">'.($i+1).' - '.$technical_skills[$i]['name'].'</option>\n';
                    // if (intval($students_in_teams[$i]['s_team']) === $equip)
                    if (in_array($technical_skills[$i]['id'], $selected_technical_skills)) {
                        echo '<option selected value="'.$technical_skills[$i]['id'].'">'.($i+1).' - '.$technical_skills[$i]['name'].'</option>\n';
                        $ts_selected[] = $technical_skills[$i]['id'];
                    }
                    else 
                        echo '<option value="'.$technical_skills[$i]['id'].'">'.($i+1).' - '.$technical_skills[$i]['name'].'</option>\n';
                }
            }
            
        ?>
    </select>

    <script>
        // Rellenar el combo.
        preFill($ts_selected);
    </script>
   

    <label class="block text-center uppercase tracking-wide text-gray-700 text-base font-bold mt-4 mb-2">
        Las RAs seleccionadas son: <?= json_encode($ts_selected) ?>
    </label>
    <div x-data="dropdown()" x-init="loadOptions()" class="w-full flex flex-col items-center h-32 mx-auto">
        <form action="<?=base_url('teachers/challenges/updateTechnicalSkills')?>" method="POST">
            <input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id']: '';?>" name="challenge_id">

            <input name="values" type="hidden" x-bind:value="selectedValues()">
            <div class="inline-block relative w-full md:w-7/8 ml-2 mr-4 md:mr-32">
                <div class="flex flex-col items-center relative">
                    <div @click="open" class="w-full  svelte-1l8159u">
                        <div class="my-2 p-1 flex border border-gray-200 bg-white rounded svelte-1l8159u">
                            <div class="flex flex-auto flex-wrap">
                                <template x-for="(option,index) in selected" :key="options[option].value">
                                    <div
                                        class="flex justify-center items-center m-1 font-medium py-1 px-2rounded-full text-teal-700 bg-teal-100 border border-teal-300 ">
                                        <div class="text-xs font-normal leading-none max-w-full flex-initial" x-model="options[option]" x-text="options[option].text">
                                        </div>
                                        <div class="flex flex-auto flex-row-reverse">
                                            <div @click="remove(index,option)">
                                                <svg class="fill-current h-6 w-6 " role="button" viewBox="0 0 20 20">
                                                    <path d="M14.348,14.849c-0.469,0.469-1.229,0.469-1.697,0L10,11.819l-2.651,3.029c-0.469,0.469-1.229,0.469-1.697,0c-0.469-0.469-0.469-1.229,0-1.697l2.758-3.15L5.651,6.849c-0.469-0.469-0.469-1.228,0-1.697s1.228-0.469,1.697,0L10,8.183l2.651-3.031c0.469-0.469,1.228-0.469,1.697,0s0.469,1.229,0,1.697l-2.758,3.152l2.758,3.15C14.817,13.62,14.817,14.38,14.348,14.849z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                                <div x-show="selected.length == 0" class="flex-1">
                                    <input placeholder=""
                                        class="bg-transparent p-1 px-2 appearance-none outline-none h-full w-full text-gray-800"
                                        x-bind:value="selectedValues()"
                                    >
                                </div>
                            </div>
                            <div class="text-gray-300 w-8 py-1 pl-2 pr-1 border-l flex items-center border-gray-200 svelte-1l8159u">
                                <button type="button" x-show="isOpen() === true" @click="open"
                                    class="cursor-pointer w-6 h-6 text-gray-600 outline-none focus:outline-none">                              
                                    <svg class="fill-current h-4 w-4" viewBox="0 0 20 20">
                                        <path d="M2.582,13.891c-0.272,0.268-0.709,0.268-0.979,0s-0.271-0.701,0-0.969l7.908-7.83c0.27-0.268,0.707-0.268,0.979,0l7.908,7.83c0.27,0.268,0.27,0.701,0,0.969c-0.271,0.268-0.709,0.268-0.978,0L10,6.75L2.582,13.891z" />
                                    </svg>

                                </button>
                                <button type="button" x-show="isOpen() === false" @click="close"
                                    class="cursor-pointer w-6 h-6 text-gray-600 outline-none focus:outline-none">
                                    <svg version="1.1" class="fill-current h-4 w-4" viewBox="0 0 20 20">
                                        <path d="M17.418,6.109c0.272-0.268,0.709-0.268,0.979,0s0.271,0.701,0,0.969l-7.908,7.83c-0.27,0.268-0.707,0.268-0.979,0l-7.908-7.83c-0.27-0.268-0.27-0.701,0-0.969c0.271-0.268,0.709-0.268,0.979,0L10,13.25L17.418,6.109z" />
                                    </svg>

                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="w-full px-4">
                        <div x-show.transition.origin.top="isOpen()"
                            class="absolute shadow top-100 bg-white z-40 w-full lef-0 rounded max-h-select overflow-y-auto svelte-5uyqqj"
                            @click.away="close">
                            <div class="flex flex-col w-full">
                                <template x-for="(option,index) in options" :key="option">
                                    <div>
                                        <div class="cursor-pointer w-full border-gray-100 rounded-t border-b hover:bg-teal-100"
                                            @click="select(index,$event)">
                                            <div x-bind:class="option.selected ? 'border-teal-600' : ''"
                                                class="flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative">
                                                <div class="w-full items-center flex">
                                                    <div class="mx-2 leading-6" x-model="option" x-text="option.text"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- on tailwind components page will no work  -->
            <!-- <button disabled class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="submit">Test</button> -->
            <div class="flex justify-around md:mt-4">
                <input type="submit" name="submit" value="<?=$g_assign[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded bg-red-500 hover:bg-red-600">
            </div>
        
        </form>
    </div>

    <script>
        function dropdown() {
            return {
                options: [],
                selected: [],
                show: false,
                open() { this.show = true },
                close() { this.show = false },
                isOpen() { return this.show === true },
                preFill(ts_selected) {

                    this.selected.push(1);
                    this.options[1].selected = true;

                    // for (let i = 0; i < ts_selected.length; i++) {
                    //     this.options.push({
                    //         value: options[i].value,
                    //         text: options[i].innerText,
                    //         selected: options[i].getAttribute('selected')
                    //     });
                    // }

                    // if (!this.options[index].selected) {

                    //     this.options[index].selected = true;
                    //     // this.options[index].element = event.target;
                    //     this.selected.push(index);

                    // } else {
                    //     this.selected.splice(this.selected.lastIndexOf(index), 1);
                    //     this.options[index].selected = false
                    // }
                },
                select(index, event) {

                    if (!this.options[index].selected) {

                        this.options[index].selected = true;
                        this.options[index].element = event.target;
                        this.selected.push(index);

                    } else {
                        this.selected.splice(this.selected.lastIndexOf(index), 1);
                        this.options[index].selected = false
                    }
                },
                remove(index, option) {
                    this.options[option].selected = false;
                    this.selected.splice(index, 1);


                },
                loadOptions() {
                    const options = document.getElementById('select').options;
                    for (let i = 0; i < options.length; i++) {
                        this.options.push({
                            value: options[i].value,
                            text: options[i].innerText,
                            selected: options[i].getAttribute('selected') != null ? options[i].getAttribute('selected') : false
                        });
                    }


                },
                selectedValues(){
                    return this.selected.map((option)=>{
                        return this.options[option].value;
                    })
                }
            }
        }
        // dropdown().loadOptions();
        preFill();
    </script>

</div>
