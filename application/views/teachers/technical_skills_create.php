<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/technical_skills/store')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center" x-data="{ selected: 'opt1' }">
        <input type="hidden" value="<?= set_value('t_owner', isset($t_owner) ? $t_owner : '');?>" name="t_owner">

        <div class="mx-auto w-full flex flex-col items-center mb-2 bg-gray-100">
            <!-- Nombre y descripción -->
            <div class="flex flex-wrap w-full mb-2">
                <div class="w-full px-3 mb-4 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-base font-bold mb-2">
                        <?=$g_name[$g_applang]?>
                    </label>
                    <input type="text" name="name" value="" 
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        <?php echo form_error('name', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>
                </div>
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-base font-bold mb-2">
                        <?=$g_description[$g_applang]?>
                    </label>
                    <textarea rows="3" name="description" value="" 
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= set_value('description', isset($rubric['description']) ? $rubric['description'] : '');?></textarea>
                        <?php echo form_error('description', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>

                </div>
            </div>
            <div class="flex flex-col mt-4 border-2 border-gray-400 rounded-lg p-2">
                <label class="block uppercase tracking-wide text-gray-700 text-lg font-bold mb-2">
                    Tria el tipus de qualificació:
                </label>
 
                <div class="flex items-center mt-3 mr-4 mb-4">
                    <input x-on:click="selected = 'opt1'" id="mark" type="radio" name="qualification" value="mark" class="w-6 h-6" checked />
                    <span for="mark" class="flex items-center cursor-pointer text-base ml-2">
                        Qualificació numérica
                    </span>
                </div>
                <div x-show="selected === 'opt1'" x-cloak class="-mt-2 mb-8">
                    <!-- Texto a mostrar si opción 1 -->
                    Qualificació de 0 a 10.
                </div>

            
                <div class="flex items-center mr-4 mb-4">
                    <input x-on:click="selected = 'opt2'" id="rubric" type="radio" name="qualification" value="rubric" class="w-6 h-6" />
                    <span for="rubric" class="flex items-center cursor-pointer text-base ml-2">
                        Rúbrica
                    </span>
                </div>
                <!-- Mostrar las rúbricas disponibles -->
                <div x-show="selected === 'opt2'" x-cloak>
                    <div class="w-full md:w-2/3 mx-1 md:mx-4">
                        <select name="rubric_id" class="flex mx-auto text-center appearance-none max-w-sm md:max-w-xl bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            <?php foreach($rubrics as $item): ?>
                                <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>  
                </div>
            </div>

            <!-- Curs Escolar + Repte a assignar -->
            <div class="p-2 mt-2 border-2 border-gray-400 rounded-lg">
                <div class="flex-col justify-center mt-4 mb-2">
                    <div class="flex w-full mb-6 md:mb-2 md:mt-4">
                        <div class="mr-2 w-full">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                <?=$g_year_name[$g_applang]?>
                            </label>
                            <select name="school_year" class="w-full block text-center appearance-none  bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            <?php 
                                for($i=0; $i<count($g_school_year); $i++) {
                                    if ($g_school_year[$i] === $current_school_year)
                                        echo '<option selected value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                                    else 
                                        echo '<option value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                    </div>
                    <div class="flex mx-auto w-full px-1 md:px-3 mb-6 md:mb-0 md:mt-4">
                        <div class="flex-col justify-center w-full">
                            <div class="w-full">
                                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                    <?=$g_challenges_name[$g_applang]?>
                                </label>
                                <select multiple size="5" name="challenges_id[]" class="block text-center appearance-none bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 ml-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                                    <?php foreach($challenges as $item): ?>
                                        <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Cursos a los que se puede aplicar este Resultado de Aprendizaje -->
                <!-- <div class="flex justify-center mt-2 mb-2">
                    <div class="flex mx-auto w-full justify-center px-0 md:px-3 mb-6 md:mb-0 md:mt-4">
                        <div class="flex-col w-full mx-auto max-w-xs md:max-w-lg ">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-2">
                                <?=$g_ch_levels[$g_applang]?>
                            </label>
                            <select multiple size="10" name="teaching_levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                                <?php
                                    for($i=0; $i<count($g_levels); $i++)
                                    echo '<option value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                                ?>
                            </select>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="flex justify-around mt-4">
                <input type="submit" name="submit" value="<?=$g_next[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded bg-red-500 hover:bg-red-700">
            </div>
        </div>
    </form>
    <!-- <script>
        new SlimSelect({
            select: '#multiple'
        })
    </script> -->
</div>
