<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/rubrics/storeCriteria')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
		<input type="hidden" value="<?= isset($rubric['id']) ? $rubric['id'] : '';?>" name="id">
        <input type="hidden" value="<?= isset($rubric['cols']) ? $rubric['cols'] : '';?>" name="cols">
		<input type="hidden" value="<?= isset($rubric['rows']) ? $rubric['rows'] : '';?>" name="rows">

        <div class="flex flex-wrap -mx-3 -mt-6 mb-2">
            <div class="w-full flex flex-row mt-2">
                <label class="w-2/6 inline-block uppercase tracking-wide text-center text-gray-700 text-lg font-bold">
                    Items
                </label>
                <label class="w-4/6 inline-block uppercase tracking-wide text-center text-gray-700 text-lg font-bold">
                    Criterios
                </label>
            </div>
            <div class="flex flex-row justify-right -mx-3 mt-2">
                <label class="w-2/6 mr-4 px-2 inline-block uppercase tracking-wide text-center text-gray-700 text-lg font-bold"></label>
                <div class="w-4/6 inline-block flex justify-around"> 
                    <?php for($j=0; $j<$rubric['cols']; $j++) { ?>
                            <div class="flex-col w-1/'<?=$rubric['cols']?>' md:ml-1 md:mr-2">
                                <textarea rows="2" name="descriptions_value[]" value="" 
                                    class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> h-8 text-center appearance-none block w-full text-gray-800 border border-gray-400 rounded py-1 px-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?=
                                    round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?></textarea>
                            </div>
                    <?php } ?>
                </div>
            </div>
            <?php for($i=0; $i<$rubric['rows']; $i++) { ?>
                    <div class="flex flex-col md:flex-row -mx-3 mt-4 mb-2">
                        <div class="justify-center w-full md:w-2/6 md:mr-4 px-2 mb-2">
                            <textarea rows="3" name="criteria[]" value="" 
                                class="<?=$g_bg_criteria?> appearance-none block w-full text-gray-800 border border-gray-400 rounded py-1 px-2 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"></textarea>
                        </div>
                        <div class="w-full md:w-4/6 inline-block flex justify-around py-0"> 
                    <?php for($j=0; $j<$rubric['cols']; $j++) { ?>
                            <div class="flex-col w-1/'<?=$rubric['cols']?>' md:ml-1 md:mr-2">
                                <textarea rows="2" name="descriptions[]" value="" 
                                    class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> appearance-none block w-full h-16 md:h-24 text-gray-800 border border-gray-400 rounded py-0 px-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"></textarea>
                            </div>
                    
                    <?php } ?>
                        </div>
                    </div>
            <?php } ?>
        </div>
<!--  -->
        <!-- <div class="flex flex-wrap -mx-3 -mt-6 mb-2">
            <?php
                for($i=1; $i<=$rubric['rows']; $i++) { ?>
                    <div class="flex flex-col md:flex-row -mx-3 mt-6 mb-2">
                        <div class="justify-center w-full md:w-2/6 md:mr-4 px-2 mb-2">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold">
                                Item <?=$i?>
                            </label>
                            <textarea rows="3" name="criteria[]" value="" 
                                class="<?=$g_bg_criteria?> appearance-none block w-full text-gray-800 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"></textarea>
                        </div>
                        <div class="w-full md:w-4/6 flex justify-around border-teal-400 border-2 py-1"> 
                        <label class="block uppercase tracking-wide text-center text-gray-700 text-xs font-bold">
                            Criterios
                        </label>
                    <?php
                        for($j=1; $j<=$rubric['cols']; $j++) { ?>
                            <div class="flex-col w-1/'<?=$rubric['cols']?>' md:ml-1 md:mr-2">
                                <textarea rows="2" name="descriptions[]" value="" 
                                    class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> appearance-none block w-full text-gray-800 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"></textarea>
                            </div>
                    
                    <?php } ?>
                        </div>
                    </div>
            <?php } ?>
        </div> -->
        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="Finalizar" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="Cancelar" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded text-xs bg-red-500 hover:bg-red-700">
        </div>
    </form>
</div>
