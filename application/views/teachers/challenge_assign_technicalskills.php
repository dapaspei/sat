<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <label class="block text-center uppercase tracking-wide text-gray-700 text-base font-bold mt-4 mb-2">
        <?= $g_ch_assign_ts_text[$g_applang]?>  - TOTAL: 
        <output type="text" name="total" id="total" class="" readonly placeholder="Max 100%"></output> %
    </label>
    <?php
        // var_dump("<br> selected_TS = ".json_encode($selected_technical_skills));
        // var_dump("<br> ALL_TS = ".json_encode($technical_skills));

        // echo "<br>";
        // for($k=0; $k<count($technical_skills); $k++) {
        //     if (in_array($technical_skills[$k]['id'], $selected_technical_skills))
        //         echo 'Sí está dentro ->'.$technical_skills[$k]['id'].'--'.($k+1).' - '.$technical_skills[$k]['name'].'<br>';
        //     else 
        //         echo 'NO está dentro ->'.$technical_skills[$k]['id'].'--'.($k+1).' - '.$technical_skills[$k]['name'].'<br>';
        // }
    ?>
    <div class="w-full mx-auto">
        <form action="<?=base_url('teachers/challenges/saveTechnicalSkills')?>" method="POST" class="flex flex-col w-full -my-3 mx-0 p-2 text-center">
        <input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id']: '';?>" name="challenge_id">
        <input type="hidden" value="<?= count($technical_skills) ?>" name="num_technical_skills">
            <div class="flex-col md:flex mt-0">
                <?php 

                    if(empty($selected_technical_skills)) {
                        for($i=0; $i<count($technical_skills); $i++) { ?>
                            <div class="border-2 border-gray-500  rounded-lg shadow-3xl mb-2">
                                <div class="flex flex-row mx-auto">
                                    <div class="flex w-4/6  md:w-10/12 p-1">
                                        <label class="flex justify-start items-start">
                                            <div class="font-semibold text-gray-700 select-none"><?= $technical_skills[$i]['name'] ?> </div>
                                        </label>
                                    </div>
                                    <!-- Porcentaje de cada item -->
                                    <div class="flex flex-col border-l-2 border-gray-400 items-center mx-auto md:w-2/12 pl-1 mb-1">
                                                <label class="w-full mx-auto text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2">
                                                    <strong><?= $g_technical_skills_percentage[$g_applang] ?></strong>
                                                </label>
                                                <!-- Input + Porcentaje -->
                                                <div class="flex"> 
                                                    <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">
                                                        <input type="checkbox" name="technical_skills[]" value="<?=$technical_skills[$i]['id']?>" 
                                                            class="opacity-0 absolute">
                                                            <!-- onclick="return ValidateSelection();" -->
                                                            <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
                                                    </div>
                                                    <div>
                                                        <input id="percentages<?=$i?>" name="percentages[]" type="number" placeholder="0.00 %" step="0.01" min="0" max="100" 
                                                            oninput="updateTotal()"
                                                            value=""
                                                            class=" bg-gray-300 border-2 border-gray-500 rounded shadow-lg w-16 md:w-20 text-center" />
                                                    </div>
                                                </div>
                                                                                      
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    } else { 
                        for($j=0; $j<count($technical_skills); $j++) { ?>
                            
                            <div class="border-2 border-gray-500  rounded-lg shadow-3xl mb-2">
                                <div class="flex flex-row mx-auto">
                                    <div class="flex w-4/6  md:w-10/12 p-1">
                                        <label class="flex justify-start items-start">
                                            <div class="font-semibold text-gray-700 select-none"><?= $technical_skills[$j]['name'] ?> </div>
                                        </label>
                                    </div>
                                    <!-- Porcentaje de cada item -->
                                    <div class="flex flex-col border-l-2 border-gray-400 items-center mx-auto md:w-2/12 pl-1 mb-1">
                                                <label class="w-full mx-auto text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2">
                                                    <strong><?= $g_technical_skills_percentage[$g_applang] ?></strong>
                                                </label>
                                                <!-- Input + Porcentaje -->
                                                <div class="flex"> 
                                                    <div class="bg-white border-2 rounded border-gray-400 w-6 h-6 flex flex-shrink-0 justify-center items-center mr-2 focus-within:border-blue-500">
                                                        <input <?= array_key_exists($technical_skills[$j]['id'], $selected_technical_skills) ? ' checked ': '' ?>
                                                            type="checkbox" name="technical_skills[]" value="<?=$technical_skills[$j]['id']?>" 
                                                            class="opacity-0 absolute">
                                                            <!-- onclick="return ValidateSelection();" -->
                                                                <svg class="fill-current hidden w-4 h-4 text-green-500 pointer-events-none" viewBox="0 0 20 20"><path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/></svg>
                                                    </div>
                                                    <div>
                                                        <input id="percentages<?=$j?>" name="percentages[]" type="number" placeholder="0.00 %" step="0.01" min="0" max="100" 
                                                            oninput="updateTotal()"
                                                            value="<?= array_key_exists($technical_skills[$j]['id'], $selected_technical_skills) ? $selected_technical_skills[$technical_skills[$j]['id']] : '' ?>"
                                                            class=" bg-gray-300 border-2 border-gray-500 rounded shadow-lg w-16 md:w-20 text-center" />  
                                                    </div>

                                                </div>
                                                                                      
                                    </div>
                                </div>
                            </div>

                    <?php
                        }
                        
                    }
                ?>
                
            </div>


            <div class="flex justify-around md:mt-4">
                <input type="submit" name="submit" value="<?=$g_assign[$g_applang]?>" onclick="return ValidateEquality();" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded bg-red-500 hover:bg-red-600">
            </div>
        
        </form>
    </div>
    <style>
        input:checked + svg {
            display: block;
        }
    </style>
    <script type="text/javascript">
        
        // Al enviar, se valida todo: porcentajes y que las selecciones estén igualadas.
        function ValidateEquality() {

            // Primero comprobamos el porcentaje total
            if(!checkTotal())
                return false;

            // Ahora validamos que el número de elementos y el número de porcentajes sea el mismo.
            var checkboxes = document.getElementsByName("technical_skills[]");  
            var percentages = document.getElementsByName('percentages[]');

            // Ver cuántos checkboxes hay marcados.
            var numberOfCheckedItems = 0;  
            for(var i = 0; i < checkboxes.length; i++) {  
                if(checkboxes[i].checked)  
                    numberOfCheckedItems++;  
            }  
            // Contar cuantos porcentajes tienen valor.
            var numberOfPercetagesItems = 0;
            for(var i=0; i<percentages.length;i++){
                // console.log("porcentaje:"+percentages[i].value);
                
                if(Number(percentages[i].value) > 0)
                    numberOfPercetagesItems++;
            }
            // Ver si son los mismos valores
            // console.log("numberOfCheckedItems="+numberOfCheckedItems);
            // console.log("numberOfPercetagesItems="+numberOfPercetagesItems);

            if(numberOfCheckedItems != numberOfPercetagesItems) {
                alert("Comprova la selecció i els percentatges!");
                return false;
            }

        }  

        // Comprobar que al menos se seleccione una tarea
        function ValidateSelection() {  
            var checkboxes = document.getElementsByName("technical_skills[]");  
            var numberOfCheckedItems = 0;  
            for(var i = 0; i < checkboxes.length; i++) {  
                if(checkboxes[i].checked)  
                    numberOfCheckedItems++;  
            }  
            if(numberOfCheckedItems < 1) {  
                alert("Tria com a mínim una tasca!");  
                // alert($g_ts_select_error[$g_applang]);
                return false;  
            }  
        }  
        

        // Comprobar que el porcentaje sea = 100%
        function checkTotal() {
            "use strict";
            var arr = document.getElementsByName('percentages[]');
            var tot = 0;
            // console.log("Tot="+tot);
        
            // console.log("Long array="+arr.length);
            for(var i=0; i<arr.length;i++) {
               
                // console.log("valores:"+arr[i].value);
                tot += Number(arr[i].value);
            }        
            console.log("total = "+tot);               
            document.getElementById('total').value = tot;   
            // check that weightage scores = 100    
            // if ((tot > 100) || (tot < 100)) {   
            if (tot != 100) {
                alert("El percentatge total ha de ser igual a 100%.");
                    // $("input").focus(function() {
                    //     $( this ).css( "border-color", "red" ).val("");
                    // });
                    // $('#form1').find('#total').val('');
                return false;
            } else return true;
    
        }    

        // Actualizar el label con el Total de los porcentajes
        function updateTotal() {
            "use strict";
            var arr = document.getElementsByName('percentages[]');
            var tot = 0;
            // console.log("Tot="+tot);
            // console.log("Long array="+arr.length);
            for(var i=0; i<arr.length;i++) {
                // console.log("valores:"+arr[i].value);
                tot += Number(arr[i].value);
            }        
            // console.log("total = "+tot);               
            document.getElementById('total').value = tot;   
        }

        // Ejecutamos la funcion al cargar la página para poner el Porcentaje Total
        window.onload = function() {
            updateTotal();
        };
    </script>

</div>
