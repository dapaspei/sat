<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/challenges/store')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">

        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-full px-3 mb-4 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_name[$g_applang]?>
                </label>
                <input type="text" name="name" value="" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                    <?php echo form_error('name', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>
            </div>
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_description[$g_applang]?>
                </label>
                <textarea rows="3" name="description" value="" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= set_value('description', isset($rubric['description']) ? $rubric['description'] : '');?></textarea>
                    <?php echo form_error('description', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>

            </div>
        </div>
        <div class="flex flex-grow flex-wrap -mx-3 mt-2 mb-2">
            <div class="flex w-full justify-around px-3 mb-6 md:mb-0">
                <div class="mr-2 w-1/2">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_ch_fechainicio[$g_applang] ?>
                    </label>
                    <input id="start_date" type="date" name="start_date" value="<?= date("Y-m-d"); ?>"
                        class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </div>
                <div class="mr-2 w-1/2">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?= $g_ch_fechafin[$g_applang] ?>
                    </label>
                    <input id="finish_date" type="date" name="finish_date" value="<?= date('Y-m-d', strtotime(date("Y-m-d"). ' +12 weeks')); ?>" 
                        class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </div>
            </div>
            <div class="flex w-full justify-around px-0 md:px-3 mb-6 md:mb-0 md:mt-4">
                <div class="mr-2 w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_year_name[$g_applang]?>
                    </label>
                    <select name="year" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                        for($i=0; $i<count($g_school_year); $i++) {
                            if($g_school_year[$i] === $school_year) 
                                echo '<option selected value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                            else
                                echo '<option value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="mr-2 w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Sprints
                    </label>
                    <select name="sprints" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($g_sprints); $i++)
                            echo '<option value="'.$g_sprints[$i].'" >'.$g_sprints[$i].'</option>\n';
                		?>
                    </select>
                </div>
                <div class="mr-2 w-2/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_ch_subject[$g_applang]?>
                    </label>
                    <input type="text" name="subject" value="" placeholder=""
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-2 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                </div>
            </div>
            <div class="flex w-full justify-around px-0 md:px-3 mb-6 md:mb-0 md:mt-4">
                <div class="w-full">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_ch_levels[$g_applang]?>
                    </label>
                    <select multiple size="10" name="teaching_levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php
                            for($i=0; $i<count($g_levels); $i++)
                            echo '<option value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                		?>
                    </select>
                </div>
            </div>
        </div>

        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_next[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded text-xs bg-red-500 hover:bg-red-700">
        </div>
    </form>
    <!-- <script>
        new SlimSelect({
            select: '#multiple'
        })
    </script> -->
</div>
