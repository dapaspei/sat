<div class="w-full mx-auto shadow-md">
    <div id="challenge_name" class="hidden"><?= isset($challenge_name) ? $challenge_name: 'Repte sense nom';?></div>    

    <!-- TailwindCSS Spinner Loading Animation  -->
    <div id="spinnerTailwind">
        <link rel="stylesheet" href="https://pagecdn.io/lib/font-awesome/5.10.0-11/css/all.min.css" integrity="sha256-p9TTWD+813MlLaxMXMbTA7wN/ArzGyW/L7c5+KkjOkM=" crossorigin="anonymous">
        <div class="w-full h-full fixed block top-0 left-0 bg-white opacity-75 z-50">
            <span class="text-green-500 opacity-75 top-1/2 my-0 mx-auto block relative w-0 h-0" style="
                top: 50%;
            ">
                <i class="fas fa-circle-notch fa-spin fa-5x"></i>
            </span>
        </div> 
    </div>
   
    <div class="w-full p-1">     
        <div class="w-full pt-4">

            <?php 
                $students_ids = array_keys($all_grades);
            ?> 
                <div id="num_students" class="hidden"><?=count($students_ids)?></div>

                <!-- All Students -->
                <?php 
                        // $students_ids = array_keys($all_grades);
                        for($student=0; $student<count($students_ids); $student++) { 
                                $current_student_id = $students_ids[$student];
                            $current_grades = array_values($all_grades[$current_student_id]);
                    ?>
                    <!-- <div class="before"> -->
                    <div id="toPdf<?=$student?>" class="w-full mt-2">
                        <div id="studentName<?=$student?>" class="hidden"><?=$students[$student]['lastname'].",".$students[$student]['firstname']?></div>
                        
                        <!-- Tabla con las calificaciones finales -->
                        <div class="border-2 border-gray-600  rounded-lg shadow-3xl">
                            <div class="flex mx-auto">
                                <label class="w-1/4 md:w-1/3 block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold border-r-2 border-gray-700">
                                    <strong><?=$students[$student]['firstname']." ".$students[$student]['lastname']?></strong>
                                </label>
                                <div class="w-3/4 md:w-2/3 flex flex-row md:flex-1">
                                    <div class="flex flex-col border-r-2 md:w-full">
                                        <label class="w-full text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                            <strong><?= $g_soft_skills_name[$g_applang] ?></strong>
                                        </label>
                                        <label class="w-full text-center uppercase tracking-wide text-gray-700 text-base md:text-lg font-bold mb-2">
                                            <strong><?= $all_marks[$current_student_id] ?></strong>
                                        </label>
                                    </div>
                                    <div class="flex flex-col border-r-2 md:w-full">
                                        <label class="w-full text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                            <strong><?= $g_technical_skills_name[$g_applang] ?></strong>
                                        </label>
                                        <label class="w-full  text-center uppercase tracking-wide text-gray-700 text-base md:text-lg font-bold mb-2">
                                        <?php if ($num_technical_skills > 0) { ?>
                                            <strong><?= $all_average_technical_marks[$student] ?></strong>
                                        <?php } ?>
                                        </label>
                                    </div>
                                    <div class="flex flex-col border-2 md:w-full border-yellow-300 bg-yellow-200">
                                        <label class="w-full bg-white text-center uppercase tracking-wide text-gray-700 text-xs md:text-lg font-bold mb-2 border-b border-gray-600">
                                            <strong><?= $g_final_qualification_name[$g_applang] ?></strong>
                                        </label>
                                        <label class="w-full h-full text-center uppercase tracking-wide text-gray-700 text-lg md:text-lg font-bold mb-2">
                                        <?php if ($num_technical_skills > 0) { ?>
                                            <!-- SoftSkills 40% - TechnicalSkills 60% -->
                                            <strong><?= number_format(floatval($all_marks[$current_student_id])*0.4 + floatval($all_average_technical_marks[$student])*0.6, 2) ?></strong>
                                        <?php } ?>
                                        </label>
                                    </div>
                                
                                </div>
                                
                            </div>
                        </div>

                        <div class="md:flex">
                            <!-- SPRINS TOTAL -->
                            <div class="w-full md:w-3/5 p-1 border-1"> 
                                <div class="w-full border-0 items-center justify-center">
                                    <?php for($k=0; $k<$cols; $k++) { ?>
                                        <div class="shadow w-full bg-gray-200 border-1 py-1 items-center">
                                            <div class="<?= $g_bg_score[floor($current_grades[$k])]?> text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left text-teal" 
                                                style="width: <?= ($current_grades[$k]==0) ? 100 : (10*$current_grades[$k]) ?>%"><?= ($current_grades[$k]==0) ? '' : number_format($current_grades[$k],2).' - ' ?><?= $criteria[$k] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- SPRINTS PARTIAL -->
                            <div class="w-full md:w-2/5">
                                <?php for($j=0; $j<$cols; $j++) { ?>
                                    <div class="shadow w-full bg-gray-400 border-1 py-0 items-center">
                                        <div class="text-xs rounded-lg leading-none py-0 px-0 text-center text-teal"><?= $criteria[$j].' - '.(1/$cols*100)."%" ?>
                                        </div>
                                    </div>
                                    <div class="w-full border-1 items-center justify-center mb-2">
                                        <?php for($i=0; $i<count($all_sprints_grades[$current_student_id]);$i++){ ?>
                                            <div class="shadow w-full bg-gray-200 border-1 py-0 items-center">
                                                <div class="<?= $g_bg_score[floor($all_sprints_grades[$current_student_id][$i][$j])]?> text-xs rounded-lg leading-none py-0 px-0 pl-2 text-left text-teal" 
                                                    style="width: <?= ($all_sprints_grades[$current_student_id][$i][$j]==0) ? 100 : (10*$all_sprints_grades[$current_student_id][$i][$j]) ?>%">S<?=($i+1)?> - <?= ($all_sprints_grades[$current_student_id][$i][$j]==0) ? '' : number_format($all_sprints_grades[$current_student_id][$i][$j],2)?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="md:flex w-full">
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <div class="mb-2">
                                    <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_my_self_comments[$g_applang]?></div>
                                        <span class="text-grey-darker text-xs md:text-sm"><?= nl2br($all_comments[$current_student_id]['self_comments']) ?></span>
                                </div>
                            </div>
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <div class="mb-2">
                                    <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_team_comments[$g_applang]?></div>
                                        <span class="text-grey-darker text-xs md:text-sm"><?= nl2br($all_comments[$current_student_id]['peer_comments']) ?></span>
                                </div>
                            </div>
                            <div class="w-full flex-1 md:w-1/3 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                <div class="mb-2">
                                    <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_teacher_comments[$g_applang]?></div>
                                        <span class="text-grey-darker text-xs md:text-sm"><?= nl2br($all_comments[$current_student_id]['teacher_comments']) ?></span>
                                </div>
                            </div>
                        </div>
                        <!-- Si NO HAY definido ningún RA en el RETO, no se muestran los HARD SKILLS.  -->
                        <?php if ($num_technical_skills > 0) { ?>
                            <!-- Technical Skills Marks -->
                            <label class="w-full block text-center uppercase tracking-wide text-gray-700 text-sm md:text-lg font-bold mb-2 mt-4">
                                <?= $g_technical_skills_name[$g_applang] ?> - <strong><?= $all_average_technical_marks[$student] ?></strong>
                            </label>
                            <div class="w-full mt-2">
                                <!-- RAs NAMES -->
                                <div class="flex-col w-full p-0 border-1"> 
                                    <?php for($k=0; $k<$num_technical_skills; $k++) { ?>
                                        <div class="flex w-full border-0 items-center justify-center">
                                            <div class="flex shadow w-full border-1 py-1 items-center">
                                                <div class="bg-white text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left">
                                                    <?= $technical_skills_percentages[$k] ?>%
                                                </div>
                                                <div class="bg-orange-200 text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left text-gray-800 w-full">
                                                    <?= $all_technical_names[$current_student_id][$k] ?>
                                                </div>
                                            </div>
                                            <div class="flex shadow w-1/7 bg-gray-200 border-1 p-1 items-center">
                                                <div class="bg-white text-sm md:text-lg rounded-lg leading-none py-2 px-2 text-left  w-full
                                                <?= $all_technical_marks[$current_student_id][$k] == '0' ? 'text-gray-400 disabled' : 'text-gray-800' ?>">
                                                    <?= $all_technical_marks[$current_student_id][$k] == '0' ? '0.00' : $all_technical_marks[$current_student_id][$k] ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- HARD SKILLS COMMENTS -->
                            <div class="w-full">
                                <div class="w-full flex-1 bg-gray-100 shadow-md border-r border-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light rounded-b lg:rounded-b-none lg:rounded-r p-2 flex flex-col justify-between leading-normal">
                                    <div class="mb-2">
                                        <div class="text-black font-semibold text-sm md:text-base mb-0"><?= $g_ch_comments[$g_applang]?>:</div>
                                            <span class="text-grey-darker text-base md:text-lg"><?= nl2br($all_technical_comments[$current_student_id]) ?></span>
                                    </div>
                                </div>
                            </div>

                        <?php } ?> 

                    </div> 
                <?php } ?>
        </div> 
        
    <div>

    <script>
    /*

    ____          _____               _ _           _       
    |  _ \        |  __ \             (_) |         | |      
    | |_) |_   _  | |__) |_ _ _ __ _____| |__  _   _| |_ ___ 
    |  _ <| | | | |  ___/ _` | '__|_  / | '_ \| | | | __/ _ \
    | |_) | |_| | | |  | (_| | |   / /| | |_) | |_| | ||  __/
    |____/ \__, | |_|   \__,_|_|  /___|_|_.__/ \__, |\__\___|
            __/ |                               __/ |        
            |___/                               |___/         
        
    ____________________________________
    / Si necesitas ayuda, contáctame en \
    \ https://parzibyte.me               /
    ------------------------------------
            \   ^__^
            \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    Creado por Parzibyte (https://parzibyte.me). Este encabezado debe mantenerse intacto,
    excepto si este es un proyecto de un estudiante.
    */
    document.addEventListener("DOMContentLoaded", () => {
   
        // Obtenemos la cantidad de alumnos que hay
        num_students = window['num_students'].innerHTML;
        name_challenge = window['challenge_name'].innerHTML;

        var zip = new JSZip();

        for (i=0; i< num_students; i++) {
            var element = document.getElementById('toPdf'+i);
            var student_name = document.getElementById('studentName'+i).innerHTML;
            // element.style.display = "block"; // Mostramos el contenido del elemento.
                
            var opt = {
                margin:       2,
                filename:     'Avaluacio-Reptes-'+i+'.pdf',
                image:        { type: 'jpeg', quality: 0.88 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'mm', format: 'a4', orientation: 'portrait', compressPDF: true}
            };

            // Guardar el informe en PDF en el directorio
            zip.folder(name_challenge).file(student_name+'.pdf', html2pdf().set(opt).from(element).toPdf().output('blob'));
            
        }

        // Generamos el pdf
        zip.generateAsync({type:'blob'})
        .then(function(content) {
            saveAs(content, name_challenge+'.zip');
        });
                
    
    });

    // Eliminar la animación del spinner
    window.addEventListener('load', function () {

        // const target = document.getElementById("spinner");
        // target.remove();
        const target = document.getElementById("spinnerTailwind");
        target.remove();
    });
    
    </script>

</div>
