<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/technical_skills/store')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center" 
        x-data="{ open: <?= $one_skill['has_rubric'] == '0' ? 'false' : 'true'?> }">
        <!-- La selección es: <strong x-text="open"></strong> -->

        <input type="hidden" value="<?= set_value('technical_skills_id', isset($id) ? $id : '');?>" name="technical_skills_id">
        <input type="hidden" value="<?= set_value('t_owner', isset($t_owner) ? $t_owner : '');?>" name="t_owner">


        <div class="mx-auto w-full flex flex-col items-center mb-2 bg-gray-100">
            <!-- Nombre y descripción -->
            <div class="flex flex-wrap w-full mb-2">
                <div class="w-full px-3 mb-4 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-base font-bold mb-2">
                        <?=$g_name[$g_applang]?>
                    </label>
                    <input type="text" name="name" value="<?= set_value('name', isset($one_skill['name']) ? $one_skill['name'] : '');?>" 
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                        <?php echo form_error('name', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>
                </div>
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-base font-bold mb-2">
                        <?=$g_description[$g_applang]?>
                    </label>
                    <textarea rows="3" name="description" value="<?= set_value('description', isset($one_skill['description']) ? $one_skill['description'] : '');?>" 
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= set_value('description', isset($one_skill['description']) ? $one_skill['description'] : '');?></textarea>
                        <?php echo form_error('description', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>

                </div>
            </div>
            <div class="flex flex-col mt-4">
                <label class="block uppercase tracking-wide text-gray-700 text-lg font-bold mb-2">
                    Tria el tipus de qualificació:
                </label>
 
                <div class="inline-flex items-center mt-3 mr-4 mb-4">
                    <input @click="open = false" id="mark" type="radio" name="qualification" value="mark" class="w-6 h-6" <?= $one_skill['has_rubric'] == '0' ? 'checked' : ''?> />
                    <span for="mark" class="flex items-center cursor-pointer text-base ml-2">
                        Qualificació numérica
                    </span>
                </div>
                <div x-show="!open" class="-mt-2 mb-8">
                    Qualificació de 0 a 10.
                </div>

            
                <div class="flex items-center mr-4 mb-4">
                    <input @click="open = true" id="rubric" type="radio" name="qualification" value="rubric" class="w-6 h-6" <?= $one_skill['has_rubric'] != '0' ? 'checked' : ''?> />
                    <span for="rubric" class="flex items-center cursor-pointer text-base ml-2">
                        Rúbrica
                    </span>
                </div>
                <div x-show="open">
                    <div class="w-full md:w-2/3 mx-1 md:mx-4">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs text-center font-bold mb-2">
                            Tria una Rúbrica
                        </label>
                        <select name="rubric_id" class="block text-center appearance-none max-w-sm md:max-w-xl bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            <?php 
                                for($i=0; $i<count($rubrics); $i++) {
                                    if ($rubrics[$i]['id'] ===  $one_skill['rubric_id'])
                                        echo '<option selected value='.$rubrics[$i]['id'].'">'.$rubrics[$i]['name'].'</option>';
                                    else 
                                        echo '<option value='.$rubrics[$i]['id'].'">'.$rubrics[$i]['name'].'</option>';

                                }
                            ?>
                        </select>
                    
                    </div>     
                </div>

            </div>

            <!-- Curs Escolar -->
            <div class="flex flex-grow flex-wrap -mx-3 mt-2 mb-2">
                <div class="flex w-full justify-around px-0 md:px-3 mb-6 md:mb-0 md:mt-4">
                    <div class="mr-2 w-full">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            <?=$g_year_name[$g_applang]?>
                        </label>
                        <select name="school_year" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            <?php 
                            for($i=0; $i<count($g_school_year); $i++) {
                                if ($g_school_year[$i] === $one_skill['school_year'])
                                    echo '<option selected value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                                else 
                                    echo '<option value="'.$g_school_year[$i].'">'.$g_school_year[$i].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <!--  -->

            <!-- Cursos a los que se puede aplicar este Resultado de Aprendizaje -->
            <!-- <div class="flex flex-grow flex-wrap -mx-3 mt-2 mb-2">
                <div class="flex w-full justify-around px-0 md:px-3 mb-6 md:mb-0 md:mt-4">
                    <div class="w-full">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            <?=$g_ch_levels[$g_applang]?>
                        </label>
                        <select multiple size="10" name="teaching_levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            <?php
                                for($i=0; $i<count($g_levels); $i++) {
                                    $selected = false;
                                    for($j=0; $j<count($one_skill['teaching_levels']); $j++) {
                                        if($g_levels[$i] === $one_skill['teaching_levels'][$j]) {
                                            $selected = true;
                                            break;
                                        }
                                    }
                                    if ($selected) echo '<option selected value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                                    else  echo '<option value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                                }
                            ?>
                        </select>
                    </div>
                </div>

                
            </div> -->

            <div class="flex justify-around md:mt-4">
                <input type="submit" name="submit" value="<?=$g_next[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
                <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded bg-red-500 hover:bg-red-700">
            </div>
        </div>
    </form>
    <!-- <script>
        new SlimSelect({
            select: '#multiple'
        })
    </script> -->
</div>
