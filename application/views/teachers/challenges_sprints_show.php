<div class="w-full mx-auto shadow-md bg-grey-200">
    <div class="bg-white mx-auto shadow-md rounded my-2 pb-0 mb-2">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-2 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-2 mx-2 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <div class="flex flex-wrap w-full">
        <div class="flex flex-wrap p-1"> 
            <?php foreach($data as $item): ?>
            <div class="w-full md:w-1/2 flex flex-col p-1">
                <div class="bg-white border-t rounded-lg shadow-lg overflow-hidden flex-1 flex flex-wrap">
                    <div class="px-1 md:px-0 w-full flex md:mr-2">
                        <div class="flex-1 text-lg text-left py-2 md:py-1 px-2 md:ml-2"><?= $item['name'] ?></div>
                        <div class="flex-1 md:flex-none mt-2 mb-2 ml-1">
                            <a href="<?=base_url('teachers/challenges/sprintsOpenClose/'.$item['id']);?>" class="inline-block text-lg md:text-lg py-1 px-3 mr-2 text-white font-bold rounded text-xs text-center bg-purple-500 hover:bg-purple-700">
                                <?=$g_ch_sprints_openclose[$g_applang]?></a>
                        </div>
                    </div>
                    <div class="px-1 w-full flex md:flex-col border-t">
                        <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_description[$g_applang]?>:</strong> <?= $item['description'] ?></div>
                    </div>
                    <div class="px-1 w-full flex md:flex-col border-t">
                        <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_subject[$g_applang]?>:</strong> <?= $item['subject'] ?></div>
                        <div class="flex-1 md:flex-shrink text-xs text-left py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_levels[$g_applang]?>:</strong> 
                        <?php
                            $levels = json_decode($item['teaching_levels'], true);
                            for($i=0; $i<count($levels); $i++) {
                                if(($i+1) === count($levels)) echo $levels[$i];
                                else echo $levels[$i].',';
                            }
                        ?>
                        </div>
                    </div>
                    <div class="px-1 w-full md:w-1/2 flex md:flex-grow border-t">
                        <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_fechainicio[$g_applang]?>:</strong><br> <?= date("d-m-Y", strtotime($item['start_date'])); ?></div>
                        <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong><?=$g_ch_fechafin[$g_applang]?>:</strong><br> <?= date("d-m-Y", strtotime($item['finish_date'])); ?></div>
                        <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong><?=$g_year_name[$g_applang]?>:</strong><br> <?= $item['year'] ?></div>
                        <div class="flex-1 md:flex-shrink text-xs text-center py-2 md:py-0 px-2 md:px-0"><strong>Sprints:</strong><br> <?= $item['sprints'] ?></div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?= $this->pagination->create_links(); ?>
</div>
