<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <!-- Display status message -->
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/challenges/storeSprints')?>" method="POST" class="mx-auto md:-ml-2 text-center">
		<input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id']: '';?>" name="challenge_id">
		<input type="hidden" value="<?= isset($challenge['sprints']) ? $challenge['sprints'] : '';?>" name="sprints">
		<input type="hidden" value="<?= isset($update_sprints) ? $update_sprints : '0';?>" name="update_sprints">
            <div class="w-full flex flex-col md:flex-row px-3 mb-6 md:mb-0 md:mt-4">
                <?php 
                    for ($i=1; $i<=$challenge['sprints']; $i++) {
                ?>
                    <div class="md:w-full w-1/4 p-2 mx-auto">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Sprint <?=$i?>
                        </label>
                        <input type="date" min="<?=$challenge['start_date']?>" max="<?=$challenge['finish_date']?>" name="sprints_date<?=$i?>" id="sprints_date<?=$i?>" 
                            value="<?= date('Y-m-d', strtotime($challenge['start_date'].' + '.($i*15).' days')); ?>"
                            class="inline-block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    </div>
                <?php 
                    }
                ?>
            </div>
        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_define[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded bg-red-500 hover:bg-red-600">
        </div>
    </form>
</div>
