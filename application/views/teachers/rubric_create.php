<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('teachers/rubrics/store')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
        <input type="hidden" value="<?= isset($rubric['id']) ? $rubric['id'] : '';?>" name="id">
        <input type="hidden" value="0" name="min_value">

        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-full px-3 mb-4 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_name[$g_applang]?>
                </label>
                <input type="text" name="name" value="" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                <?php echo form_error('name', '<div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 mb-4 rounded relative text-center" role="alert">','</div>' ); ?>
            </div>
            <div class="w-full px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_description[$g_applang]?>
                </label>
                <textarea rows="3" name="description" value="" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= set_value('description', isset($rubric['description']) ? $rubric['description'] : '');?></textarea>
            </div>
        </div>
        <div class="flex -mx-3 mt-2 mb-2">
            <div class="flex flew-grow w-full justify-around px-3 mb-6 md:mb-0">
                <div class="mr-2 w-1/3 md:w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_cols[$g_applang]?> (<?= $g_min_cols.' - '.$g_max_cols?>)
                    </label>
                    <select name="cols" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                            for($i=$g_min_cols; $i<=$g_max_cols; $i++)
                                echo '<option value="'.$i.'">'.$i.'</option>'
                        ?>
                    </select>

                </div>
                <div class="mr-2 w-1/3 md:w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_rows[$g_applang]?> (<?= $g_min_rows.' - '.$g_max_rows?>)
                    </label>
                    <select name="rows" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                            for($i=$g_min_rows; $i<=$g_max_rows; $i++)
                                echo '<option value="'.$i.'">'.$i.'</option>'
                        ?>
                    </select>
                </div>
                <!-- <div class="mr-2 w-1/2 md:w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Valor Min (<?= $g_min_value_1.' - '.$g_min_value_2?>)
                    </label>
                    <select name="min_value" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                            for($i=$g_min_value_1; $i<=$g_min_value_2; $i++)
                                echo '<option value="'.$i.'">'.$i.'</option>'
                        ?>
                    </select>
                </div> -->
                <div class="w-1/3 md:w-1/4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_qualifications[$g_applang]?>
                    </label>
                    <select name="max_value" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php 
                            for($i=$g_max_value_2; $i>=$g_max_value_1; $i--) {
                                if( $i == $g_max_value_2)
                                    echo '<option selected value="'.$i.'">'.$i.'</option>';
                                else 
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="w-full flex-wrap -mx-3 mt-2 mb-2">
            <div class="flex flex-col md:flex-row w-full justify-around px-3 mb-6 md:mb-0 md:mt-4">
                <div class="w-full md:w-1/2 mx-1">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    <?=$g_course[$g_applang]?>
                    </label>
                    <select multiple size="10" name="teaching_levels[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 ml-3 md:-ml-3 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                    <?php
                        for($i=0; $i<count($g_levels); $i++)
                            echo '<option value="'.$g_levels[$i].'" >'.$g_levels_name[$i].'</option>\n';
                    ?>
                    </select>
                </div>
                <div class="w-full md:w-1/2 mx-1 mt-4 md:mt-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        <?=$g_for_teachers[$g_applang]?>
                    </label>
                    <select multiple size="10" name="teachers[]" class="block text-center appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-2 md:px-4 py-2 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                        <?php foreach($teachers as $item): ?>
                            <option value="<?= $item['id'] ?>"><?= $item['firstname'].' '.$item['lastname'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
       
        <div class="flex justify-around md:mt-4">
            <input type="submit" name="submit" value="<?=$g_create[$g_applang]?>" class="inline-block text-2xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block text-2xl md:text-lg text-white font-bold ml-4 px-3 py-1 rounded text-xs bg-red-500 hover:bg-red-700">
        </div>
    </form>
</div>
