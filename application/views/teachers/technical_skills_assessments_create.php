<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <!-- <input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id'] : '';?>" name="challenge_id"> -->

    <div class="w-full flex flex-col mt-6">
        <?php 
            for ($i=0; $i<count($technical_skills); $i++) {
        ?>
            <div class="w-full justify-center py-1 px-1">
                <!-- class="<?= ($sprints_openfor_assess[$i-1]) == 1 ? '' : 'cursor-not-allowed opacity-25' ?> bg-teal-500 hover:bg-teal-700 w-full inline-block md:text-xl p-2 text-white text-xs font-bold rounded"> -->
                <a href="<?= base_url('teachers/technical_skills_assessments/assess/'.$challenge['id'].'/'.$technical_skills[$i]['id'].'/'.$technical_skills[$i]['has_rubric'].'/'.$current_team); ?>"
                    class="bg-yellow-700 hover:bg-yellow-500 w-full inline-block text-lg mx-auto md:text-xl p-1 text-white font-bold rounded">
                    <div class="w-full bg-gray-100 text-center rounded shadow-lg my-2 mx-0 ">
                        <label class="font-bold text-2xl mb-2 text-gray-600"><?= $technical_skills[$i]['name'] ?></label>
                    </div>
                </a>
            </div>
        <?php 
            }
        ?>
    </div>
</div>
