<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('students/dashboard/changePassword')?>" method="POST" class="flex flex-col -my-3 mx-3 p-6 text-center">
		<input type="hidden" value="<?= isset($id) ? $id : '';?>" name="id">
        <!-- <?= var_dump("edit -> id=".$id); ?> -->
        <div class="max-w-xl w-full mb-6 mx-auto justify-center bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md">
            <div class="flex justify-center">
                <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                <div>
                <p class="font-bold">Cambia tu contraseña</p>
                </div>
            </div>
        </div>

        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="md:w-1/2 m-auto px-3 mb-4 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Elige una Contraseña
                </label>
                <input type="password" name="password" value="" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                    <?php echo form_error('password'); ?>
            </div>
            <div class="md:w-1/2 m-auto px-3">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Repite la Contraseña
                </label>
                <input type="password" name="password_confirm" value="" 
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                    <?php echo form_error('password_confirm'); ?>
            </div>
        </div>
        
        <!-- --  -->
        <div class="flex flex-wrap justify-around md:mt-4">
            <input type="submit" name="submit" value="Actualizar" class="inline-block text-2xl md:text-lg py-1 px-3 mb-2 text-white font-bold rounded text-xs bg-blue-400 hover:bg-blue-600">
            <input type="submit" name="submit" value="Cancelar" formnovalidate class="inline-block text-2xl md:text-lg px-3 py-1 mb-2 text-white font-bold  rounded text-xs bg-red-400 hover:bg-red-600">
        </div>
        
    </form>

</div>
