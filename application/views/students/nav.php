        <nav class="sticky top-0 w-full flex items-center justify-between flex-wrap bg-teal-500 p-3 -mt-2">
            <div class="flex items-center flex-no-shrink text-white mr-6">
            <a class="font-bold text-2xl tracking-tight" href="<?=base_url('/')?>" title="<?= $g_appname_desc ?>"><?= $g_appname ?></a>
            </div>
 
            <label for="menu-toggle" class="pointer-cursor md:hidden block mt-2">
                <svg class="fill-current text-teal-100" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title>Menú</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                </svg>
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle" />

            <div id="menu" class="w-full flex-grow sm:flex items-center sm:w-auto hidden">
                <div class="justify-start flex-grow sm:flex">
                    <!-- Valorar els Sprints -->
                    <!-- <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="#">
                                <span class="firstlevel"><?=$g_challenges_name[$g_applang]?></span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('students/challenges_assessments')?>">
                                        <span class=""><?=$g_assess[$g_applang]?> <?=$g_challenges_name[$g_applang]?></span>
                                    </a>
                                </li>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('students/challenges_assessments')?>">
                                        <span class=""><?=$g_grades[$g_applang]?> <?=$g_challenges_name[$g_applang]?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul> -->

                    <ul class="flex flex-wrap p-1 text-2xl md:text-lg mt-2 md:mt-0" id="menu_nav">
                        <li class="relative mx-1 p-0 group mb-1 md:mb-0" id="button_admin">
                            <a class="font-bold  whitespace-no-wrap text-teal-100 hover:text-white mr-4" href="<?=base_url('students/dashboard')?>">
                                <span class="firstlevel"><?=$g_profile_name[$g_applang]?></span>
                            </a>
                            <ul class="absolute left-0 top-0 mt-10 md:mt-8 p-2 rounded-lg shadow-lg bg-white z-10 hidden group-hover:block">
                                <svg class="block fill-current text-white w-4 h-4 absolute left-0 top-0 ml-3 -mt-3 z-0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path></svg>
                                <li class="p-2 whitespace-no-wrap rounded font-bold text-lg text-teal-600 hover:text-white hover:bg-teal-600">
                                    <a class="px-2 py-2" href="<?=base_url('students/dashboard/editPassword')?>">
                                        <span class=""><?=$g_chPassword[$g_applang]?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>


                </div>
                <div class="justify-end flex-grow sm:flex">
                    <!-- Dropdown: Year of the Challenges -->
                    <div x-data="{ show: false }"  @click.away="show = false" class="mb-2 mr-2">
                        <button @click="show = ! show" type="button" class="flex bg-blue-600 text-gray-200 rounded-lg px-3 py-2 focus:outline-none focus:border-white text-sm">
                        <span class="pr-0 text-lg"><?= $g_year_name[$g_applang].' '.$school_year ?></span> 
                            <svg class="fill-current text-gray-100" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M7 10l5 5 5-5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        </button>
                        <div x-show="show" class="absolute bg-gray-100 z-10 shadow-md rounded" style="min-width:8rem">
                            <?php foreach($g_school_year as $item): ?>
                                <a class="block px-3 py-2" href="<?= base_url('changeSchoolYear/').$item ?>"><?= $item ?></a>
                            <?php endforeach; ?>
                        </div>
                        <!-- Guardar la URL actual para volver en caso de cambiar de año -->
                        <?php $this->session->set_userdata('previous_url', current_url()); ?>
                    </div>
                        
                    <a href="<?=base_url('login/logout')?>" class="inline-block text-lg px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 mb-2 md:mt-0 md:mb-0">
                        <?=$g_exit[$g_applang]?>
                    </a>
                </div>
            </div>
        </nav>