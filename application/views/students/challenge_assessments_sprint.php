<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>

    <form action="<?=base_url('students/challenges_assessments/storeSprint')?>" method="POST" 
          class="flex flex-col -my-3 mx-3 p-1 text-center" enctype="multipart/form-data">

		<input type="hidden" value="<?= isset($challenge['id']) ? $challenge['id'] : '';?>" name="id">
		<input type="hidden" value="<?= isset($student_id) ? $student_id : '';?>" name="student_id">
		<input type="hidden" value="<?= isset($s_team) ? $s_team : '';?>" name="s_team">
        <input type="hidden" value="<?= isset($current_sprint) ? $current_sprint : '';?>" name="current_sprint">
		<input type="hidden" value="<?= isset($num_students) ? $num_students : '';?>" name="num_students">
        <input type="hidden" value="<?= isset($rubric['max_value']) ? $rubric['max_value'] : '';?>" name="max_value">
		<input type="hidden" value="<?= isset($rubric['cols']) ? $rubric['cols'] : '';?>" name="cols">
		<input type="hidden" value="<?= isset($rubric['rows']) ? $rubric['rows'] : '';?>" name="rows">

    <div class="w-full flex-col justify-center">
        <div class="max-w-xl mx-auto justify-center bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md">
            <div class="flex justify-center">
                <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                <div>
                <p class="font-bold"><?=$g_ch_self_assessment[$g_applang]?>Sprint <?=$current_sprint?></p>
                </div>
            </div>
        </div>
        <div class="w-full flex flex-wrap justify-center mt-4 mr-4">
            <input type="hidden" value="<?= $students[0]['id'];?>" name="students_ids[]">
            <div class="w-full md:w-2/5 justify-center md:mr-4">
                <label class="block tracking-wide text-gray-700 text-sm md:text-base font-bold">
                    <strong class="uppercase"><?= $students[0]['firstname'].' '.$students[0]['lastname'] ?></strong>
                </label>
                <div class="w-full bg-gray-300 text-center rounded shadow-lg mx-auto">
                    <!-- Criteria -->
                    <?php for($i=0; $i<$rubric['rows']; $i++) { ?>
                        <!-- Options -->
                        <div class="flex flex-col justify-around w-full">
                            <div class="flex">
                                <label class="flex items-center justify-center w-full bg-blue-400 text-xs text-white text-center m-1 mr-1 p-2 font-bold rounded">
                                    <?php echo $criteria[$i] ?>
                                </label>
                            </div>

                            <div class="flex w-full min-h-full h-full">
                                <?php for ($j=0; $j<$rubric['cols']; $j++) { 
                                        if(!empty($self_assessment_data)) { ?>
                                            <input class="hidden" type="radio" name="criteria-0<?=$i?>" id="option<?php echo '0-'.$i.$j ?>" 
                                                    value="option0<?php echo '-'.round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?>" 
                                                    <?= ($self_assessment_data[$i] === strval(round(($rubric['max_value']/($rubric['cols']-1))*$j, 2))) ? 'checked' : '' ;?>
                                                    />
                                    <?php } else { ?>
                                            <input class="hidden" type="radio" name="criteria-0<?=$i?>" id="option<?php echo '0-'.$i.$j ?>" 
                                                    value="option0<?php echo '-'.round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?>"/>
                                    <?php }  ?>
                                    <!-- <label for="option0<?php echo '-'.$i.$j ?>" 
                                            class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> inline-block w-full hover:bg-teal-400 self-center py-3 justify-center text-base text-transparent text-center font-bold mr-1 my-1 cursor-pointer rounded">
                                        <?php echo round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?>
                                    </label> -->
                                        <label for="option<?php echo '0-'.$i.$j ?>" 
                                                class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> inline-block w-1/<?=$rubric['cols']?> min-h-full hover:bg-teal-600 self-center py-1 justify-center text-xs text-center font-gray-800 cursor-pointer rounded">
                                            <?php  echo empty($performance_descriptions[$i][$j]) ? "&nbsp;&nbsp;&nbsp; " : $performance_descriptions[$i][$j]; ?>
                                        </label>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="w-full px-3">
                    <label class="block tracking-wide text-gray-700 text-xs font-bold mb-0">
                        <?=$g_ch_self_comments[$g_applang]?>: <?= $students[0]['firstname'] ?>
                    </label>
                    <textarea rows="2" name="comments[]" value="" 
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= empty($self_comments) ? '' : $self_comments ?></textarea>
                </div>

            </div>
        </div>
    </div>
    <div class="w-full flex-col justify-center mt-6">
        <div class="max-w-xl mx-auto justify-center bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md">
            <div class="flex justify-center">
                <div class="py-1"><svg class="fill-current h-6 w-6 text-teal-500 mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg></div>
                <div>
                <p class="font-bold"><?=$g_ch_peer_assessment[$g_applang]?>Sprint <?=$current_sprint?></p>
                </div>
            </div>
        </div>
        <div class="w-full flex flex-wrap justify-center mt-4 mr-4">
                <?php
                    for($std=1; $std<count($students); $std++) {
                    ?>
                    <input type="hidden" value="<?= $students[$std]['id'];?>" name="students_ids[]">
                    <div class="w-full md:w-2/5 justify-center md:mr-4 mt-4">
                        <label class="block tracking-wide text-gray-700 text-sm md:text-base font-bold">
                            <strong class="uppercase"><?= $students[$std]['firstname'].' '.$students[$std]['lastname'] ?></strong>
                        </label>
                        <div class="w-full bg-gray-300 text-center rounded shadow-lg mx-auto">
                            <!-- Criteria -->
                            <?php for($i=0; $i<$rubric['rows']; $i++) {  ?>
                                <!-- Options -->
                                <div class="flex flex-col justify-around w-full my-1 mr-2">
                                    <div class="flex">

                                        <label class="flex items-center justify-center w-full bg-blue-400 text-xs text-white text-center m-1 mr-3 font-semibold rounded">
                                            <?php echo $criteria[$i] ?>
                                        </label>
                                    </div>

                                    <div class="flex w-full min-h-full h-full">

                                        <?php for ($j=0; $j<$rubric['cols']; $j++) {
                                                if(!empty($peer_assessment_data)) { ?>
                                                    <input class="hidden" type="radio" name="criteria-<?=$std.$i?>" id="option<?php echo $std.'-'.$i.$j ?>" 
                                                        value="option<?php echo $std.'-'.round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?>" 
                                                        <?= ($peer_assessment_data[$std][$students_ids[$std]][$i] === strval(round(($rubric['max_value']/($rubric['cols']-1))*$j, 2))) ? 'checked' : '' ;?>
                                                        />
                                            <?php } else { ?>
                                                <input class="hidden" type="radio" name="criteria-<?=$std.$i?>" id="option<?php echo $std.'-'.$i.$j ?>" 
                                                            value="option<?php echo $std.'-'.round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?>"/>
                                            <?php }  ?>  
                                            <!-- <label for="option<?php echo $std.'-'.$i.$j ?>" 
                                                    class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> inline-block w-full hover:bg-teal-400 self-center py-3 justify-center text-base text-transparent text-center font-bold mr-1 my-1 cursor-pointer rounded">
                                                <?php echo round(($rubric['max_value']/($rubric['cols']-1))*$j, 2)?>
                                            </label> -->
                                            <label for="option<?php echo $std.'-'.$i.$j ?>" 
                                                    class="<?=$g_bg_descriptions[$rubric['cols']][$j]?> inline-block w-1/<?=$rubric['cols']?> min-h-full hover:bg-teal-600 self-center py-1 justify-center text-xs text-center font-gray-800 cursor-pointer rounded">
                                                <?php  echo empty($performance_descriptions[$i][$j]) ? "&nbsp;&nbsp;&nbsp; " : $performance_descriptions[$i][$j]; ?>
                                            </label>

                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="w-full px-3">
                            <label class="block tracking-wide text-gray-700 text-xs font-bold mb-0">
                                <?=$g_ch_peer_comments[$g_applang]?>: <?= $students[$std]['firstname'] ?>
                            </label>
                            <textarea rows="2" name="comments[]" value="" 
                                class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-400 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"><?= empty($peer_comments) ? '' : $peer_comments[$std][$students_ids[$std]] ?></textarea>
                        </div>

                    </div>
                 <?php
                    }
                ?>
        </div>
    </div>
        <div class="flex flex-col md:flex-row md:justify-around mt-4">
            <input type="submit" name="submit" value="<?=$g_assess[$g_applang]?>" class="inline-block mb-2 text-xl md:text-lg py-1 px-3 text-white font-bold rounded text-xs bg-blue-500 hover:bg-blue-700">
            <input type="submit" name="submit" value="<?=$g_cancel[$g_applang]?>" formnovalidate class="inline-block  mb-2 text-xl md:text-lg text-white font-bold px-3 py-1 rounded text-xs bg-red-500 hover:bg-red-700">
        </div>
    </form>
</div>
