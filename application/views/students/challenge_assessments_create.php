<div class="w-full mx-auto mb-6 shadow-md bg-gray-100">
    <!-- Display status message -->
    <div class="bg-white shadow-md rounded my-2 pb-0">
        <?php if($msg = $this->session->flashdata('msg')): ?>
            <div class="bg-blue-100 border border-blue-400 text-blue-700 m-2 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('success_msg')): ?>
        <div class="bg-teal-100 border border-teal-400 text-teal-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
        <?php if($msg = $this->session->flashdata('error_msg')): ?>
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                <strong class="font-bold"><?= $msg ?></strong>
            </div>
        <?php endif; ?>
    </div>
    <input type="hidden" value="<?= set_value('id', isset($challenge['id']) ? $challenge['id'] : '');?>" name="id">

    <div class="w-full flex flex-col md:flex-row">
        <?php 
            for ($i=1; $i<=$challenge['sprints']; $i++) {
        ?>
            <div class="w-full md:w-1/2 justify-center p-3">
                <a href="<?= ($sprints_openfor_assess[$i-1]) == 1 ? base_url('students/challenges_assessments/assessSprint/'.$challenge['id'].'/'.$i) : '#' ?>"
                    class="<?= ($sprints_openfor_assess[$i-1]) == 1 ? '' : 'cursor-not-allowed opacity-25' ?> bg-teal-500 hover:bg-teal-700 w-full inline-block text-lg md:text-xl p-2 text-white text-xs font-bold rounded">
                    <div class="w-4/5 bg-gray-100 text-center rounded shadow-lg my-4 mx-auto ">
                        <label class="font-bold text-2xl mb-2 text-gray-600">Valorar Sprint <?=$i?></label>
                    </div>
                </a>
            </div>
        <?php 
            }
        ?>

    </div>
</div>
