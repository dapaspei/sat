<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('getCreateAdminRules')){
    /**
     * Función que nos devolverá las reglas de validación.
     */
    function getCreateAdminRules() {
        return array (
            array(
                'field' => 'firstname',
                'label' => 'Nombre',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'lastname',
                'label' => 'Apellidos',
                'rules' => 'max_length[150]',
                'errors' => array (
                    'max_lenght' => 'Los %s son demasiado largos.'
                ),
            ),
            array(
                'field' => 'email',
                'label' => 'correo electrónico',
                'rules' => 'required|trim|valid_email',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'valid_email' => 'El formato del %s no es válido.'
                ),
            ),array(
                'field' => 'password',
                'label' => 'contraseña',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'La %s es necesaria.'
                ),
            ),array(
                'field' => 'password_confirm',
                'label' => 'contraseña',
                'rules' => 'required|matches[password]',
                'errors' => array(
                    'required' => 'La $s es necesaria.',
                    'matches' => 'Las %ss no coindicen.'
                ),
            ),array(
                'field' => 'username',
                'label' => 'Nombre de usuario',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            )
        );

    }
}
if(!function_exists('getUpdateAdminRules')) {
    /**
     * Función para actualizar los datos de un Estudiante
     */
    function getUpdateAdminRules() {
        return array (
            array(
                'field' => 'firstname',
                'label' => 'Nombre',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'lastname',
                'label' => 'Apellidos',
                'rules' => 'max_length[150]',
                'errors' => array (
                    'max_lenght' => 'Los %s son demasiado largos.'
                ),
            ),
            array(
                'field' => 'email',
                'label' => 'correo electrónico',
                'rules' => 'required|trim|valid_email',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'valid_email' => 'El formato del %s no es válido.'
                ),
            ),array(
                'field' => 'password',
                'label' => 'contraseña',
                'rules' => '',
                'errors' => array(
                    'required' => 'La %s es necesaria.'
                ),
            ),array(
                'field' => 'password_confirm',
                'label' => 'contraseña',
                'rules' => 'required|matches[password]',
                'errors' => array(
                    'required' => 'La $s es necesaria.',
                    'matches' => 'Las %ss no coindicen.'
                ),
            ),array(
                'field' => 'username',
                'label' => 'Nombre de usuario',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
        );

    }

}
