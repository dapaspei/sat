<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('getCreateTechnicalSkillsRules')){
    /**
     * Función que nos devolverá las reglas de validación de la creación
     * de un Resultado de Aprendizaje (RA)
     */
    function getCreateTechnicalSkillsRules() {
        return array (
            array(
                'field' => 'name',
                'label' => 'nombre',
                'rules' => 'required|max_length[250]',
                'errors' => array (
                    'required' => 'Falta el %s del Resultado de Aprendizaje.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'descripción',
                'rules' => 'max_length[250]',
                'errors' => array (
                    'max_lenght' => 'La %s es muy larga.'
                ),
            ),
        );

    }

    /**
     * Función para actualizar los datos de un Resultado de Aprendizaje (RA)
     */
    function getUpdateTechnicalSkillsRules() {
        return array (
            array(
                'field' => 'name',
                'label' => 'nombre',
                'rules' => 'required|max_length[250]',
                'errors' => array (
                    'required' => 'Falta el %s del Resultado de Aprendizaje.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'descripción',
                'rules' => 'max_length[250]',
                'errors' => array (
                    'max_lenght' => 'La %s es muy larga.'
                ),
            ),
        );

    }

}
