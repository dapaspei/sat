<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('getCreateStudentRules')){
    /**
     * Función que nos devolverá las reglas de validación de login + password
     */
    function getCreateStudentRules() {
        return array (
            array(
                'field' => 'firstname',
                'label' => 'Nombre',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'lastname',
                'label' => 'Apellidos',
                'rules' => 'max_length[150]',
                'errors' => array (
                    'max_lenght' => 'Los %s son demasiado largos.'
                ),
            ),
            array(
                'field' => 'email',
                'label' => 'correo electrónico',
                'rules' => 'required|trim|valid_email',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'valid_email' => 'El formato del %s no es válido.'
                ),
            ),array(
                'field' => 'password',
                'label' => 'contraseña',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'La %s es necesaria.'
                ),
            ),array(
                'field' => 'password_confirm',
                'label' => 'confirmación',
                'rules' => 'required|matches[password]',
                'errors' => array(
                    'required' => 'La %s es necesaria.',
                    'matches' => 'La %s no coindice.'
                ),
            ),array(
                'field' => 'username',
                'label' => 'Nombre de usuario',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
        );

    }
}

if(!function_exists('getUpdateStudentRules')) {
    /**
     * Función para actualizar los datos de un Estudiante
     */
    function getUpdateStudentRules() {
        return array (
            array(
                'field' => 'firstname',
                'label' => 'Nombre',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'lastname',
                'label' => 'Apellidos',
                'rules' => 'max_length[100]',
                'errors' => array (
                    'max_lenght' => 'Los %s son demasiado largos.'
                ),
            ),
            array(
                'field' => 'email',
                'label' => 'correo electrónico',
                'rules' => 'required|trim|valid_email',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'valid_email' => 'El formato del %s no es válido.'
                ),
            ),array(
                'field' => 'username',
                'label' => 'Nombre de usuario',
                'rules' => 'required|max_length[50]',
                'errors' => array (
                    'required' => 'Falta el %s.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
        );

    }

}

if(!function_exists('getChangePasswordStudentRules')) {
    /**
     * Función para actualizar los datos de un Estudiante
     */
    function getChangePasswordStudentRules() {
        return array (
            array(
                'field' => 'password',
                'label' => 'contraseña',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'La %s es necesaria.'
                ),
            ),array(
                'field' => 'password_confirm',
                'label' => 'confirmación',
                'rules' => 'required|matches[password]',
                'errors' => array(
                    'required' => 'La %s es necesaria.',
                    'matches' => 'Las contraseñas no coindicen.'
                ),
            ),
        );

    }

}
