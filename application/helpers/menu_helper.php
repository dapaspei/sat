<?php

function main_menu() {
    return array (
        // estructura del login
        array(
            'title' => 'Entrada',
            'url' => base_url('login'),
        ),
        // estructura del registro
        array(
            'title' => 'Registro',
            'url' => base_url('register'),
        ),
    );
}