<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('getCreateRubricsRules')){
    /**
     * Función que nos devolverá las reglas de validación de login + password
     */
    function getCreateRubricsRules() {
        return array (
            array(
                'field' => 'name',
                'label' => 'nombre',
                'rules' => 'required|max_length[250]',
                'errors' => array (
                    'required' => 'Falta el %s de la rúbrica.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'descripción',
                'rules' => 'max_length[100]',
                'errors' => array (
                    'max_lenght' => 'La %s es muy larga.'
                ),
            ),
        );

    }
}

if(!function_exists('getUpdateRubricsRules')){
    /**
     * Función para actualizar los datos de un Profesor
     */
    function getUpdateRubricsRules() {
        return array (
            array(
                'field' => 'name',
                'label' => 'nombre',
                'rules' => 'required|max_length[250]',
                'errors' => array (
                    'required' => 'Falta el %s de la rúbrica.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'descripción',
                'rules' => 'max_length[100]',
                'errors' => array (
                    'max_lenght' => 'La %s es muy larga.'
                ),
            ),
        );

    }
}
