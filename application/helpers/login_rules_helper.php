<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Función que nos devolverá las reglas de validación de login + password
 */
function getLoginRules() {
    return array (
        /*array(
            'field' => 'email',
            'label' => 'Correo electrónico',
            'rules' => 'required|trim|valid_email',
            'errors' => array (
                'required' => 'Falta el %s.',
                'valid_email' => '%s inválido.'
            ),
        ),*/ 
        array(
        	'field' => 'username',
        	'label' => 'nombre de usuario',
        	'rules' => 'required|trim',
        	'errors' => array (
        		'required' => 'Falta el %s.'
        	),
  		),
        array(
            'field' => 'password',
            'label' => 'contraseña',
            'rules' => 'required',
            'errors' => array(
                'required' => 'Falta la %s.'
            ),
        ),
    );

}