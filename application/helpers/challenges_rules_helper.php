<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('getCreateChallengesRules')){
    /**
     * Función que nos devolverá las reglas de validación de login + password
     */
    function getCreateChallengesRules() {
        return array (
            array(
                'field' => 'name',
                'label' => 'nombre',
                'rules' => 'required|max_length[250]',
                'errors' => array (
                    'required' => 'Falta el %s del reto.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'descripción',
                'rules' => 'max_length[250]',
                'errors' => array (
                    'max_lenght' => 'La %s es muy larga.'
                ),
            ),/*array(
                'field' => 'year',
                'label' => 'curso escolar',
                'rules' => 'required',
                'errors' => array (
                    'required' => 'Falta el número de %s.',
                ),
            ),array(
                'field' => 'sprints',
                'label' => 'sprints',
                'rules' => 'required',
                'errors' => array (
                    'required' => 'Falta el número de %s.',
                ),
            ),array(
                'field' => 'start_date',
                'label' => 'valor máximo',
                'rules' => 'date',
                'errors' => array (
                    'numeric' => '%s no es un número',
                ),
            ),array(
                'field' => 'min_value',
                'label' => 'valor mínimo',
                'rules' => 'numeric',
                'errors' => array (
                    'numeric' => '%s no es un número',
                ),
            ),*/
        );

    }

    /**
     * Función para actualizar los datos de un Profesor
     */
    function getUpdateChallengesRules() {
        return array (
            array(
                'field' => 'name',
                'label' => 'nombre',
                'rules' => 'required|max_length[250]',
                'errors' => array (
                    'required' => 'Falta el %s del reto.',
                    'max_lenght' => 'El %s es demasiado largo.'
                ),
            ),
            array(
                'field' => 'description',
                'label' => 'descripción',
                'rules' => 'max_length[250]',
                'errors' => array (
                    'max_lenght' => 'La %s es muy larga.'
                ),
            ),/*array(
                'field' => 'year',
                'label' => 'curso escolar',
                'rules' => 'required',
                'errors' => array (
                    'required' => 'Falta el número de %s.',
                ),
            ),array(
                'field' => 'sprints',
                'label' => 'sprints',
                'rules' => 'required',
                'errors' => array (
                    'required' => 'Falta el número de %s.',
                ),
            ),*/
        );

    }

}
