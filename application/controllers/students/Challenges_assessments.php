<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges_assessments extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        //$this->load->library(array('form_validation','pagination'));

        // cargamos el helper con las reglas de los retos
        //$this->load->helper(array('challenges_rules'));
        // Cargar los modelos necesarios
        $this->load->model('challenges_model');
        $this->load->model('challenges_assessments_model');
        $this->load->model('technical_skills_model');
        $this->load->model('technical_skills_assessments_model');
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');
        $this->load->model('students_model');
    }

    /**
     * Función principal por defecto para mostrar las valoraciones de un reto.
     */
    public function index() {
        // Comprobación de permisos
        $this->checkPermission();

        // Obtenemos el año escolar en el que estamos
        $current_school_year = $this->session->school_year;
        // var_dump("<br>El año escolar es: ".$current_school_year);

        // Obtenemos la información de en qué reto(s) está el alumno
        // $student_id = $this->students_model->getStudentId($this->session->id)['id'];
        $student_id = $this->students_model->getStudentId($this->session->id);
        $challenge_id = $this->challenges_model->getChallengesOfStudent($student_id, $current_school_year);
        // var_dump("<br>challenge_id=".json_encode($challenge_id));
        // exit(0);
        $data = array();
        foreach($challenge_id as $item) {
            // Guardamos los identificadores de retos.
            $challenges[] = $item['challenge_id'];
            $sql_res = $this->challenges_model->getChallenge($item['challenge_id'], $current_school_year);
            if($sql_res) {
                $data[] = $sql_res;
            }
        }
        // var_dump("<br><br>data = ".json_encode($data));
        // var_dump("data = ".$data);
        // var_dump("challenges=".json_encode($challenges));
        // exit(0);

        $vista = $this->load->view('students/challenges_show', array('data' => $data, 'school_year' => $current_school_year), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para crear una Evaluación
     */
    public function create($id = '') {
        // Comprobación de permisos
        $this->checkPermission();

        // Obtenemos la información del RETO
        $challenge = $this->challenges_model->getChallenge($id);
        // print_r($challenge);
        // exit(0);

        // $hasAssessments = $this->challenges_assessments_model->isChallengeInAssessment($id);

        /*
        // Comprobamos que no tenga creado todavía ninguna evaluación, sinó, la creamos.
        if(!$hasAssessments) {
            // Buscar los alumnos pertenecientes al reto
            $students = $this->challenges_model->getChallengeStudents($id);
            // Buscar los profesores asociados al reto
            $teachers = $this->challenges_model->getChallengeTeachers($id);
            // print_r("t=".$teachers);
            // exit(0);
            // Con los estudiantes, creamos sus registros de evaluación en la BBDD
            if(!$this->challenges_assessments_model->start($id, $challenge, $students, $teachers ))
                $this->output->set_status_header(500);
        }
        */

        // Buscamos los sprints que se pueden valorar.
        $sprints_openfor_assess = $this->challenges_model->getSprintsStatus($id);

        $vista = $this->load->view('students/challenge_assessments_create', array(
            'challenge' => $challenge,
            'current_sprint' => 1,
            'sprints_openfor_assess' => $sprints_openfor_assess,
        ), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para VALORAR un RETO
     */
    public function assessSprint($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos de la URL (http:/...../Challenge/Sprint)
        $challenge_id = $this->uri->segment(4);
        $current_sprint = $this->uri->segment(5);

        // Comprobamos si el estudiante ha realizado la valoración de este sprint
        $student_id = $this->students_model->getStudentId($this->session->id);
        // var_dump("<br>student id=".$student_id);
        $s_team = $this->students_model->getStudentTeam($challenge_id, $student_id);
        // var_dump("<br>s_team=".$s_team);

        
        // Obtenemos los datos de la valoración.
        $sprintAssessData = $this->challenges_assessments_model->getStudentsSprintAssessData($challenge_id, $current_sprint, $student_id, $s_team);
        // var_dump("<br>sprintAssessData = ".json_encode($sprintAssessData));
        // exit(0);

        // Obtenemos los datos del alumno (si los hay)
        if(empty($sprintAssessData['self_assessment_data']))
            $self_assessment_data = array();
        else 
            $self_assessment_data = $sprintAssessData['self_assessment_data'];

        // Obtenemos los datos del los compañeros de equipo (si los hay)
        if(empty($sprintAssessData['peer_assessment_data']))
            $peer_assessment_data = array();
        else 
            $peer_assessment_data = $sprintAssessData['peer_assessment_data'];
        
        // var_dump("<br>self_data=".json_encode($self_assessment_data).",<br>peer_data=".json_encode($peer_assessment_data)."<br>");
        // exit(0);

        if(!empty($peer_assessment_data)) {
            // var_dump("<br>peer_data[0]=".json_encode($peer_assessment_data[0]));
            // var_dump("<br>peer_data[1]=".json_encode($peer_assessment_data[1]));
            // Si el contenido se corresponde con el identificador del alumno, lo insertamos, sino lo borramos.
            $p_a_d = array();
            foreach($peer_assessment_data as $item){
                if(!is_null($item)) {
                    // print_r($item);
                    // exit(0);
                    // $p_a_d[]=$item[$student_id];
                    if (isset($item[$student_id])) {
                        $p_a_d[]=$item[$student_id];
                    }

                }
            }
            $peer_assessment_data = $p_a_d;
            if(!empty($peer_assessment_data))
                array_unshift($peer_assessment_data, '');
            // var_dump("<br> after empty() -> self_data=".json_encode($self_assessment_data).",<br>peer=".json_encode($peer_assessment_data)."<br>");
            // var_dump("<br>peer_a_d[1][1][0]=>".json_encode($peer_assessment_data[1][1][0]));
            // exit(0);
        }
        // var_dump("<br> al final, peer_assessment_data=".json_encode($peer_assessment_data));
        
        // Obtenemos todos los comentarios
        $sprintCommentsData = $this->challenges_assessments_model->getStudentsSprintCommentsData($challenge_id, $current_sprint, $student_id, $s_team);
        // var_dump("<br>sprintsCommentsData=".json_encode($sprintCommentsData));
        // exit(0);
        
        // Separamos los comentarios propios de los de los compañeros.
        
        // Obtenemos los comentarios del alumno (si los hay)
        if(empty($sprintCommentsData['self_comments']))
            $self_comments = array();
        else 
            $self_comments = $sprintCommentsData['self_comments'];

        // Obtenemos los comentarios del los compañeros de equipo (si los hay)
        if(empty($sprintCommentsData['peer_comments']))
            $peer_comments = array();
        else
            $peer_comments = $sprintCommentsData['peer_comments'];

        // var_dump("<br>BEFORE self_comments=".json_encode($self_comments).",peer_comments=".json_encode($peer_comments));
        // exit(0);
        if(!empty($peer_comments)) {
            // Si el contenido se corresponde con el identificador del alumno, lo insertamos, sino lo borramos.
            $p_c_d = array();
            foreach($peer_comments as $item){
                if(!is_null($item))
                    // $p_c_d[]=$item[$student_id];
                    if (isset($item[$student_id])) {
                        $p_c_d[]=$item[$student_id];
                    }
            }
            $peer_comments = $p_c_d;
            if(!empty($peer_comments))
                array_unshift($peer_comments, '');
            // var_dump("entra aquí!!");
        }

        // var_dump("<br>self_comments=".json_encode($self_comments)."<br>peer_comments=".json_encode($peer_comments));
        // exit(0);

        // Obtenemos los datos del reto.
        $challenge = $this->challenges_model->getChallenge($challenge_id);
        // Buscar los alumnos pertenecientes al reto y al equipo
        $students = $this->challenges_model->getChallengeStudents($challenge_id, $s_team);
        // var_dump("<br>los estudiantes del reto son:<br>".json_encode($students));

        // Reordeno el array para poner al estudiante actual en primera posición.
        $std_tmp = array();
        $res_std = array();
        foreach($students as $std) {
            if($std['id'] == $student_id) {
                $std_tmp = $std;
                // var_dump("<br> al coger el estudiante-> std_tmp=".json_encode($std_tmp));
            } else {
                $res_std[] = $std;
            }
        }
        // var_dump("<br>al acabar, quitando el estudiante se queda:".json_encode($res_std));
        // exit(0);
        array_unshift($res_std, $std_tmp);
        // var_dump("<br><br><br>al poner el estudiante se queda:".json_encode($res_std));
        // exit(0);
        $students = $res_std;
        foreach($students as $std)
            $students_ids[] = $std['id'];
        // var_dump("<br><br>students=".json_encode($students).", count(students)=".count($students));
        // var_dump("<br><br>students_ids=".json_encode($students_ids));
//         exit(0);
        // Obtenemos los datos de la rúbrica asociada a este reto.
        $rubric_id = $this->challenges_model->getChallengeRubric($challenge_id);
        $rubric = $this->rubrics_model->getRubric($rubric_id);
        $criteria = $this->rubrics_model->getRubricCriteria($rubric_id);
//         var_dump("La rúbrica es:".$rubric['name']);
//         exit(0);
        
        // Obtenemos las descripciones de los criterios de la rúbrica.
        foreach ($rubric['data'] as $criterion) {
            // var_dump(array_values($criterion)[1]);
            // var_dump("<br>");
            $performance_descriptions[] = array_values($criterion)[1]; // Nos interesa solo el valor de las descripciones no el criterio
        }
        // var_dump($performance_descriptions);
        // exit(0);

        $vista = $this->load->view('students/challenge_assessments_sprint', array(
            'challenge' => $challenge,
            'student_id' => $student_id,
            'students_ids' => $students_ids,
            's_team' => $s_team,
            'current_sprint' => $current_sprint,
            'num_students' => count($students),
            'students' => $students,
            'rubric' => $rubric,
            'criteria' => $criteria,
            'performance_descriptions' => $performance_descriptions,
            'self_assessment_data' => (!empty($self_assessment_data) ? $self_assessment_data[$student_id] : $self_assessment_data),
            // 'peer_assessment_data' => $peer_assessment_data, //(!empty($peer_assessment_data) ? $peer_assessment_data : array()),
            'peer_assessment_data' => (!empty($peer_assessment_data) ? $peer_assessment_data : array()),
            'self_comments' => (!empty($self_comments) ? $self_comments[$student_id] : $self_comments),
            // 'peer_comments' => $peer_comments, //(!empty($peer_comments) ? $peer_comments : array()),
            'peer_comments' => (!empty($peer_comments) ? $peer_comments : array()),
        ), TRUE);
        $this->getTemplate($vista);
//         var_dump("students = ".count($students));
        // exit(0);
    }

    /**
     * Función para guardar la VALORACIÓN de un RETO
     */
    public function storeSprint() {
        // Comprobación de permisos
        $this->checkPermission();

        $challenge_id = $this->input->post('id');
        $student_id = $this->input->post('student_id');
        $s_team = $this->input->post('s_team');
        $current_sprint = $this->input->post('current_sprint');
        $num_students = $this->input->post('num_students');
        $max_value = $this->input->post('max_value');
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $comentarios = $this->input->post('comments'); // Array con los comentarios para los alumnos.
        $students_ids = $this->input->post('students_ids');
        // var_dump("<br>students_ids en StoreSprint =".json_encode($students_ids));
//         exit(0);

        // Recuperamos los datos de la evaluación (id, student_id, learning_outcomes, comments,...)
        $assessment = $this->challenges_assessments_model->getAssessments($challenge_id, $current_sprint, $student_id); // array con todos los datos
        // $assessment = $this->challenges_assessments_model->getAssessments($challenge_id, $current_sprint, ''); // array con todos los datos
        // var_dump("assessment=".json_encode($assessment)." <br> Cantidad=".count($assessment)); // "assessments=["6","7","8","9"]"
        // var_dump("assessment_id=".$assessments[0]['id'].", student_id=".$assessments[0]['student_id']);
//         exit(0);
//         $assessment_data['current_sprint'] = $current_sprint;
        $assessment_id = $assessment['id'];
        $outcome_id = $assessment['learning_outcomes_id'];
        $student_id = $assessment['student_id'];
        // var_dump($assessment_id.",".$student_id.",".$outcome_id."<br>");

        for($std=0; $std<$num_students; $std++) {
            for($i=0; $i<$rows; $i++) {
                $option = $this->input->post('criteria-'.$std.$i);
                // $option = substr($option,8);
                // Elimnar "criteria-" de la opción para quedarnos con el valor
                $option = substr(strrchr($option,"-"),1);
                $criteria_row[] = $option;
            }
            // Comentarios de cada estudiante
            $all_comments[] = array($students_ids[$std] => $comentarios[$std]);
            // Valores seleccionados por cada estudiante
            $all_assessment_data[] = array($students_ids[$std] => $criteria_row);
            $criteria_row = array();
        }
//         var_dump("<br>all_comments=".json_encode($all_comments));
//         exit(0);
        // Recuperamos los datos del estudiante que hace la evaluación, que es el primero
        $self_assessment_data = array_shift($all_assessment_data);
        $self_comments = array_shift($all_comments);
        // Guardamos la evaluación y comentarios del primer estudinate (self)
        if(!$this->challenges_assessments_model->saveSelfStudentAssessment($assessment_id, $student_id, $outcome_id, $self_assessment_data, $self_comments)) {
                $this->output->set_status_header(500);
        }
        // exit(0);

        // para el resto de estudiantes, guardamos sus datos en su evaluación correspondiente
//         var_dump("count=".$num_students-1);
        for($i=0; $i<$num_students-1; $i++) {
            /* $peer_assessment_data = array_shift($all_assessment_data);
            $peer_comments = array_shift($all_comments); */
            $peer_assessment_data = array($student_id => array_shift($all_assessment_data));
            $peer_comments = array( $student_id => array_shift($all_comments));
//             var_dump("<br>peer_assessment_data".json_encode($peer_assessment_data));
//             var_dump("<br>peer_comments".json_encode($peer_comments));

//             exit(0);
            // El id del alumno al que hay que guardar su evaluación.
            $peer_std_id = strval(array_keys(array_values($peer_assessment_data)[0])[0]);
//             var_dump("<br>tmp=".json_encode(array_keys(array_values($peer_assessment_data)[0])[0]));
//             var_dump("<br>peer_std_id=".$peer_std_id."<br>");
//             exit(0);
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, $current_sprint, $peer_std_id);
//             var_dump("<br>outcome_id=".$outcome_id);
//             exit(0);
            if(empty($outcome_id)) {
                $this->output->set_status_header(500);
            }
            // Guardamos los datos de cada estudiante en su correspondiente evaluación.
//             if(!$this->challenges_assessments_model->savePeerStudentAssessment($outcome_id, $peer_assessment_data, $peer_comments)) {
            if(!$this->challenges_assessments_model->savePeerStudentAssessment($outcome_id, $peer_assessment_data, $peer_comments)) {
                var_dump("eeror500");
                $this->output->set_status_header(500);
            }
//             exit(0);
        }
//         exit(0);
//         var_dump("all_comments = ".json_encode($all_comments)."<br>all_assessemnt_data=".json_encode($all_assessment_data)."<br>");
//         var_dump($assessment_data);
//         exit(0);
        $this->session->set_flashdata('success_msg', 'Valoración <strong>guardada correctamente</strong>.');
        redirect(base_url('students/challenges_assessments'));
    }


    /**
     * Función para obtener los resultados/calificaciones de un estudiante.
     */
    public function showGrades($challenge_id) {
        // Comprobación de permisos
        $this->checkPermission();

        // Si no tiene creada ninguna evaluación, no podemos mostrar resultados.
        $hasAssessments = $this->challenges_assessments_model->isChallengeInAssessment($challenge_id);
        // var_dump("hasAssessments=".json_encode($hasAssessments));
        // exit(0);
        if(!$hasAssessments) {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', 'Encara no s\'ha fet cap avaluació per aquest repte.');
            redirect(base_url('students/challenges_assessments'), 'refresh');
            return;
        }

        // Buscamos el identificador del estudiante que quiere ver sus calificaciones.
        $student_id = $this->students_model->getStudentId($this->session->id);
        // var_dump("student_id=".$student_id);
        // Buscamos los datos asociados al estudiante en este reto.
        $students = $this->challenges_model->getChallengeStudents($challenge_id);
        foreach($students as $std){
            if($std['id'] === $student_id)
                $student = $std;
        }
        // var_dump(json_encode($student));
        // var_dump("<br>students[0]['id']".json_encode($students[0]['id']));
        // exit(0);
        $sprints = $this->challenges_model->getChallengeSprints($challenge_id);
        // var_dump("<br>".json_encode($sprints));
        // exit(0);
        // Buscamos cuantos valores tiene la rúbrica, es decir, cuantas columnas
        $rubric_cols = $this->rubrics_model->getRubricRows($this->challenges_model->getChallengeRubric($challenge_id));
        // Buscamos los nombres de los criterios de la rúbrica
        $rubric_criteria = $this->rubrics_model->getRubricCriteria($this->challenges_model->getChallengeRubric($challenge_id));
        // Buscamos el valor máximo de la rúbrica
        $max_value = $this->rubrics_model->getRubricMaxValue($this->challenges_model->getChallengeRubric($challenge_id));


        // Comprobamos quién ha valorado.
        $all_who_has_assess = $this->getWhoHasAssess($challenge_id, $student_id, $students, $sprints);
        // print_r("all_who_has_assess:<br>");
        // print_r($all_who_has_assess);
        // exit(0);


        // Comprobamos si algún profesor ha evaluado al student
        $teacher_did_assessment = $this->teacherDidAssessment($challenge_id, $student_id, $sprints);
        // var_dump("¿Algún profesor a evaluado??: ");
        // print_r($teacher_did_assessment);
        // var_dump($teacher_did_assessment?'Si':'No');
        // var_dump("<br>Cuantos Sprints? ".count($teacher_did_assessment));
        // exit(0);

        
        // Eliminamos aquellas notas que no han sido evaluadas todavía por profesores.
        $allAssessed = 0;
        foreach ($teacher_did_assessment as $key => $value) {
            if ($value) {
                // unset($all_my_grades[$key]);
                $allAssessed++;
            }
        }
        // var_dump("<br>Cuantos Sprints han sido evaluados? ".$allAssessed);
        // exit(0);

        // Obtenemos las CALIFICACIONES PARCIALES del estudiante.
        $all_my_grades = $this->challenges_assessments_model->getStudentPartialGrades($challenge_id, $student_id, $sprints, $teacher_did_assessment);
        // var_dump("<br>(1) My Partial Grades= <br>".json_encode($all_my_grades)."count(my_grades)=".count($all_my_grades));
        // exit(0);

        
        // Obtenemos las calificaciones por sprints
        $my_sprints_grades = array();
        for($i=0; $i<count($all_my_grades)-1; $i++) {
            $sprint_number = 'sprint'.(string)($i+1);
            $tmp_grades = $all_my_grades[$sprint_number];
            
            // Transformamos las notas a BASE 10
            for($j=0; $j<count($tmp_grades); $j++) {
                $tmp_grades[$j] = number_format($tmp_grades[$j] * 10 / $max_value, 2, '.', ',');
            }
            $my_sprints_grades[] = $tmp_grades;
        }
        // var_dump("<br>my_sprints_grades=<br>".json_encode($my_sprints_grades));
        // exit(0);

        // Transformamos las notas a BASE 10
        $my_grades = array();
        $final_mark = '';
        if(!empty($all_my_grades)) {
            $my_grades = $all_my_grades['final_grades'];
            $final_mark = 0;
            for($iter=0; $iter<count($my_grades); $iter++) {
                $my_grades[$iter] = $my_grades[$iter] * 10 / $max_value;
                $final_mark += $my_grades[$iter];
            }
            $final_mark = number_format($final_mark/count($my_grades), 2, '.','.');
            // var_dump("<br>myGrades (BASE 10)=".json_encode($my_grades)."<br>MARK = ".$final_mark);
            // exit(0);
    
        }
        
    
        // Obtenemos TODOS los COMENTARIOS del estudiante
        $my_comments = $this->challenges_assessments_model->getStudentComments($challenge_id, $student_id, $sprints);
        // var_dump("<br>los comentarios de id(".$student_id.") son: <br>".json_encode(array_values($my_comments)));
        // exit(0);

        // ************************************************************
        // VAMOS POR LAS TECHNICAL SKILLS - TAREAS EVALUABLES
        // ************************************************************
        $num_technical_skills = 0;

        // Buscamos los RAs asociados al reto. 
        // $technical_skills_ids = explode(",",$this->challenges_model->getTechnicalSkills($challenge_id));
        $technical_skills = $this->challenges_model->getTechnicalSkills($challenge_id);
        
        // print_r("<br>Los RAs del Reto(".$challenge_id.") son =".json_encode($technical_skills)."<br><br>");
        // exit(0);
        $all_technical_comments = array();
        $all_technical_names = array();
        $all_technical_marks = array();
        $all_average_technical_marks = array();
        $group_technical_marks = array();
        $group_technical_marks_orginals = array();
        $group_technical_names = array();
        $group_technical_comments = '';
        $technical_skills_percentages = array();
        if(!empty($technical_skills)) {
            // Reiniciamos las variables
            $group_technical_marks = array();
            $group_technical_marks_orginals = array();
            $group_technical_names = array();
            $group_technical_comments = '';

            $num_technical_skills = count($technical_skills);
            // Identificadores de los RAs
            $technical_skills_ids = array_keys($technical_skills);
            // Porcentajes de los RAs
            $technical_skills_percentages = array_values($technical_skills);
            for($n=0; $n<$num_technical_skills; $n++) {
                
                $technical_skills_name = $this->technical_skills_model->getTechnicalSkillsName($technical_skills_ids[$n]);
                // print_r("<br>TS Name = ".$technical_skills_name);
                $group_technical_names[] = $technical_skills_name;

                $has_rubric = $this->technical_skills_model->hasRubric($technical_skills_ids[$n]);

                // Comprobamos el tipo de calificación que tenemos que calcular
                if($has_rubric == 0) {
                    // Buscamos las calificaciones numéricas de los profesores y calculamos la nota media.
                    $final_technical_mark = json_decode($this->technical_skills_assessments_model->getFinalMark($technical_skills_ids[$n], $challenge_id, $student_id,TRUE));
                    // print_r("<br>final_technical_mark=".json_encode($final_technical_mark));
                    // exit(0);
                    $average_marks = 0;
                    $average_marks_originals = 0;
                    $count = 0;
                    if (!empty($final_technical_mark)) {
                        foreach($final_technical_mark as $mark){
                            // A la nota final le aplicamos el porcentaje de cada RA
                            $average_marks += floatval($mark) * ($technical_skills_percentages[$n]*0.01); 
                            $average_marks_originals += floatval($mark); 
                            $count++;  
                        }
                        $average_marks = $average_marks / $count;
                        $average_marks_originals = $average_marks_originals / $count;
                        // Montamos un array con el identificador del RA y su calificación final.
                        // print_r($average_marks);
                        if($count > 0) {
                            // print_r($technical_skills_percentages[$n]*0.01);
                            // exit(0);
                            $group_technical_marks[] = number_format($average_marks, 2);
                            $group_technical_marks_orginals[] = number_format($average_marks_originals, 2);
                            // $group_technical_marks[] = number_format($average_marks / $count, 2);
                        } else {
                            $group_technical_marks[] = 0.00;
                            $group_technical_marks_orginals[] = 0.00;
                        }

                        // print_r("<br>average_marks=".json_encode($group_technical_marks));
                    } else {
                        $group_technical_marks[] = 0.00;
                        $group_technical_marks_orginals[] = 0.00;
                    }
                    
                } else {
                    // Si has_rubric != 0 es que tiene una rúbrica.
                    $group_technical_marks[] = 10.00;
                    $group_technical_marks_orginals[] = 10.00;
                }

                // json_encode( $text, JSON_UNESCAPED_UNICODE );
                $technical_teachers_comments = $this->technical_skills_assessments_model->getTeachersComments($technical_skills_ids[$n], $challenge_id, $student_id);
                // var_dump(json_decode($technical_teachers_comments));
                // exit(0);

                // $all_comments = array($teacher_id, $technical_teachers_comments);
                if(!empty($technical_teachers_comments)) {

                    // Voy a trabajar los comentarios como un array. 
                    // En el índice estará el id del profe
                    // Como valor estará el comentario.
                    $tech_comments = json_decode($technical_teachers_comments, JSON_UNESCAPED_UNICODE);
                    // var_dump($all_technicall_comments);
                    // exit(0);

                    // $teacher_plus_comment = '';
                    // $group_technical_comments = '';
                    foreach($tech_comments as $keyid => $comment) {
                        if(!empty($comment)) {
                            $tech_name = $this->teachers_model->getTeacherFullName($keyid);
                            // var_dump($tch_name);
                            $teacher_plus_comment = join(": ",array($tech_name, $comment));
                            $group_technical_comments .= (string)$teacher_plus_comment . '<br>';
                            // Añadimos el nombre del Technical Skill al inicio de todos los comentarios
                            $group_technical_comments = '<strong>'.$technical_skills_name.':</strong><br>'.$group_technical_comments;

                        } else  {
                            $teacher_plus_comment = join(" ",array());
                        }
                        
                    }
                    
                } else {
                    // Si no tiene comentarios de ningún profesor
                    //$group_technical_comments .= '<br>';
                }
                                                    
            }
            
            $all_average_technical_marks[] = number_format(array_sum($group_technical_marks), 2);
            // Juntamos las calificaciones de este alumno.
            $all_technical_names[] = $group_technical_names;
            $all_technical_marks[] = $group_technical_marks_orginals;
            $all_technical_comments[] = $group_technical_comments;
            // exit(0);
            

        } else {
            $num_technical_skills = 0;
        }
        // exit(0);
        // FIN TAREAS EVALUABLES - TECHNICAL SKILLS - RAs

        // Obtener los datos de las calificaciones de profesores y alumnos (propio y compañeros)
        $vista = $this->load->view('students/challenges_grades_show', array(
            'teacher_did_assessment' => $teacher_did_assessment,
            'who_has_assess' => $all_who_has_assess,
            'my_sprints_grades' => $my_sprints_grades,
            'my_grades' => $my_grades,
            'my_comments' => array_values($my_comments), // Un array con los comentarios de [0]=mios, [1]=peers, [2]=teachers
            'final_mark' => $final_mark,
            'all_technical_names' => $all_technical_names,
            'all_technical_marks' => $all_technical_marks,
            'all_average_technical_marks' => $all_average_technical_marks,
            'all_technical_comments' => $all_technical_comments,
            'num_technical_skills' => $num_technical_skills,
            'technical_skills_percentages' => $technical_skills_percentages,
            'challenge_id' => $challenge_id,
            'cols' => $rubric_cols,
            'student' => $student,
            'student_id' => $student_id,
            'criteria' => $rubric_criteria,
            'max_value' => $max_value,
            'sprints' => $sprints,
            'school_year' => $this->session->school_year, 
        ), TRUE);
        $this->getTemplate($vista);

    }

    /** 
     * Función para comprobar si un profesor ha realizado la evaluación de un alumno
     */
    public function teacherDidAssessment($challenge_id, $student_id = '', $sprints='') {

        $teacher_did_assessment = array(); 
        if(empty($sprints)) {
            // var_dump("No hay sprint definido.");
            return $teacher_did_assessment;
        }

        for($i=0; $i<intval($sprints); $i++) {
            $teacher_did_assessment[$i] = false;
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($i+1) /* # sprint */, $student_id);
            // print_r("<br>outcome_id:".$outcome_id."<br>");
            // exit(0);
            // Si no hay resultados porqué no se han evaluado todavía los sprints, saltamos al siguiente
            if (is_null($outcome_id)) 
                continue;

            $teacher_data = $this->challenges_assessments_model->getTeachersAssessmentData($outcome_id); 
            // print_r($teacher_data);
            // exit(0);
            if(is_null($teacher_data))
                continue;
            // Comprobar si no hay resultados
            foreach ($teacher_data as $key => $value) {
                // print_r("key=".$key." - value=".$value);
                if (!empty($value)) {
                    $teacher_did_assessment[$i] = true;
                }
            }
        }
        // print_r("teacher_did_assessment: <br>");
        // print_r($teacher_did_assessment);
        // exit(0);
        return $teacher_did_assessment;
    }

    /**
     * Función para obtener quién ha valorado un reto
     */
    public function getWhoHasAssess($challenge_id, $student_id, $students, $sprints = '') {

        // print_r($students);
        // print_r("<br>student_id:".$student_id);
        // exit(0);
        foreach ($students as $key => $value) {
            if ($value['id'] === $student_id) {
                $student_data = $value;
            }
        }
        // print_r("<br>");
        // print_r($student_data);
        // exit(0);

        // Montamos la estructura de Estudiante + Profesores + Compañeros
        // Para comprobar quién ha valorado y quien no.
        $tmp_who_has_assess_teachers = $this->challenges_model->getChallengeTeachers($challenge_id);
        $who_has_assess_teachers = array();
        $teachers_names =  array();
        $teachers_ids = array();
        foreach($tmp_who_has_assess_teachers as $key => $value){
            // $who_has_assess_teachers[] = $value['firstname'].' '.$value['lastname'];
            $who_has_assess_teachers[] = $value['id'];
            $teachers_names[] = $value['firstname'].' '.$value['lastname'];
            $teachers_ids[] = $value['id'];

        }
        
        $all_who_has_assess = array();
        $self_assess_values = array();
        $self_who_has_assess = array();

        // Buscamos sus propias valoraciones.          
        for($k=0; $k<intval($sprints); $k++) {
            // $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $students[$i]['id']);
            $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $student_id);
            // print_r($outcome_id.", ");    
            // exit(0);
            if (is_null($outcome_id)) {
                $self_assess_values[0][$student_data['firstname'].' '.$student_data['lastname']][] = false;
                // break;
            } else {
                $self_data = $this->challenges_assessments_model->getStudentAssessmentData($outcome_id);
                // print_r($self_data);
                if(is_null($self_data) || empty($self_data)) {
                    $self_assess_values[0][$student_data['firstname'].' '.$student_data['lastname']][] = false; 
                } else {
                    $self_assess_values[0][$student_data['firstname'].' '.$student_data['lastname']][] = true; 
                }
            }
        }
        $self_who_has_assess[0] = $self_assess_values;
        $self_assess_values = array();

        // print_r("self_values:<br>");
        // print_r($self_who_has_assess);
        // exit(0);

        $teachers_assess_values = array();
        $teachers_who_has_assess = array();

        // Buscamos las valoraciones de todo el profesorado para este estudiante.
        for($j=0; $j<count($teachers_ids); $j++) {
            // $teachers_assess_values[$i][] =  $teachers_names[$j];
            // print_r($teachers_names[0]);
            // exit(0);

            for($k=0; $k<intval($sprints); $k++) {
                $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $student_id);
                // print_r($outcome_id);    
                // exit(0);
                if (is_null($outcome_id)) {
                    $teachers_assess_values[0][$teachers_names[$j]][] = false;
                    // break;
                } else {
                    // $teachers_data = json_encode($this->challenges_assessments_model->getTeachersAssessmentData($outcome_id));
                    $teachers_data = $this->challenges_assessments_model->getTeachersAssessmentData($outcome_id);
                    // $teachers_data = json_decode($this->challenges_assessments_model->getTeachersAssessmentData($outcome_id), TRUE);
                    // $teachers_data = $this->challenges_assessments_model->getTeachersAssessmentData($outcome_id);
                    
                    if(is_null($teachers_data) || empty($teachers_data)) {
                        $teachers_assess_values[0][$teachers_names[$j]][] = false;
                    } else {

                        $teachers_data_ids = array_keys($teachers_data);
                        // print_r($teachers_data);
                        // print_r("<br>");
                        // print_r($teachers_data_ids);
                        // print_r("<br>");
                        // print_r($teachers_ids[$j]);
                        // exit(0);
                        if(in_array($teachers_ids[$j], $teachers_data_ids)) {
                            // var_dump("Si que hay coincidencia<br>");
                            $teachers_assess_values[0][$teachers_names[$j]][] = true;
                        } else {
                            $teachers_assess_values[0][$teachers_names[$j]][] = false;
                        }
                    }
                }
            }
            $teachers_who_has_assess[0] = $teachers_assess_values;
        }
        $teachers_assess_values = array();

        // print_r($teachers_who_has_assess);
        // exit(0);

        // return $all_who_has_assess;
        $team_mates_assess_values = array();
        $team_who_has_assess = array();

        // Buscamos las valoraciones de sus compañeros de equipo.
        $team_mates = $this->challenges_model->getChallengeTeamMatesId($challenge_id, $student_id);
        // print_r($team_mates);
        // exit(0);

        // Pomemos los compañeros de equipo
        foreach($team_mates as $key => $team_mate_id) {
            $tmp_team_mate = $this->students_model->getStudent($this->students_model->getUserIdFromStudent($team_mate_id));
            // print_r($tmp_team_mate);
            // exit(0);
            
            for($k=0; $k<intval($sprints); $k++) {
                $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $student_id);
                // print_r($outcome_id);    
                // exit(0);
                if (is_null($outcome_id)) {
                    $team_mates_assess_values[0][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = false;
                    // print_r($team_mates_assess_values);
                    // exit(0);
                    continue;
                } else { 
                    $team_mate_data = $this->challenges_assessments_model->getPeerAssessmentData($outcome_id);
                    // print_r($team_mate_data);
                    // exit(0);
                    if(is_null($team_mate_data) || empty($team_mate_data)) {
                        $team_mates_assess_values[0][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = false;
                        // print_r($team_mates_assess_values);
                        // exit(0);
                    } else {
                        // print_r($team_mate_data);
                        // exit(0);

                        if(isset($team_mate_data[$team_mate_id])) {
                            $team_mates_assess_values[0][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = true;
                        } else {
                            $team_mates_assess_values[0][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = false;
                        }
                    }
                }
            }
            $team_who_has_assess[0] = $team_mates_assess_values;
        }
        $team_mates_assess_values = array();

        // print_r("<br>team_mates_values:<br>");
        // print_r($team_who_has_assess);
        // print_r("<br><br>");
        // exit(0);

        // Juntamos las valoraciones: propias, profesores y compañeros
        // $all_who_has_assess[] = $self_assess_values;
        // $all_who_has_assess[] = $teachers_assess_values;
        // $all_who_has_assess[] = $team_mates_assess_values;
        $all_who_has_assess[] = $self_who_has_assess;
        $all_who_has_assess[] = $teachers_who_has_assess;
        $all_who_has_assess[] = $team_who_has_assess;

        // print_r($all_who_has_assess);
        // exit(0);        
        return $all_who_has_assess;     
    }


    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'header' => $this->load->view('students/header', '', TRUE), 
            'nav' => $this->load->view('students/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('students/footer', '', TRUE),

        );
        $this->load->view('students/challenges', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status != 'active')
            redirect(base_url('students/dashboard/editPassword'), 'refresh');

        // Si es un profesor no puede entrar aquí
        if ($this->session->role === 'teacher')
            redirect('teachers/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        //var_dump("formSubmit = ".$formSubmit);
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('students/dashboard'), 'refresh');
        }
    }
}