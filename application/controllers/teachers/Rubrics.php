<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubrics extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','pagination'));
        // Cambiar el formato del tipo de error
        //$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
        // Ya no hace falta con Tailwindcss
        // cargamos el helper con las reglas de las rúbricas
        $this->load->helper(array('rubrics_rules'));
        // Cargar el modelo de las rúbricas
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');

    }

    /**
     * Función principal por defecto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        $current_school_year = $this->session->school_year;
            
        // $data = $this->rubrics_model->getRubrics();
        if($this->session->role === 'admin') {
            // Si es el administrador, puede ver todas las rúbricas
            $data = $this->rubrics_model->getRubrics();
        } else {
            // Obtenemos las rúbricas a las que está asignado el profesor o es el creador.
            $data = $this->rubrics_model->getMyRubrics($this->teachers_model->getTeacherId($this->session->id));
        }

        // Obtenemos la información de los profesores
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        
        // Para hacer la paginación
        $config['base_url']= base_url('teachers/rubrics/index');
        // Elementos por página
        $config['per_page']= 9;
        $config['total_rows']= count($data);

        // Paginación con TailwindCSS
        $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        $config['full_tag_close']= '</ul></div>';
        $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['num_tag_close']= '</li>';
        $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teal-500 border-r border-grey-light px-3 py-2 active">';
        $config['cur_tag_close']= '</li>';
        $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['next_tag_close']= '</li>';
        $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['prev_tag_close']= '</li>';
        $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['first_tag_close']= '</li>';
        $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        $config['last_tag_close']= '</li>';

        $this->pagination->initialize($config);
        $page = $this->rubrics_model->get_Paginate($config['per_page'],$offset);

        $vista = $this->load->view('teachers/rubrics_show', array(
            'data' => $page, 
            'school_year' => $current_school_year,
            'teacher_id' => $teacher_id,
        ),  TRUE);
        $this->getTemplate($vista);
            
    }

    /**
     * Función para crear una rúbrica en la BBDD
     */
    public function create() {
        // Comprobación de permisos
        $this->checkPermission();
        
        if($this->session->role === 'admin') {
            $teachers = $this->teachers_model->getTeachers();
        } else {
            // Recuperamos los profesores de la BBDD que compartan nivel de docencia con el profesor actual
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $teaching_levels = $this->teachers_model->getTeachingLevels($teacher_id);
            // var_dump("teaching_levels=".json_encode($teaching_levels));
            // exit(0);
            // $teachers = array();
            // foreach($teaching_levels as $level){
            //     $teachers[] = $this->teachers_model->getTeachers($level);
            // }
            // $teachers = $teachers[0];

            $teachers = $this->teachers_model->getTeachers($teaching_levels);
            // var_dump("<br>teachers=<br>".json_encode($teachers));
            // exit(0);

        }
        
        $vista = $this->load->view('teachers/rubric_create', array(
            'teachers' => $teachers,
        ), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para guardar los datos básicos de una Rúbrica
     */
    public function store() {
        // Comprobación de permisos
        $this->checkPermission();
            
        // Capturamos los datos del formulario
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $max_value = $this->input->post('max_value');
        $min_value = $this->input->post('min_value');
        $rate = 100/$rows;
        $teaching_levels = $this->input->post('teaching_levels');
        $teachers = $this->input->post('teachers');
        $teacher_owner = $this->teachers_model->getTeacherId($this->session->id);

        $this->form_validation->set_rules(getCreateRubricsRules());
        if( $this->form_validation->run() === FALSE ) {
            // ERRORES
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $teaching_levels = $this->teachers_model->getTeachingLevels($teacher_id);
            $teachers = $this->teachers_model->getTeachers($teaching_levels);
            $this->session->set_flashdata('error_msg', 'La rúbrica debe tener al menos un nivel de docencia seleccionado.');
            $this->output->set_status_header(400);
            $vista = $this->load->view('teachers/rubric_create', array(
                'teachers' => $teachers,
            ), TRUE);            
            $this->getTemplate($vista);
            return;
        } 
        // Comprobar que al menos tenga un nivel de docencia seleccionado
        if(empty($teaching_levels)) {
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $teaching_levels = $this->teachers_model->getTeachingLevels($teacher_id);
            $teachers = $this->teachers_model->getTeachers($teaching_levels);
            $this->session->set_flashdata('error_msg', 'La rúbrica debe tener al menos un nivel de docencia seleccionado.');
            $vista = $this->load->view('teachers/rubric_create', array(
                'teachers' => $teachers,
            ), TRUE);            
            $this->getTemplate($vista);
            return;
        }
        // Añadimos el profesor creador para que esté en la lista de profesores que pueden ver la rúbrica
        $teachers[] = $teacher_owner;
        // Si está duplicado, lo borramos.
        $teachers = array_unique($teachers);
        $rubric = array (
            'name' => $name,
            'description' => $description,
            'cols' => $cols,
            'rows' => $rows,
            'max_value' => $max_value,
            'min_value' => $min_value,
            'rate' => $rate,
            't_owner' => $teacher_owner,
            't_available_for' => $teachers,
            'teaching_levels' => $teaching_levels,
        );

        // Creamos la RÚBRICA en la BBDD
        $rubric_id = $this->rubrics_model->create($rubric);
        if(!$rubric_id) {
            $this->output->set_status_header(500);
        } 
        // Redirección a la vista principal
        // $this->session->set_flashdata('success_msg', 'Rúbrica \''.$name.'\' guardada correctamente!');
        // redirect(base_url('rubrics'));   
        // Redirigmos a crear los items de la rúbrica
        $data = $this->rubrics_model->getRubric($rubric_id);
        $this->session->set_flashdata('success_msg', 'Rúbrica \''.$name.'\' creada correctamente.<br>Ahora establece los items de la misma.');
        $this->session->set_flashdata('error_msg', '');
        $vista = $this->load->view('teachers/rubric_create_criteria', array('rubric' => $data), TRUE);
        $this->getTemplate($vista);

    }
    /**
     * Función para guardar los datos específicos de una rúbrica
     */
    public function storeCriteria ($data = array()){
        // Comprobación de permisos
        $this->checkPermission();
        // Capturamos los datos del formulario
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $rubric_id = $this->input->post('id');
        $criteria = $this->input->post('criteria');
        $descriptions = $this->input->post('descriptions');
        // var_dump("rubrid_id(dentro de StoreCriterion=".$rubric_id."<br>");

        $data = array();
        // Montamos el array de datos
        for($i=0; $i<$rows; $i++) {
            $desc_col_tmp = array();

            for($j=$i*$cols; $j<($i+1)*$cols; $j++)
                $desc_col_tmp[] = $descriptions[$j];

            //var_dump("columnas = ".$columnas);
            // $fila = 'row'.($i+1);
            // $data[] = array(
            //     $fila => array(
            //         'criterion' => $criteria[$i],
            //         'description' => $desc_col_tmp,
            //     ) 
            // );
            $data[] = array(
                'criteria' => $criteria[$i],
                'descriptions' => $desc_col_tmp,
            );
            // vaciamos el array temporal
            $desc_col_tmp = array();
        }
        // var_dump($data);
        // exit(0);

        // Actualizamos la RÚBRICA en la BBDD con los items y descripciones de la rúbrica
        if(!$this->rubrics_model->updateCriteria($rubric_id, $data)) {
            $this->session->set_flashdata('error_msg', 'Error al guardar los criterios de la rúbrica.');
            redirect(base_url('teachers/rubrics'), 'refresh');
        }
        // Redirección a la vista principal
        $this->session->set_flashdata('success_msg', 'Rúbrica <strong>guardada correctamente</strong>!');
        redirect(base_url('teachers/rubrics'), 'refresh');
    }

    /**
     * Función para editar una Rúbrica
     */
    public function edit($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        $current_school_year = $this->session->school_year;

        // Obtenemos la información de la rúbrica
        $data = $this->rubrics_model->getRubric($id);
        // var_dump("<br>data=".json_encode($data));
        // exit(0);
        // Obtenemos la información de los profesores
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        $teaching_levels = $this->teachers_model->getTeachingLevels($teacher_id);
        $teacher_owner = $this->rubrics_model->getTeacherOwner($id);
        $isowner = ($teacher_id == $teacher_owner) ? 1 : 0;
        // Si es admin, le atribuimos la autoría.
        if($this->session->role === 'admin') 
            $isowner = 1;
            
        // var_dump("t_id=".$teacher_id." vs t_own=".$teacher_owner.", isowner=".$isowner);
        // exit(0);
        // var_dump("teaching_levels=".json_encode($teaching_levels));
        // exit(0);
        // foreach($teaching_levels as $level){
        //     // Cogemos solo los identificadores de los profes
        //     $teachers[] = $this->teachers_model->getTeachers($level);
        // }
        // $teachers = $teachers[0];
        $teachers = $this->teachers_model->getTeachers($teaching_levels);


        $view = $this->load->view('teachers/rubric_edit', array(
            'rubric' => $data,
            'teachers' => $teachers,
            'isowner' => $isowner,
            'school_year' => $current_school_year,

            ) ,TRUE);
        $this->getTemplate($view);
    }

    /**
     * Función para actualizar los datos de una rúbrica
     */
    public function update() {
        // Comprobación de permisos
        $this->checkPermission();
            
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') !== 'POST')
            show_404();

        // Capturamos los datos del formulario
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $max_value = $this->input->post('max_value');
        $min_value = $this->input->post('min_value');
        $rate = 100/$rows;
        $teachers = $this->input->post('teachers');
        $teaching_levels = $this->input->post('teaching_levels');
        $formSubmit = $this->input->post('submit');

        $this->form_validation->set_rules(getUpdateRubricsRules());
        if( $this->form_validation->run() === FALSE ) {
            // ERRORES
            $this->output->set_status_header(400);
            $vista = $this->load->view('teachers/rubric_edit','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        $rubric = array (
            'id' => $id,
            'name' => $name,
            'description' => $description,
            'cols' => $cols,
            'rows' => $rows,
            'max_value' => $max_value,
            'min_value' => $min_value,
            't_available_for' => $teachers,
            'teaching_levels' => $teaching_levels,
        );   
        
        // Si hemos llamado a modificar los items/criterion de la rúbrica
        // var_dump($formSubmit);
        // exit(0);
        //if( $formSubmit === 'Modificar Items' ) {
        if ( $formSubmit === 'Modificar Items' or $formSubmit === 'Modify Items') {
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $teacher_owner = $this->rubrics_model->getTeacherOwner($id);
            $isowner = ($teacher_id == $teacher_owner) ? 1 : 0;
            // Si es admin, le atribuimos la autoría.
            if($this->session->role === 'admin') 
                $isowner = 1;
            $data = $this->rubrics_model->getRubric($id);
            // var_dump($data['data']);
            // var_dump($data['data'][0]['criteria']);
            // exit(0);
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', '');
            $vista = $this->load->view('teachers/rubric_update_criteria', array(
                'rubric' => $data,
                'data' => $data['data'],
                'isowner' => $isowner,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Llamamos al modelo para actualizar los datos.
        if(!$this->rubrics_model->update($id, $rubric))
            $this->output->set_status_header(500);

        // Recuperamos los datos actualizdos
        $data = $this->rubrics_model->getRubric($id);
        $this->session->set_flashdata('success_msg', 'Rúbrica "'.$name.'" modificada correctamente.');
        $this->session->set_flashdata('error_msg', '');
        // Redirigmos a la vista principal de las rúbricas
        redirect(base_url('teachers/rubrics'), 'refresh');

    }
    /**
     * Función para actualizar los items/criterios de una rúbrica
     */
    public function updateCriteria($data = array()) {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos del formulario
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $rubric_id = $this->input->post('id');
        $criteria = $this->input->post('criteria');
        $descriptions = $this->input->post('descriptions');
        // var_dump("rubrid_id(dentro de StoreCriterion=".$rubric_id."<br>");
        // var_dump("en UpdateCriteria(criteria)=".json_encode($criteria));
            

        $data = array();
        // Montamos el array de datos
        for($i=0; $i<$rows; $i++) {
            $desc_col_tmp = array();

            for($j=$i*$cols; $j<($i+1)*$cols; $j++)
                $desc_col_tmp[] = $descriptions[$j];

            //var_dump("columnas = ".$columnas);
            $data[] = array(
                'criteria' => $criteria[$i],
                'descriptions' => $desc_col_tmp,
            );
            // vaciamos el array temporal
            $desc_col_tmp = array();
        }
        // var_dump($data);
        // exit(0);

        // Actualizamos la RÚBRICA en la BBDD con los items y descripciones de la rúbrica
        if(!$this->rubrics_model->updateCriteria($rubric_id, $data)) {
            $this->session->set_flashdata('error_msg', 'Error al guardar los criterios de la rúbrica.');
            redirect(base_url('teachers/rubrics'), 'refresh');
        }
        // Redirección a la vista principal
        $this->session->set_flashdata('success_msg', 'Rúbrica <strong>guardada correctamente</strong>!');
        redirect(base_url('teachers/rubrics'), 'refresh');


    }

    /**
     * Borrar una rúbrica de la BBDD
     */
    public function delete($id) {
        // Comprobación de permisos
        $this->checkPermission();
        
        if(!$this->rubrics_model->delete($id)){
            // ERROR No se ha podido borrar el usuario
            show_500();
        } else {
            // Se ha borrado con éxito. Volver a la pantalla de estudiantes
            redirect('teachers/rubrics');
        }
    }
   
    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE), 
            // 'nav' => $this->load->view('teachers/nav', '', TRUE), 
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );
        $this->load->view('teachers/rubrics', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');
        
        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');

        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/rubrics'), 'refresh');
        }
    }
}