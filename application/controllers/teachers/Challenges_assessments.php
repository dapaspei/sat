<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges_assessments extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','pagination'));

        // cargamos el helper con las reglas de los retos
        //$this->load->helper(array('challenges_rules'));
        // Cargar los modelos necesarios
        $this->load->model('challenges_model');
        $this->load->model('challenges_assessments_model');
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');
        $this->load->model('students_model');
    }

    /**
     * Función principal por defecto para mostrar las Evaluaciones de un Reto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // if ($this->session->role === 'teacher') {
        //     $teacher = $this->teachers_model->getTeacherId($this->session->id);
        //     $teacher_id=$teacher['id'];
        //     $data = $this->challenges_model->getChallenges($teacher_id);
        //     //$data = $this->challenges_assessments_model->getAssessment($teacher_id);

        // } else { // admin
        //     $teacher_id = '';
        //     $data = $this->challenges_model->getChallenges();
        //     //$data = $this->challenges_assessments_model->getAssessment($teacher_id);

        // }


        // // Para hacer la paginación
        // $config['base_url']= base_url('teachers/challenge_assessments/index');
        // // Elementos por página
        // $config['per_page']= 6;
        // $config['total_rows']= count($data);

        // // Paginación con TailwindCSS
        // $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        // $config['full_tag_close']= '</ul></div>';
        // $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['num_tag_close']= '</li>';
        // $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teal-500 border-r border-grey-light px-3 py-2 active bg-teal-500">';
        // $config['cur_tag_close']= '</li>';
        // $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['next_tag_close']= '</li>';
        // $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['prev_tag_close']= '</li>';
        // $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['first_tag_close']= '</li>';
        // $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        // $config['last_tag_close']= '</li>';

        // $this->pagination->initialize($config);
        // $page = $this->challenges_model->get_Paginate($teacher_id,$config['per_page'],$offset);

        // $vista = $this->load->view('teachers/challenge_assessments_show', array('data' => $page), TRUE);
        // //$vista = $this->load->view('teachers/challenge_assessments_show', array('data' => $page), TRUE);
        // $this->getTemplate($vista);
    }

    // /**
    //  * Función para INICIALIZAR las evaluaciones de los alumnos
    //  */
    // public function start() {
    //     // Comprobación de permisos
    //     $this->checkPermission();

    //     // Obtener los retos en los que están asignados todos los alumnos
    //     //$students_ids = $this->

    // }

    // /**
    //  * Función para DETENER las evaluaciones
    //  */
    // public function stop() {

    // }
 
    /**
     * Función para crear una Evaluación
     */
    public function create($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // Obtenemos la información del RETO
        $challenge = $this->challenges_model->getChallenge($id);
        // print_r($challenge);
        // exit(0);

        // Buscamos los sprints que se pueden evaluar.
        $sprints_openfor_assess = $this->challenges_model->getSprintsStatus($id);

        $current_sprint = 0;
        for($i=0; $i<count($sprints_openfor_assess); $i++){
            if ($sprints_openfor_assess[$i] != 0) {
                $current_sprint = $i+1; // Los sprints empiezan desde 1
                // var_dump("create(current_sprint)=".$current_sprint);
            }

        }
        // exit(0);

        /*
        // Buscamos si tiene creadas los registros para las calificaciones del Sprint actual
        $hasAssessments = $this->challenges_assessments_model->isChallengeInAssessment($id, $current_sprint);
        
        // Comprobamos que no tenga creado todavía ninguna evaluación, sinó, la creamos.
        if(!$hasAssessments) {
            // Buscar los alumnos pertenecientes al reto
            $students = $this->challenges_model->getChallengeStudents($id);
            // var_dump("Students=".json_encode($students));
            // Buscar los profesores asociados al reto
            $teachers = $this->challenges_model->getChallengeTeachers($id);
            // print_r("<br>t=".json_encode($teachers));
            // exit(0);
            // Con los estudiantes, creamos sus registros de evaluación en la BBDD
            if(!$this->challenges_assessments_model->start($id, $challenge, $students, $teachers, $current_sprint ))
                $this->output->set_status_header(500);
        }
        */
        $vista = $this->load->view('teachers/challenge_assessments_create', array(
            'challenge' => $challenge,
            'current_sprint' => $current_sprint,
            'current_team' => 0,
            'sprints_openfor_assess' => $sprints_openfor_assess,
            'school_year' => $this->session->school_year, 
        ), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para VALORAR un RETO
     */
    public function assessSprint($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos de la URL (http:/...../Challenge/Sprint)
        $challenge_id = $this->uri->segment(4);
        $current_sprint = $this->uri->segment(5);
        $current_team = $this->uri->segment(6);
//         $next_team = $current_team + 1;
        // var_dump("current_team=".$current_team);
        // $assessments = $this->challenges_assessments_model->getAssessmentsId($challenge_id); //challenge_id
        // var_dump("assessments=".$assessments[0]); // "assessments=["6","7","8","9"]"
//         exit(0);
        // Obtenemos los datos del reto.
        $challenge = $this->challenges_model->getChallenge($challenge_id);
        // Buscar los alumnos pertenecientes al reto
        $students = $this->challenges_model->getChallengeStudents($challenge_id, $current_team);

        // Comprobamos si el profesor ha realizado la valoración de este sprint
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        $sprintAssessData = $this->challenges_assessments_model->getTeachersSprintAssessData($challenge_id, $current_sprint, $teacher_id, $students, $current_team);
        $sprintCommentsData = $this->challenges_assessments_model->getTeachersSprintCommentsData($challenge_id, $current_sprint, $teacher_id, $students, $current_team);
        // var_dump("<br>sprintAssessData = ".json_encode($sprintAssessData));
        // var_dump("sprintAssessData[9] = ".json_encode($sprintAssessData[1]['9']['4'])); // "sprintAssessData[7] = ["0","1.67","3.33","5"]"
        // var_dump("sprintsCommentsData=".json_encode($sprintCommentsData));
        // exit(0);

        $assess_data = array();
        $assess_comments = array();
        for($i=0; $i<count($students); $i++) {
//             $assess_data[] = $sprintAssessData[$i][$teacher_id];
            if(!empty($sprintAssessData))
                $assess_data[] = array_values($sprintAssessData[$i])[0];
            // implode( ) para convertir de array a string.
            if(!empty($sprintCommentsData))
                $assess_comments[] = implode(array_values($sprintCommentsData[$i]));
        }
//         var_dump("<br>assess_data=".json_encode($assess_data)."<br>comennts=".json_encode($assess_comments));
//         exit(0);

        // Tenemos la evaluación de los alumnos solo de un profesor (el actual).

        // Obtenemos los datos de la rúbrica asociada a este reto.
        $rubric_id = $this->challenges_model->getChallengeRubric($challenge_id);
        $rubric = $this->rubrics_model->getRubric($rubric_id);
        $criteria = $this->rubrics_model->getRubricCriteria($rubric_id);
        // $performance_descriptions = $rubric['data'];
        // var_dump("La rúbrica es:".$rubric['name']);
        // var_dump($criteria);

        // Obtenemos las descripciones de los criterios de la rúbrica.
        foreach ($rubric['data'] as $criterion) {
            // var_dump(array_values($criterion)[1]);
            // var_dump("<br>");
            $performance_descriptions[] = array_values($criterion)[1]; // Nos interesa solo el valor de las descripciones no el criterio
        }


        // var_dump($performance_descriptions);
        // exit(0);

        $vista = $this->load->view('teachers/challenge_assessments_sprint', array(
            'challenge' => $challenge,
            'current_sprint' => $current_sprint,
            'num_students' => count($students),
            'students' => $students,
//             'students' => $students_array,
            'rubric' => $rubric,
            'criteria' => $criteria,
            'performance_descriptions' => $performance_descriptions,
            'assessments' => $assess_data,
            'comments' => $assess_comments,
            'current_team' => $current_team,
//             'next_team' => $next_team,
            'school_year' => $this->session->school_year, 

        ), TRUE);
        $this->getTemplate($vista);
        // var_dump("students = ".count($students));
        // exit(0);
    }

    /**
     * Función para guardar la VALORACIÓN de un RETO
     */
    public function storeSprint() {
        // Comprobación de permisos
        $this->checkPermission();

        $challenge_id = $this->input->post('id');
        $students_ids = $this->input->post('students_ids');
//         var_dump("students_ids=".json_encode($students_ids[0]));
//         exit(0);
        $current_sprint = $this->input->post('current_sprint');
        $current_team = $this->input->post('current_team');
        $num_students = $this->input->post('num_students');
        $max_value = $this->input->post('max_value');
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $comentarios = $this->input->post('comments'); // Array con los comentarios para los alumnos.
        //$students_ids = $this->input->post('students_ids'); // Array con los identificadores de los alumnos.
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        // var_dump("teacher_id=".$teacher_id);
        // var_dump("students_id=".$students_ids[0]);
        // exit(0);


        $assessment_data = array();
        $teacher_assessment_data = array();
        $assessment_data['current_sprint'] = $current_sprint;

        // Número de estudiantes
        for($std=0; $std<$num_students; $std++) {
            // Numero de filas
            //var_dump('Alumno['.$std.'] de '.$num_students.'<br>');
            // Recuperamos los datos de la evaluación (id, student_id, learning_outcomes, comments,...)
            $assessments = $this->challenges_assessments_model->getAssessments($challenge_id, $current_sprint, $students_ids[$std]); // array con todos los datos
            // var_dump("assessments=".json_encode($assessments)." <br> Cantidad=".count($assessments)); // "assessments=["6","7","8","9"]"
            // exit(0);
            /* $assessment_id = $assessments[$std]['id'];
            $student_id = $assessments[$std]['student_id'];
            $outcomes_id = $assessments[$std]['learning_outcomes_id']; */
            $assessment_id = $assessments['id'];
            $student_id = $assessments['student_id'];
            $outcomes_id = $assessments['learning_outcomes_id'];
            //var_dump($assessment_id.",".$student_id.",".$outcomes_id."<br>rows=".$rows."<br>");

            for($i=0; $i<$rows; $i++) {
                $option = $this->input->post('criteria-'.$std.$i);
                // $option = substr($option,8);
                // Elimnar "criteria-" de la opción para quedarnos con el valor
                $option = substr(strrchr($option,"-"),1);
                $criteria_row[] = $option;
                $teachers_comments = array($teacher_id => $comentarios[$std]);
                $teacher_assessment_data = array($teacher_id => $criteria_row);
            }
//             var_dump("options = ".json_encode($option).", criteria=".json_encode($criteria_row)."<br>");
//             exit(0);
            // Modelo: modificar un registro para añadir la nota del estudiante y los comentarios del profesor.
            if(!$this->challenges_assessments_model->saveTeacherAssessment($assessment_id, $student_id, $outcomes_id, $teacher_assessment_data, $teachers_comments)) {
                $this->output->set_status_header(500);
            }
            // var_dump("Los datos para guardar al estudiante(".$i.") son=".json_encode($assessment_data));
//             var_dump("Los resultados de un estudiante son: ". json_encode($criteria_row));
            $criteria_row = array();
//             exit(0);

        }
        //var_dump($assessment_data );
        //exit(0);

        if ($current_team < $this->challenges_model->getChallengeTeams($challenge_id)-1) {
            redirect(base_url('teachers/challenges_assessments/assessSprint/'
                              .$challenge_id.'/'
                              .$current_sprint.'/'
                              .($current_team+1))); // Vamos a valorar al siguiente equipo.
        } else  {
            // Redirección a la vista principal
            $this->session->set_flashdata('success_msg', 'Valoración <strong>guardada correctamente</strong>.');
            redirect(base_url('teachers/challenges'));
        }
    }


    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE), 
            // 'nav' => $this->load->view('teachers/nav', '', TRUE), 
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );
        $this->load->view('teachers/challenges', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');


        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        //var_dump("formSubmit = ".$formSubmit);
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/challenges'), 'refresh');
        }
    }
}