<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technical_skills extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','pagination'));

        // cargamos el helper con las reglas de los retos
        $this->load->helper(array('technical_skills_rules'));
        // Cargar los modelos necesarios
        $this->load->model('technical_skills_model');
        $this->load->model('technical_skills_assessments_model');
        $this->load->model('challenges_model');

        // $this->load->model('challenges_assessments_model');
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');
        $this->load->model('students_model');
        $this->load->model('users_model');
    }

    /**
     * Función principal por defecto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // var_dump("school-y = ".$this->session->school_year);
        // exit(0);
        $current_school_year = $this->session->school_year;

        $teacher_id = '';
        if ($this->session->role === 'teacher') {
            
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            // Profesor Daniel Pastor Peidro
            // $teacher_id = '30'; 
            $data = $this->technical_skills_model->getAllTechnicalSkills($teacher_id, $current_school_year, '', '');

        } else {
            $data = $this->technical_skills_model->getAllTechnicalSkills('', $current_school_year);

        }

        // var_dump("<br> data = ".json_encode($data));
        // exit(0);

        // // Para hacer la paginación
        // $config['base_url']= base_url('teachers/technical_skills/index');
        // // Elementos por página
        // $config['per_page']= 10000;
        // $config['total_rows']= count($data);

        // // Paginación con TailwindCSS
        // $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        // $config['full_tag_close']= '</ul></div>';
        // $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['num_tag_close']= '</li>';
        // $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teal-500 border-r border-grey-light px-3 py-2 active">';
        // $config['cur_tag_close']= '</li>';
        // $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['next_tag_close']= '</li>';
        // $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['prev_tag_close']= '</li>';
        // $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['first_tag_close']= '</li>';
        // $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        // $config['last_tag_close']= '</li>';

        // $this->pagination->initialize($config);
        // // $page = $this->technical_skills_model->get_Paginate($teacher_id, $config['per_page'], $offset, $current_school_year);
        // $page = $this->technical_skills_model->getAllTechnicalSkills($teacher_id, $current_school_year, $config['per_page'], $offset);

        // var_dump("index(page)=".json_encode($page));
        // exit(0);

        // $vista = $this->load->view('teachers/technical_skills_show', array('data' => $page, 'school_year' => $current_school_year), TRUE);
        $vista = $this->load->view('teachers/technical_skills_show', array(
            'data' => $data, 
            'school_year' => $current_school_year,
            'teacher_id' => $teacher_id, 
        ), TRUE);
        $this->getTemplate($vista);

    }

    
    /**
     * Función para crear un Resultado de Aprendizaje en la BBDD
     */
    public function create() {
        // Comprobación de permisos
        $this->checkPermission();

        $current_school_year = $this->session->school_year;
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        $challenges = array();

        // Recuperar el identificador del profesor que crea el RA.
        if ($this->session->role === 'teacher') {
            $challenges = $this->challenges_model->getChallenges($teacher_id, $current_school_year);

        } else {
            $challenges = $this->challenges_model->getChallenges('', $current_school_year);
        }
        
        // Recuperar las rúbricas almacenadas.
        $rubrics = $this->rubrics_model->getMyRubrics($teacher_id);
        // var_dump("<br> Las rúbricas son: ".json_encode($rubrics));
        // exit(0);

        $vista = $this->load->view('teachers/technical_skills_create', array(
            't_owner' => $teacher_id,
            'rubrics' => $rubrics,
            'challenges' => $challenges,
            'current_school_year' => $current_school_year,
        ) , TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para guardar los datos de un Resultado de Aprendizaje (RA)
     */
    public function store() {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos del formulario
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $school_year = $this->input->post('school_year');
        // $teaching_levels = $this->input->post('teaching_levels');
        $qualification = $this->input->post('qualification');
        $rubric_id = $this->input->post('rubric_id');
        $challenges_id = $this->input->post('challenges_id');
        $t_owner = $this->teachers_model->getTeacherId($this->session->id);

        // var_dump("<br>create(teaching_levels=".json_encode($teaching_levels));
        // var_dump("<br>create(qualification=".json_encode($qualification));
        // var_dump("<br>create(rubric_id=".json_encode($rubric_id));
        // var_dump("<br>create(teaching_levels=".json_encode($teaching_levels));
        // var_dump("<br>create(challenges_id=".json_encode($challenges_id));

        // exit(0);

        // Comprobamos los errors del formuario
        $this->form_validation->set_rules(getCreateTechnicalSkillsRules());
        if( $this->form_validation->run() === FALSE ) {
            // ERRORES
            $this->output->set_status_header(400);
            $vista = $this->load->view('teachers/technical_skills_create','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // if(empty($teaching_levels)) {
        //     // ERRORES
        //     $this->output->set_status_header(400);
        //     $this->session->set_flashdata('success_msg', '');
        //     $this->session->set_flashdata('error_msg', 'No s\'ha seleccionat cap curs. Tria un nivell');
        //     $vista = $this->load->view('teachers/technical_skills_create','', TRUE);
        //     $this->getTemplate($vista);
        //     return;
        // }

        // quizás no queremos asignar la tarea a ningún reto todavía.

        // ******************************************************************************************
        // if(empty($challenges_id)) {
        //     // ERRORES
        //     $this->output->set_status_header(400);
        //     $this->session->set_flashdata('success_msg', '');
        //     $this->session->set_flashdata('error_msg', 'No s\'ha seleccionat cap Repte. Tria un!');
        //     $vista = $this->load->view('teachers/technical_skills_create','', TRUE);
        //     $this->getTemplate($vista);
        //     return;
        // }
        // ******************************************************************************************
        // exit(0);

        // Si se ha marcado 'Nota numerica' se borra la rúbrica, sinó se deja tal cual.
        $has_rubric = 1;
        if($qualification == 'mark') {
            $has_rubric = 0;
            $rubric_id = null;
        }

        // Llamar al modelo y meter los datos.
        $ra = array (
            'name' => $name,
            'description' => $description,
            'school_year' => $school_year,
            't_owner' => $t_owner,
            'has_rubric' => $has_rubric,
            'rubric_id' => $rubric_id,
            'challenges_in' => $challenges_id,
        );

        // Creamos el RA en la BBDD
        $ra_id = $this->technical_skills_model->create($ra);

        // Comprobamos si hay error.
        if (!array_key_exists('technical_skills_id', $ra_id)){
            //$data['error_msg'] = 'Ocurrió un error al introducir los datos del usuario';
            //$this->load->view('dashboard_admin', $user_id);
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', 'Ha <strong>ocurrido un error</strong> al guardar el Resultado de Aprendizaje "'.$name.'"');
            $vista = $this->load->view('teachers/technical_skills_create','', TRUE);
            $this->getTemplate($vista);
            return;

        }

        // Asignamos el RA a los Retos seleccionados
        // Dejamos los ids solos.
        // $challenges = implode(",",$challenges_id);
        // var_dump("<br>los retos a actualizar son: ".json_encode($challenges_id));
        // var_dump("<br>el identificador del RA es: ".$ra_id['technical_skills_id']);
        // exit(0);

        // Actualizamos los RAs de cada uno de los retos.
        // Si no están vacíos los retos a los que asignar el RA.
        if(!empty($challenges_id)) {

            for($i=0; $i<count($challenges_id); $i++) {
                // var_dump("<br> el identificador es: ".$challenges_id[$i]);
                // if(!$this->challenges_model->updateTechnicalSkills($challenges_id[$i], $ra_id['technical_skills_id'])) {
                if(!$this->challenges_model->updateTechnicalSkills($challenges_id[$i], $ra_id['technical_skills_id'])) {
                    $this->output->set_status_header(500);
                }   
            }
        }
        // exit(0);

        redirect(base_url('teachers/technical_skills'), 'refresh');
        
    }
    
    /**
     * Función para editar un RA de la base de datos
     */
    public function edit($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();


        // Obtenemos la información de un Reto de Aprendizaje
        $one_skill = $this->technical_skills_model->getOneTechnicalSkill($id);
        // var_dump("<br>one_technical_skill = ".json_encode($one_skill)."<br><br>");
        // exit(0);

        // Recuperar el identificador del profesor que crea el RA.
        $t_owner = $this->teachers_model->getTeacherId($this->session->id);
        
        // Recuperar las rúbricas almacenadas.
        $rubrics = $this->rubrics_model->getMyRubrics($t_owner);
        // var_dump("<br>edit(rubrics)=".json_encode($rubrics));
        // exit(0);

        $vista = $this->load->view('teachers/technical_skills_edit', array (
            'one_skill' => $one_skill,
            'rubrics' => $rubrics,
            'id' => $id,
            't_owner' => $t_owner,
        ), TRUE);

        // var_dump("<br>technical_skill_levels=".$one_skill['teaching_levels']);
        // exit(0);
        $this->getTemplate($vista);
    }

    /**
     * Función para actualizar los datos de una rúbrica
     */
    public function update() {
        // Comprobación de permisos
        $this->checkPermission();

        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') != 'POST')
            show_404();

        // Capturamos los datos del formulario
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $school_year = $this->input->post('school_year');
        // $teaching_levels = $this->input->post('teaching_levels');
        $qualification = $this->input->post('qualification');
        $rubric_id = $this->input->post('rubric_id');
        $t_owner = $this->input->post('t_owner');
        $technical_skills_id = $this->input->post('technical_skills_id');

        // var_dump("<br>UPDATE DATOS: <br>");
        // var_dump("<br>name=".$name."<br>");
        // var_dump("<br>description=".$description."<br>");
        // var_dump("<br>school_year=".$school_year."<br>");
        // var_dump("<br>teaching_levels=".json_encode($teaching_levels)."<br>");
        // var_dump("<br>qualification=".$qualification."<br>");
        // var_dump("<br>rubric_id=".$rubric_id."<br>");
        // var_dump("<br>t_owner=".$t_owner."<br>");
        // exit(0);

        // Comprobamos los errors del formuario
        $this->form_validation->set_rules(getUpdateTechnicalSkillsRules());
        if( $this->form_validation->run() === FALSE ) {
            // ERRORES
            $this->output->set_status_header(400);

            // Obtenemos la información de un Reto de Aprendizaje
            $one_skill = $this->technical_skills_model->getOneTechnicalSkill($technical_skills_id);
            // Recuperar las rúbricas almacenadas.
            $rubrics = $this->rubrics_model->getMyRubrics($t_owner);
            $vista = $this->load->view('teachers/technical_skills_edit', array (
                'one_skill' => $one_skill,
                'rubrics' => $rubrics,
                'id' => $technical_skills_id,
                't_owner' => $t_owner,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // if(empty($teaching_levels)) {
        //     // ERRORES
        //     $this->output->set_status_header(400);
        //     $this->session->set_flashdata('success_msg', '');
        //     $this->session->set_flashdata('error_msg', 'No s\'ha seleccionat cap curs. Tria un nivell');

        //     // Obtenemos la información de un Reto de Aprendizaje
        //     $one_skill = $this->technical_skills_model->getOneTechnicalSkill($technical_skills_id);
        //     // Recuperar las rúbricas almacenadas.
        //     $rubrics = $this->rubrics_model->getMyRubrics($t_owner);
        //     $vista = $this->load->view('teachers/technical_skills_edit', array (
        //         'one_skill' => $one_skill,
        //         'rubrics' => $rubrics,
        //         'id' => $technical_skills_id,
        //         't_owner' => $t_owner,
        //     ), TRUE);
        //     $this->getTemplate($vista);
        //     return;
        // }

        // Si se ha marcado 'Nota numerica' se borra la rúbrica, sinó se deja tal cual.
        $has_rubric = 1;
        if($qualification === 'mark') {
            $has_rubric = 0;
            $rubric_id = null;
        }

        // Llamar al modelo y meter los datos.
        $ra = array (
            'name' => $name,
            'description' => $description,
            'school_year' => $school_year,
            't_owner' => $t_owner,
            'has_rubric' => $has_rubric,
            'rubric_id' => $rubric_id,
            // 'teaching_levels' => $teaching_levels,
        );

        // Creamos el RA en la BBDD
        if(!$this->technical_skills_model->update($technical_skills_id, $ra)) {
            $this->output->set_status_header(500);
        }

        // Redirección a la vista de Resultados de Aprendizaje
        $this->session->set_flashdata('success_msg', 'El Resultado de Aprendizaje "'.$name.'" ha sido <strong>actualizado correctamente.</strong>');
        $this->session->set_flashdata('error_msg', '');
        redirect(base_url('teachers/technical_skills'), 'refresh');

        
    }
    
    /**
     * Borrar un Resultado de Aprendizaje de la BBDD
     */
    public function delete($id) {
        // Comprobación de permisos
        $this->checkPermission();
    
        // *****************
        //  ELIMINAR EL RESULTADO DE APRENDIZAJE DEL RETO ASIGNADO.
        // *****************

        // REcuperar el Challenge_ID donde está este reto.
        $challenges = $this->technical_skills_model->getAllChallengesOfTechnicalSkills($id);
        // var_dump("<br>los Retos asociados al TS(".$id.") son=".json_encode($challenges));
        // exit(0);

        if(!empty($challenges)) {
            // Obtenemos todos los retos en los que está este RA y borramos los que están vacíos.
            $challenges_in = array_filter(explode(",", $challenges));
            // var_dump("<br>Los retos son: ".json_encode($challenges_in));

            // Tenemos que quitar el RA ($id) para cada reto.
            foreach($challenges_in as $item) {
                // var_dump("<br> un reto es: ".$item);
                $technical_skills = $this->challenges_model->getTechnicalSkills($item);
                // var_dump("<br>all TS = ".json_encode($technical_skills));

                if(!empty($technical_skills)) {
                    $to_delete = array($id => '');
                    // var_dump("<br>to_delete=".json_encode($to_delete));
                    // array_diff_key elimina un key+value si lo encuentra en "to_delete".
                    $result_technical_skills=array_diff_key($technical_skills,$to_delete);
                    // var_dump("<br>result de UNSET, TS = ".json_encode($result_technical_skills));
    
                    $data['technical_skills'] = json_encode($result_technical_skills);
    
                    if(!$this->challenges_model->update($item, $data )) {
                        $this->output->set_status_header(500);
                        return;
                    }
                    // var_dump("<BR><BR>Reto Actualizado!");
                }
            }
        }
        // } else {
        //     // No hay que hacer nada, porqué no está asociado a ningún reto.
        // }
        // exit(0);
        
        // *****************
        // BORRAR LAS REFERENCIAS AL RA EN LAS ASIGNACIONES
        // *****************
        if(!$this->technical_skills_assessments_model->delete($id)) {
            // $this->output->set_status_header(500);
            // return;
            // var_dump("<br>NO HABÍA ASSESSMENTS DEL TS(".$id.") QUE BORRAR");
        }


        // exit(0);
        

        if(!$this->technical_skills_model->delete($id)){
            // ERROR No se ha podido borrar el usuario
            // $this->output->set_status_header(500);
            show_500();
        } else {
            // exit(0);
            // Se ha borrado con éxito. Volver a la pantalla de los RAs
            redirect(base_url('teachers/technical_skills'), 'refresh');
        }
    }

    // /**
    //  * Función para obtener el nombre de una RA de la BBDD
    //  */
    // public function getTechnicalSkillsName($technical_skills_id) {


    // }

    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE),          
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );
        // var_dump("<br> en GetTemplate(data)=".json_encode($data));
        // var_dump("nav -> schoolyear = ".$this->session->school_year);
        // exit(0);
        $this->load->view('teachers/technical_skills', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission($school_year_changed = false) {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');

        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student' && !$school_year_changed)
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/technical_skills'), 'refresh');
        }
    }
}
