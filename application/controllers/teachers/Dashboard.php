<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de sesión:
        $this->load->library(array('form_validation','session','pagination'));
        // Helpers
        $this->load->helper(array('teachers_rules'));
        // Cargar los modelos de Proyectos y Retos
        // $this->load->model('projects_model');
        $this->load->model('challenges_model');
        $this->load->model('users_model');
        $this->load->model('teachers_model');
        

    }
    /**
     * Función principal por defecto
     */
    public function index() {
        // Comprobación de permisos
        $this->checkPermission();

        // Panel de control de profesores
        // Proyectos y/o Retos
        if ($this->session->role === 'teacher') {
            $teacher = $this->teachers_model->getTeacherId($this->session->id);
            $teacher_id=$teacher;
            $hasChallenges = $this->teachers_model->isTeacherEnrolledInChallenge($teacher_id);
            if (!empty($hasChallenges)) {
                $data = array (
                    'challenges' => 'si',
                    'projects' => 'no',

                );
                redirect(base_url('teachers/challenges'), 'refresh');

            } else {
                $data = array (
                    'challenges' => 'no',
                    'projects' => 'no',
                );
                // De momento no hace falta que no tenemos proyectos
                // redirect(base_url('teachers/projects'));

                redirect(base_url('teachers/challenges'));
            }

        } else { // es un administrador
            $data = array (
                'challenges' => 'si',
                'projects' => 'si',
            );
            redirect(base_url('admin/dashboard'));


        }

        $vista = $this->load->view('teachers/dashboard', $data, TRUE);
        $this->getTemplate($vista);
    }

   /**
     * Función para cambiar de contraseña
     */
    public function editPassword() {

        $current_school_year = $this->session->school_year;

        $vista = $this->load->view('teachers/change_password', array(
            'id' => $this->session->id,
            'school_year' => $current_school_year,
        ), TRUE);
        $this->getTemplate($vista);
    }
    /**
     * Función para llamar a la vista de cambio de datos personales
     */
    public function changePassword() {
        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/dashboard'), 'refresh');
        }
        
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') !== 'POST')
            show_404();

        // Capturamos los datos del formulario
        $id = $this->input->post('id'); // Oculto
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');

        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $this->form_validation->set_rules(getChangePasswordTeacherRules());

        if ($this->form_validation->run() === FALSE) {
            // Errores de validación.
            $errors = array (
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
            );
            $vista = $this->load->view('teachers/change_password','', TRUE);
            $this->getTemplate($vista);
            return;
        } 

        if (empty($password) or empty($password_c)) {
            $this->session->set_flashdata('error_msg', 'Las contraseñas no pueden estar vacía.');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('teachers/dashboard', '', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        if ($password !== $password_c) {
            $this->session->set_flashdata('error_msg', 'Las contraseñas no coinciden.');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('teachers/dashboard', '', TRUE);
            $this->getTemplate($vista);
            return;
        } 

        // Actualizamos los datos de usuario
        if(!$this->users_model->updatePassword($id, $password)) {
            $this->session->set_flashdata('error_msg', 'Ocurrió un <strong>error al cambiar la contraseña</strong>.');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('teachers/dashboard', '', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // Se ha modificado correctamente la contraseña
        
        // Cambiamos su estado en la sesión y en la base de datos.
        $this->session->unset_userdata('status');
        $this->session->set_userdata('status', 'active');
        $this->users_model->saveStatus($id);

        // volvemos al dashboard de estudiantes
        $this->session->set_flashdata('error_msg', '');
        $this->session->set_flashdata('success_msg', 'Contraseña modificada <strong>correctamente!</strong>');
        redirect(base_url('teachers/dashboard'), 'refresh');

    }


    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE), 
            'nav' => $this->load->view('teachers/nav', '', TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );

        $this->load->view('dashboard_teachers', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');

        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/dashboard'), 'refresh');
        }
    }
}