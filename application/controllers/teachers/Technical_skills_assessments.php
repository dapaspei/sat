<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technical_skills_assessments extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        // $this->load->library(array('form_validation'));

        // cargamos el helper con las reglas de los retos
        //$this->load->helper(array('challenges_rules'));
        // Cargar los modelos necesarios
        $this->load->model('challenges_model');
        $this->load->model('technical_skills_model');
        $this->load->model('technical_skills_assessments_model');
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');
        $this->load->model('students_model');
    }

    /**
     * Función principal por defecto para mostar un listado con las RAs 
     * asociadas a un reto (y su evaluación si la hubiera)
     * @param offset Numero de página
     */
    public function index($challenge_id = '') {
        // Comprobación de permisos
        $this->checkPermission();


        // // Obtenemos la información del RETO
        // $challenge = $this->challenges_model->getChallenge($id);
        // var_dump($challenge);
        // exit(0);
        
    }
 
    /**
     * Función para crear una Evaluación Técnica
     */
    public function create($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // Obtenemos la información del RETO
        $challenge = $this->challenges_model->getChallenge($id);
        // var_dump($challenge);
        // exit(0);

        // Buscamos los identificadores de los RAs que se puedan evaluar.
        // $ts_ids = $this->challenges_model->getTechnicalSkills($id);
        $ts_ids = $this->challenges_model->getTechnicalSkillsIds($id);
        // var_dump("<br>ts_ids = ".json_encode($ts_ids));
        // exit(0);

        // SI NO HAY ASIGNADO TODAVÍA NINGÚN RA AL RETO, ENVIAMOS A LA VISTA DE ASIGNAR RAs
        if(empty($ts_ids)){
            redirect(base_url('teachers/technical_skills/create'));
        }

        // $technical_skills_ids = explode(",",$ts_ids);
        // var_dump($technical_skills_ids);
        // exit(0);

        // $technical_skills_toassess = array_keys($ts_ids);
        // foreach ($technical_skills_ids as $item)
        //     $technical_skills_toassess[] = $this->technical_skills_model->getOneTechnicalSkill($item);
        
        // var_dump($technical_skills_toassess);
        // var_dump("<br> count(ts_ids=".count($technical_skills_toassess));
        // exit(0);

        // Buscamos si tiene creadas los registros para las calificaciones.
        // $hasAssessments = $this->technical_skills_assessments_model->isTechnicalSkillInAssessment($id);
        $hasAssessments = $this->technical_skills_assessments_model->isTechnicalSkillInAssessment($id,$ts_ids);
        // var_dump("<br>hasAssessments=<br>");
        // print_r($hasAssessments);
        // exit(0);
        
        // Comprobamos que no tenga creado todavía ninguna evaluación, sinó, la creamos.
        if(!$hasAssessments) {
            // Buscar los alumnos pertenecientes al reto
            $students = $this->challenges_model->getChallengeStudents($id);
            // var_dump("Students=".json_encode($students));
            // Buscar los profesores asociados al reto
            $teachers = $this->challenges_model->getChallengeTeachers($id);
            // var_dump("<br>t=".json_encode($teachers));
            // exit(0);
            // Con los estudiantes, creamos sus registros de evaluación en la BBDD
            // if(!$this->technical_skills_assessments_model->start($id, $challenge, $students, $teachers, $ts_ids))
            if(!$this->technical_skills_assessments_model->start($id, $students, $teachers, $ts_ids))
                $this->output->set_status_header(500);

            // var_dump("<BR>REGISTROS INICIALES CREADOS<BR>");
        }
        for ($i=0; $i<count($ts_ids); $i++){
            $technical_skills_toassess[] = $this->technical_skills_model->getOneTechnicalSkill($ts_ids[$i]);
        }
        // var_dump("technical_skills_to_assess=".json_encode($technical_skills_toassess));
        // exit(0);
        $vista = $this->load->view('teachers/technical_skills_assessments_create', array(
            'challenge' => $challenge,
            'current_team' => 0,
            'technical_skills' => $technical_skills_toassess,
            'school_year' => $this->session->school_year, 
        ), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para VALORAR un RA
     */
    public function assess($challenge_id = '', $technical_skills_id = '', $has_rubric = 0, $current_team = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // var_dump("<br> ch_id=".$challenge_id."<br> tsk_id=".$technical_skills_id."<br> has_rubric=".$has_rubric."<br>current_team=".$current_team);
        // exit(0);
 
        // Buscar los alumnos pertenecientes al equipo + reto
        $students = $this->challenges_model->getChallengeStudents($challenge_id, $current_team);

        // var_dump("<br>students =<br>".json_encode($students));
        // exit(0);

        // Obtenemos el identificador del profesor.
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);

        $assess_data = array();
        $assess_comments = array();
        $students_ids = array();
        $all_assess_comments = array();
        // Para cada estudiante comprobamos si tiene la nota asociada
        for($i=0; $i<count($students); $i++) {

            // Guardamos los identificadores de los estudiantes
            $students_ids [] = $students[$i]['id'];

            $AllTeacherAssessData = $this->technical_skills_assessments_model->getTeacherAssessData($technical_skills_id, $challenge_id, $students[$i]['id'], $has_rubric);
            $AllTeacherCommentsData = $this->technical_skills_assessments_model->getTeacherCommentsData($technical_skills_id, $challenge_id, $students[$i]['id']);
            // var_dump("<br>AllTeacherAssessData = ".json_encode($AllTeacherAssessData));
            // var_dump("<br>AllTeacherCommentsData=".json_encode($AllTeacherCommentsData));
            // exit(0);

            if(!empty($AllTeacherAssessData)) {
                // Comprobar si son los datos del profesor
                $all_assess_data = array_values($AllTeacherAssessData);
                // var_dump("<br>array_values_data = ".json_encode($all_assess_data));
                foreach($all_assess_data as $data) {
                    $decoded_data = json_decode($data, TRUE);
                    // var_dump("<br>decoded_data = ".json_encode($decoded_data));

                    if(!empty($decoded_data[$teacher_id])) {
                        $assess_data[] = $decoded_data[$teacher_id];
                    } else {
                        $assess_data[] = '';
                    }
                }

            }

            if(!empty($AllTeacherCommentsData))
                // $assess_comments[] = implode(array_values($AllTeacherCommentsData));
                $all_assess_comments = array_values($AllTeacherCommentsData);
                foreach($all_assess_comments as $comment) {
                    $decoded_comment = json_decode($comment, TRUE);
                    if(!empty($decoded_comment[$teacher_id]))
                        $assess_comments[] = $decoded_comment[$teacher_id];
                    else 
                        $assess_comments[] = '';
                }

        }
        // var_dump("<br><br><br>assess_data=".json_encode($assess_data)."<br>comennts=".json_encode($assess_comments));
        // exit(0);
        
        if($has_rubric) {
            // Obtenemos los datos de la rúbrica asociada a este RA.
            $rubric_id = $this->technical_skills_model->getTechnicalSkillsRubric($technical_skills_id);
            $rubric = $this->rubrics_model->getRubric($rubric_id);
            $criteria = $this->rubrics_model->getRubricCriteria($rubric_id);
            // var_dump("La rúbrica es:".$rubric['name']);
            // exit(0);
            $vista = $this->load->view('teachers/technical_skills_assessments_rubric', array(
                'technical_skills_id' => $technical_skills_id,
                'challenge_id' => $challenge_id,
                'num_students' => count($students),
                'students' => $students,
                'students_ids' => $students_ids,
                'rubric' => $rubric,
                'criteria' => $criteria,
                'assessments' => $assess_data,
                'comments' => $assess_comments,
                'current_team' => $current_team,
                'school_year' => $this->session->school_year, 
    
            ), TRUE);
        } else {
            $vista = $this->load->view('teachers/technical_skills_assessments_mark', array(
                'technical_skills_id' => $technical_skills_id,
                'challenge_id' => $challenge_id,
                'num_students' => count($students),
                'students' => $students,
                'students_ids' => $students_ids,
                'assessments' => $assess_data,
                'comments' => $assess_comments,
                'current_team' => $current_team,
                'school_year' => $this->session->school_year, 
    
            ), TRUE);
        }
        $this->getTemplate($vista);
        
    }

    /**
     * Función para guardar la VALORACIÓN de un RA con Nota Numérica
     */
    public function storeMark() {
        // Comprobación de permisos
        $this->checkPermission();

        $challenge_id = $this->input->post('challenge_id');
        $technical_skills_id = $this->input->post('technical_skills_id');
        $current_team = $this->input->post('current_team');
        $num_students = $this->input->post('num_students');
        $students_ids = $this->input->post('students_ids');
        $comentarios = $this->input->post('comments'); // Array con los comentarios para los alumnos.
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        // var_dump($comentarios);

        // Recogemos las notas del alumnado
        $mark_values = array();
        for($i=0; $i<$num_students; $i++)
            $mark_values[] = $this->input->post('mark_value-'.$i);

        // var_dump("<br> Num_students=".$num_students."<br> Students_ids=".json_encode($students_ids)."<br>ch_id=".$challenge_id."<br>tsk_id=".$technical_skills_id."<br>mark_values=".json_encode($mark_values));
        // var_dump("teacher_id=".$teacher_id);
        // var_dump("students_id=".$students_ids[0]);
        // exit(0);

        $teacher_assessment_data = array();
        $teacher_comments = array();

        // Guardamos la nota para cada alumno.
        for($std=0; $std<$num_students; $std++) {

            $teacher_assessment_data = array($teacher_id => $mark_values[$std]);
            $teacher_comments = array($teacher_id => $comentarios[$std]);
            // var_dump("<br>Nota: ".json_encode($teacher_assessment_data)."<br>Comentarios: ".json_encode($teacher_comments));
            // var_dump($teacher_comments);
            // exit(0);            

            // Modelo: modificar un registro para añadir la Calificación y los Comentarios del profesor.
            if(!$this->technical_skills_assessments_model->saveTeacherMarkAssessment($technical_skills_id, $challenge_id, $students_ids[$std], $teacher_id, $teacher_assessment_data, $teacher_comments)) {
                $this->output->set_status_header(500);
            }

        }
        // var_dump("<br>Notas: ".json_encode($teacher_assessment_data)."<br>Comentarios: ".json_encode($teacher_comments));
        // exit(0);

        if ($current_team < $this->challenges_model->getChallengeTeams($challenge_id)-1) {
            redirect(base_url('teachers/technical_skills_assessments/assess/'
                              .$challenge_id.'/'
                              .$technical_skills_id.'/'
                              .'0/' // No tiene rúbrica, ya lo sabemos ;)
                              .($current_team+1))); // Vamos a valorar al siguiente equipo.

        } else  {
            // Redirección a la vista principal
            $this->session->set_flashdata('success_msg', 'Valoración <strong>guardada correctamente</strong>.');
            redirect(base_url('teachers/challenges'), 'refresh');
            // $vista = $this->load->view('teachers/challenges/index', '', TRUE);
            // $this->getTemplate($vista);
        }
    }

        /**
     * Función para guardar la VALORACIÓN de un RA con Rúbrica
     */
    public function storeRubric() {
        // Comprobación de permisos
        $this->checkPermission();

        $challenge_id = $this->input->post('id');
        $students_ids = $this->input->post('students_ids');
//         var_dump("students_ids=".json_encode($students_ids[0]));
//         exit(0);
        $current_sprint = $this->input->post('current_sprint');
        $current_team = $this->input->post('current_team');
        $num_students = $this->input->post('num_students');
        $max_value = $this->input->post('max_value');
        $cols = $this->input->post('cols');
        $rows = $this->input->post('rows');
        $comentarios = $this->input->post('comments'); // Array con los comentarios para los alumnos.
        //$students_ids = $this->input->post('students_ids'); // Array con los identificadores de los alumnos.
        $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
        // var_dump("teacher_id=".$teacher_id);
        // var_dump("students_id=".$students_ids[0]);
        // exit(0);


        $assessment_data = array();
        $teacher_assessment_data = array();
        $assessment_data['current_sprint'] = $current_sprint;

        // Número de estudiantes
        for($std=0; $std<$num_students; $std++) {
            // Numero de filas
            //var_dump('Alumno['.$std.'] de '.$num_students.'<br>');
            // Recuperamos los datos de la evaluación (id, student_id, learning_outcomes, comments,...)
            $assessments = $this->challenges_assessments_model->getAssessments($challenge_id, $current_sprint, $students_ids[$std]); // array con todos los datos
            // var_dump("assessments=".json_encode($assessments)." <br> Cantidad=".count($assessments)); // "assessments=["6","7","8","9"]"
            // exit(0);
            /* $assessment_id = $assessments[$std]['id'];
            $student_id = $assessments[$std]['student_id'];
            $outcomes_id = $assessments[$std]['learning_outcomes_id']; */
            $assessment_id = $assessments['id'];
            $student_id = $assessments['student_id'];
            $outcomes_id = $assessments['learning_outcomes_id'];
            //var_dump($assessment_id.",".$student_id.",".$outcomes_id."<br>rows=".$rows."<br>");

            for($i=0; $i<$rows; $i++) {
                $option = $this->input->post('criteria-'.$std.$i);
                // $option = substr($option,8);
                // Elimnar "criteria-" de la opción para quedarnos con el valor
                $option = substr(strrchr($option,"-"),1);
                $criteria_row[] = $option;
                $teachers_comments = array($teacher_id => $comentarios[$std]);
                $teacher_assessment_data = array($teacher_id => $criteria_row);
            }
//             var_dump("options = ".json_encode($option).", criteria=".json_encode($criteria_row)."<br>");
//             exit(0);
            // Modelo: modificar un registro para añadir la nota del estudiante y los comentarios del profesor.
            if(!$this->challenges_assessments_model->saveTeacherAssessment($assessment_id, $student_id, $outcomes_id, $teacher_assessment_data, $teachers_comments)) {
                $this->output->set_status_header(500);
            }
            // var_dump("Los datos para guardar al estudiante(".$i.") son=".json_encode($assessment_data));
//             var_dump("Los resultados de un estudiante son: ". json_encode($criteria_row));
            $criteria_row = array();
//             exit(0);

        }
        //var_dump($assessment_data );
        //exit(0);

        if ($current_team < $this->challenges_model->getChallengeTeams($challenge_id)-1) {
            redirect(base_url('teachers/challenges_assessments/assessSprint/'
                              .$challenge_id.'/'
                              .$current_sprint.'/'
                              .($current_team+1))); // Vamos a valorar al siguiente equipo.
        } else  {
            // Redirección a la vista principal
            $this->session->set_flashdata('success_msg', 'Valoración <strong>guardada correctamente</strong>.');
            redirect(base_url('teachers/challenges'), 'refresh');
        }
    }


    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE), 
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );
        $this->load->view('teachers/challenges', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');


        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        //var_dump("formSubmit = ".$formSubmit);
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/challenges'), 'refresh');
        }
    }
}