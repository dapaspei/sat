<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','pagination'));

        // cargamos el helper con las reglas de los retos
        $this->load->helper(array('challenges_rules'));
        // Cargar los modelos necesarios
        $this->load->model('challenges_model');
        $this->load->model('challenges_assessments_model');
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');
        $this->load->model('students_model');
        $this->load->model('users_model');
        $this->load->model('technical_skills_model'); 
        $this->load->model('technical_skills_assessments_model');
    }

    /**
     * Función principal por defecto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        // var_dump("school-y = ".$this->session->school_year);
        // exit(0);
        $current_school_year = $this->session->school_year;

        $teacher_id = '';
        if ($this->session->role === 'teacher') {
            
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $data = $this->challenges_model->getChallenges($teacher_id, $current_school_year);

        } else {
            $data = $this->challenges_model->getChallenges('', $current_school_year);

        }

        // var_dump("<br> data = ".json_encode($data));
        // exit(0);

        // Para hacer la paginación
        // $config['base_url']= base_url('teachers/challenges/index');
        // // Elementos por página
        // $config['per_page']= 1000;
        // $config['total_rows']= count($data);

        // // Paginación con TailwindCSS
        // $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        // $config['full_tag_close']= '</ul></div>';
        // $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['num_tag_close']= '</li>';
        // $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teal-500 border-r border-grey-light px-3 py-2 active">';
        // $config['cur_tag_close']= '</li>';
        // $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['next_tag_close']= '</li>';
        // $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['prev_tag_close']= '</li>';
        // $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        // $config['first_tag_close']= '</li>';
        // $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        // $config['last_tag_close']= '</li>';

        // $this->pagination->initialize($config);
        // $page = $this->challenges_model->get_Paginate($teacher_id, $config['per_page'], $offset, $current_school_year);
        $page = $this->challenges_model->get_Paginate($teacher_id, '', '', $current_school_year);

        // var_dump("index(page)=".json_encode($page));
        // exit(0);

        $vista = $this->load->view('teachers/challenges_show', array('data' => $page, 'school_year' => $current_school_year), TRUE);
        $this->getTemplate($vista);

    }

    /**
     * Función para cambiar el curso escolar y así mostrar los retos correspondientes a ese curso.
     */
    public function changeSchoolYear($new_date) {
        
        // Comprobación de permisos
        if ($this->session->role === 'student')
            $this->checkPermission(true);
        else 
            $this->checkPermission();


        $this->session->school_year = $new_date;
        // var_dump("<br>La nueva fecha cambiada es:".$this->school_year);
        // exit(0);

        $previous_url = $this->session->userdata('previous_url');
        // var_dump("<br>previous_url=".$previous_url);
        // exit(0);
        if(!empty($previous_url))
            redirect($previous_url);
        else 
            // Volvemos a la vista principal de retos.
            redirect(base_url('teachers/challenges'));
    }

    /**
     * Función para crear un RETO en la BBDD
     */
    public function create() {
        // Comprobación de permisos
        $this->checkPermission();

        $vista = $this->load->view('teachers/challenge_create', array( 'school_year' => $this->session->school_year), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para guardar los datos de un RETO
     */
    public function store() {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos del formulario
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $year = $this->input->post('year');
        $sprints = $this->input->post('sprints');
        $start_date = $this->input->post('start_date');
        $finish_date = $this->input->post('finish_date');
        $subject = $this->input->post('subject');
        $teaching_levels = $this->input->post('teaching_levels');
        $t_owner = $this->teachers_model->getTeacherId($this->session->id);

        // var_dump("create(teaching_levels=".json_encode($teaching_levels));
        // exit(0);
        // Comprobamos los errors del formuario
        $this->form_validation->set_rules(getCreateChallengesRules());
        if( $this->form_validation->run() === FALSE ) {
            // ERRORES
            $this->output->set_status_header(400);
            $vista = $this->load->view('teachers/challenge_create','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        if(empty($teaching_levels)) {
             // ERRORES
             $this->output->set_status_header(400);
             $this->session->set_flashdata('success_msg', '');
             $this->session->set_flashdata('error_msg', 'No s\'ha seleccionat cap curs. Tria un nivell');
             $vista = $this->load->view('teachers/challenge_create','', TRUE);
             $this->getTemplate($vista);
             return;
        }
        // Llamar al modelo y meter los datos.
        $challenge = array (
            'name' => $name,
            'description' => $description,
            'year' => $year,
            'sprints' => $sprints,
            'start_date' => $start_date,
            'finish_date' => $finish_date,
            'subject' => $subject,
            't_owner' => $t_owner,
            'teaching_levels' => $teaching_levels,
        );

        // Creamos el RETO en la BBDD
        $challenge_id = $this->challenges_model->create($challenge);

        // Comprobamos si hay error.
        if (!array_key_exists('challenge_id', $challenge_id)){
            //$data['error_msg'] = 'Ocurrió un error al introducir los datos del usuario';
            //$this->load->view('dashboard_admin', $user_id);
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', 'Ha <strong>ocurrido un error</strong> al almacenar el reto "'.$name.'"');
            $vista = $this->load->view('teachers/challenge_create','', TRUE);
            $this->getTemplate($vista);
            return;

        }
        
        // Buscar los alumnos para pasarlos a la vista
        // $students = $this->students_model->getStudents($teaching_levels);
        // Buscar los alumnos que estén 'enrolados' este curso escolar.
        $students = $this->students_model->getStudents($teaching_levels, $this->session->school_year);
        // print_r("<br>Los estudiantes son: <br".json_encode($students));
        // exit(0);
        if(empty($students)){
            // SI NO HAY NINGÚN ESTUDIANTE ENROLADO ESTE CURSO ESCOLAR
            $this->session->set_flashdata('error_msg', 'No hi ha <strong>alumnat matriculat</strong>. Parla amb el administrador.');
            $this->session->set_flashdata('success_msg', '');

            // $vista = $this->load->view('teachers/challenges', '', TRUE);
            // $this->getTemplate($vista);
            // return;
            redirect(base_url('teachers/challenges'), 'refresh');
        }

        // Buscamos los profesores para pasarlos a la vista.
        if($this->session->role === 'admin') {
            // Si el rol es administrador, recuperamos todos los profesores.
            $teachers = $this->teachers_model->getTeachers();
        } else {
            // Recuperamos los profesores de la BBDD que compartan nivel de docencia con el profesor actual
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            $teacher_teaching_levels = $this->teachers_model->getTeachingLevels($teacher_id);
            // var_dump("teaching_levels=".json_encode($teaching_levels));
            // exit(0);
            // $teachers = array();
            // foreach($teaching_levels as $level){
            //     $teachers[] = $this->teachers_model->getTeachers($level);
            // }
            // $teachers = $teachers[0];
            $teachers = $this->teachers_model->getTeachers($teacher_teaching_levels);
            // var_dump("<br>teachers=<br>".json_encode($teachers[0]));
            // exit(0);

        }
        // Obtenemos las rúbricas a las que está asignado el profesor o es el creador.
        $rubrics = $this->rubrics_model->getMyRubrics($this->teachers_model->getTeacherId($this->session->id));
        // var_dump("store(rubrics)=".json_encode($rubrics));
        // exit(0);

        // Redirección a la vista para seleccionar los alumnos y profesores
        $this->session->set_flashdata('success_msg', 'Reto "'.$name.'" <strong>guardado correctamente</strong>.<br> Asigna los alumnos y profesores al reto.');
        $this->session->set_flashdata('error_msg', '');

        $vista = $this->load->view('teachers/challenge_create_assignments', array (
            'teachers' => $teachers,
            'students' => $students,
            'rubrics' => $rubrics,
            'challenge_id' => $challenge_id['challenge_id'],
            'name' => $name,
            // 'teaching_levels' => $teaching_levels,
        ), TRUE);
        $this->getTemplate($vista);
    }
    /**
     * Función para guardar los profesores y estudiantes
     * asignados a un RETO
     */
    public function storeAssignments() {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos del formulario
        $teachers = $this->input->post('teachers');
        $students = $this->input->post('students');
        $challenge_id = $this->input->post('challenge_id');
        $name = $this->input->post('name');
        $num_teams = $this->input->post('num_teams');
        // $teaching_levels = $this->input->post('teaching_levels');
        $rubric_id = $this->input->post('rubric_id');

        // var_dump("<br>teaching_levels=".json_encode($teaching_levels));

        //if(!isset($teachers) or !isset($students)) {
        // Error si no hay estudiantes seleccionados.
        if(!isset($students)) {
            // ERRORES
            $this->session->set_flashdata('error_msg', 'Falta <strong>seleccionar participantes</strong> del Reto "'.$name.'".');
            $this->session->set_flashdata('success_msg', '');

            // Buscar los alumnos y profesores y pasarlos a la vista
            $teacher_teaching_levels = $this->teachers_model->getTeachingLevels($this->teachers_model->getTeacherId($this->session->id));
            $teachers = $this->teachers_model->getTeachers($teacher_teaching_levels);
            $students = $this->students_model->getStudents($teacher_teaching_levels);
            $rubrics = $this->rubrics_model->getMyRubrics($this->teachers_model->getTeacherId($this->session->id));

            $vista = $this->load->view('teachers/challenge_create_assignments',array (
                'teachers' => $teachers,
                'students' => $students,
                'rubrics' => $rubrics,
                'challenge_id' => $challenge_id,
                'name' => $name,
                'num_teams' => $num_teams,
                'teaching_levels' => $teacher_teaching_levels,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Enrolamos a los estudiantes y profesores al Reto
        foreach ($students as $student_id) {
            if(!$this->students_model->enrolChallenge($student_id, $challenge_id)) {
                $this->output->set_status_header(500);
            }
        }

        // Si no hay ningún profesor asignado, por defecto, se asigna a quien crea el reto
        if(isset($teachers)) {
            // Asignamos todos los profesores seleccionados
            foreach ($teachers as $teacher_id) {
                if(!$this->teachers_model->enrolChallenge($teacher_id, $challenge_id)) {
                    $this->output->set_status_header(500);
                }
            }
        } else {
            $teachers = array ();
        }
        
        // Si es un profesor el que crea el reto, le asignamos por defecto.
        if ($this->session->role === 'teacher') {
            // $teacher = $this->teachers_model->getTeacherId($this->session->id);
            // $teacher_id=$teacher['id'];
            $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
            // Comprobamos si el profesor está en la lista de los profesores asignados al reto.
            if(!in_array($teacher_id, $teachers)) {
                if(!$this->teachers_model->enrolChallenge($teacher_id, $challenge_id)) {
                    // Si hay error
                    $this->output->set_status_header(500);
                }
            }
            // Si ya está asignado, no hacemos nada.
        }
        // Asignamos la Rúbrica al Reto (si no está vacía).
        if(!empty($rubric_id)) {
            if(!$this->challenges_model->updateRubric($challenge_id, $rubric_id)) {
                $this->output->set_status_header(500);
            }
        }

        // Guardamos el número de equipos que hay en el reto.
        if(!$this->challenges_model->update($challenge_id, array('teams' => $num_teams))) {
                $this->output->set_status_header(500);
        }

        //$rubrics = $this->rubrics_model->getRubrics();
        $this->session->set_flashdata('error_msg', '');
        $this->session->set_flashdata('success_msg', 'Asigna las fechas de los sprints.');
        // $vista = $this->load->view('teachers/challenge_define_sprints', array (
        //     'challenge' => $this->challenges_model->getChallenge($challenge_id),
        // ), TRUE);
        $sprints = $this->challenges_model->getChallengeSprints($challenge_id);
        // print_r($sprints);
        $start_date = $this->challenges_model->getChallengeStartDate($challenge_id);
        // print_r($start_date);
        $finish_date = $this->challenges_model->getChallengeFinishDate($challenge_id);
        //print_r($finish_date);
        $challenge = array(
            'id' => $challenge_id,
            'sprints' => $sprints,
            'start_date' => $start_date['start_date'],
            'finish_date' => $finish_date['finish_date'],
        );
        // print_r($challenge);
        // exit(0);
        $vista = $this->load->view('teachers/challenge_define_sprints', array('challenge' => $challenge), TRUE);
        $this->getTemplate($vista);

    }

    /**
     * Función para guardar los profesores y estudiantes
     * asignados a un RETO
     */
    public function storeSprints() {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos del formulario
        $challenge_id = $this->input->post('challenge_id');
        $sprints = $this->input->post('sprints');
        $update_sprints = $this->input->post('update_sprints');

        $sprints_date = array();
        for ($i=1; $i<=$sprints; $i++){
            //$sprints_date[] = $this->input->post('sprints_date'.$i);
            //$sprints_date[] = array('sprints_date'.$i => $this->input->post('sprints_date'.$i));
            $sprints_date['sprints_date'.$i] = $this->input->post('sprints_date'.$i);
        }
        // print_r("sprints_dateX = ".$sprints_date);
        // exit(0);
        // Asignamos las fechas de los Sprints
        if(!$this->challenges_model->updateSprintsDate($challenge_id, $sprints_date)) {
        //if(!$this->challenges_model->updateSprintsDate($challenge_id, array('sprints_date' => $sprints_date))) {
            $this->output->set_status_header(500);
        }

        // Si viene de solo hacer un update de las fechas de los sprints.
        if($update_sprints) {
            redirect(base_url('teachers/challenges'), 'refresh');
        }

        // Obtenemos la información del RETO
        $challenge = $this->challenges_model->getChallenge($challenge_id);
        // Obtenemos la información de los estudiantes que están en el reto
        $students = $this->challenges_model->getChallengeStudents($challenge_id);
        // $students = $this->challenges_model->getChallengeStudents($challenge_id, $this->session->school_year);

        // $students = $this->challenges_model->getChallengeStudents($challenge_id, );
        // Vemos cuántos equipos hay
        $num_teams = $this->challenges_model->getChallengeTeams($challenge_id);

        $this->session->set_flashdata('success_msg', 'Define los grupos de alumnos.');
        $this->session->set_flashdata('error_msg', '');
        $vista = $this->load->view('teachers/challenge_define_teams', array (
            'challenge' => array('id' => $challenge_id),
            'students' => $students,
            'num_teams' => $num_teams,
        ), TRUE);
        $this->getTemplate($vista);
        /* // Redirección a la vista principal
        $this->session->set_flashdata('success_msg', 'Sprints <strong>asigandos correctamente</strong>.');
        redirect(base_url('teachers/challenges'), 'refresh'); */
    }

    /**
     * Función para guardar equipos de los estudiantes
     * asignados a un RETO
     */
    public function storeTeams() {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos los datos del formulario
        $challenge_id = $this->input->post('challenge_id');
        $num_teams = $this->input->post('num_teams');
        $num_students = $this->input->post('num_students');
        //         var_dump("num_teams".$num_teams);

        $all_teams = array();
        $vacios = array();
        $count_students = array();
        for ($i=0; $i<$num_teams; $i++){
            //$sprints_date[] = $this->input->post('sprints_date'.$i);
            //$sprints_date[] = array('sprints_date'.$i => $this->input->post('sprints_date'.$i));
            $all_teams["std_teams".$i] = $this->input->post('std_teams'.$i);
            // var_dump("<br>all_teams\$".$i."=". json_encode($all_teams["std_teams".$i]));
            if (empty($all_teams["std_teams".($i)]))
                $vacios[$i] = true;
            else {
                $vacios[$i] = false;

                // Guardamos todos los identificadores de los estudiantes seleccionados para comprobar si hay duplicados.
                for($j=0; $j<count(array_values($all_teams["std_teams".$i])); $j++) {
                    $count_students[] = $all_teams["std_teams".$i][$j];
                }
            }
        }
        // print_r("<br>count1=".count($count_students));
        // print_r("<br>count unique=".count(array_unique($count_students)));
        // exit(0);

        // Comprobar que hayan seleccionado al menos un estudiante.
        if(in_array(true, $vacios)) {
            $students_selected = array();
            $this->session->set_flashdata('error_msg', '<strong>Error:</strong> no puede haber grupos sin alumnos asignados.');
            $this->session->set_flashdata('success_msg', '');
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            $students_in_teams = $this->challenges_model->getStudentsInTeam($challenge_id);
            $vista = $this->load->view('teachers/challenge_define_teams', array (
                'challenge' => array('id' => $challenge_id),
                'students' => $students_selected,
                'students_in_teams' => $students_in_teams,
                'num_teams' => $num_teams,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }
        // Comprobamos si hay alumnos que estén en más de un grupo.
        if(count($count_students) != count(array_unique($count_students))) {
            $students_selected = array();
            $this->session->set_flashdata('error_msg', '<strong>¡Error:</strong> un estudiante no puede estar en dos equipos.');
            $this->session->set_flashdata('success_msg', '');
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            $students_in_teams = $this->challenges_model->getStudentsInTeam($challenge_id);
            $vista = $this->load->view('teachers/challenge_define_teams', array (
                'challenge' => array('id' => $challenge_id),
                'students' => $students_selected,
                'students_in_teams' => $students_in_teams,
                'num_teams' => $num_teams,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }
        // Comprobar que todos los alumnos estén en algún equipo
        if (count($count_students) != $num_students) {
            $students_selected = array();
            $this->session->set_flashdata('error_msg', '<strong>¡Error:</strong> falta algún estudiante por asignar.');
            $this->session->set_flashdata('success_msg', '');
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            $students_in_teams = $this->challenges_model->getStudentsInTeam($challenge_id);
            $vista = $this->load->view('teachers/challenge_define_teams', array (
                'challenge' => array('id' => $challenge_id),
                'students' => $students_selected,
                'students_in_teams' => $students_in_teams,
                'num_teams' => $num_teams,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }
        // print_r("sprints_dateX = ".$sprints_date);
//         exit(0);

        for ($i=0; $i<$num_teams; $i++) {
            // Asignamos los equipos a los alumnos
            $students = $all_teams["std_teams".$i];
            for($j=0; $j<count($students); $j++){
                // Modificamos los equipos en el RETO
                if(!$this->challenges_model->updateTeams($challenge_id, $students[$j], $i /*team*/)) {
                    $this->output->set_status_header(500);
                }
                // if(!$this->students_model->updateTeams($students[$j], array('s_team' => $i))) {
                //     $this->output->set_status_header(500);
                // }

            }

        }

        // Redirección a la vista principal
        $this->session->set_flashdata('success_msg', 'Equipos <strong>guardados correctamente</strong>.');
        $this->session->set_flashdata('error_msg', '');
        redirect(base_url('teachers/challenges'), 'refresh');
    }


    /**
     * Función para editar un Reto de la base de datos
     */
    public function edit($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();


        // Obtenemos la información del RETO
        $challenge = $this->challenges_model->getChallenge($id);
        // var_dump("challenge = ".json_encode($challenge));

        // Comprobamos si el reto está en fase de evaluación para no dejar cambiar los equipos.
        $assessment_in_process = $this->challenges_assessments_model->isChallengeInAssessment($id);
        // Devuelve true si existe una evaluación. False si todavía no se ha evaluado.

        // Buscar los alumnos y profesores y pasarlos a la vista
        $students = $this->students_model->getStudents($challenge['teaching_levels'], $this->session->school_year);
        $teachers = $this->teachers_model->getTeachers($challenge['teaching_levels']);
        // var_dump("<br>edit(teachers)=".json_encode($teachers));
        // var_dump("<br>edit(students)=".json_encode($students));
        // exit(0);
        // Buscamos los alumnos y profesores que ya están en el reto
        $students_selected = $this->challenges_model->getChallengeStudents($id);
        $teachers_selected = $this->challenges_model->getChallengeTeachers($id);
        // var_dump("edit(teachers_selected)=".json_encode($teachers_selected));
        // var_dump("edit(students_selected)=".json_encode($students_selected));

        // exit(0);

        // Obtenemos las rúbricas y la rúbrica asignada actualmente
        $rubrics = $this->rubrics_model->getRubrics();
        $rubric_selected = $this->challenges_model->getChallengeRubric($id);
        $num_teams = $this->challenges_model->getChallengeTeams($id);

        // Comprobamos si el profesor actual es el creador del reto
        $current_teacher = $this->teachers_model->getTeacherId($this->session->id);
        $teacher_owner = $this->challenges_model->getTeacherOwner($id);
        $isowner = ($current_teacher == $teacher_owner) ? 1 : 0;
        // Si es admin, le atribuimos la autoría.
        if($this->session->role === 'admin') 
            $isowner = 1;

        $view = $this->load->view('teachers/challenge_edit', array (
            'students' => $students,
            'teachers' => $teachers,
            'rubrics' => $rubrics,
            'students_selected' => $students_selected,
            'teachers_selected' => $teachers_selected,
            'challenge' => $challenge,
            'id' => $id,
            'isowner' => $isowner,
            'num_teams' => $num_teams,
            'rubric_selected' => $rubric_selected,
            'assessment_in_process' => $assessment_in_process,
        ), TRUE);

        // var_dump("challenge->teaching_levels=".$challenge['teaching_levels']);
        // exit(0);
        //array('challenge' => $challenge) ,TRUE);
        $this->getTemplate($view);
    }

    /**
     * Función para actualizar los datos de una rúbrica
     */
    public function update() {
        // Comprobación de permisos
        $this->checkPermission();

        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') != 'POST')
            show_404();

        // Capturamos los datos del formulario
        $challenge_id = $this->input->post('id');
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $year = $this->input->post('year');
        $sprints = $this->input->post('sprints');
        $start_date = $this->input->post('start_date');
        $finish_date = $this->input->post('finish_date');
        $subject = $this->input->post('subject');
        $teaching_levels = $this->input->post('teaching_levels');
        $teachers = $this->input->post('teachers');
        $students = $this->input->post('students');
        $rubric_id = $this->input->post('rubric_id');
        $num_teams = $this->input->post('num_teams');
        $isowner = $this->input->post('isowner');


        $this->form_validation->set_rules(getUpdateChallengesRules());
        if( $this->form_validation->run() === FALSE ) {
            // ERRORES
            $this->output->set_status_header(400);
            // Obtenemos la información del RETO
            $challenge = $this->challenges_model->getChallenge($challenge_id);
            // Obtenemos las rúbricas y la rúbrica asignada actualmente
            $rubrics = $this->rubrics_model->getRubrics();
            $rubric_selected = $this->challenges_model->getChallengeRubric($challenge_id);
            // Buscamos los alumnos y profesores que ya están en el reto
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            $teachers_selected = $this->challenges_model->getChallengeTeachers($challenge_id);
            // Comprobamos si el reto está en fase de evaluación para no dejar cambiar los equipos.
            $assessment_in_process = $this->challenges_assessments_model->isChallengeInAssessment($challenge_id);
            $num_teams = $this->challenges_model->getChallengeTeams($id);
            $this->session->set_flashdata('error_msg', 'El reto "'.$name.'" <strong>NO se ha actualizado.</strong>');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('teachers/challenge_edit', array (
                'students' => $students,
                'teachers' => $teachers,
                'rubrics' => $rubrics,
                'students_selected' => $students_selected,
                'teachers_selected' => $teachers_selected,
                'challenge' => $challenge,
                'id' => $challenge_id,
                'isowner' => $isowner,
                'num_teams' => $num_teams,
                'rubric_selected' => $rubric_selected,
                'assessment_in_process' => $assessment_in_process,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }
        $challenge = array (
            'id' => $challenge_id,
            'name' => $name,
            'description' => $description,
            'year' => $year,
            'sprints' => $sprints,
            // 'num_teams' => $num_teams,
            'teams' => $num_teams,
            'start_date' => $start_date,
            'finish_date' => $finish_date,
            'subject' => $subject,
            'teaching_levels' => $teaching_levels,
        );

        // Si sólo actualizmos las fechas de los sprints
        $formSubmit = $this->input->post('submit');
        if( $formSubmit === 'Actualizar Fechas de Sprints' or 
            $formSubmit === 'Actualitzar Dates d\'Sprints' or 
            $formSubmit === 'Update Sprint Dates')
        {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', '');
            // Obtenemos la información del RETO
            $challenge = $this->challenges_model->getChallenge($challenge_id);
            $vista = $this->load->view('teachers/challenge_define_sprints', array (
                'challenge' => $challenge,
                'update_sprints' => '1',
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Si sólo se pueden VER los equipos
        if( $formSubmit === 'Ver Equipos' or 
            $formSubmit === 'Vore Equips' or 
            $formSubmit === 'Show Teams')
        {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', '');

            //$challenge = $this->challenges_model->getChallenge($challenge_id);
            // Llamamos a la vista para definir los equipos
            $num_teams = $this->challenges_model->getChallengeTeams($challenge_id);
            // var_dump("num_teams=".$num_teams);
            // exit(0);
            // Recuperarmos los estudiantes del reto
            $students = $this->challenges_model->getChallengeStudents($challenge_id);
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            $students_in_teams = $this->challenges_model->getStudentsInTeam($challenge_id);
                    // $students_selected = array();
                    // for($i=0; $i<$num_teams; $i++){
                    //     // $students_selected[] = $this->challenges_model->getChallengeStudents($challenge_id, $i /*team*/);
                    //     $students_selected[$i] = $this->challenges_model->getChallengeStudents($challenge_id, $i /*team*/);
                    // }

            
            // var_dump("challenge_id=".$challenge_id.", students=".json_encode($students_selected));
            // var_dump("students_selected[0]".json_encode($students_selected[0]));
            // exit(0);
            $vista = $this->load->view('teachers/challenge_show_teams', array (
                'challenge' => array('id' => $challenge_id),
                'students' => $students,
                'students_selected' => $students_selected,
                'students_in_teams' => $students_in_teams,
                'num_teams' => $num_teams,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Si sólo Crearmos o Modificamos los equipos de alumnos dentro del reto
        if( $formSubmit === 'Definir Equipos' or 
            $formSubmit === 'Definir Equips' or 
            $formSubmit === 'Establish Teams')
        {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', '');
            // Buscamos los alumnos y profesores que ya están en el reto
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            $students_in_teams = $this->challenges_model->getStudentsInTeam($challenge_id);
            // var_dump("students_selected=".json_encode($students_selected));
            // var_dump("students_in_teams=".json_encode($students_in_teams));
            // exit(0);
            // Obtenemos los equipos que tiene el reto
            $num_teams = $this->challenges_model->getChallengeTeams($challenge_id);
            // var_dump("num_teams=".$num_teams);
            // exit(0);

            $vista = $this->load->view('teachers/challenge_define_teams', array (
                'challenge' => $challenge,
                'students' => $students_selected,
                'students_in_teams' => $students_in_teams,
                'num_teams' => $num_teams,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Si deseamos Resetear Contraseñas de los alumnos.
        if( $formSubmit === 'Cambiar contraseña alumnado' or 
            $formSubmit === 'Canviar contrasenya alumnat' or 
            $formSubmit === 'Change student\'s password')
        {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', '');
            // Buscamos los alumnos que ya están en el reto
            $students_selected = $this->challenges_model->getChallengeStudents($challenge_id);
            // $students_in_teams = $this->challenges_model->getStudentsInTeam($challenge_id);
            // var_dump("students_selected=".json_encode($students_selected));
            // // var_dump("students_in_teams=".json_encode($students_in_teams));
            // // exit(0);
            // // Obtenemos los equipos que tiene el reto
            // $num_teams = $this->challenges_model->getChallengeTeams($challenge_id);
            // // var_dump("num_teams=".$num_teams);
            // exit(0);

            $vista = $this->load->view('teachers/challenge_reset_password', array (
                'challenge' => $challenge,
                'students' => $students_selected,
                // 'students_in_teams' => $students_in_teams,
                // 'num_teams' => $num_teams,
            ), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Si deseamos asignar Resultados de Aprendizaje al Reto.
        if( $formSubmit === 'Asignar Tareas' or 
            $formSubmit === 'Assignar Tasques' or 
            $formSubmit === 'Assign Tasks')
        {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', '');
            // Buscamos los RAs que pueda tener asociados el profesor. 
            $current_school_year = $this->session->school_year;
            $teacher_id = '';
            
            $technical_skills = array();
            if ($this->session->role === 'teacher') {
                
                $teacher_id = $this->teachers_model->getTeacherId($this->session->id);
                $technical_skills = $this->technical_skills_model->getAllTechnicalSkills($teacher_id, $current_school_year, '', '');
    
            } else { // Es Administrador
                $technical_skills = $this->technical_skills_model->getAllTechnicalSkills('', $current_school_year);
            }

            // Obtener los RAs que estuvieran ya seleccionados para el reto
            $selected_technical_skills = $this->challenges_model->getTechnicalSkills($challenge_id);
            // var_dump("<br>selected_TS=".json_encode($selected_technical_skills)."<br>");
            // exit(0);

            // var_dump("<br> selected_technical_skills_array =".json_encode($selected_technical_skills_array));
            // var_dump("<br> selected_technical_skills =".json_encode($selected_technical_skills));
            // var_dump("<br> technical_skills =".json_encode($technical_skills));
            // exit(0);

            if(empty($technical_skills)) {
                // Redirección a la vista principal
                $this->session->set_flashdata('success_msg', '');
                $this->session->set_flashdata('error_msg', 'No hi ha cap <strong>resultat d\'aprenentatge creat o assignat</strong> a este professor.');
                redirect(base_url('teachers/challenges'), 'refresh');
            } else {
                // var_dump("si que hi han resultats d\'aprenentatge associats");
                // exit(0);
                $vista = $this->load->view('teachers/challenge_assign_technicalskills', array (
                    'challenge' => $challenge,
                    'technical_skills' => $technical_skills,
                    'selected_technical_skills' => $selected_technical_skills,
                    // 'selected_technical_skills' => $selected_technical_skills_array,
                ), TRUE);
                $this->getTemplate($vista);
            }

            
            return;
        }



        /* Realizar los cambios en la BBDD */


        // 1. Revisar si las fechas de Sprints son diferentes
        $sprints_before_update = $this->challenges_model->getChallengeSprints($challenge_id);
        $sprints_modified = false;
        if ($sprints != $sprints_before_update)
            $sprints_modified = true;

        // Intentar actualizar los datos. Si no se puede damos un error.
        if(!$this->challenges_model->update($challenge_id, $challenge)) {
            //$this->output->set_status_header(500);
            // Redirección a la vista de Retos
            $this->session->set_flashdata('error_msg', 'El reto "'.$name.'" <strong>NO se ha actualizado.</strong>');
            $this->session->set_flashdata('success_msg', '');
            redirect(base_url('teachers/challenges'), 'refresh');
        } else {

            // Borramos los estudiantes y profesores seleccionados previamente
            if(!$this->challenges_model->disenrolChallenge($challenge_id)) {
                $this->session->set_flashdata('error_msg', 'Error al borrar los profesores/estudiantes del reto: "'.$name.'".');
                $this->session->set_flashdata('success_msg', '');
                redirect(base_url('teachers/challenges'));
            }
            
            // Enrolamos a los estudiantes y profesores al Reto
            foreach ($students as $student_id) {
                if(!$this->students_model->enrolChallenge($student_id, $challenge_id)) {
                    $this->output->set_status_header(500);
                }
            }
            foreach ($teachers as $teacher_id) {
                if(!$this->teachers_model->enrolChallenge($teacher_id, $challenge_id)) {
                    $this->output->set_status_header(500);
                }
            }

            // Modificamos las rúbrica elegida
            if(!$this->challenges_model->updateRubric($challenge_id, $rubric_id)) {
                $this->output->set_status_header(500);
            }

            // Si se han modificado los sprints, llamamos a la vista para actualizar sus fechas.
            // if ($sprints_modified) {
            //     $vista = $this->load->view('teachers/challenge_define_sprints', array (
            //         'challenge' => $challenge,
            //     ), TRUE);
            //     $this->session->set_flashdata('success_msg', '');
            //     $this->session->set_flashdata('error_msg', '');
            //     $this->getTemplate($vista);
            //     return;
            // }

            // Si se han modificado los sprints cambiamos la estructura guarda para abrirlos (open_for_assess)
            if ($sprints_modified) {
                if(!$this->challenges_model->updateSprintsOpenForAssess($challenge_id, $sprints))
                    $this->output->set_status_header(500);
            }

            // Redirección a la vista de Retos
            $this->session->set_flashdata('success_msg', 'El reto "'.$name.'" ha sido <strong>actualizado correctamente.</strong>');
            $this->session->set_flashdata('error_msg', '');
            redirect(base_url('teachers/challenges'), 'refresh');
            
        }                    
 
    }
    /**
     * Función para actualizar las fechas de los SPRINTS de un RETO
     */
    public function updateSprints() {
        // Comprobación de permisos
        $this->checkPermission();
            
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') != 'POST')
            show_404();
 
        // Capturamos los datos del formulario
        $challenge_id = $this->input->post('id');
        $name = $this->input->post('name');
        $description = $this->input->post('description');
        $year = $this->input->post('year');
        $sprints = $this->input->post('sprints');
        $start_date = $this->input->post('start_date');
        $finish_date = $this->input->post('finish_date');
        $subject = $this->input->post('subject');
        $teaching_levels = $this->input->post('teaching_levels');
        $teachers = $this->input->post('teachers');
        $students = $this->input->post('students');
        $rubric_id = $this->input->post('rubric_id');
        $sprints = $this->input->post('sprints');

        /***
         * FALTA : hacer una vista para modificar las fechas de sprint si se modifica el numero de sprints
         */
        /*
        $sprints_date = array();

        for ($i=0; $i<$sprints; $i++) {
            //$sprints_date[] = $this->input->post('sprints_date'.$i);
            //$sprints_date[] = array('sprints_date'.$i => $this->input->post('sprints_date'.$i));
            $sprints_date['sprints_date'.$i] = $this->input->post('sprints_date'.$i);
        }

        // Asignamos las fechas de los Sprints
        if(!$this->challenges_model->updateSprintsDate($challenge_id, $sprints_date)) {
        //if(!$this->challenges_model->updateSprintsDate($challenge_id, array('sprints_date' => $sprints_date))) {
            $this->output->set_status_header(500);
        }

        // Redirección a la vista principal
        $this->session->set_flashdata('success_msg', 'Reto <strong>creado correctamente</strong>.');
        redirect(base_url('teachers/challenges'), 'refresh');  
        *********/
         
        // -------

        //var_dump($teachers);
        //exit(0);
    }

    /**
     * Abrir/Cerrar un Sprint para evaluar
     */
    public function setSprints() {
        // Comprobación de permisos
        $this->checkPermission();

        if ($this->session->role === 'teacher') {
            $teacher = $this->teachers_model->getTeacherId($this->session->id);
            $teacher_id=$teacher;
            $data = $this->challenges_model->getChallenges($teacher_id);

        } else {
            $teacher_id = '';
            $data = $this->challenges_model->getChallenges();

        }


        $vista = $this->load->view('teachers/challenges_sprints_show', array('data' => $data), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para establecer qué sprints se activan/desactivan
     */
    public function switchSprints($id = '') {
        // Comprobación de permisos
        $this->checkPermission();
        
        // Obtenemos la información del RETO
        //$challenge = $this->challenges_model->getChallenge($id);

        // Array con los sprints abiertos para valorar.
        $sprints_openfor_assess = $this->challenges_model->getSprintsStatus($id);
        // var_dump("sprintsOpenClose(status)=".json_encode($sprints_openfor_assess[0]));

        $num_sprints = $this->challenges_model->getChallengeSprints($id);
        // var_dump("num_sprints=".$num_sprints);
        // exit(0);

        $view = $this->load->view('teachers/challenges_sprints_openclose', array (
            'id' => $id,
            'num_sprints' => $num_sprints,
            'sprints_openfor_assess' => $sprints_openfor_assess,
        ), TRUE);
        $this->getTemplate($view);
    }
    /**
     * Función para guardar los sprints abiertos/cerrados para evaluación.
     */
    public function saveSprints() {
        // Comprobación de permisos
        $this->checkPermission();
                    
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') != 'POST')
            show_404();

        // Capturamos los datos del formulario
        $challenge_id = $this->input->post('challenge_id');
        $num_sprints = $this->input->post('num_sprints');
        $sprints_selected = $this->input->post('sprint');
        // var_dump("Sprint selected = ".json_encode($sprints_selected));
        // exit(0);
			// sumamos al sprint 1 para acceder al sprint correcto en la bbdd
		 //$sprints_selected += 1;

        /**
         *  Queremos comprobar si se ha creado la estructura en la Base de Datos. Sinó la creamos
         */
        // 1. Si no hay seleccionado ningún sprint => no se desea evaluar.
        if (!is_null($sprints_selected)) {
            // Si hay seleccionado algun sprint
            // 2. Comprobamos si tiene creadas los registros para las calificaciones.
            $hasAssessments = $this->challenges_assessments_model->isChallengeInAssessment($challenge_id, $sprints_selected+1);
            if(!$hasAssessments) {
                // Obtenemos la información del RETO
                $challenge = $this->challenges_model->getChallenge($challenge_id);
                // Buscar los alumnos pertenecientes al reto
                $students = $this->challenges_model->getChallengeStudents($challenge_id);
                // var_dump("Students=".json_encode($students));
                // Buscar los profesores asociados al reto
                $teachers = $this->challenges_model->getChallengeTeachers($challenge_id);
                // print_r("<br>t=".json_encode($teachers));
                // exit(0);
                // Con los estudiantes, creamos sus registros de evaluación en la BBDD
                if(!$this->challenges_assessments_model->start($challenge_id, $challenge, $students, $teachers, $sprints_selected+1))
                    $this->output->set_status_header(500);
            }
            /** Fin de creación de estructura en BBDD */

        }

        // Guardamos los sprints 
        if(!$this->challenges_model->updateSprintsAssessments($challenge_id, $sprints_selected)) {
            $this->session->set_flashdata('error_msg', 'Error al guardar.');
            $this->session->set_flashdata('success_msg', '');
            redirect(base_url('teachers/challenges'), 'refresh');
        }

        // Redirección a la vista de Retos
        $this->session->set_flashdata('success_msg', 'Sprints <strong>guardados correctamente.</strong>');
        $this->session->set_flashdata('error_msg', '');
        redirect(base_url('teachers/challenges'), 'refresh');

    }

    /**
     * Función para guardar los Resultados de Aprendizaje
     * asignados a un RETO
     */
    public function saveTechnicalSkills() {
        // Comprobación de permisos
        $this->checkPermission();

        // Capturamos TODOS los datos del formulario
        foreach ($_POST as $key=>$val)
        {
          $sanitized[$key] = $this->input->post($key);
        }
        // var_dump("<br>all_post=".json_encode($sanitized));

        // Capturamos los datos del formulario
        // $challenge_id = $this->input->post('challenge_id');
        $challenge_id = $sanitized['challenge_id'];
        // Capturamos las RAs enviadas.
        // $num_technical_skills = $this->input->post('num_technical_skills');
        $num_technical_skills = $sanitized['num_technical_skills'];
        // Ids de los RAs seleccionados.
        // $technical_skills = $this->input->post('technical_skills');
        $technical_skills = $sanitized['technical_skills'];
        // var_dump("<br>num_ts=".$num_technical_skills."<br>ts(values=".json_encode($technical_skills)."<br>");
        // Recogemos los porcentajes, filtrando los inputs vacíos.
        $percentages = array_values(array_filter($sanitized['percentages']));
        // var_dump("<br>percentages=".json_encode($percentages));

        if(count($technical_skills) !== count($percentages)) {
            // Error, no hay asignados igual número de porcentajes que de taras.
            $this->session->set_flashdata('error_msg', 'Error d\'assignació de tasques i percentatges.');
            $this->session->set_flashdata('success_msg', '');
            redirect(base_url('teachers/challenges/edit/'.$challenge_id), 'refresh');

        }
        for($i=0; $i<count($technical_skills); $i++) {
            $ts_with_percentages[$technical_skills[$i]] = $percentages[$i];
        }

        // var_dump("<br>Tasques y percentages unides:".json_encode($ts_with_percentages));
        // exit(0);
        
        // Actualizamos el listado de Resultados de Aprendizaje en la BBDD
        if(!$this->challenges_model->saveTechnicalSkills($challenge_id, $ts_with_percentages)) {
            $this->output->set_status_header(500);
        }
        // Actualizamos los RAs para que incluyan el RETO.
        if(!$this->technical_skills_model->updateTechnicalSkillsInChallenge($technical_skills, $challenge_id)) {
            $this->output->set_status_header(500);
        }

        // $this->session->set_flashdata('success_msg', $g_ch_operation_ok[$g_applang]);
        // $this->session->set_flashdata('error_msg', '');
        
        // Redirección a la vista principal
        redirect(base_url('teachers/challenges'), 'refresh');
    }

    /**
     * Función para Resetear las contraseñas de los alumnos.
     */
    public function resetPasswords() {
        // Comprobación de permisos
        $this->checkPermission();
                    
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') != 'POST')
            show_404();

        // Capturamos los datos del formulario
        $challenge_id = $this->input->post('challenge_id');
        $students_selected = $this->input->post('students');
        // Tenemos los ids de los estudiantes.
        // var_dump("Students selected = ".json_encode($students_selected));
        // var_dump("<br>Count(Students) = ".count($students_selected));

        // exit(0);

        // Para los alumnos seleccionados, les cambiamos la contraseña
        if(isset($students_selected)) {

            // Con los estudiantes, creamos sus registros de evaluación en la BBDD
            foreach ($students_selected as $student_id) {
                $user_id = $this->students_model->getUserIdFromStudent($student_id);
                // var_dump("<br>El user_id es: ".$user_id);
                if(!$this->users_model->resetPassword($user_id)) {
                    $this->output->set_status_header(500);
                }
            }

            $this->session->set_flashdata('success_msg', 'Contraseñas <strong>modificadas correctamente.</strong>');

        } else {
            $this->session->set_flashdata('success_msg', '');

        }
        

        // Redirección a la vista de Retos
        $this->session->set_flashdata('error_msg', '');
        redirect(base_url('teachers/challenges'), 'refresh');

    }

    /**
     * Borrar un RETO de la BBDD
     */
    public function delete($id) {
        // Comprobación de permisos
        $this->checkPermission();
        
        // Borrar los equipos a los que estaban asignados los estudiantes
        // $students = $this->challenges_model->getChallengeStudents($id);
        // foreach($students as $student){
        //     $this->students_model->removeFromTeam($student['id']);
        // }
        
        if(!$this->challenges_model->disenrolChallenge($id)) {
            $this->session->set_flashdata('error_msg', 'Error al borrar los profesores/estudiantes del reto.');
            redirect(base_url('teachers/challenges'), 'refresh');
        }
        // Borrar los datos de sus evaluaciones (ASSESSMENTS)

        if(!$this->challenges_assessments_model->deleteChallenge($id)) {
            $this->session->set_flashdata('error_msg', 'Error al borrar las EVALUACIONES y NOTAS del reto.');
            redirect(base_url('teachers/challenges'), 'refresh');
        }

        // Borrar las Tareas Evaluables (RAs) asociadas a un RETO
        if(!$this->technical_skills_assessments_model->deleteChallenge($id)){
        // if(!$this->technical_skills_assessments_model->deleteTechnicalSkillsInChallenge($id)){
            // $this->session->set_flashdata('error_msg', 'Error al borrar las RAs asociadas al reto.');
            redirect(base_url('teachers/challenges'), 'refresh');
        }

        if(!$this->challenges_model->delete($id)){
            // ERROR No se ha podido borrar el RETO
            show_500();
        } else {
            // Se ha borrado con éxito. Volver a la pantalla de estudiantes
            redirect(base_url('teachers/challenges'), 'refresh');
        }
    }

    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE),          
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );
        // var_dump("nav -> schoolyear = ".$this->session->school_year);
        // exit(0);
        $this->load->view('teachers/challenges', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission($school_year_changed = false) {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');

        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student' && !$school_year_changed)
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        //var_dump("formSubmit = ".$formSubmit);
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/challenges'), 'refresh');
        }
    }
}
