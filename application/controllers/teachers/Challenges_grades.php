<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Challenges_grades extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','pagination'));

        // cargamos el helper con las reglas de los retos
        //$this->load->helper(array('challenges_rules'));
        // Cargar los modelos necesarios
        $this->load->model('challenges_model');
        $this->load->model('challenges_assessments_model');
        $this->load->model('technical_skills_model');
        $this->load->model('technical_skills_assessments_model');
        $this->load->model('rubrics_model');
        $this->load->model('teachers_model');
        $this->load->model('students_model');
    }

    /**
     * Función principal por defecto para mostrar las Evaluaciones de un Reto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        if ($this->session->role === 'teacher') {
            $teacher = $this->teachers_model->getTeacherId($this->session->id);
            $teacher_id=$teacher;
            $data = $this->challenges_model->getChallenges($teacher_id);

        } else {
            $teacher_id = '';
            $data = $this->challenges_model->getChallenges();

        }

        // Para hacer la paginación
        $config['base_url']= base_url('teachers/challenges_grades/index');
        // Elementos por página
        $config['per_page']= 6;
        $config['total_rows']= count($data);

        // Paginación con TailwindCSS
        $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        $config['full_tag_close']= '</ul></div>';
        $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['num_tag_close']= '</li>';
        $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teaal-500 border-r border-grey-light px-3 py-2 active">';
        $config['cur_tag_close']= '</li>';
        $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['next_tag_close']= '</li>';
        $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['prev_tag_close']= '</li>';
        $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['first_tag_close']= '</li>';
        $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        $config['last_tag_close']= '</li>';

        $this->pagination->initialize($config);
        $page = $this->challenges_model->get_Paginate($teacher_id,$config['per_page'],$offset);

        $vista = $this->load->view('teachers/challenges_grades_show', array('data' => $page), TRUE);
        $this->getTemplate($vista);

    }
 
    /**
     * Función para mostrar las calificaciones de los alumnos de un Reto
     */
    public function show($challenge_id = 0, $generateReport = false) {
        // Comprobación de permisos
        $this->checkPermission();

        // Si no tiene creada ninguna evaluación, no podemos mostrar resultados.
        $hasAssessments = $this->challenges_assessments_model->isChallengeInAssessment($challenge_id);
        if(!$hasAssessments) {
            $this->session->set_flashdata('success_msg', '');
            $this->session->set_flashdata('error_msg', 'Encara no s\'ha fet cap avaluació per aquest repte.');
            redirect(base_url('teachers/challenges'), 'refresh');
            return;

        }

        // Obtenemos el nombre del RETO
        $challenge = $this->challenges_model->getChallenge($challenge_id);
        // print_r($challenge);
        // exit(0);
        $challenge_name = $challenge['name'];

        // Buscamos los alumnos asociados a este reto.
        $students = $this->challenges_model->getChallengeStudents($challenge_id);
        // var_dump(json_encode($students));
        // var_dump("<br>students[0]['id']".json_encode($students[0]['id']));
        // exit(0);
        $sprints = $this->challenges_model->getChallengeSprints($challenge_id);
        // var_dump("<br>".json_encode($sprints));
        // exit(0);

        // Buscamos cuantos valores tiene la rúbrica, es decir, cuantas columnas
        $rubric_cols = $this->rubrics_model->getRubricRows($this->challenges_model->getChallengeRubric($challenge_id));
        // Buscamos los nombres de los criterios de la rúbrica
        $rubric_criteria = $this->rubrics_model->getRubricCriteria($this->challenges_model->getChallengeRubric($challenge_id));
        // Buscamos el valor máximo de la rúbrica
        $max_value = $this->rubrics_model->getRubricMaxValue($this->challenges_model->getChallengeRubric($challenge_id));
        // print_r($max_value);
        // exit(0);

        // Comprobamos quién ha valorado.
        $all_who_has_assess = $this->getWhoHasAssess($challenge_id, $students, $sprints);
        // print_r("Han valorado:<br>");
        // print_r($all_who_has_assess);
        // print_r($all_who_has_assess[0]); // Self
        // print_r($all_who_has_assess[1]); // Teachers
        // print_r($all_who_has_assess[2]); // TeamMates
        // exit(0);

        // Para cada estudiante buscamos sus calificaciones
        $all_grades = array();
        $all_technical_comments = array();
        $all_technical_names = array();
        $all_technical_marks = array();
        $all_average_technical_marks = array();
        $all_sprints_grades = array();
        $all_marks = array();
        $all_comments = array();
        $num_technical_skills = 0;
        
        // Inicio del bucle de todos los estudiantes.
        for($i=0; $i<count($students); $i++) {
            // Obtenemos las CALIFICACIONES TOTALES del estudiante.
            $student_grades_tmp = $this->challenges_assessments_model->getAllStudentGrades($challenge_id, $students[$i]['id'], $sprints);
            // var_dump("<br>student_grades=<br");
            // print_r($student_grades_tmp);
            // exit(0);

            if(empty($student_grades_tmp)){
                $student_grades = array();

                // No hay notas transversales.
                // $no_sprints_grades = true;
                $all_sprints_grades[$students[$i]['id']] = array();
                $all_grades[$students[$i]['id']] = array();
                $all_marks[$students[$i]['id']] = array();
                $all_comments[$students[$i]['id']] = array();
                // var_dump("<br>No hay notas para el estudiante ".$students[$i]['id']);
            } else {
                // $no_sprints_grades = false;
                $student_grades = array($students[$i]['id'] => $student_grades_tmp);
                // var_dump("<br>Las notas del estudiante ".$students[$i]['id']." son:".json_encode($student_grades));

                // Obtenemos las calificaciones por sprints
                $student_grades_by_sprint = array();
                for($j=0; $j< count($student_grades[$students[$i]['id']]) - 1 ; $j++) {
                    $sprint_number = 'sprint'.(string)($j+1);
                    $tmp_grades = $student_grades[$students[$i]['id']][$sprint_number];
                    // var_dump("<br>tmp_grades= ".json_encode($tmp_grades)."- count(tmp_grades)=".count($tmp_grades).",  count2=".strval(count($student_grades[$students[$i]['id']])-1));
                    // Transformamos las notas a BASE 10
                    for($k=0; $k<count($tmp_grades); $k++) {
                        // print_r("tmp_grades[".$k."]=".$tmp_grades[$k]);
                        // $tmp_grades[$j] = number_format($tmp_grades[$j] * 10 / $max_value, 2, '.', ',');                    
                        $tmp_grades[$k] = number_format($tmp_grades[$k] * 10 / $max_value, 2, '.', ',');
                    }
                    // print_r("<br>Tras la transformación BASE 10, tmp_grades=".json_encode($tmp_grades));
                    $student_grades_by_sprint[] = $tmp_grades;
                }
                $all_sprints_grades[$students[$i]['id']] = $student_grades_by_sprint;
                // var_dump("<br>all_sprints_grades=<br>".json_encode($all_sprints_grades));
                // exit(0);
            
                $final_grades = $student_grades[$students[$i]['id']]['final_grades'];
                // var_dump("<br>final_grades=".json_encode($final_grades));
                // exit(0);
                // Transformamos las notas a BASE 10
                $mark = 0;
                for($iter=0; $iter<count($final_grades); $iter++) {
                    $final_grades[$iter] = $final_grades[$iter] * 10 / $max_value;
                    $mark += $final_grades[$iter];
                }
                $mark = number_format($mark/count($final_grades), 2, '.','.');
                // var_dump("<br>only_brades (BASE 10)=".json_encode($final_grades)."<br>MARK = ".$mark);
                // exit(0);
                $all_grades[$students[$i]['id']] = $final_grades;
                // var_dump("<br>BASE 10: student_grades=".json_encode($student_grades));
                // exit(0);
                $all_marks[$students[$i]['id']] = $mark;

                // Obtenemos TODOS los COMENTARIOS de un estudiante.
                $student_comments = $this->challenges_assessments_model->getStudentComments($challenge_id, $students[$i]['id'], $sprints);
                // var_dump("<br>los comentarios de id(".$students[$i]['id'].") son: <br>".json_encode(array_values($student_comments)));
                // exit(0);
                $all_comments[$students[$i]['id']] = $student_comments;
                // var_dump($all_comments);
                // exit(0);

            }
            // var_dump("<br>My Grades= <br>");
            // print_r($student_grades);
            // exit(0);

            

            // *****************************************************************************
            // ****** VAMOS POR LAS CALIFICACIONES TÉCNICAS -> TECHNICAL SKILLS - RAs ******
            // *****************************************************************************

        
            // Buscamos los RAs asociados al reto. 
            // $technical_skills_ids = explode(",",$this->challenges_model->getTechnicalSkills($challenge_id));
            $technical_skills = $this->challenges_model->getTechnicalSkills($challenge_id);
            
            // print_r("<br>Los RAs del Reto(".$challenge_id.") son =".json_encode($technical_skills));
            // exit(0);
            
            $group_technical_marks = array();
            $group_technical_marks_orginals = array();
            $group_technical_names = array();
            $group_technical_comments = '';
            $technical_skills_percentages = array();
            if(!empty($technical_skills)) {
                // Reiniciamos las variables
                $group_technical_marks = array();
                $group_technical_marks_orginals = array();
                $group_technical_names = array();
                $group_technical_comments = '';

                $num_technical_skills = count($technical_skills);
                // Identificadores de los RAs
                $technical_skills_ids = array_keys($technical_skills);
                // Porcentajes de los RAs
                $technical_skills_percentages = array_values($technical_skills);
                for($n=0; $n<$num_technical_skills; $n++) {
                // foreach($technical_skills_ids as $ts_id){
                 
                    $technical_skills_name = $this->technical_skills_model->getTechnicalSkillsName($technical_skills_ids[$n]);
                    // print_r("<br>TS Name = ".$technical_skills_name);
                    $group_technical_names[] = $technical_skills_name;

                    $has_rubric = $this->technical_skills_model->hasRubric($technical_skills_ids[$n]);

                    // Comprobamos el tipo de calificación que tenemos que calcular
                    if($has_rubric == 0) {
                        // Buscamos las calificaciones numéricas de los profesores y calculamos la nota media.
                        $final_mark = json_decode($this->technical_skills_assessments_model->getFinalMark($technical_skills_ids[$n], $challenge_id, $students[$i]['id'],TRUE));
                        // print_r("<br>final_mark=".json_encode($final_mark));
                        // exit(0);
                        $average_marks = 0;
                        $average_marks_originals = 0;
                        $count = 0;
                        if (!empty($final_mark)) {
                            foreach($final_mark as $mark){
                                // A la nota final le aplicamos el porcentaje de cada RA
                                $average_marks += floatval($mark) * ($technical_skills_percentages[$n]*0.01); 
                                $average_marks_originals += floatval($mark); 
                                $count++;  
                            }
                            $average_marks = $average_marks / $count;
                            $average_marks_originals = $average_marks_originals / $count;
                            // Montamos un array con el identificador del RA y su calificación final.
                            // print_r($average_marks);
                            if($count > 0) {
                                // print_r($technical_skills_percentages[$n]*0.01);
                                // exit(0);
                                $group_technical_marks[] = number_format($average_marks, 2);
                                $group_technical_marks_orginals[] = number_format($average_marks_originals, 2);
                                // $group_technical_marks[] = number_format($average_marks / $count, 2);
                            } else {
                                $group_technical_marks[] = 0.00;
                                $group_technical_marks_orginals[] = 0.00;
                            }

                            // print_r("<br>average_marks=".json_encode($group_technical_marks));
                        } else {
                            $group_technical_marks[] = 0.00;
                            $group_technical_marks_orginals[] = 0.00;
                        }
                        
                    } else {
                        // Si has_rubric != 0 es que tiene una rúbrica.
                        $group_technical_marks[] = 10.00;
                        $group_technical_marks_orginals[] = 10.00;
                    }

                    // json_encode( $text, JSON_UNESCAPED_UNICODE );
                    $technical_teachers_comments = $this->technical_skills_assessments_model->getTeachersComments($technical_skills_ids[$n], $challenge_id, $students[$i]['id']);
                    //var_dump(json_decode($technical_teachers_comments));
                    //exit(0);

                    // $all_comments = array($teacher_id, $technical_teachers_comments);
                    if(!empty($technical_teachers_comments)) {

                        // Voy a trabajar los comentarios como un array. 
                        // En el índice estará el id del profe
                        // Como valor estará el comentario.
                        $tech_comments = json_decode($technical_teachers_comments, JSON_UNESCAPED_UNICODE);
                        //$tech_comments = json_decode($technical_teachers_comments, JSON_UNESCAPED_UNICODE);

                        //var_dump($tech_comments);
                        // exit(0);

                        // $teacher_plus_comment = '';
                        // $group_technical_comments = '';
                        foreach($tech_comments as $keyid => $comment) {
                            if(!empty($comment)) {
                                $tech_name = $this->teachers_model->getTeacherFullName($keyid);
                                // var_dump($tch_name);
                                $teacher_plus_comment = join(": ",array($tech_name, $comment));
                                $group_technical_comments .= (string)$teacher_plus_comment . '<br>';
                                // Añadimos el nombre del Technical Skill al inicio de todos los comentarios
                                $group_technical_comments = '<strong>'.$technical_skills_name.':</strong><br>'.$group_technical_comments;

                            } else  {
                                $teacher_plus_comment = join(" ",array());
                            }
                           
                        }
                        
                        
                    } else {
                        // Si no tiene comentarios de ningún profesor
                        //$group_technical_comments .= '<br>';
                    }
                                                       
                }
               
                $all_average_technical_marks[] = number_format(array_sum($group_technical_marks), 2);
                // Juntamos las calificaciones de este alumno.
                $all_technical_names[$students[$i]['id']] = $group_technical_names;
                $all_technical_marks[$students[$i]['id']] = $group_technical_marks_orginals;
                $all_technical_comments[$students[$i]['id']] = $group_technical_comments;
                // exit(0);
                

            } else {
                $num_technical_skills = 0;
            }
            //exit(0);

 
        }
        // FIN BUCLE FOR DE TODOS LOS ESTUDIANTES


        // print_r("<br>ALL TS Names = ".json_encode($all_technical_names));
        // print_r("<br>ALL TS marks=".json_encode($all_technical_marks));
        // print_r("<br>ALL TS teachers_comments=".json_encode($all_technical_comments)."<br><br>");
        // print_r("<br>ALL AVERAGE marks=".json_encode($all_average_technical_marks));


        // var_dump("<br>All Students Grades= <br>".json_encode($all_grades)."count(grades)=".count($all_grades));
        // var_dump("<br>All Sprint Grades= <br>".json_encode($all_sprints_grades)."count(grades)=".count($all_sprints_grades));
        // var_dump("<br>All MARKS = <br>".json_encode($all_marks));
        // var_dump("<br>All SOFTSKILLS COMMENTS = <br>");
        // var_dump($all_comments);
        // var_dump("<br>All TECHNICAL COMMENTS = <br>");
        // var_dump($all_technical_comments);
        // exit(0);

        // Obtener los datos de las calificaciones de profesores y alumnos (propio y compañeros)

        // Si generateReport es === true vamos a la vista para generar el PDF. 
        $view_to_show = $generateReport === false ? 'teachers/challenges_grades_scores' : 'teachers/challenges_grades_generatePDFReports' ; 
        $vista = $this->load->view($view_to_show, array(
            'who_has_assess' => $all_who_has_assess,
            //'no_sprints_grades' => $no_sprints_grades,
            'all_sprints_grades' => $all_sprints_grades,
            'all_grades' => $all_grades,
            'all_marks' => $all_marks,
            'all_comments' => $all_comments,
            'all_technical_names' => $all_technical_names,
            'all_technical_marks' => $all_technical_marks,
            'all_average_technical_marks' => $all_average_technical_marks,
            'all_technical_comments' => $all_technical_comments,
            'num_technical_skills' => $num_technical_skills,
            'technical_skills_percentages' => $technical_skills_percentages,
            'challenge_id' => $challenge_id,
            'challenge_name' => $challenge_name, // For Report purspose.
            'sprints' => $sprints,
            'current_sprint' => 1,
            'cols' => $rubric_cols,
            'students' => $students,
            'criteria' => $rubric_criteria,
            'max_value' => $max_value,
            'school_year' => $this->session->school_year, 
        ), TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para obtener quién ha valorado un reto
     */
    public function getWhoHasAssess($challenge_id, $students, $sprints = '') {

        // Montamos la estructura de Estudiante + Compañeros + Profesores
        // Para comprobar quién ha valorado y quien no.
        $tmp_who_has_assess_teachers = $this->challenges_model->getChallengeTeachers($challenge_id);
        $who_has_assess_teachers = array();
        $teachers_names =  array();
        $teachers_ids = array();
        foreach($tmp_who_has_assess_teachers as $key => $value){
            // $who_has_assess_teachers[] = $value['firstname'].' '.$value['lastname'];
            $who_has_assess_teachers[] = $value['id'];
            $teachers_names[] = $value['firstname'].' '.$value['lastname'];
            $teachers_ids[] = $value['id'];

        }
        
        $all_who_has_assess = array();
        $self_assess_values = array();
        $self_who_has_assess = array();

        // Para cada estudiante buscamos sus propias valoraciones.
        for($i=0; $i<count($students); $i++) {
            // print_r($i.", ");
            
            for($k=0; $k<intval($sprints); $k++) {
                $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $students[$i]['id']);
                // print_r($outcome_id.", ");    
                // exit(0);
                if (is_null($outcome_id)) {
                    $self_assess_values[$i][$students[$i]['firstname'].' '.$students[$i]['lastname']][] = false;
                    // break;
                } else {
                    $self_data = $this->challenges_assessments_model->getStudentAssessmentData($outcome_id);
                    // print_r($self_data);
                    if(is_null($self_data) || empty($self_data)) {
                        $self_assess_values[$i][$students[$i]['firstname'].' '.$students[$i]['lastname']][] = false; 
                    } else {
                        $self_assess_values[$i][$students[$i]['firstname'].' '.$students[$i]['lastname']][] = true; 
                    }
                }
            }
            $self_who_has_assess[$i] = $self_assess_values;
            $self_assess_values = array();
        }
        // print_r("self_values:<br>");
        // print_r($self_who_has_assess);
        // exit(0);

        $teachers_assess_values = array();
        $teachers_who_has_assess = array();

        // Para cada estudiante buscamos las valoraciones de todo el profesorado.
        for($i=0; $i<count($students); $i++) {
           
            // $teachers_assess_values[$i];

            // foreach($teachers_names as $tname) {
            for($j=0; $j<count($teachers_ids); $j++) {
                // $teachers_assess_values[$i][] =  $teachers_names[$j];
                // print_r($teachers_names[0]);
                // exit(0);

                for($k=0; $k<intval($sprints); $k++) {
                    $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $students[$i]['id']);
                    // print_r($outcome_id);    
                    // exit(0);
                    if (is_null($outcome_id)) {
                        $teachers_assess_values[$i][$teachers_names[$j]][] = false;
                        // break;
                    } else {
                        // $teachers_data = json_encode($this->challenges_assessments_model->getTeachersAssessmentData($outcome_id));
                        $teachers_data = $this->challenges_assessments_model->getTeachersAssessmentData($outcome_id);
                        // $teachers_data = json_decode($this->challenges_assessments_model->getTeachersAssessmentData($outcome_id), TRUE);
                        // $teachers_data = $this->challenges_assessments_model->getTeachersAssessmentData($outcome_id);
                        
                        if(is_null($teachers_data) || empty($teachers_data)) {
                            $teachers_assess_values[$i][$teachers_names[$j]][] = false;
                        } else {

                            $teachers_data_ids = array_keys($teachers_data);
                            // print_r($teachers_data);
                            // print_r("<br>");
                            // print_r($teachers_data_ids);
                            // print_r("<br>");
                            // print_r($teachers_ids[$j]);
                            // exit(0);
                            if(in_array($teachers_ids[$j], $teachers_data_ids)) {
                                // var_dump("Si que hay coincidencia<br>");
                                $teachers_assess_values[$i][$teachers_names[$j]][] = true;
                            } else {
                                $teachers_assess_values[$i][$teachers_names[$j]][] = false;
                            }
                        }
                    }
                }
                $teachers_who_has_assess[$i] = $teachers_assess_values;
            }
            $teachers_assess_values = array();

        }
        // print_r($teachers_who_has_assess);
        // exit(0);

        // return $all_who_has_assess;
        $team_mates_assess_values = array();
        $team_who_has_assess = array();

        // Para cada estudiante buscamos las valoraciones de sus compañeros de equipo.
        for($i=0; $i<count($students); $i++) {
        // for($i=0; $i<1; $i++) {
            $team_mates = $this->challenges_model->getChallengeTeamMatesId($challenge_id, $students[$i]['id']);
            // print_r($team_mates);
            // exit(0);
            if(empty($team_mates)) break; // Si no tiene compañeros, no realizamos esta búsqueda.

            // Pomemos los compañeros de equipo
            foreach($team_mates as $key => $team_mate_id) {
                $tmp_team_mate = $this->students_model->getStudent($this->students_model->getUserIdFromStudent($team_mate_id));
                // print_r($tmp_team_mate); //908 y 909
                // exit(0);
                
                for($k=0; $k<intval($sprints); $k++) {
                    $outcome_id = $this->challenges_outcomes_model->getOutcomeIdForStudent($challenge_id, ($k+1) /* # sprint */, $students[$i]['id']);
                    // print_r($outcome_id);    
                    // exit(0);
                    if (is_null($outcome_id)) {
                        $team_mates_assess_values[$i][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = false;
                        // print_r($team_mates_assess_values);
                        // exit(0);
                        continue;
                    } else { 
                        $team_mate_data = $this->challenges_assessments_model->getPeerAssessmentData($outcome_id);
                        // print_r($team_mate_data);
                        // exit(0);
                        if(is_null($team_mate_data) || empty($team_mate_data)) {
                            $team_mates_assess_values[$i][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = false;
                            // print_r($team_mates_assess_values);
                            // exit(0);
                        } else {
                            // print_r($team_mate_data);
                            // exit(0);

                            if(isset($team_mate_data[$team_mate_id])) {
                                $team_mates_assess_values[$i][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = true;
                            } else {
                                $team_mates_assess_values[$i][$tmp_team_mate['firstname'].' '.$tmp_team_mate['lastname']][] = false;
                            }
                        }
                    }
                }
                $team_who_has_assess[$i] = $team_mates_assess_values;
            }
            $team_mates_assess_values = array();

        }

        // print_r("<br>team_mates_values:<br>");
        // print_r($team_who_has_assess);
        // print_r("<br><br>");
        // exit(0);

        // Juntamos las valoraciones: propias, profesores y compañeros
        $all_who_has_assess[] = $self_who_has_assess;
        $all_who_has_assess[] = $teachers_who_has_assess;
        $all_who_has_assess[] = $team_who_has_assess;

        // print_r($all_who_has_assess);
        // exit(0);        
        return $all_who_has_assess;     
    }

   
    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('teachers/head', '', TRUE), 
            // 'nav' => $this->load->view('teachers/nav', '', TRUE), 
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('teachers/footer', '', TRUE),
            
        );
        $this->load->view('teachers/challenges', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es la primera vez que entra, tiene que cambiarse la contraseña
        if($this->session->status !== 'active')
            redirect(base_url('teachers/dashboard/editPassword'), 'refresh');


        // Si es un estudiante no puede entrar aquí
        if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        //var_dump("formSubmit = ".$formSubmit);
        if( $formSubmit === 'Cancelar' or $formSubmit === 'Cancel·lar' or $formSubmit ===  'Cancel') {
            // Redirección a la vista principal
            redirect(base_url('teachers/challenges'), 'refresh');
        }
    }

    /**
     * Replaces an array key and preserves the original
     * order.
     *
     * @param $array The array in question.
     * @param $oldKey The key that you want to replace.
     * @param $newKey The name of the new key.
     *
     * @return array
     */
    private function replaceArrayKey($array, $oldKey, $newKey){
        //If the old key doesn't exist, we can't replace it...
        if(!isset($array[$oldKey])){
            return $array;
        }
        //Get a list of all keys in the array.
        $arrayKeys = array_keys($array);
        //Replace the key in our $arrayKeys array.
        $oldKeyIndex = array_search($oldKey, $arrayKeys);
        $arrayKeys[$oldKeyIndex] = $newKey;
        //Combine them back into one array.
        $newArray =  array_combine($arrayKeys, $array);
        return $newArray;
    }
}