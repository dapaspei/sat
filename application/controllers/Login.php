<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Validación de datos en los formularios, sesión y variables globales
        $this->load->library(array('form_validation', 'globals'));
        // Cargamos el helper de validación de login
        $this->load->helper(array('login_rules'));
        // Cargamos el modelo Auth
        $this->load->model('Auth');
        // Helpers
        $this->load->helper('url');
        $this->load->helper('array');
    }
    /**
     * Función principal por defecto
     */
    public function index($data = '') {
        // Comprobación de credenciales
        if($this->session->userdata('is_logged')) {
            if ($this->session->role === 'student') {
                redirect(base_url('students/dashboard'), 'refresh');
            } else if ($this->session->role  === 'admin') {
                redirect(base_url('admin/dashboard'), 'refresh');
            } if ($this->session->role  === 'teacher') {
                redirect(base_url('teachers/dashboard'), 'refresh');
            }
        }
        else 
            $this->load->view('login', $data);
    }
    

    /**
     * Función para validar un username & contraseña
     */
    public function validate() {

        // Tailwind CSS Framework
        $this->form_validation->set_error_delimiters('<div class="bg-orange-100 border-l-4 border-orange-500 text-orange-700 p-4" role="alert">');

        $rules = getLoginRules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            // Volvemos a la pantalla de login
            //$this->load->view('login');
            $errors = array (
                'username' => form_error('username'),
                'password' =>  form_error('password'),
            );
            //echo json_encode($errors);
            //$this->output->set_status_header(400);
            //$this->load->view('login');

            // Recargo la página con los errores
            $this->index($errors);
            // Redirect no guarda los errores.
            //redirect('/');

       } else {
            // Si la validación de campos es correcta ..
            $username = $this->input->post('username');
            $pass = $this->input->post('password');

            // Comprobamos credenciales

            $res = $this->Auth->login($username,$pass);
            // Si no tienen mensaje es que ha encontrado un usuario correcto.
            if (element('error_msg', $res) == NULL) {
                //var_dump($res);
                // Si estamos en septiembre o más, es un curso escolar nuevo
                if(intval(date('m')) >= 9) {
                    $school_year = "20".date('y').'-'.date('y', strtotime('+1 Years'));
                    // $school_year = "20".date('y', strtotime('+1 Years')).'-'.date('y', strtotime('+2 Years'));
                } else {
                    $school_year = "20".date('y', strtotime('-1 Years')).'-'.date('y');
                }                
                
                $data = array (
                    'id' => $res['tupla']->id,
                    'role' => $res['tupla']->role,
                    'username' => $res['tupla']->username,
                    'status' => $res['tupla']->status,
                    'is_logged' => TRUE,
                    'school_year' => $school_year // Añadida variable para controlar el curso escolar y así mostrar los retos correspondientes.
                );
                //var_dump($data);
                $this->session->set_userdata($data);
                $this->session->set_flashdata('success_msg', 'Bienvenid@ '.$data['username']);

                /** Comprobar el tipo de usuario y redirigir a su dashboard */
                if ($data['role']=='student') {
                    /* if ($res['tupla']->status === 'first_time'){
                        redirect(base_url('students/dashboard/editPassword'), 'refresh');
                    } */
                    redirect('students/dashboard', 'refresh');
                } else if ($data['role']=='admin') {
                    redirect('admin/dashboard', 'refresh');
                } if ($data['role']=='teacher') {
                    if ($res['tupla']->status === 'first_time'){
                        redirect(base_url('teachers/dashboard/editPassword'), 'refresh');
                    }
                    redirect('teachers/dashboard', 'refresh');
                }

            } else {
                // Si es incorrecto, error
                $this->output->set_status_header(401);
                $errors = array (
                    'errors' => $res['error_msg']
                );
                // Recargo la página con los errores
                $this->index($errors);
            }
       }
    }
    /**
     * Función para cerrar la sesión
     */
    public function logout() {
        $data = array ('id', 'role', 'username', 'is_logged');
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }
}