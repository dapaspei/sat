<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Utilizamos funciones de menu
        //$this->load->helper(array('menu'));
        $this->load->helper(array('users_rules'));
        // Cargamos el modelo de usuarios, teachers y students
        $this->load->model('Users_model');
        $this->load->model('Teachers_model');
        $this->load->model('Students_model');
        // Validación de datos en los formularios
        $this->load->library('form_validation');
        
    }
    /**
     * Función principal por defecto
     */
    public function index($data = '') 
    {
        //$data['menu'] = main_menu();
        //$this->load->view('register', $data);
        //$query = $this->db->get('users');
        
        //$this->load->view('register', $data);
        
        if($this->isLogged()) {
            $this->load->view('register', $data);
        } else {
            redirect('login');
        }
    }

    /**
     *  Método para crear un usuario
     */
    public function create() 
    {
        // Recoger los datos del formulario
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');
        $role = $this->input->post('role');

        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $rules = getCreateUsersRules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {
             // Mandamos la variable del menú para que lo muestre
             //$data['menu'] = main_menu();
            $errors = array (
                'firstname' => form_error('firstname'),
                'lastname' => form_error('lastname'),
                'email' => form_error('email'),
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
                'username' => form_error('username'),
                'role' => form_error('role')
            );
            $this->load->view('register', $errors);
        } else {
            // Si no hay errores de validación, intentamos crear el registro
            $datos_usuario = array (
                'username' => $username,
                'email' => $email,
                'role' => $role,
                'password' => password_hash($password, PASSWORD_DEFAULT)
            );

            // Insertamos el usuario
            $user_id = $this->Users_model->create($datos_usuario);
            
            // Comprobamos si hay error.
            if (!array_key_exists('id', $user_id)){
                //$data['error_msg'] = 'Ocurrió un error al introducir los datos del usuario';
                //$this->load->view('register', $data);
                $this->load->view('register', $user_id);
            } else {
                $datos = array (
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'user_id' => $user_id['id'],
                );
                
                // Si es un profesor -> rellenar la tabla de teacher
                if ($role === "teacher") {
                    // Mandamos la variable del menú para que lo muestre
                   
                    if(!$this->Teachers_model->create($datos)) {
                        $data['error_msg'] = 'Ocurrió un error al introducir los datos del profesor';
                    } else  {
                        redirect('register');
                    }                
                }
                // Si es un estudiante -> rellenar la tabla 'students' 
                else if ($role === "student") {
                    if(!$this->Students_model->create($datos)) {
                        $data['error_msg'] = 'Ocurrió un error al introducir los datos del estudiante';
                        //$this->load->view('register', $data);   
                    } else  {
                        redirect('register');
                    }     
                }
                else {
                    // Es un administrador
                    redirect('register');
                }
                $this->load->view('register', $data);
            }            
        }
    }
    
    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }
}