<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','email','pagination'));
        $this->email->initialize();

        $this->load->helper(array('students_rules', 'teachers_rules', 'admin_rules', 'file'));
        // Cargar el modelo de usuarios
        $this->load->model('users_model');
        // Cargar el modelo de los profesores & estudiantes
        $this->load->model('teachers_model');
        $this->load->model('students_model');
    }

    /**
     * Función principal por defecto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de permisos
        $this->checkPermission();

        $data = $this->users_model->getUsers();
        // Para hacer la paginación
        $config['base_url']= base_url('admin/dashboard/index');
        // Elementos por página
        $config['per_page']= 18;
        $config['total_rows']= count($data);

        // Paginación con TailwindCSS
        $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        $config['full_tag_close']= '</ul></div>';
        $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['num_tag_close']= '</li>';
        $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teal-500 border-r border-grey-light px-3 py-2 active">';
        $config['cur_tag_close']= '</li>';
        $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['next_tag_close']= '</li>';
        $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['prev_tag_close']= '</li>';
        $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['first_tag_close']= '</li>';
        $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        $config['last_tag_close']= '</li>';


        $this->pagination->initialize($config);

        // $page = $this->users_model->get_Paginate($config['per_page'],$offset);

        // $vista = $this->load->view('admin/users_show', ['data' => $page], TRUE);
        $page = $this->users_model->get_Paginate($config['per_page'],$offset, $current_school_year);

        $vista = $this->load->view('admin/users_show', array('data' => $page, 'school_year' => $current_school_year), TRUE);

        $this->getTemplate($vista);
    }

    /** ***************************************************
     *  ***********           CREATE            *********** 
    ***************************************************  */

    /**
     * Función para crear un STUDENT en la base de datos
     */
    public function createStudent() {
        // Comprobación de permisos
        $this->checkPermission();
            
        $vista = $this->load->view('admin/student_create','', TRUE);
        $this->getTemplate($vista);
    }
    /**
     * Función para crear un TEACHER en la base de datos
     */
    public function createTeacher() {
        // Comprobación de permisos
        $this->checkPermission();
            
        $vista = $this->load->view('admin/teacher_create','', TRUE);
        $this->getTemplate($vista);
    }
    /**
     * Función para crear un TEACHER COORDINATOR en la base de datos
     */
    public function createTeacherCoord() {
        // Comprobación de permisos
        $this->checkPermission();
            
        $vista = $this->load->view('admin/teacher_createcoord','', TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para guardar los datos de un ESTUDIANTE
     */
    public function storeStudent() {
        // Comprobación de permisos
        $this->checkPermission();
        // Recoger los datos del formulario
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');
        $teaching_levels = $this->input->post('teaching_levels');
        
        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $this->form_validation->set_rules(getCreateStudentRules());

        if ($this->form_validation->run() === FALSE) {
            $errors = array (
                'firstname' => form_error('firstname'),
                'lastname' => form_error('lastname'),
                'email' => form_error('email'),
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
                'username' => form_error('username'),
            );
            $vista = $this->load->view('admin/student_create','', TRUE);
            $this->getTemplate($vista);
            return;
            
        } 
        // Si no hay errores de validación, intentamos crear el registro
        $datos_usuario = array (
            'username' => $username,
            'email' => $email,
            'role' => 'student',
            'password' => password_hash($password, PASSWORD_DEFAULT)
        );

        // Insertamos el usuario
        $user_id = $this->users_model->create($datos_usuario);
        
        // Comprobamos si hay error.
        if (!array_key_exists('id', $user_id)){
            $this->session->set_flashdata('error_msg', $user_id['error_msg']);
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('admin/student_create', $datos_usuario, TRUE);
            $this->getTemplate($vista);
            return;
        }
        
        print_r($teaching_levels);
        // exit(0);
        // Comprobamos que tenga asigando al menos un curso y no más de uno
        if( empty($teaching_levels) or (count($teaching_levels) > 1) ) {
            $this->session->set_flashdata('error_msg', 'No se ha asignado ningún curso al usuario "'.$username.'", o tiene demasiados cursos asignados.');
            $this->session->set_flashdata('success_msg', '');
            // Borramos el usuario acabado de crear.
            $this->users_model->delete($user_id['id']);
            $vista = $this->load->view('admin/student_create','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // Un único level
        // var_dump($teaching_levels);
        // exit(0);
        // $teaching_levels = array();
        // foreach($levels as $level)
        //     $teaching_levels[] = array($level);
        // // Varios levels
        // // Creamos el array con los niveles que da
        // $teaching_levels = array();
        // foreach($levels as $level) {
        //     $teaching_levels[] = $level;
        
        // Creamos el array de datos
        $datos = array (
            'firstname' => $firstname,
            'lastname' => $lastname,
            'user_id' => $user_id['id'],
            //'challenge_id' => '0',
            'teaching_levels' => $teaching_levels,
        );
        
        // Insertamos los datos del STUDENT
        if(!$this->students_model->create($datos)) {
            $this->session->set_flashdata('error_msg', 'Ocurrió un error al introducir los datos del ususario "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            // Borramos el usuario acabado de crear.
            $this->users_model->delete($user_id);
            $vista = $this->load->view('admin/student_create', $datos, TRUE);
            $this->getTemplate($vista);
            return;
        } 
        
        // Estudiante  insertado correctamente
        redirect(base_url('admin/dashboard'), 'refresh');     
    }
    /**
     * Función para guardar los datos de un PROFESOR
     */
    public function storeTeacher() {
        // Comprobación de permisos
        $this->checkPermission();
        // Recoger los datos del formulario
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');
        $challenge_coordinator = $this->input->post('challenge_coordinator');
        $project_coordinator = $this->input->post('project_coordinator');
        if(empty($challenge_coordinator)) $challenge_coordinator = '0';
        if(empty($project_coordinator)) $project_coordinator = '0';

        $teaching_levels = $this->input->post('teaching_levels');
        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $this->form_validation->set_rules(getCreateTeacherRules());

        if ($this->form_validation->run() === FALSE) {
            $errors = array (
                'firstname' => form_error('firstname'),
                'lastname' => form_error('lastname'),
                'email' => form_error('email'),
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
                'username' => form_error('username'),
            );
            $vista = $this->load->view('admin/teacher_create','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        
        // Si no hay errores de validación, intentamos crear el registro
        $datos_usuario = array (
            'username' => $username,
            'email' => $email,
            'role' => 'teacher',
            'password' => password_hash($password, PASSWORD_DEFAULT)
        );

        // Insertamos el usuario
        $user_id = $this->users_model->create($datos_usuario);
        
        // Comprobamos si hay error.
        if (!array_key_exists('id', $user_id)){
            $this->session->set_flashdata('error_msg', $user_id['error_msg']);
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('admin/teacher_create', $datos_usuario, TRUE);
            $this->getTemplate($vista);
            return;
        }
        
        // Comprobamos que tenga asigando al menos un curso
        if(empty($teaching_levels)) {
            $this->session->set_flashdata('error_msg', 'No se ha asignado ningún curso al profesor "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            // Borramos el usuario acabado de crear.
            $this->users_model->delete($user_id['id']);
            $vista = $this->load->view('admin/teacher_create','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // Un único level
        // $teaching_levels = array();
        // foreach($levels as $level)
        //     $teaching_levels[] = array($level);

        // Uno o Varios levels ???
        // Creamos el array con los niveles de docencia
        if (count($teaching_levels) === 1) {
            $levels[] = $teaching_levels[0];
        } else {
            $levels = array();
            foreach($teaching_levels as $level)
                $levels[] = $level;
        }

        // Creamos el array de datos
        $datos = array (
            'firstname' => $firstname,
            'lastname' => $lastname,
            'user_id' => $user_id['id'],
            //'challenge_id' => '0',
            'challenge_coordinator' => $challenge_coordinator,
            'project_coordinator' => $project_coordinator,
            'teaching_levels' => $levels,
        );
       
        // Insertamos los datos en la tabla de TEACHERS
        if(!$this->teachers_model->create($datos)) {
            $this->session->set_flashdata('error_msg', 'Ocurrió un error al introducir los datos del ususario "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            // Borramos el usuario acabado de crear.
            $this->users_model->delete($user_id['id']);
            $vista = $this->load->view('admin/teacher_create','', TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // Teacher insertado correctamente
        redirect(base_url('admin/dashboard'), 'refresh');                
    }

    /** ***************************************************
     *  ***********            EDIT             *********** 
    ***************************************************  */

    /**
     * Función para editar un STUDENT
     */
    public function edit($id = 0) {
        // Comprobación de permisos
        $this->checkPermission();
        // Obtenemos la información del usuario
        $user = $this->users_model->getUser($id);
        if($user['role'] === 'student'){
            $student = $this->students_model->getStudent($id);
            $user_complete = $user + $student;
            $user_view = 'admin/student_edit';
        } else {
            $teacher = $this->teachers_model->getTeacher($id);
            $user_complete = $user + $teacher;
            $user_view = 'admin/teacher_edit';
        }
        $view = $this->load->view($user_view, array('user' => $user_complete) ,TRUE);
        $this->getTemplate($view);
    }

    /**
     * Función para actualizar los datos de un STUDENT
     */
    public function updateStudent() {
        // Comprobación de permisos
        $this->checkPermission();
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') !== 'POST')
            show_404();

        // Capturamos los datos del formulario
        $id = $this->input->post('id'); // Oculto
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');
        $teaching_levels = $this->input->post('teaching_levels');
        // Viene en un array con un único dato
        
        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $this->form_validation->set_rules(getUpdateStudentRules());

        if ($this->form_validation->run() === FALSE) {
            // Errores de validación.
            $errors = array (
                'firstname' => form_error('firstname'),
                'lastname' => form_error('lastname'),
                'email' => form_error('email'),
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
                'username' => form_error('username'),
            );
            //$this->load->view('dashboar_admin', $errors);
            $vista = $this->load->view('admin/student_create','', TRUE);
            $this->getTemplate($vista);
        } 
        
        if (!empty($password)) {
            $user = array (
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'username' => $username,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                //'teaching_levels' => $teaching_levels[0],
                'teaching_levels' => array($teaching_levels),

            );
        } else {
            $user = array (
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'username' => $username,
                'teaching_levels' => array($teaching_levels),
                // 'teaching_levels' => $teaching_levels[0],
            );
        } 
        
        // Comprobrobamos que tenga sólo un curso.
        if(count($teaching_levels) !== 1) {
            $this->session->set_flashdata('error_msg', 'Sólo se puede asignar un cursos al ususario "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('admin/student_edit', array('user' => $user), TRUE);
            $this->getTemplate($vista);
            return;
        }
        
        // Actualizamos los datos de usuario
        if(!$this->users_model->updateStudent($id, $user)) {
            $this->session->set_flashdata('error_msg', 'Ocurrió un <strong>error al modificar</strong> los datos del ususario "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('admin/student_edit', array('user' => $user), TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // Redirección a la vista de Estudiates
        $this->session->set_flashdata('success_msg', 'El usuario "'.$username.'" ha sido <strong>actualizado correctamente</strong>.');
        redirect(base_url('admin/dashboard'));
    }
    /**
     * Función para actualizar los datos de un TEACHER
     */
    public function updateTeacher() {
        // Comprobación de permisos
        $this->checkPermission();
        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') !== 'POST')
            show_404();

        // Capturamos los datos del formulario
        $id = $this->input->post('id'); // Oculto
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');
        $challenge_coordinator = $this->input->post('challenge_coordinator'); 
        //$challenge_coordinator = $challenge_coordinator ?? '0';
        $project_coordinator = $this->input->post('project_coordinator'); 
        //$project_coordinator = $project_coordinator ?? '0';
        if(empty($challenge_coordinator)) $challenge_coordinator = '0';
        if(empty($project_coordinator)) $project_coordinator = '0';
        $teaching_levels = $this->input->post('teaching_levels');
        // var_dump($challenge_coordinator);
        // var_dump($project_coordinator);

        // exit(0);
        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $this->form_validation->set_rules(getUpdateTeacherRules());
        if ($this->form_validation->run() === FALSE) {
            // Errores de validación.
            $errors = array (
                'firstname' => form_error('firstname'),
                'lastname' => form_error('lastname'),
                'email' => form_error('email'),
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
                'username' => form_error('username'),
            );
            $vista = $this->load->view('admin/teacher_edit','', TRUE);
            $this->getTemplate($vista);
            return;
        } 

        $levels = array();
        foreach($teaching_levels as $level)
            $levels[] = array($level);

        // Si no hay errores de validación, intentamos modificar el registro
        if (!empty($password)) {
            $user = array (
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'username' => $username,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'challenge_coordinator' => $challenge_coordinator,
                'project_coordinator' => $project_coordinator,
                'teaching_levels' => $teaching_levels,
            );
        } else {
            $user = array (
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'username' => $username,
                'challenge_coordinator' => $challenge_coordinator,
                'project_coordinator' => $project_coordinator,
                'teaching_levels' => $teaching_levels,
            );
        } 
        // var_dump($user);
        // exit(0);

        // Comprobrobamos que tenga al menos un curso.
        if(count($teaching_levels) === 0) {
            $this->session->set_flashdata('error_msg', 'Al menos un curso se ha de asignar al profesor "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('admin/teacher_edit', array('user' => $user), TRUE);
            $this->getTemplate($vista);
            return;
        }

        // Actualizamos los datos de usuario
        if(!$this->users_model->updateTeacher($id, $user)) {
            $this->session->set_flashdata('error_msg', 'Ocurrió un <strong>error al modificar</strong> los datos del ususario "'.$username.'".');
            $this->session->set_flashdata('success_msg', '');
            $vista = $this->load->view('admin/teacher_edit', array('user' => $user), TRUE);
            $this->getTemplate($vista);
            return;
        } 
        // Redirección a la vista de Estudiates
        $this->session->set_flashdata('success_msg', 'El profesor/a "'.$username.'" ha sido <strong>actualizado correctamente</strong>.');
        redirect(base_url('admin/dashboard'));

    }

    /**
     * Función para borrar Usuarios
     */
    public function delete($id) {
        // Comprobación de permisos
        $this->checkPermission();

        // Comprobar si es estudiante u otra cosa
        $role = $this->users_model->getRole($id);

        // // Borramos el usuario de la tabla 'users'
        // if(!$this->users_model->delete($id)){
        //     // ERROR No se ha podido borrar el usuario
        //     show_500();
        // } 

        // // Borramos el estudiante o profesor de su tabla
        // if (array_values($role)[0] === "student") {
        //     if(!$this->students_model->delete($id))
        //         show_500();	
        // } else {
        //     if(!$this->teachers_model->delete($id))
        //         show_500();	
        // }

        // // Se ha borrado con éxito. Volver a la pantalla de usuarios
        // redirect('admin/dashboard');
        // -----
        // Borramos el estudiante o profesor de su tabla
        if (array_values($role)[0] === "student") {
            if(!$this->students_model->delete($id))
                show_500();	
        } else {
            if(!$this->teachers_model->delete($id))
                show_500();	
        }
        
        // Ahora borramos el usuario de la tabla 'users'
        if(!$this->users_model->delete($id)){
            // ERROR No se ha podido borrar el usuario
            show_500();
        } else {
            // Se ha borrado con éxito. Volver a la pantalla de usuarios
            redirect('admin/dashboard');
        }
        
    }

    /**
     * Cargar la vista de importación de usuarios
     */
    public function import() {
        // Comprobación de permisos
        $this->checkPermission();

        // Cargamos la vista para importar usuarios
        $vista = $this->load->view('admin/users_import', '', TRUE);
        $this->getTemplate($vista);
    }
    /**
     * Importa usuarios de un fichero .csv
     */
    public function importUsers() {
        // Comprobación de permisos
        $this->checkPermission();

        $data = array();
        $memData = array();
        // $fichero = $this->input->post('file');
        // $importSubmit = $this->input->post('importSubmit');
        // var_dump($fichero);
        // var_dump("<br>".$importSubmit);
        // exit(0);
        
        // If import request is submitted
        if($this->input->post('importSubmit')){
            // Form field validation rules
            $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');
            //$this->form_validation->set_rules('file', 'CSV file', 'trim|callback_chk_attachment');
            
            // Validate submitted form data
            if($this->form_validation->run() === TRUE){
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
                
                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');
                    
                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
                    // var_dump($csvData);
                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row) { 
                            // var_dump($row);
                            $rowCount++;
                            // Prepare data for DB insertion
                            // Transformarmos los niveles de docencia en un array de strings
                            $levels = explode(":", $row['teaching_levels']);
                            $memData = array(
                                // User
                                'username' => $row['username'],
                                'password' => password_hash($row['password'], PASSWORD_DEFAULT),
                                'email' => $row['email'],
                                'role' => $row['role'],
                                // Students
                                'firstname' => $row['firstname'],
                                'lastname' => $row['lastname'],
                                's_group' => $row['student_group'],
                                's_team' => $row['student_team'],
                                's_role' => $row['student_role'],
                                'teaching_levels' => $levels,
                                // teacher
                                't_role' => $row['teacher_role'],
                                'department' => $row['department'],
                                'challenge_coordinator' => $row['challenge_coordinator'],
                                'project_coordinator' => $row['project_coordinator'],
                                'school_year' => $row['school_year'],
                            );
                            // print_r("<br>schoo_year_file=>".$row['school_year']);
                            // var_dump("<br>teaching_levels=".json_encode($levels));
                            // exit(0);
                            //var_dump('Teaching_levels = ' . $row['teaching_levels'].'<br>');
                            // Check whether email already exists in the database
                            $con = array(
                                'where' => array(
                                    'email' => $row['email'],
                                    'username' => $row['username']
                                ),
                                'returnType' => 'count'
                            );

                            $prevCount = $this->users_model->getRows($con);
                            //var_dump("prevCount = ".$prevCount."<br>");
                            if($prevCount > 0){
                                // Update member data
                                // $condition = array(
                                //     'username' => $row['username'],
                                //     'email' => $row['email'],
                                // );
                                //$update = $this->users_model->insert($memData, $condition);

                                // Obtenemos el identificador del usuario
                                // var_dump("username=".json_encode($row['username']));
                                // exit(0);
                                $user_id = $this->users_model->getUserId($row['username'], $row['email']);
                                if ($row['role'] === 'student')
                                    $update = $this->users_model->updateStudent($user_id, $memData);
                                else 
                                    $update = $this->users_model->updateTeacher($user_id, $memData);

                                if($update)
                                    $updateCount++;
                                    
                            }else{
                                // Insert member data
                                $insert = $this->users_model->insertUserImported($memData);
                                
                                if($insert){
                                    $insertCount++;
                                }
                            }
                        }
                        
                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'Usuarios importados correctamente. Usuarios totales ('.$rowCount.') | Insertados ('.$insertCount.') | Actualizados ('.$updateCount.') | No insertados ('.$notAddCount.')';
                        $this->session->set_flashdata('succes_msg', $successMsg);

                    }
                }else{
                    $this->session->set_flashdata('error_msg', 'Error en la carga del fichero. Por favor, vuelve a intentarlo.');
                    redirect(base_url('admin/dashboard'), 'refresh');

                }
            }else{
                $this->session->set_flashdata('error_msg', 'Fichero inválido. Por favor, selecciona un fichero CSV válido.');
                redirect(base_url('admin/dashboard'), 'refresh');

            }
        } else {
            $this->session->set_flashdata('error_msg', 'Formulario de importación erróneo.');
            redirect(base_url('admin/dashboard'), 'refresh');

        }
        // $this->session->set_flashdata('success_msg', 'Usuarios importados <strong>CORRECTAMENTE</strong>.');
        redirect(base_url('admin/dashboard'), 'refresh');
    }
    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 
            'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 
            'application/vnd.msexcel', 'text/plain', 'csv');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }

    /** 
     * Función para enviar emails
     */
    public function sendEmail($user = '') {
        // Comprobación de permisos
        $this->checkPermission();

        $this->email->from('soc@iescotesbaixes.org', 'Cotes AvaluApp');
        //$this->email->to($student['email']);
        $this->email->to('dapaspei@gmail.com');
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');

        $this->email->subject('Datos de cuenta');
        $this->email->message('Testing the email class.');

        $this->email->send();
    } 

    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('admin/header', '', TRUE), 
            'nav' => $this->load->view('teachers/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('admin/footer', '', TRUE),
            
        );

        $this->load->view('dashboard_admin', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Función para comprobar si el usuario es 'admin' o no
     */
    private function checkIfAdmin() {
        // Si es un profesor o estudiante no puede entrar aquí
        if($this->session->role === 'teacher')
            redirect('teachers/dashboard', 'refresh');
        else if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');
    }

    /**
     * Función para comprobar los permisos para acceder a las funciones
     */
    private function checkPermission() {
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es un estudiante o profesor no puede entrar aquí
        $this->checkIfAdmin();

        // Si se ha pulsado el botón CANCELAR del formulario volvemos al inicio
        $formSubmit = $this->input->post('submit');
        if( $formSubmit === 'Cancelar' ) {
            // Redirección a la vista principal
            redirect(base_url('admin/dashboard'), 'refresh');
        }
    }
}