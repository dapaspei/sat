<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Cargamos la librería de validación de formularios
        $this->load->library(array('form_validation','email','pagination'));
        $this->email->initialize();

        // Cambiar el formato del tipo de error
        //$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

        $this->load->helper(array('students_rules', 'teachers_rules', 'admin_rules'));
        // Cargar el modelo de usuarios
        $this->load->model('users_model');
        // Cargar el modelo de los profesores & estudiantes
        $this->load->model('teachers_model');
        $this->load->model('students_model');
        // No los cargo, Cada usuario/teacher/student puede cambiar solo sus datos
        // Load file helper
        $this->load->helper('file');

    }

    /**
     * Función principal por defecto
     * @param offset Numero de página
     */
    public function index($offset = 0) {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login', 'refresh');

        // Si es un profesor o estudiante no puede entrar aquí
        if($this->session->role === 'teacher')
            redirect('teachers/dashboard', 'refresh');
        else if ($this->session->role === 'student')
            redirect('students/dashboard', 'refresh');

        $current_school_year = $this->session->school_year;


        $data = $this->users_model->getUsers();
        // Para hacer la paginación
        $config['base_url']= base_url('admin/dashboard/index');
        // Elementos por página
        $config['per_page']= 18;
        $config['total_rows']= count($data);

        // Paginación con TailwindCSS
        $config['full_tag_open']= '<div class="flex justify-center text-center"><ul class="flex list-reset border border-grey-light rounded w-auto font-sans">';
        $config['full_tag_close']= '</ul></div>';
        $config['num_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['num_tag_close']= '</li>';
        $config['cur_tag_open']= '<li class="block hover:text-white hover:bg-teal-600 text-white bg-teal-500 border-r border-grey-light px-3 py-2 active">';
        $config['cur_tag_close']= '</li>';
        $config['next_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['next_tag_close']= '</li>';
        $config['prev_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['prev_tag_close']= '</li>';
        $config['first_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 border-r border-grey-light px-3 py-2">';
        $config['first_tag_close']= '</li>';
        $config['last_tag_open']= '<li class="block hover:text-white hover:bg-teal-500 text-teal-500 px-3 py-2">';
        $config['last_tag_close']= '</li>';


        $this->pagination->initialize($config);

        $page = $this->users_model->get_Paginate($config['per_page'],$offset);

        $vista = $this->load->view('admin/users_show', array('data' => $page, 'school_year' => $current_school_year), TRUE);
        $this->getTemplate($vista);
        
        // $params['limit'] = RECORDS_PER_PAGE; 
        // $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        // $config = $this->config->item('pagination');
        // $config['base_url'] = site_url('user/index?');
        // $config['total_rows'] = $this->User_model->get_all_users_count();
        // $this->pagination->initialize($config);

        // $data['users'] = $this->User_model->get_all_users($params);
        
        // $data['_view'] = 'user/index';
        // $this->load->view('layouts/main',$data);
    }


    /**
     * Cargar la vista de importación de usuarios
     */
    public function import() {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login');
        

        // Cargamos la vista para importar usuarios
        $vista = $this->load->view('admin/users_import', '', TRUE);
        $this->getTemplate($vista);
    }
    /**
     * Importa usuarios de un fichero .csv
     */
    public function importUsers() {
        $data = array();
        $memData = array();
        
        // If import request is submitted
        if($this->input->post('importSubmit')){
            // Form field validation rules
            $this->form_validation->set_rules('file', 'CSV file', 'callback_file_check');
            
            // Validate submitted form data
            if($this->form_validation->run() == true){
                $insertCount = $updateCount = $rowCount = $notAddCount = 0;
                
                // If file uploaded
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    // Load CSV reader library
                    $this->load->library('CSVReader');
                    
                    // Parse data from CSV file
                    $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);

                    // Insert/update CSV data into database
                    if(!empty($csvData)){
                        foreach($csvData as $row){ $rowCount++;
                            
                            // Prepare data for DB insertion
                            $memData = array(
                                //firstname,lastname,email,username,password,group,role,avatar,student_role,team
                                'firstname' => $row['firstname'],
                                'lastname' => $row['lastname'],
                                'username' => $row['username'],
                                'email' => $row['email'],
                                //'password' => $row['password'],
                                'password' => password_hash($row['password'], PASSWORD_DEFAULT),
                                'group' => $row['group'],
                                'role' => $row['role'],
                                'avatar' => $row['avatar'],
                                'student_role' => $row['student_role'],
                                'student_team' => $row['student_team'],
                            );
                            
                            // Check whether email already exists in the database
                            $con = array(
                                'where' => array(
                                    'email' => $row['email']
                                ),
                                'returnType' => 'count'
                            );

                            $prevCount = $this->users_model->getRows($con);
                            
                            if($prevCount > 0){
                                // Update member data
                                $condition = array('email' => $row['email']);
                                $update = $this->users_model->insert($memData, $condition);
                                
                                if($update){
                                    $updateCount++;
                                }
                            }else{
                                // Insert member data
                                $insert = $this->users_model->insertUser($memData);
                                
                                if($insert){
                                    $insertCount++;
                                }
                            }
                        }
                        
                        // Status message with imported data count
                        $notAddCount = ($rowCount - ($insertCount + $updateCount));
                        $successMsg = 'Usuarios importados correctamente. Usuarios totales ('.$rowCount.') | Insertados ('.$insertCount.') | Actualizados ('.$updateCount.') | No insertados ('.$notAddCount.')';
                        //$this->session->set_userdata('success_msg', $successMsg);
                        $this->session->set_flashdata('succes_msg', $successMsg);

                    }
                }else{
                    $this->session->set_flashdata('error_msg', 'Error en la carga del fichero. Por favor, vuelve a intentarlo.');
                    //$this->session->set_userdata('error_msg', 'Error on file upload, please try again.');
                    
                }
            }else{
                //$this->session->set_userdata('error_msg', 'Invalid file, please select only CSV file.');
                $this->session->set_flashdata('error_msg', 'Fichero inválido. Por favor, selecciona un fichero CSV válido.');

            }
        } else {
            $this->session->set_flashdata('error_msg', 'Formulario de importación erróneo.');
        }
        $this->session->set_flashdata('success_msg', 'Sin error.');

        // $vista = $this->load->view('admin/users_import','', TRUE);
        // $this->getTemplate($vista);
        
        redirect('admin/dashboard');
    }
    /*
     * Callback function to check file value and type during validation
     */
    public function file_check($str){
        $allowed_mime_types = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != ""){
            $mime = get_mime_by_extension($_FILES['file']['name']);
            $fileAr = explode('.', $_FILES['file']['name']);
            $ext = end($fileAr);
            if(($ext == 'csv') && in_array($mime, $allowed_mime_types)){
                return true;
            }else{
                $this->form_validation->set_message('file_check', 'Please select only CSV file to upload.');
                return false;
            }
        }else{
            $this->form_validation->set_message('file_check', 'Please select a CSV file to upload.');
            return false;
        }
    }
    /**
     * Función para crear un usuario en la base de datos
     */
    public function create() {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login');
            
        $vista = $this->load->view('admin/user_create','', TRUE);
        $this->getTemplate($vista);
    }

    /**
     * Función para guardar los datos del usuario
     */
    public function store() {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login');
            
        // Recoger los datos del formulario
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password_c = $this->input->post('password_confirm');
        $role = $this->input->post('role');

        // Comprobar datos
        $this->form_validation->set_error_delimiters('<div class="bg-red-100 border-l-4 border-red-500 text-red-700 p-4" role="alert">', '</div>');
        $rules = getCreateUsersRules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {
                // Mandamos la variable del menú para que lo muestre
                //$data['menu'] = main_menu();
            $errors = array (
                'firstname' => form_error('firstname'),
                'lastname' => form_error('lastname'),
                'email' => form_error('email'),
                'password' =>  form_error('password'),
                'password_confirm' => form_error('password_confirm'),
                'username' => form_error('username'),
                'role' => form_error('role')
            );
            //$this->load->view('dashboar_admin', $errors);
            $vista = $this->load->view('admin/user_create','', TRUE);
            $this->getTemplate($vista);
            
        } else {
            // Si no hay errores de validación, intentamos crear el registro
            $datos_usuario = array (
                'username' => $username,
                'email' => $email,
                'role' => $role,
                'password' => password_hash($password, PASSWORD_DEFAULT)
            );

            // Insertamos el usuario
            $user_id = $this->Users_model->create($datos_usuario);
            
            // Comprobamos si hay error.
            if (!array_key_exists('id', $user_id)){
                //$data['error_msg'] = 'Ocurrió un error al introducir los datos del usuario';
                //$this->load->view('dashboard_admin', $user_id);
                $vista = $this->load->view('admin/user_create','', TRUE);
                $this->getTemplate($vista);
        
            } else {
                $datos = array (
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'user_id' => $user_id['id'],
                );
                
                // Si es un profesor -> rellenar la tabla de teacher
                if ($role === "teacher" || $role === "admin") {
                    // Mandamos la variable del menú para que lo muestre
                    
                    if(!$this->teachers_model->create($datos)) {
                        $data['error_msg'] = 'Ocurrió un error al introducir los datos del profesor';
                    } else  {
                        // Teacher insertado correctamente
                        redirect('register');
                    }                
                }
                // Si es un estudiante -> rellenar la tabla 'students' 
                else if ($role === "student") {
                    if(!$this->students_model->create($datos)) {
                        $data['error_msg'] = 'Ocurrió un error al introducir los datos del estudiante';
                        //$this->load->view('register', $data);   
                    } else  {
                        // Estudiante  insertado correctamente
                        redirect('admin/dashboard');
                    }     
                }
                //$this->load->view('dashboard_admin', $data);
                $vista = $this->load->view('admin/user_create','', TRUE);
                $this->getTemplate($vista);
        
            }            
        }
    }

    /**
     * Función para actualizar los datos de un estudiante
     */
    public function update() {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login');

        // Para evitar acceder a la url sin id de usuario
        if($this->input->server('REQUEST_METHOD') === 'POST') {

            // Capturamos los datos del formulario
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $username = $this->input->post('username');
            $id = $this->input->post('id');
            $email = $this->input->post('email');
            //$role = $this->input->post('role');
            $password = $this->input->post('password');

            $this->form_validation->set_rules(getUpdateUsersRules());
            if( $this->form_validation->run() === FALSE ) {
                // ERRORES
                $this->output->set_status_header(400);
            } else {

                // Si se ha pulsado el botón CANCELAR
                $formSubmit = $this->input->post('submit');
                if( $formSubmit == 'Cancelar' ) {
                    // Redirección a la vista de Estudiates
                    $this->session->set_flashdata('msg', 'El usuario "'.$username.'" no ha sido actualizado.');
                    redirect(base_url('admin/dashboard'));
                }
                
                // Añadimos los campos al array
                
                if ( $password != '') {
                    $user = array (
                        'firstname' => $firstname,
                        'lastname' => $lastname,
                        'email' => $email,
                        //'role' => $role,
                        'username' => $username,
                        'password' => password_hash($password, PASSWORD_DEFAULT));
                } else {
	                $user = array (
	                    'firstname' => $firstname,
	                    'lastname' => $lastname,
	                    'email' => $email,
	                    //'role' => $role,
	                    'username' => $username,
	                    
	                );
                } 

                // $user_data = array (
                //     'firstname' => $firstname,
                //     'lastname' => $lastname,
                // );
                
                //var_dump($user);
                //var_dump($id);
                // Actualizamos los datos de Estudiante o Profesor
                // $role = $this->users_model->getRole($id);
                // if ($role === 'student') {
                //     if(!$this->students_model->update($id, $user_data)) 
                //         $this->output->set_status_header(500);
                    
                // } else {
                //     if(!$this->teachers_model->update($id, $user_data)) 
                //         $this->output->set_status_header(500);
                // }

                // Actualizamos los datos de usuario
                if(!$this->users_model->update($id, $user)) {
                    $this->output->set_status_header(500);
                } else {
                    // Redirección a la vista de Estudiates
                    $this->session->set_flashdata('success_msg', 'El usuario "'.$username.'" ha sido actualizado correctamente.');
                    redirect(base_url('admin/dashboard'));
                }

                
            }
            $vista = $this->load->view('admin/user_edit','', TRUE);
            $this->getTemplate($vista);

        } else {
            show_404();
        }

    }

    /**
     * Función para editar un usuario de la base de datos
     */
    public function edit($id = 0) {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login');
            
        // Obtenemos la información del usuario
        $user = $this->users_model->getUser($id);

        //var_dump($user);
        if($user['role']==='student') {
            $student = $this->students_model->getStudent($id);
            $user_complete = $user + $student;
        } else {
            $teacher = $this->teachers_model->getTeacher($id);
            //var_dump($teacher);
            $user_complete = $user + $teacher;
        }
        //var_dump($user_complete);
         
        $view = $this->load->view('admin/user_edit', array('user' => $user_complete) ,TRUE);
        $this->getTemplate($view);
    }

    /**
     * Función para borrar Usuarios
     */
    public function delete($id) {
        // Comprobación de credenciales
        if(!$this->isLogged())
            redirect('login');

        // Comprobar si es estudiante u otra cosa
        $role = $this->users_model->getRole($id);
        
        // Borramos el estudiante o profesor de su tabla
        if (array_values($role)[0] === "student") {
            if(!$this->students_model->delete($id))
                show_500();	
        } else {
            if(!$this->teachers_model->delete($id))
                show_500();	
        }
        
        // Ahora borramos el usuario de la tabla 'users'
        if(!$this->users_model->delete($id)){
            // ERROR No se ha podido borrar el usuario
            show_500();
        } else {
            // Se ha borrado con éxito. Volver a la pantalla de usuarios
            redirect('admin/dashboard');
        }
    }
    /** 
     * Función para enviar emails
     */
    public function sendEmail($user) {
    //public function sendEmail() {
        $this->email->from('soc@iescotesbaixes.org', 'Cotes AvaluApp');
        //$this->email->to($student['email']);
        $this->email->to('dapaspei@gmail.com');
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');

        $this->email->subject('Datos de cuenta');
        $this->email->message('Testing the email class.');

        $this->email->send();
    } 

    /**
     * Función para cargar todo el template
     */
    public function getTemplate($view) {
        // Paso de parámetros a la vista dashboard para construir la página
        $data = array (
            'head' => $this->load->view('admin/header', '', TRUE), 
            'nav' => $this->load->view('admin/nav', array('school_year' => $this->session->school_year), TRUE), 
            'content' => $view, 
            'footer' => $this->load->view('admin/footer', '', TRUE),
            
        );

        $this->load->view('dashboard_admin', $data);
    }

    /** 
     * Función para comprobar que el usuario esté logado.
     */
    private function isLogged() {
        if($this->session->userdata('is_logged')) {
            return true;
        } else {
            return false;
        }
    }
}