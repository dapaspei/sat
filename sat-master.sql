
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Base de datos: `sat-master`
--
CREATE DATABASE IF NOT EXISTS `sat-master` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sat-master`;

-- --------------------------------------------------------

--
-- Volcado de tabla challenge_assessment
-- ------------------------------------------------------------

CREATE TABLE `challenge_assessment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `challenge_id` int(10) unsigned NOT NULL,
  `student_id` int(10) unsigned NOT NULL,
  `teachers_id` longtext NOT NULL,
  `current_sprint` int(5) unsigned DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `course_date` varchar(10) DEFAULT NULL,
  `learning_outcomes_id` int(10) unsigned NOT NULL,
  `teachers_comments` longtext DEFAULT NULL,
  `self_comments` longtext DEFAULT NULL,
  `peer_comments` longtext DEFAULT NULL,
  `final_mark` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `challenge_id` (`challenge_id`),
  KEY `student_id` (`student_id`),
  KEY `learning_outcomes_id` (`learning_outcomes_id`),
  CONSTRAINT `challenge_assessment_ibfk_1` FOREIGN KEY (`challenge_id`) REFERENCES `challenges` (`id`),
  CONSTRAINT `challenge_assessment_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  CONSTRAINT `challenge_assessment_ibfk_3` FOREIGN KEY (`learning_outcomes_id`) REFERENCES `challenge_learning_outcomes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de tabla challenge_learning_outcomes
-- ------------------------------------------------------------

CREATE TABLE `challenge_learning_outcomes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `self_assessment_mark` decimal(4,2) DEFAULT 0.00,
  `self_assessment_done` tinyint(1) unsigned DEFAULT 0,
  `self_assessment_data` longtext DEFAULT NULL,
  `peer_assessment_mark` decimal(4,2) DEFAULT 0.00,
  `peer_assessment_done` tinyint(1) unsigned DEFAULT 0,
  `peer_assessment_data` longtext DEFAULT NULL,
  `teacher_assessment_mark` decimal(4,2) DEFAULT 0.00,
  `teacher_assessment_done` tinyint(1) unsigned DEFAULT 0,
  `teacher_assessment_data` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 
-- Volcado de tabla challenges
-- ------------------------------------------------------------

CREATE TABLE `challenges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `sprints` tinyint(3) unsigned DEFAULT NULL,
  `sprints_openfor_assess` varchar(250) NOT NULL DEFAULT '[]',
  `sprints_date` longtext NOT NULL,
  `teams` tinyint(2) unsigned NOT NULL,
  `subject` varchar(150) DEFAULT NULL,
  `rubric_id` int(10) unsigned DEFAULT NULL,
  `technical_skills` longtext DEFAULT NULL,
  `t_owner` int(10) DEFAULT NULL,
  `teaching_levels` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubric_id` (`rubric_id`),
  CONSTRAINT `challenges_ibfk_1` FOREIGN KEY (`rubric_id`) REFERENCES `rubrics` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `challenges` WRITE;
/*!40000 ALTER TABLE `challenges` DISABLE KEYS */;

INSERT INTO `challenges` (`id`, `name`, `description`, `year`, `start_date`, `finish_date`, `sprints`, `sprints_openfor_assess`, `sprints_date`, `teams`, `subject`, `rubric_id`, `technical_skills`, `t_owner`, `teaching_levels`)
VALUES
	(1,'Reto de Prueba','Reto de prueba con alumnos de prueba','2021-22','2021-09-06','2021-11-23',4,'[0,0,0,0]','{\"sprints_date1\":\"2021-09-21\",\"sprints_date2\":\"2021-10-06\",\"sprints_date3\":\"2021-10-21\",\"sprints_date4\":\"2021-11-05\"}',3,'Ámbito XXXX de 1º de ESO',2,'{\"1\":\"15\",\"2\":\"25\",\"3\":\"15\",\"4\":\"25\",\"5\":\"20\"}',2,'[\"5ESO-A\",\"5ESO-B\"]');

/*!40000 ALTER TABLE `challenges` ENABLE KEYS */;
UNLOCK TABLES;

-- 
-- Volcado de tabla challenges_students_enrolment
-- ------------------------------------------------------------

CREATE TABLE `challenges_students_enrolment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `challenge_id` int(10) unsigned NOT NULL,
  `s_team` varchar(50) DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index1` (`student_id`,`challenge_id`),
  KEY `challenge_id` (`challenge_id`),
  CONSTRAINT `challenges_students_enrolment_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  CONSTRAINT `challenges_students_enrolment_ibfk_2` FOREIGN KEY (`challenge_id`) REFERENCES `challenges` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `challenges_students_enrolment` WRITE;
/*!40000 ALTER TABLE `challenges_students_enrolment` DISABLE KEYS */;

INSERT INTO `challenges_students_enrolment` (`id`, `student_id`, `challenge_id`, `s_team`)
VALUES
	(1,1,1,'0'),
	(2,2,1,'0'),
	(3,3,1,'0'),
	(4,4,1,'0'),
	(5,5,1,'1'),
	(6,6,1,'1'),
	(7,7,1,'1'),
	(8,8,1,'1'),
	(9,9,1,'2'),
	(10,10,1,'2'),
	(11,11,1,'2'),
	(12,12,1,'2');

/*!40000 ALTER TABLE `challenges_students_enrolment` ENABLE KEYS */;
UNLOCK TABLES;

-- 
-- Volcado de tabla challenges_teachers_enrolment
-- ------------------------------------------------------------

CREATE TABLE `challenges_teachers_enrolment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `challenge_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index2` (`teacher_id`,`challenge_id`),
  KEY `challenge_id` (`challenge_id`),
  CONSTRAINT `challenges_teachers_enrolment_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`),
  CONSTRAINT `challenges_teachers_enrolment_ibfk_2` FOREIGN KEY (`challenge_id`) REFERENCES `challenges` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `challenges_teachers_enrolment` WRITE;
/*!40000 ALTER TABLE `challenges_teachers_enrolment` DISABLE KEYS */;

INSERT INTO `challenges_teachers_enrolment` (`id`, `teacher_id`, `challenge_id`)
VALUES
	(1,2,1);

/*!40000 ALTER TABLE `challenges_teachers_enrolment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla rubrics
-- ------------------------------------------------------------

CREATE TABLE `rubrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cols` tinyint(4) DEFAULT NULL,
  `rows` tinyint(4) DEFAULT NULL,
  `min_value` tinyint(4) DEFAULT 0,
  `max_value` tinyint(4) DEFAULT NULL,
  `rate` decimal(4,2) DEFAULT NULL,
  `data` longtext DEFAULT NULL,
  `t_owner` int(10) NOT NULL,
  `t_available_for` longtext NOT NULL,
  `teaching_levels` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `rubrics` WRITE;
/*!40000 ALTER TABLE `rubrics` DISABLE KEYS */;

INSERT INTO `rubrics` (`id`, `name`, `description`, `cols`, `rows`, `min_value`, `max_value`, `rate`, `data`, `t_owner`, `t_available_for`, `teaching_levels`)
VALUES
	(1,'Rúbrica Genèrica - Reptes','',4,4,0,10,25.00,'[{\"criteria\":\"Actitud\",\"descriptions\":[\"\",\"\",\"\",\"\"]},{\"criteria\":\"Fer Equip\",\"descriptions\":[\"\",\"\",\"\",\"\"]},{\"criteria\":\"Resoluci\\u00f3 de Problemes\",\"descriptions\":[\"\",\"\",\"\",\"\"]},{\"criteria\":\"Planificaci\\u00f3 i\\/o Organitzaci\\u00f3\",\"descriptions\":[\"\",\"\",\"\",\"\"]}]',1,'[\"1\",\"2\"]','[\"1ESO-A\",\"1ESO-B\",\"1ESO-C\",\"1ESO-D\",\"2ESO-A\",\"2ESO-B\",\"2ESO-C\",\"2ESO-D\",\"3ESO-A\",\"3ESO-B\",\"3ESO-C\",\"4ESO-A\",\"4ESO-B\",\"4ESO-C\",\"1BAC-A\",\"1BAC-B\",\"1BAC-TEC\",\"1BAC-D\",\"2BAC-TEC\",\"1CFM-A\",\"1CFM-D\",\"1CFM-E\",\"1CFM-M\",\"1CFM-T\",\"1CFM-U\",\"1CFM-V\",\"1CFM-Z\",\"1CFS-A\",\"1CFS-B\",\"1CFS-D\",\"1CFS-F\",\"1CFS-O\",\"1CFS-P\",\"1CFS-U\",\"2CFM-A\",\"2CFM-D\",\"2CFM-E\",\"2CFM-M\",\"2CFM-T\",\"2CFM-U\",\"2CFM-V\",\"2CFM-Z\",\"2CFS-A\",\"2CFS-B\",\"2CFS-D\",\"2CFS-F\",\"2CFS-O\",\"2CFS-P\",\"5ESO-A\",\"5ESO-B\"]'),
	(2,'3r ESO - Transversals (Treball en Equip)','Rúbrica per valorar el treball en Equip',4,5,0,10,20.00,'[{\"criteria\":\"ACTITUD\",\"descriptions\":[\"No es comporta, ni escolta, ni comparteix idees amb els companys. No ajuda a mantenir la uni\\u00f3 en el grup.\",\"De vegades es comporta adequadament, escolta les idees dels companys i accepta integrar-les. No li preocupa la uni\\u00f3 del grup.\",\"Sol comportar-se, escoltar i compartir les idees dels seus companys, per\\u00f2 no ofereix com integrar-les. Normalment, col\\u00b7labora a mantenir la uni\\u00f3 del grup.\",\"Sempre t\\u00e9 una actitud positiva front al treball. Es comporta adequadament, escolta i comparteix les idees del seus companys i intenta integrar-les. Busca com mantenir la uni\\u00f3 del grup.\"]},{\"criteria\":\"RESPONSABILITAT\",\"descriptions\":[\"No ha complit amb la seua responsabilitat de fer les tasques que li tocaven fer.\",\"No ha fet tot el seu treball i no ha posat massa atenci\\u00f3 al fet que la seua feina contribu\\u00efra als objectius del grup.\",\"Ha realitzat b\\u00e9 el seu treball, per\\u00f2 no ha procurat que el seu treball contribu\\u00efsca als objectius del grup.\",\"Ha realitzat perfectament el seu treball i ha procurat que la seua part haja contribu\\u00eft als objectius del grup.\"]},{\"criteria\":\"PARTICIPACI\\u00d3 I ESFOR\\u00c7\",\"descriptions\":[\"Mai ofereix idees per a fer el treball\\/tasques, ni posposa suggeriments per a la seua millora. A vegades dificulta les propostes d\\u2019uns altres per a aconseguir els objectius del grup.\",\"Algunes vegades ofereix idees per a fer el treball, per\\u00f2 mai proposa suggeriments per a la seua millora. Accepta les propostes d\\u2019uns altres per a aconseguir els objectius del grup, sense esfor\\u00e7ar-se de forma individual\",\"Ofereix idees per a fer el treball, encara que poques vegades proposa suggeriments per a la seua millora. S\\u2019esfor\\u00e7a per a aconseguir els objectius del grup.\",\"Sempre ofereix idees per a fer el treball i proposa suggeriments per a la seua millora. S\\u2019esfor\\u00e7a per a aconseguir els objectius del grup.\"]},{\"criteria\":\"RESOLUCI\\u00d3 DE CONFLICTES\",\"descriptions\":[\"No escolta altres opinions o accepta suggeriments. No proposa alternatives i\\/o li costa acceptar el consens o la soluci\\u00f3.\",\"Poques vegades escolta altres opinions o accepta suggeriments. No proposa alternatives per al consens per\\u00f2 els accepta.\",\"Quasi sempre escolta altres opinions i accepta suggeriments. A vegades proposa alternatives per a consens o soluci\\u00f3.\",\"Sempre escolta altres opinions i accepta suggeriments. Sempre proposa alternatives per al consens o la soluci\\u00f3.\"]},{\"criteria\":\"PLANIFICACI\\u00d3 I ORGANITZACI\\u00d3\",\"descriptions\":[\"No ajuda a que les tasques estiguen ben definides, ben repartides i ben destru\\u00efdes durant el temps del en la planificaci\\u00f3 ni organitzaci\\u00f3 de les tasques a fer. No \\u00e9s aut\\u00f2nom en l\\u2019execuci\\u00f3 de les tasques.\",\"Poques vegades ajuda a que les tasques estiguen ben definides, ben repartides i ben distruibu\\u00efdes en el temps.\",\"Quasi sempre ajuda a que les tasques estiguen ben definides, ben repartides i ben organitzades en el temps.\",\"Sempre ajuda a que les tasques estiguen ben definides, ben repartides i ben organitzades en el temps.\"]}]',2,'[\"1\",\"2\"]','[\"5ESO-A\",\"5ESO-B\"]');

/*!40000 ALTER TABLE `rubrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla sat_sessions
-- ------------------------------------------------------------

CREATE TABLE `sat_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de tabla student_level_enrolled
-- ------------------------------------------------------------

CREATE TABLE `student_level_enrolled` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `school_year` varchar(100) NOT NULL,
  `level` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_student_level_enrolled_index1` (`student_id`,`school_year`,`level`),
  CONSTRAINT `student_level_enrolled_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `student_level_enrolled` WRITE;
/*!40000 ALTER TABLE `student_level_enrolled` DISABLE KEYS */;

INSERT INTO `student_level_enrolled` (`id`, `student_id`, `school_year`, `level`)
VALUES
	(1,1,'2021-22','[\"5ESO-A\"]'),
	(2,2,'2021-22','[\"5ESO-A\"]'),
	(3,3,'2021-22','[\"5ESO-A\"]'),
	(4,4,'2021-22','[\"5ESO-A\"]'),
	(5,5,'2021-22','[\"5ESO-A\"]'),
	(6,6,'2021-22','[\"5ESO-A\"]'),
	(7,7,'2021-22','[\"5ESO-B\"]'),
	(8,8,'2021-22','[\"5ESO-B\"]'),
	(9,9,'2021-22','[\"5ESO-B\"]'),
	(10,10,'2021-22','[\"5ESO-B\"]'),
	(11,11,'2021-22','[\"5ESO-B\"]'),
	(12,12,'2021-22','[\"5ESO-B\"]');

/*!40000 ALTER TABLE `student_level_enrolled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla students
-- ------------------------------------------------------------

CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(250) DEFAULT NULL,
  `s_group` varchar(20) DEFAULT NULL,
  `s_team` varchar(50) DEFAULT NULL,
  `s_role` varchar(50) DEFAULT NULL,
  `course_name` varchar(100) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;

INSERT INTO `students` (`id`, `firstname`, `lastname`, `s_group`, `s_team`, `s_role`, `course_name`, `user_id`)
VALUES
	(1,'ALU1','DEMOSAT','A','','','SECUNDÀRIA 5éA',3),
	(2,'ALU2','DEMOSAT','A','','','SECUNDÀRIA 5éA',4),
	(3,'ALU3','DEMOSAT','A','','','SECUNDÀRIA 5éA',5),
	(4,'ALU4','DEMOSAT','A','','','SECUNDÀRIA 5éA',6),
	(5,'ALU5','DEMOSAT','A','','','SECUNDÀRIA 5éA',7),
	(6,'ALU6','DEMOSAT','A','','','SECUNDÀRIA 5éA',8),
	(7,'ALU7','DEMOSAT','B','','','SECUNDÀRIA 5éB',9),
	(8,'ALU8','DEMOSAT','B','','','SECUNDÀRIA 5éB',10),
	(9,'ALU9','DEMOSAT','B','','','SECUNDÀRIA 5éB',11),
	(10,'ALU10','DEMOSAT','B','','','SECUNDÀRIA 5éB',12),
	(11,'ALU11','DEMOSAT','B','','','SECUNDÀRIA 5éB',13),
	(12,'ALU12','DEMOSAT','B','','','SECUNDÀRIA 5éB',14);

/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla teachers
-- ------------------------------------------------------------

CREATE TABLE `teachers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(250) DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `t_role` varchar(50) DEFAULT NULL,
  `challenge_coordinator` tinyint(1) DEFAULT NULL,
  `project_coordinator` tinyint(1) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `teaching_levels` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;

INSERT INTO `teachers` (`id`, `firstname`, `lastname`, `department`, `t_role`, `challenge_coordinator`, `project_coordinator`, `user_id`, `teaching_levels`)
VALUES
	(1,'ADMIN','SAT','','',0,0,1,'[\"1ESO-A\",\"1ESO-B\",\"1ESO-C\",\"1ESO-D\",  \"2ESO-A\",\"2ESO-B\",\"2ESO-C\",\"2ESO-D\", \"3ESO-A\", \"3ESO-B\", \"3ESO-C\", \"4ESO-A\", \"4ESO-B\", \"4ESO-C\", \"1BAC-A\", \"1BAC-B\", \"1BAC-TEC\", \"1BAC-D\", \"2BAC-TEC\", \"1CFM-A\", \"1CFM-D\", \"1CFM-E\", \"1CFM-M\", \"1CFM-T\", \"1CFM-U\", \"1CFM-V\", \"1CFM-Z\", \"1CFS-A\", \"1CFS-B\", \"1CFS-D\", \"1CFS-F\", \"1CFS-O\", \"1CFS-P\",  \"1CFS-U\", \"2CFM-A\", \"2CFM-D\", \"2CFM-E\", \"2CFM-M\", \"2CFM-T\", \"2CFM-U\", \"2CFM-V\", \"2CFM-Z\", \"2CFS-A\", \"2CFS-B\", \"2CFS-D\", \"2CFS-F\", \"2CFS-O\", \"2CFS-P\"]'),
	(2,'Profesor','De Pruebas',NULL,NULL,0,0,2,'[\"5ESO-A\",\"5ESO-B\"]');

/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla technical_skills
-- ------------------------------------------------------------

CREATE TABLE `technical_skills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `school_year` varchar(10) DEFAULT NULL,
  `has_rubric` tinyint(1) DEFAULT NULL,
  `rubric_id` int(10) unsigned DEFAULT NULL,
  `challenges_in` longtext DEFAULT NULL,
  `t_owner` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rubric_id` (`rubric_id`),
  CONSTRAINT `technical_skills_ibfk_1` FOREIGN KEY (`rubric_id`) REFERENCES `rubrics` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `technical_skills` WRITE;
/*!40000 ALTER TABLE `technical_skills` DISABLE KEYS */;

INSERT INTO `technical_skills` (`id`, `name`, `description`, `school_year`, `has_rubric`, `rubric_id`, `challenges_in`, `t_owner`)
VALUES
	(1,'01-Producte Final','','2021-22',0,NULL,'1,',2),
	(2,'02-Vídeo del procés','','2021-22',0,NULL,'1,',2),
	(3,'03-Ús del Trello','','2021-22',0,NULL,'1,',2),
	(4,'04-Exposició Oral (Presentació)','','2021-22',0,NULL,'1,',2),
	(5,'05-Memòria Final','','2021-22',0,NULL,'1,',2);

/*!40000 ALTER TABLE `technical_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla technical_skills_assessment
-- ------------------------------------------------------------

CREATE TABLE `technical_skills_assessment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `technical_skills_id` int(10) unsigned NOT NULL,
  `challenge_id` int(10) unsigned NOT NULL,
  `student_id` int(10) unsigned NOT NULL,
  `teachers_id` longtext NOT NULL,
  `assessment_date` date DEFAULT NULL,
  `teachers_comments` longtext DEFAULT NULL,
  `final_mark` longtext DEFAULT NULL,
  `rubric_mark` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_technical_skills_index1` (`technical_skills_id`,`student_id`,`challenge_id`),
  KEY `technical_skills_id` (`technical_skills_id`),
  KEY `challenge_id` (`challenge_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `technical_skills_assessment_ibfk_1` FOREIGN KEY (`challenge_id`) REFERENCES `challenges` (`id`),
  CONSTRAINT `technical_skills_assessment_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  CONSTRAINT `technical_skills_assessment_ibfk_3` FOREIGN KEY (`technical_skills_id`) REFERENCES `technical_skills` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

LOCK TABLES `technical_skills_assessment` WRITE;
/*!40000 ALTER TABLE `technical_skills_assessment` DISABLE KEYS */;

INSERT INTO `technical_skills_assessment` (`id`, `technical_skills_id`, `challenge_id`, `student_id`, `teachers_id`, `assessment_date`, `teachers_comments`, `final_mark`, `rubric_mark`)
VALUES
	(1,1,1,1,'[\"2\"]',NULL,NULL,NULL,NULL),
	(2,1,1,2,'[\"2\"]',NULL,NULL,NULL,NULL),
	(3,1,1,3,'[\"2\"]',NULL,NULL,NULL,NULL),
	(4,1,1,4,'[\"2\"]',NULL,NULL,NULL,NULL),
	(5,1,1,5,'[\"2\"]',NULL,NULL,NULL,NULL),
	(6,1,1,6,'[\"2\"]',NULL,NULL,NULL,NULL),
	(7,1,1,7,'[\"2\"]',NULL,NULL,NULL,NULL),
	(8,1,1,8,'[\"2\"]',NULL,NULL,NULL,NULL),
	(9,1,1,9,'[\"2\"]',NULL,NULL,NULL,NULL),
	(10,1,1,10,'[\"2\"]',NULL,NULL,NULL,NULL),
	(11,1,1,11,'[\"2\"]',NULL,NULL,NULL,NULL),
	(12,1,1,12,'[\"2\"]',NULL,NULL,NULL,NULL),
	(13,2,1,1,'[\"2\"]',NULL,NULL,NULL,NULL),
	(14,2,1,2,'[\"2\"]',NULL,NULL,NULL,NULL),
	(15,2,1,3,'[\"2\"]',NULL,NULL,NULL,NULL),
	(16,2,1,4,'[\"2\"]',NULL,NULL,NULL,NULL),
	(17,2,1,5,'[\"2\"]',NULL,NULL,NULL,NULL),
	(18,2,1,6,'[\"2\"]',NULL,NULL,NULL,NULL),
	(19,2,1,7,'[\"2\"]',NULL,NULL,NULL,NULL),
	(20,2,1,8,'[\"2\"]',NULL,NULL,NULL,NULL),
	(21,2,1,9,'[\"2\"]',NULL,NULL,NULL,NULL),
	(22,2,1,10,'[\"2\"]',NULL,NULL,NULL,NULL),
	(23,2,1,11,'[\"2\"]',NULL,NULL,NULL,NULL),
	(24,2,1,12,'[\"2\"]',NULL,NULL,NULL,NULL),
	(25,3,1,1,'[\"2\"]',NULL,NULL,NULL,NULL),
	(26,3,1,2,'[\"2\"]',NULL,NULL,NULL,NULL),
	(27,3,1,3,'[\"2\"]',NULL,NULL,NULL,NULL),
	(28,3,1,4,'[\"2\"]',NULL,NULL,NULL,NULL),
	(29,3,1,5,'[\"2\"]',NULL,NULL,NULL,NULL),
	(30,3,1,6,'[\"2\"]',NULL,NULL,NULL,NULL),
	(31,3,1,7,'[\"2\"]',NULL,NULL,NULL,NULL),
	(32,3,1,8,'[\"2\"]',NULL,NULL,NULL,NULL),
	(33,3,1,9,'[\"2\"]',NULL,NULL,NULL,NULL),
	(34,3,1,10,'[\"2\"]',NULL,NULL,NULL,NULL),
	(35,3,1,11,'[\"2\"]',NULL,NULL,NULL,NULL),
	(36,3,1,12,'[\"2\"]',NULL,NULL,NULL,NULL),
	(37,4,1,1,'[\"2\"]',NULL,NULL,NULL,NULL),
	(38,4,1,2,'[\"2\"]',NULL,NULL,NULL,NULL),
	(39,4,1,3,'[\"2\"]',NULL,NULL,NULL,NULL),
	(40,4,1,4,'[\"2\"]',NULL,NULL,NULL,NULL),
	(41,4,1,5,'[\"2\"]',NULL,NULL,NULL,NULL),
	(42,4,1,6,'[\"2\"]',NULL,NULL,NULL,NULL),
	(43,4,1,7,'[\"2\"]',NULL,NULL,NULL,NULL),
	(44,4,1,8,'[\"2\"]',NULL,NULL,NULL,NULL),
	(45,4,1,9,'[\"2\"]',NULL,NULL,NULL,NULL),
	(46,4,1,10,'[\"2\"]',NULL,NULL,NULL,NULL),
	(47,4,1,11,'[\"2\"]',NULL,NULL,NULL,NULL),
	(48,4,1,12,'[\"2\"]',NULL,NULL,NULL,NULL),
	(49,5,1,1,'[\"2\"]',NULL,NULL,NULL,NULL),
	(50,5,1,2,'[\"2\"]',NULL,NULL,NULL,NULL),
	(51,5,1,3,'[\"2\"]',NULL,NULL,NULL,NULL),
	(52,5,1,4,'[\"2\"]',NULL,NULL,NULL,NULL),
	(53,5,1,5,'[\"2\"]',NULL,NULL,NULL,NULL),
	(54,5,1,6,'[\"2\"]',NULL,NULL,NULL,NULL),
	(55,5,1,7,'[\"2\"]',NULL,NULL,NULL,NULL),
	(56,5,1,8,'[\"2\"]',NULL,NULL,NULL,NULL),
	(57,5,1,9,'[\"2\"]',NULL,NULL,NULL,NULL),
	(58,5,1,10,'[\"2\"]',NULL,NULL,NULL,NULL),
	(59,5,1,11,'[\"2\"]',NULL,NULL,NULL,NULL),
	(60,5,1,12,'[\"2\"]',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `technical_skills_assessment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Volcado de tabla users
-- ------------------------------------------------------------

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `password_changed` tinyint(10) NOT NULL DEFAULT 0,
  `role` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT 'first_time',
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `email`, `password`, `password_changed`, `role`, `status`, `avatar`)
VALUES
	(1,'adminsat','admin@invalidemai.not','$2y$10$B/hJRl9vRw91YPvBEtcq9.SZ.c20YzcVKcArNf.v.66o.vyd808gy',0,'admin','first_time',NULL),
	(2,'profesat','profesat@correonovalido.no','$2y$10$ETi2Z/Zsvl.oePnC2e7/aOkl5PofwU1z3yJxEcMFYGj2ZFl9lts5S',0,'teacher','first_time',NULL),
	(3,'alumne01','alumne01@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(4,'alumne02','alumne02@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(5,'alumne03','alumne03@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(6,'alumne04','alumne04@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(7,'alumne05','alumne05@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(8,'alumne06','alumne06@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(9,'alumne07','alumne07@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(10,'alumne08','alumne08@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(11,'alumne09','alumne09@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(12,'alumne10','alumne10@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(13,'alumne11','alumne11@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL),
	(14,'alumne12','alumne12@correonovalido.no','$2y$10$XH2./KQluut.M4XlatV0Tu7qxRBSr0Sh6WGLoNGMWLxCapLUN6E9i',0,'student','first_time',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
