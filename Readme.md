# Skills Assessment Tool 

Skills Assessment Tool (SAT), es una ***aplicación web*** que **unifica las valoraciones transversales y específicas de los alumnos que realizan un reto**. 

Un reto (https://observatorio.tec.mx/edutrendsabr) presenta a los alumnos un problema real de su entorno al cual han de buscar una posible solución.

Utilizando SAT obtendremos dos tipos de valoraciones de nuestro alumnado. La primera hace referencia a las **habilidades transversales** que ha adquirido con el trabajo colaborativo durante el reto. La segunda se obtendrá mediante diversas **tareas evaluables** específicas en cada reto.

Si deseas utilizar esta herramienta solo necesitarás un servidor web (Apache o Nginx) y una base de datos (MySQL o MariaDB).


## Base de Datos

En tu servidor, has de [crear un usuario y una nueva base de datos](https://docs.phpmyadmin.net/es/latest/privileges.html) con el mismo nombre.

Para que funcione correctamente utiliza esto nombre de usuario y contraseña: 
usuario de la base de datos: 
`sat-master`
Contraseña: 
`-S4t-M4s73r-`


## Aplicación SAT 1.0
Descargar la app desde Gitlab en formato .zip

https://gitlab.com/dapaspei/sat/-/archive/master/sat-master.zip

Descomprimir el zip en la carpeta "public_html" de tu servidor y modificar estod dos siguientes ficheros: 
- config-sample.php
- database-sample.php

### Config-sample.php
Modificar el nombre del fichero `/aplication/config/config-sample.php` por **`config.php`**.

Cambiar la línea `$config\['base_url'\] = 'https://mysite.com/';` con la URL correcta. Podría ser http:l//localhost o cualquier nombre de dominio. 

### Database-sample.php
Modificar el nombre del fichero `/aplication/config/database-sample.php` por **`database.php`**.

Cambiar, si fuera necesario, las siguientes variables:
- hostname
- port
- username
- password
- database

## Creación de la base de datos e inserción de datos de ejemplo

Una vez descomprimida el fichero `sat-master.zip` veremos un archivo con el nombre: `sat-master.sql`

Este fichero es el que se ha de utilizar para [crear las tablas necesarias](https://docs.phpmyadmin.net/es/latest/import_export.html) e introducir los datos de ejemplo. 

Una vez creado, ya podremos utilizar la aplicación.

## Uso de la aplicación SAT 1.0

Una vez creada la base de datos con el usuario,  descomprimido el .zip y modificados los ficheros, podremos acceder, a través de la URL introducida en el fichero 'config.php' utilizando cualquiera de los siguientes usuarios:

### Usuario administrador

Este usuario puede: crear, modificar y borrar usuarios, retos, tareas evaluables y rúbricas.

usuario: adminsat
contraseña: adminsat

### Usuario profesor

Hay un único profesor cuyas credenciales son: 

usuario: profesat
contraseña: profesat

### Usuarios alumnos
Por defecto hay 12 usuarios con las siguientes credenciales:
usuario: alumne01
contraseña: alumnesat

usuario: alumne02
contraseña: alumnesat

…

usuarios: alumn12
contraseña: alumnesat

