module.exports = {
  theme: {
    extend: {}
  },
  variants: {
    //  tableLayout: ['responsive', 'hover', 'focus'],
    borderColors: ['responsive', 'hover', 'focus', 'group-hover'], 
    visibility: ['responsive', 'group-hover'],
  },
  plugins: [
    // require('tailwindcss-plugins/pagination')({ /* Customizations here */ }),
    // require('@tailwindcss/custom-forms'),
  ]
}
